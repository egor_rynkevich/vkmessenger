//
//  MainOneAppSettings.swift
//  OneApp
//
//  Created by Егор Рынкевич on 10/6/18.
//

import UIKit
import CoreData

class MainOneAppSettings: NSManagedObject {
    
    @NSManaged var haveAnyAccounts : Bool
    
}
