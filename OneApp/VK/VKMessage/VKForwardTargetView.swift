//
//  VKForwardTargetView.swift
//  OneApp
//
//  Created by Егор Рынкевич on 2/14/19.
//

import UIKit
import DifferenceKit
import RealmSwift
import AsyncDisplayKit

class VKForwardTargetView: UIView {
    @IBOutlet weak var view: UIView!
    
    @IBOutlet weak var selectChatsLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var viewForCollection: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var collectionHeightConstr: NSLayoutConstraint!
    
    private var conversDiff = [VKConversation]()
    private var conversInput: [VKConversation] {
        get { return conversDiff }
        set {
            let changeset = StagedChangeset(source: conversDiff, target: newValue)
            //print(changeset)
            collectionNode?.reload(using: changeset) { data in
                self.conversDiff = data
            }
        }
    }
    var conversations : [VKConversation] = []
    var count: Int = 0
    var collectionNode: ASCollectionNode?
    var isLoading : Bool = false
    var lastMsgID: Int64 = 0
    var imgSize: Int = 0
    var selectPeerIndex: [IndexPath] = []
    var searchText = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("VKForwardTargetView", owner: self, options: nil)
        addSubview(view)
        view.frame = self.bounds
        //view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        view.layer.maskedCorners = [CACornerMask.layerMinXMinYCorner, CACornerMask.layerMaxXMinYCorner]
        view.layer.cornerRadius = 15
    }
    
    func config() {
        
        cancelBtn.setImage(mainIcons.back_s21, for: .normal)
        searchBar.setBackgroundImage(UIColor.white.image(), for: .any, barMetrics: .default)
        
        for subView in searchBar.subviews {
            for subViewInSubView in subView.subviews {
                if subViewInSubView.isKind(of: UITextField.self) {
                    subViewInSubView.backgroundColor = UIColor(red: 0.9725, green: 0.9725, blue: 0.9725, alpha: 1)
                }
            }
        }
        
        imgSize = Int(((view.frame.width - 20) / 5) - 10)
        let height = view.frame.height - viewForCollection.frame.origin.y - 50
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        //flowLayout.minimumLineSpacing = CGFloat(5)
        flowLayout.itemSize = CGSize(width: imgSize, height: imgSize + 30)
        collectionNode = ASCollectionNode(collectionViewLayout: flowLayout)
        collectionNode?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: height )
        collectionNode?.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        collectionHeightConstr.constant = height
        
        collectionNode?.delegate = self
        collectionNode?.dataSource = self
        
        viewForCollection.addSubnode(collectionNode!)
        
        sendBtn.isEnabled = false
        
        self.conversations = vkDataCache.getVKConversations(count: 30, isNeedReload: false)
        count = self.conversations.count
        
        if count == 0 {
            vkLoadData.loadVKConversations(view: self.view) { [weak self] (result) in
                if result == true {
                    DispatchQueue.main.async { self?.initConversDiff(needCount: 30, isNeedReload: true) }
                }
            }
        } else {
            initConversDiff(needCount: count, isNeedReload: true)
        }
    }
    
    func initConversDiff(needCount: Int, isNeedReload: Bool) {
        conversations = vkDataCache.getVKConversations(count: needCount, isNeedReload: isNeedReload) // need 30
        count = conversations.count
        lastMsgID = conversations.last?.last_message?.id ?? 0
        var convers: [VKConversation] = []

        for conv in conversations {
            convers.append(conv)
        }
        conversInput = convers
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}



extension VKForwardTargetView: UISearchBarDelegate {
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange textSearched: String) {
        searchBar.showsCancelButton = true
        searchText = textSearched
        //searchingUsers(offset: 0, isReload: true, isInfinite: false, q: textSearched)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = nil
        searchBar.resignFirstResponder()
        searchText = ""
        //searchingUsers(offset: 0, isReload: true, isInfinite: false, q: searchText)
    }
    
    //func searchConv()
}

extension VKForwardTargetView: ASCollectionDelegate, ASCollectionDataSource {
    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        return conversDiff.count
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let conv = conversDiff[indexPath.row]
        let convRef = ThreadSafeReference(to: conv)
        return {
            
            let node = VKForwardTargetCellNode(convRef: convRef, imageWidth: self.imgSize, select: self.selectPeerIndex.contains(indexPath))
                return node
        }
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, willBeginBatchFetchWith context: ASBatchContext) {
        if self.isLoading == false && self.count > 0 {
            vkLoadData.loadVKConversations(startID: self.lastMsgID, offset: 1, view: self.view, completionBlock: { [weak self] (result) in
                if result == true {
                    DispatchQueue.main.async {
                        self?.initConversDiff(needCount: (self?.conversDiff.count ?? 0) + 30, isNeedReload: false)
                        self?.isLoading = false
                        context.completeBatchFetching(true)
                    }
                } else if result == false {
                    DispatchQueue.main.async {
                        self?.isLoading = false
                        context.completeBatchFetching(true)
                    }
                }
                
            })
        }
    }
    
    func shouldBatchFetch(for collectionNode: ASCollectionNode) -> Bool {
        //if self.conversDiff.count < vkLoadData.countConvers { return true }
        return false
    }
    
    @objc(collectionNode:constrainedSizeForItemAtIndexPath:)
    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        let min = CGSize(width: imgSize, height: imgSize + 40)
        let max = CGSize(width: imgSize, height: imgSize + 40)
        return ASSizeRange(min: min, max: max)
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        collectionNode.deselectItem(at: indexPath, animated: true)

        if !conversations[indexPath.row].can_write_allowed { return }
        
        if selectPeerIndex.contains(indexPath) {
            selectPeerIndex.remove(at: selectPeerIndex.firstIndex(of: indexPath)!)

        } else {
            selectPeerIndex.append(indexPath)
        }
        collectionNode.reloadItems(at: [indexPath])
        
        updateSelectChatsLbl()
        sendBtn.isEnabled = !selectPeerIndex.isEmpty
    }
    
    func updateSelectChatsLbl() {
        var result = ""
        for index in selectPeerIndex {
            let conv = conversations[index.row]
            var name = ""
            if conv.userInfo != nil { name = conv.userInfo!.first_name }
            else if conv.chatInfo != nil { name = conv.chatInfo!.title }
            else if conv.groupInfo != nil { name = conv.groupInfo!.name }
            
            if result.count > 0 { result.append(", ") }
            result.append(name)
        }
        if result.count == 0 { result = "Select chats" }
        selectChatsLbl.text = result
    }
}

class VKForwardTargetCellNode: ASCellNode {
    
    fileprivate let nameTextNode: ASTextNode
    fileprivate let imageNode: ASNetworkImageNode
    fileprivate let selectNode: ASImageNode
    fileprivate let blockNode: ASImageNode
    
    var photo_id: Int64 = 0
    var owner_id: Int64 = 0
    var photo_url: String = ""
    var isSelect: Bool = false
    var isCantWrite: Bool = false
    
    init(convRef: ThreadSafeReference<VKConversation>, imageWidth: Int, select: Bool) {
        nameTextNode = ASTextNode()
        imageNode = ASNetworkImageNode()
        selectNode = ASImageNode()
        blockNode = ASImageNode()
        isSelect = select
        super.init()
        automaticallyManagesSubnodes = true
        
        let realm = try! Realm()
        
        if let convers = realm.resolve(convRef) {
            
            if convers.userInfo == nil && convers.groupInfo == nil && convers.chatInfo == nil {
                
                try! realm.write {
                    if convers.peer_type == VKUserType.isUser.rawValue {
                        convers.userInfo = realm.objects(VKUser.self).filter("id = %@", convers.peer_id).first
                    }
                    else if convers.peer_type == VKUserType.isGroup.rawValue {
                        convers.groupInfo = realm.objects(VKGroup.self).filter("id = %@", convers.peer_id).first
                    }
                    else if convers.peer_type == VKUserType.isChat.rawValue {
                        convers.chatInfo = realm.objects(VKChat.self).filter("id = %@", convers.peer_id).first
                    }
                }
                
            }
            
            isCantWrite = !convers.can_write_allowed
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .center
            paragraphStyle.lineBreakMode = .byWordWrapping
            
            nameTextNode.attributedText = NSAttributedString(string: vkDataCache.getUsernameByIDAndType(id: convers.peer_id, type: convers.peer_type), attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.systemFont(ofSize: 11), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: isCantWrite ? mainColors.lightGray! : mainColors.usernameText!])
            nameTextNode.truncationMode = .byTruncatingTail
            nameTextNode.backgroundColor = UIColor.clear
            nameTextNode.placeholderColor = mainColors.lightGray
            nameTextNode.maximumNumberOfLines = 2
            nameTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(imageWidth))
        
        
            switch convers.peer_type {
            case VKUserType.isUser.rawValue:
                photo_url = convers.userInfo?.photo_100 ?? ""
                break
            case VKUserType.isChat.rawValue:
                photo_url = convers.chatInfo?.photo_100 ?? ""
                break
            case VKUserType.isGroup.rawValue:
                photo_url = convers.groupInfo?.photo_100 ?? ""
                break
            default:
                break
                
            }
            let peer_id = convers.peer_id
            
            var data = vkDataCache.getProfileImgFromCache(sourceID: peer_id, ownerID: peer_id, type: "image", tag: "ava")
            
            if vkDataCache.getProfileURLFromCache(sourceID: peer_id, ownerID: peer_id, type: "image", tag: "ava") != photo_url {
                vkDataCache.removeDataFromCachedData(sourceID: peer_id, ownerID: peer_id, type: "image", tag: "ava")
                data = nil
            }
            
            if data == nil {
                if photo_url != "" { imageNode.url = NSURL(string: photo_url)! as URL }
                else {
                    imageNode.image = Helper.textToImage(drawText: vkDataCache.getUserShortNameByIDAndType(id: convers.peer_id, type: convers.peer_type), height: imageWidth, width: imageWidth)
                }
            }
            else {
                imageNode.defaultImage = nil
                imageNode.image = UIImage(data: data! as Data)
            }
            
            imageNode.imageModificationBlock = { [weak self] image in
                if vkDataCache.getProfileImgFromCache(sourceID: peer_id, ownerID: peer_id, type: "image", tag: "ava") == nil {
                    vkDataCache.saveDataInCachedData(sourceID: peer_id, ownerID: peer_id, type: "image", tag: "ava", url: self?.photo_url ?? "", image: image)
                }
                return image
            }
            
            imageNode.cornerRadius = CGFloat(imageWidth / 2)
            imageNode.style.preferredSize = CGSize(width: imageWidth, height: imageWidth)
        
            let color = UIColor(red: 92 / 255, green: 155 / 255, blue: 223 / 255, alpha: 1)
            
            if isSelect {
                imageNode.borderWidth = 2
                imageNode.borderColor = color.cgColor
            }
            
            blockNode.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            blockNode.cornerRadius = CGFloat(imageWidth / 2)
            
            selectNode.image = mainIcons.tick_s20
            //selectNode.backgroundColor = mainColors.writeRow
            selectNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(color)
            
            selectNode.isHidden = !isSelect
            
            //selectNode.style.preferredSize = CGSize(width: 20, height: 20)
            //selectNode.style.maxSize = CGSize(width: 20, height: 20)
            addSubnode(selectNode)
            addSubnode(imageNode)
        }
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        
        var absoluteSpec = ASLayoutSpec() //ASAbsoluteLayoutSpec(children: [spec])
        //absoluteSpec = ASAbsoluteLayoutSpec(children: [spec])
        //absoluteSpec.sizing = .sizeToFit
        
        //if isSelect {
            selectNode.style.layoutPosition = CGPoint(x: constrainedSize.max.width / 2, y: constrainedSize.max.width / 2)
            //absoluteSpec.children?.append(selectNode)
        let centerInsets = constrainedSize.max.width / 2 - 10
        absoluteSpec = ASOverlayLayoutSpec(child: imageNode, overlay: isCantWrite ? blockNode : ASInsetLayoutSpec(insets: UIEdgeInsets(top: centerInsets, left: centerInsets, bottom: centerInsets, right: centerInsets), child: selectNode))
        //absoluteSpec = ASOverlayLayoutSpec(child: imageNode, overlay: selectNode)
        //}
        
        let titleStack = ASStackLayoutSpec(direction: .vertical, spacing: 2, justifyContent: .start, alignItems: .center, children: [absoluteSpec, nameTextNode])
        
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: titleStack)
    }
}
