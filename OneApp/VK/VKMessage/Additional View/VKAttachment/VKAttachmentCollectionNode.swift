//
//  VKAttachmentCollectionNode.swift
//  OneApp
//
//  Created by Егор Рынкевич on 10/27/18.
//

import Foundation
import AsyncDisplayKit
import SwiftyVK
import SwiftyJSON
import Alamofire
import UICircularProgressRing

class VKAttachmentCollectionNode: ASCellNode {
    fileprivate let attachment: VKAttachmentShort
    fileprivate let image: ASImageNode
    
    fileprivate let playImageNode: ASImageNode
    fileprivate let durationTextNode: ASTextNode
    fileprivate let headerBackgroundImageNode: ASImageNode
    
    fileprivate let map: ASMapNode
    fileprivate let close: ASImageNode
    
    fileprivate let fileNameTextNode: ASTextNode
    fileprivate let extTextNode: ASTextNode
    fileprivate let sizeTextNode: ASTextNode
    fileprivate let fileImageNode: ASImageNode
    
    var backRightInsets: CGFloat = 0
    var isDocImg: Bool = false
    
    var delegate: VKAttachmentNodeDelegate?
    var progressRing: UICircularProgressRing?
    
    init(att: VKAttachmentShort, contentWidth: Int) {
        attachment = att
        image = ASImageNode()
        durationTextNode = ASTextNode()
        playImageNode = ASImageNode()
        headerBackgroundImageNode = ASImageNode()
        map = ASMapNode()
        close = ASImageNode()
        
        fileNameTextNode  = ASTextNode()
        extTextNode = ASTextNode()
        sizeTextNode = ASTextNode()
        fileImageNode = ASImageNode()
        
        super.init()
        
        automaticallyManagesSubnodes = true
        let msgColor = mainColors.getMessTextColor(true)
        switch att.type {
        case "photo":
            if attachment.image != nil {
                image.image = attachment.image //UIImage(data: attachment.data!)
                image.style.preferredSize = CGSize(width: 60, height: 60)
                image.cornerRadius = 5
                addSubnode(image)
            }
            break
        case "video":
            if attachment.image != nil {
                image.image = attachment.image //UIImage(data: attachment.data!)
                image.style.preferredSize = CGSize(width: 60, height: 60)
                image.cornerRadius = 5
                
                durationTextNode.attributedText = NSAttributedString(string: Helper.getDurationString(attachment.duration), attributes: [.font : UIFont.systemFont(ofSize: 10), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.white])
                durationTextNode.backgroundColor = UIColor.clear
                durationTextNode.placeholderColor = mainColors.lightGray
                durationTextNode.maximumNumberOfLines = 1
                
                let durationTxtWdth = durationTextNode.calculateSizeThatFits(CGSize(width: 60, height: 20)).width
                
                headerBackgroundImageNode.cornerRadius = 9
                headerBackgroundImageNode.contentMode = .scaleAspectFill
                headerBackgroundImageNode.backgroundColor = mainColors.mainBackgroundView
                
                backRightInsets = 40 - durationTxtWdth
                
                playImageNode.image = mainIcons.play
                playImageNode.style.preferredSize = CGSize(width: 8, height: 8)
                playImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColor.white)
                
                
                addSubnode(playImageNode)
                addSubnode(durationTextNode)
                addSubnode(headerBackgroundImageNode)
                addSubnode(image)
            }
            break
        case "geo":
            map.style.preferredSize = CGSize(width: 60, height: 60)
            let location = CLLocationCoordinate2DMake(attachment.latitude, attachment.longitude)
            let region = MKCoordinateRegion(center: location, span: MKCoordinateSpan(latitudeDelta: 0.009, longitudeDelta: 0.009))
            map.region = region
            map.cornerRadius = 5
            let annotation = MKPointAnnotation()
            annotation.coordinate = location
            map.annotations.append(annotation)
            addSubnode(map)
            break
        case "doc":
            if attachment.image != nil {
                isDocImg = true
                
                headerBackgroundImageNode.cornerRadius = 5
                headerBackgroundImageNode.contentMode = .scaleAspectFill
                headerBackgroundImageNode.backgroundColor = UIColor(named: "DarkMainTableViewColor")
                
                image.image = attachment.image //UIImage(data: attachment.data!)
                image.style.preferredSize = CGSize(width: 60, height: 60)
                image.cornerRadius = 5
                
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = .center
                paragraphStyle.lineBreakMode = .byWordWrapping
                
                extTextNode.attributedText = NSAttributedString(string: "\(attachment.doc?.ext.uppercased() ?? attachment.file_name.getPathExtension().uppercased()) • \(ByteCountFormatter.string(fromByteCount: Int64(attachment.doc?.size ?? attachment.file_size), countStyle: .file))", attributes: [.font : UIFont.systemFont(ofSize: 8), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: msgColor.messageIcon!, NSAttributedString.Key.paragraphStyle : paragraphStyle])
                extTextNode.backgroundColor = UIColor.clear
                //extTextNode.cornerRadius = 5
                extTextNode.borderWidth = 1
                extTextNode.borderColor = msgColor.messageIcon?.cgColor
                extTextNode.textContainerInset = UIEdgeInsets(top: 2, left: 3, bottom: 2, right: 3)
            }
            else {
                image.style.minSize = CGSize(width: 60, height: 60)
                image.cornerRadius = 5
                image.backgroundColor = mainColors.mainBackgroundView
                
                fileImageNode.image = mainIcons.file_s40?.resize(targetSize: CGSize(width: 30.0, height: 30.0)).withRenderingMode(.alwaysTemplate)
                fileImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(msgColor.messageSubText!)
                
                fileNameTextNode.attributedText = NSAttributedString(string: attachment.doc!.title.count > 0 ? attachment.doc!.title : "...", attributes: [.font : UIFont.systemFont(ofSize: 10), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: msgColor.messageText!])
                fileNameTextNode.truncationMode = .byTruncatingTail
                fileNameTextNode.backgroundColor = UIColor.clear
                fileNameTextNode.maximumNumberOfLines = 1
                fileNameTextNode.style.maxWidth = ASDimension(unit: .points, value: 60)
                
                extTextNode.attributedText = NSAttributedString(string: attachment.doc!.ext.uppercased(), attributes: [.font : UIFont.systemFont(ofSize: 6), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: msgColor.messageIcon!])
                extTextNode.backgroundColor = UIColor.clear
                extTextNode.cornerRadius = 2
                extTextNode.borderWidth = 1
                extTextNode.borderColor = msgColor.messageIcon?.cgColor
                extTextNode.textContainerInset = UIEdgeInsets(top: 2, left: 3, bottom: 2, right: 3)
                
                sizeTextNode.attributedText = NSAttributedString(string: ByteCountFormatter.string(fromByteCount: Int64(attachment.doc!.size), countStyle: .file), attributes: [.font : UIFont.systemFont(ofSize: 10), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: msgColor.messageSubText!])
                sizeTextNode.backgroundColor = UIColor.clear
                
                addSubnode(image)
            }
            break
        case "audio":
            image.style.minSize = CGSize(width: 60, height: 60)
            image.cornerRadius = 5
            image.backgroundColor = mainColors.mainBackgroundView
            
            fileImageNode.image = mainIcons.playAudio_s40?.resize(targetSize: CGSize(width: 30.0, height: 30.0)).withRenderingMode(.alwaysTemplate)
            fileImageNode.style.preferredSize = CGSize(width: 30, height: 30)
            fileImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(msgColor.messageIcon!)
            
            fileNameTextNode.attributedText = NSAttributedString(string: attachment.audio!.title.count > 0 ? attachment.audio!.title : "...", attributes: [.font : UIFont.systemFont(ofSize: 10), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: msgColor.messageText!])
            fileNameTextNode.truncationMode = .byTruncatingTail
            fileNameTextNode.backgroundColor = UIColor.clear
            fileNameTextNode.maximumNumberOfLines = 1
            fileNameTextNode.style.maxWidth = ASDimension(unit: .points, value: 60)
            
            sizeTextNode.attributedText = NSAttributedString(string: attachment.audio!.artist, attributes: [.font : UIFont.systemFont(ofSize: 10), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: msgColor.messageSubText!])
            sizeTextNode.backgroundColor = UIColor.clear
            
            addSubnode(image)
            break
        case "wall":
            image.style.minSize = CGSize(width: 60, height: 60)
            image.cornerRadius = 5
            image.backgroundColor = mainColors.mainBackgroundView
            
            fileImageNode.image = mainIcons.wall_s40?.resize(targetSize: CGSize(width: 30.0, height: 30.0)).withRenderingMode(.alwaysTemplate)
            fileImageNode.style.preferredSize = CGSize(width: 30, height: 30)
            fileImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(msgColor.messageIcon!)
            fileImageNode.cornerRadius = 5
            fileImageNode.borderWidth = 2
            fileImageNode.borderColor = msgColor.messageAditionalIcon?.cgColor
            
            var text = attachment.wall!.text.count > 0 ? attachment.wall!.text : "..."
            
            if text != "..." { text = String(text.filter { !"\n\t\r".contains($0) }) }
            
            fileNameTextNode.attributedText = NSAttributedString(string: text, attributes: [.font : UIFont.systemFont(ofSize: 10), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: msgColor.messageText!])
            fileNameTextNode.truncationMode = .byTruncatingTail
            fileNameTextNode.backgroundColor = UIColor.clear
            fileNameTextNode.maximumNumberOfLines = 1
            fileNameTextNode.style.maxWidth = ASDimension(unit: .points, value: 60)
            
            sizeTextNode.attributedText = NSAttributedString(string: "Post", attributes: [.font : UIFont.systemFont(ofSize: 10), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: msgColor.messageSubText!])
            sizeTextNode.backgroundColor = UIColor.clear
            
            addSubnode(image)
            break
        default:
            break
        }
        

        close.image = mainIcons.close_s20
        close.style.preferredSize = CGSize(width: 20, height: 20)
        close.imageModificationBlock = ASImageNodeTintColorModificationBlock(mainColors.sendMessage!)
        close.backgroundColor = mainColors.mainNavigationBarLight
        close.cornerRadius = 10
        close.borderWidth = 2
        close.borderColor = mainColors.mainNavigationBarLight?.cgColor
        close.addTarget(self, action: #selector(deleteAtt), forControlEvents: .touchUpInside)
        
        addSubnode(close)
        //addSubnode(collectionNode)
        NotificationCenter.default.addObserver(self, selector: #selector(updateProgressRingValue(notification:)), name: NSNotification.Name.updateUploadAttachment, object: nil)
    }
    
    override func didLoad() {
        if (attachment.type == "video" || attachment.type == "photo" || attachment.type == "doc") && attachment.vk_id.count == 0 {
            print(attachment.file_name)
            progressRing = UICircularProgressRing(frame: CGRect(x: 12, y: 17, width: 45, height: 45))
            progressRing!.backgroundColor = UIColor.clear
            progressRing!.isOpaque = false
            progressRing!.outerRingColor = mainColors.darkGray!
            progressRing!.innerRingColor = mainColors.readMessage!
            progressRing!.outerRingWidth = 4
            progressRing!.innerRingWidth = 3
            progressRing!.style = .ontop
            progressRing!.font = UIFont.boldSystemFont(ofSize: 10)
            progressRing!.fontColor = UIColor.white
            
            view.addSubview(progressRing!)
            loadToServer()
        }
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        close.style.layoutPosition = CGPoint(x: 60, y: 5)
        var finalSpec: ASLayoutSpec = ASLayoutSpec()
        
        switch attachment.type {
        case "photo":
            finalSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: image)
            break
        case "video":
            let headerStack = ASStackLayoutSpec(direction: .horizontal, spacing: 2, justifyContent: .start, alignItems: .center, children: [playImageNode, durationTextNode])
            let headerBack = ASBackgroundLayoutSpec(child: ASInsetLayoutSpec(insets: UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5), child: headerStack), background: headerBackgroundImageNode)
            finalSpec = ASOverlayLayoutSpec(child: image, overlay: ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: CGFloat.infinity, right: backRightInsets), child: headerBack))
            break
        case "geo":
            finalSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: map)
            break
        case "doc":
            if isDocImg == true {
                let headerBack = ASBackgroundLayoutSpec(child: ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: extTextNode), background: headerBackgroundImageNode)
                finalSpec = ASOverlayLayoutSpec(child: image, overlay: ASInsetLayoutSpec(insets: UIEdgeInsets(top: CGFloat.infinity, left: 0, bottom: 0, right: 0), child: headerBack))
            }
            else {
                extTextNode.style.layoutPosition = CGPoint(x: 0, y: 11)
                fileImageNode.style.layoutPosition = CGPoint(x: 0, y: 0)
                
                let absoluteSpec = ASAbsoluteLayoutSpec(children: [fileImageNode, extTextNode])
                absoluteSpec.sizing = .sizeToFit
                let stack = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .start, alignItems: .start, children: [absoluteSpec, fileNameTextNode, sizeTextNode])
                stack.style.minSize = CGSize(width: 50, height: 60)
                stack.style.maxSize = CGSize(width: 50, height: 60)
                let elem = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 5, bottom: 0, right: 5), child: stack)
                finalSpec = ASBackgroundLayoutSpec(child: elem, background: image)
            }
            break
        case "audio", "wall":
            let stack = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .start, alignItems: .start, children: [fileImageNode, fileNameTextNode, sizeTextNode])
            stack.style.minSize = CGSize(width: 50, height: 60)
            stack.style.maxSize = CGSize(width: 50, height: 60)
            let elem = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 5, bottom: 0, right: 5), child: stack)
            finalSpec = ASBackgroundLayoutSpec(child: elem, background: image)
            break
        default:
            break
        }
        
        finalSpec.style.layoutPosition = CGPoint(x: 20 / 2.0, y: 20 / 2.0)
        
        let absoluteSpec = ASAbsoluteLayoutSpec(children: [finalSpec, close])
        absoluteSpec.sizing = .sizeToFit
        
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0), child: absoluteSpec)
    }
    
    func loadToServer() {
        let manager = PHImageManager.default()
        if attachment.type == "photo" {
            manager.requestImageData(for: attachment.phAssest!, options: nil) { (data, str, orient, dict) in
                if data != nil {
                    self.attachment.data = data
                    vkLoadData.uploadImageToServerForMes(peer_id: nil, data: data!, attID: self.attachment.attID) { [weak self] (data) in
                        self?.updateValue(data)
                    }
                }
            }
        } else if attachment.type == "video" {
            manager.requestAVAsset(forVideo: attachment.phAssest!, options: nil, resultHandler: { (avasset, audio, info) in
                if let avassetURL = avasset as? AVURLAsset {
                    guard let video = try? Data(contentsOf: avassetURL.url) else { return }
                    vkLoadData.uploadVideoToServerForMes(peer_id: nil, data: video, attID: self.attachment.attID, completion: { (data) in
                        self.updateValue(data)
                    })
                }
            })
        } else if attachment.type == "doc" {
            if self.attachment.duration > 0 {
                manager.requestAVAsset(forVideo: attachment.phAssest!, options: nil, resultHandler: { (avasset, audio, info) in
                    if let avassetURL = avasset as? AVURLAsset {
                        guard let video = try? Data(contentsOf: avassetURL.url) else { return }
                        self.changeExtText(video.count)
                        
                        vkLoadData.uploadDocFileToServer(peer_id: String(vkLoadData.mainUserID), data: video, type: "video", file_name: self.attachment.file_name, attID: self.attachment.attID, completion: { [weak self] (data) in
                            self?.updateValue(data)
                        })
                    }
                })
            } else {
                manager.requestImageData(for: attachment.phAssest!, options: nil) { (data, str, orient, dict) in
                    if data != nil {
                        self.changeExtText(data!.count)
                        self.attachment.data = data
                        vkLoadData.uploadDocFileToServer(peer_id: String(vkLoadData.mainUserID), data: data!, type: "image", file_name: self.attachment.file_name, attID: self.attachment.attID, completion: { [weak self] (data) in
                            self?.updateValue(data)
                        })
                    }
                }
            }
            
        }
    }
    
    func changeExtText(_ size: Int) {
        self.attachment.file_size = Int32(size)
        let msgColor = mainColors.getMessTextColor(true)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineBreakMode = .byWordWrapping
        self.extTextNode.attributedText = NSAttributedString(string: "\(self.attachment.doc?.ext.uppercased() ?? self.attachment.file_name.getPathExtension().uppercased()) • \(ByteCountFormatter.string(fromByteCount: Int64(self.attachment.doc?.size ?? self.attachment.file_size), countStyle: .file))", attributes: [.font : UIFont.systemFont(ofSize: 8), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: msgColor.messageIcon!, NSAttributedString.Key.paragraphStyle : paragraphStyle])
    }
    
    func updateValue(_ json: JSON?) {
        DispatchQueue.main.async {
            self.setProgressRingValue(1.0)
        }
        if json != nil {
            if attachment.type == "photo" {
                let photo = VKPhoto(json: json![0], prnt_id: "")
                attachment.photo = photo
                attachment.vk_id = "\(photo.owner_id)_\(photo.id)"
                saveDataInDB(photo.id)
            } else if attachment.type == "video" {
                attachment.vk_id = "\(vkLoadData.mainUserID)_\(json!["video_id"])"
                saveDataInDB(json!["video_id"].int64 ?? 0)
            } else if attachment.type == "doc" {
                let doc = VKDocument(json: json![0], prnt_id: "")
                attachment.doc = doc
                attachment.vk_id = "\(doc.owner_id)_\(doc.id)"
            }
            delegate?.updateAttValue(attachment)
        }
    }
    
    @objc func updateProgressRingValue(notification: NSNotification) {
        let userInfo = notification.userInfo ?? [:]
        if userInfo.count > 0 {
            let attID: String = userInfo["attID"] as! String
            let progressString: String = userInfo["progress"] as! String
            let progress: Double = Double(progressString) ?? 0.0
            if attID == attachment.attID {
                setProgressRingValue(progress)
            }
        }
    }
    
    func setProgressRingValue(_ value: Double) {
        progressRing?.startProgress(to: CGFloat(value * 100.0), duration: 0.25, completion: { [weak self] in
            if value == 1.0 {
                self?.progressRing?.removeFromSuperview()
            }
        })
    }
    
    func saveDataInDB(_ id: Int64) {
        if vkDataCache.getDataFromCachedData(sourceID: id, ownerID: vkLoadData.mainUserID, type: "image", tag: attachment.type) == nil {
            
            if attachment.data != nil {
                vkDataCache.saveNSDataInCachedData(sourceID: id, ownerID: vkLoadData.mainUserID, type: "image", tag: attachment.type, url: "", data: attachment.data! as NSData)
            }
            else {
                let heightInPoints = attachment.image!.size.height
                let heightInPixels = heightInPoints * attachment.image!.scale
                
                let widthInPoints = attachment.image!.size.width
                let widthInPixels = widthInPoints * attachment.image!.scale
                print("\(id) - \(attachment.file_name) \(heightInPixels)x\(widthInPixels)")
                
                vkDataCache.saveDataInCachedData(sourceID: id, ownerID: vkLoadData.mainUserID, type: "image", tag: attachment.type, url: "", image: attachment.image!)
            }
        }
    }
    
    @objc func deleteAtt() {
        self.progressRing?.removeFromSuperview()
        delegate?.deleteAtt(attachment.attID)
    }
}
