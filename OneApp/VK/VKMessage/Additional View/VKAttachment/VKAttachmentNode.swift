//
//  VKAttachmentNode.swift
//  OneApp
//
//  Created by Егор Рынкевич on 10/28/18.
//

import Foundation
import AsyncDisplayKit
import SwiftyVK

class VKAttachmentShort {
    var attID = UUID().uuidString
    var type: String = ""
    var data: Data? = nil
    var file_name: String = ""
    var file_size: Int32 = 0
    var imageType: ImageType = .bmp
    var image: UIImage? = nil
    var photo: VKPhoto?
    
    var duration: Int64 = 0
    var phAssest: PHAsset?
    var vk_id: String = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    
    var doc: VKDocument?
    var audio: VKAudio?
    var wall: VKWall?
    
    func setImageType() {
        let ext = file_name.getPathExtension().lowercased()
        
        switch ext {
        case "jpg":
            imageType = .jpg
            break
        case "png":
            imageType = .png
            break
        case "gif":
            imageType = .gif
            break
        default:
            break
        }
    }
}

protocol VKAttachmentNodeDelegate: class {
    func deleteAtt(_ attID: String)
    func updateAttValue(_ att: VKAttachmentShort)
}

class VKAttachmentNode: ASDisplayNode, ASCollectionDelegate, ASCollectionDataSource {
    fileprivate let collectionNode: ASCollectionNode
    var attachments: [VKAttachmentShort] = []
    var contentWidth: Int = 0
    
    init(att: [VKAttachmentShort], contentWidth: Int) {
        self.contentWidth = contentWidth
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumInteritemSpacing = 1
        flowLayout.minimumLineSpacing = 1
        
        collectionNode = ASCollectionNode(collectionViewLayout: flowLayout)
        
        super.init()
        
        automaticallyManagesSubnodes = true
        
        attachments = att
        
        collectionNode.backgroundColor = mainColors.mainNavigationBarLight
        collectionNode.style.preferredSize = CGSize(width: contentWidth, height: 200)
        
        addSubnode(collectionNode)
        
        collectionNode.delegate = self
        collectionNode.dataSource = self
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: collectionNode)
    }
    
    func insertNewAtt(_ newAtts: [VKAttachmentShort]) {
        var insertedIndex: [IndexPath] = []
        
        for elem in newAtts {
            let index = attachments.firstIndex(where: { (att) -> Bool in att.attID == elem.attID })
            
            if index == nil {
                attachments.append(elem)
                insertedIndex.append(IndexPath(row: attachments.count - 1, section: 0))
            }
        }
        collectionNode.insertItems(at: insertedIndex)
    }
    
//    func removeItem(_ attID: String) {
//        if let index = attachments.firstIndex(where: { (att) -> Bool in
//            att.attID == attID
//        }) {
//            attachments.remove(at: index)
//            collectionNode.deleteItems(at: [IndexPath(row: index, section: 0)])
//        }
//    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        return attachments.count
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        return {
            let node = VKAttachmentCollectionNode(att: self.attachments[indexPath.row], contentWidth: 80)
            node.delegate = self
            //node.style.preferredSize = CGSize(width: 80, height: 80)
            return node
        }
    }
}

extension VKAttachmentNode: VKAttachmentNodeDelegate {
    
    func deleteAtt(_ attID: String) {
        if let index = attachments.firstIndex(where: { (att) -> Bool in
            att.attID == attID
        }) {
            attachments.remove(at: index)
            collectionNode.deleteItems(at: [IndexPath(row: index, section: 0)])
            
            //if attachments.count == 0 {
                var info: [String: Int] = [:]
                info["count"] = attachments.count
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateAttachmentNode"), object: nil, userInfo: info)
            //}
        }
    }
    
    func updateAttValue(_ att: VKAttachmentShort) {
        if let index = attachments.firstIndex(where: { (atch) -> Bool in
            atch.attID == att.attID
        }) {
            attachments[index] = att
        }
    }
    
}
