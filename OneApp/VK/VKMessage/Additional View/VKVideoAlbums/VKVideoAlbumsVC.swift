//
//  VKVideoAlbumsVC.swift
//  OneApp
//
//  Created by Егор Рынкевич on 12/13/18.
//

import UIKit
import SwiftyJSON
import AsyncDisplayKit

class VKVideoAlbum {
    var id: Int64 = 0
    var owner_id: Int64 = 0
    var title: String = ""
    var count: Int = 0
    var update_time: Int64 = 0
    var photo_320: String = ""
    var photo_160: String = ""
    
    convenience init(json: JSON) {
        self.init()
        id = json["id"].int64 ?? 0
        owner_id = json["owner_id"].int64 ?? 0
        title = json["title"].string ?? ""
        count = json["count"].int ?? 0
        update_time = json["update_time"].int64 ?? 0
        photo_320 = json["photo_320"].string ?? ""
        photo_160 = json["photo_160"].string ?? ""
    }
}


protocol VKVideoAlbumsVCDelegate: class {
    func addVideoToArr(choosedVideos: [String], choosedVideosObject: [VKVideo])
    func addVideo()
}



class VKVideoAlbumsVC: UIViewController, ASCollectionDelegate, ASCollectionDataSource, VKVideoAlbumsVCDelegate {
    
    var imgCollectionNode: ASCollectionNode?
    var albumArr: [VKVideoAlbum] = []
    var choosedVideosArr: [String] = []
    var choosedVideosObjectArr: [VKVideo] = []
    var imgSize: Int = 0
    
    var delegate: VKPhotosVideosAlbumAttDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumLineSpacing = CGFloat(5)
        flowLayout.minimumInteritemSpacing = CGFloat(5)
        imgCollectionNode = ASCollectionNode(collectionViewLayout: flowLayout)
        
        imgCollectionNode?.backgroundColor = UIColor.clear
        imgCollectionNode?.style.preferredSize = CGSize(width: 100, height: 100)
        
        imgCollectionNode?.delegate = self
        imgCollectionNode?.dataSource = self
        
        view.addSubnode(imgCollectionNode!)
        view.backgroundColor = mainColors.mainTableView
        
        let rightBtn = UIBarButtonItem(title: "Add", style: .done, target: self, action: #selector(addVideos))
        rightBtn.isEnabled = false
        self.navigationItem.setRightBarButton(rightBtn, animated: true)
        
        imgSize = Int((UIScreen.main.bounds.width - 30) / 2 )
        
        
        
        vkLoadData.loadUserVideoAlbums(peer_id: vkLoadData.mainUserID) { [weak self] (arr) in
            if arr.count > 0 {
                for item in arr { if item.count > 0 { self?.albumArr.append(item) } }
                
                DispatchQueue.main.async {
                    
                    if self != nil { ToastView.shared.long(self!.view, txt_msg: "You can choose a lot of photos") }
                    
                    let frame = self?.view.frame
                    if frame != nil {
                        self?.imgCollectionNode?.frame = CGRect(x: /*frame!.origin.x +*/ 10, y: /*frame!.origin.y +*/ 5, width: frame!.width - 20, height: frame!.height - 10)
                        self?.imgCollectionNode?.reloadData()
                    }
                }
            }
        }
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        return albumArr.count
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        return ASSizeRange(min: CGSize(width: imgSize, height: imgSize), max: CGSize(width: imgSize, height: imgSize))
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        return {
            let node = VKVideoAlbumNode(album: self.albumArr[indexPath.row], imageWith: self.imgSize)
            return node
        }
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        let cntrl = VKSingleVideoAlbumVC(nibName: "VKSingleVideoAlbumVC", bundle: nil)
        cntrl.album_id = albumArr[indexPath.row].id
        cntrl.title = albumArr[indexPath.row].title
        cntrl.choosedVideos = choosedVideosArr
        cntrl.choosedVideosObj = choosedVideosObjectArr
        cntrl.delegate = self
        self.navigationController!.pushViewController(cntrl, animated: true)
    }
    
    func addVideoToArr(choosedVideos: [String], choosedVideosObject: [VKVideo]) {
        choosedVideosArr = choosedVideos
        choosedVideosObjectArr = choosedVideosObject
        self.navigationItem.rightBarButtonItem?.title = choosedVideos.count > 0 ? "Add (\(choosedVideos.count))" : "Add"
        self.navigationItem.rightBarButtonItem?.isEnabled = choosedVideos.count > 0
    }
    
    func addVideo() {
        addVideos()
    }

    @objc func addVideos() {
        delegate?.addVideosAtt(videos: choosedVideosObjectArr)
        self.navigationController?.popViewController(animated: true)
    }
}


class VKVideoAlbumNode: ASCellNode {
    
    fileprivate let titleTextNode: ASTextNode
    fileprivate let imageNode: ASNetworkImageNode
    fileprivate let countTextNode: ASTextNode
    fileprivate let gradientNode: GradientNode
    
    var photo_id: Int64 = 0
    var owner_id: Int64 = 0
    var photo_url: String = ""
    
    init(album: VKVideoAlbum, imageWith: Int) {
        titleTextNode = ASTextNode()
        imageNode = ASNetworkImageNode()
        countTextNode = ASTextNode()
        gradientNode = GradientNode()
        super.init()
        automaticallyManagesSubnodes = true
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineBreakMode = .byWordWrapping
        
        titleTextNode.attributedText = NSAttributedString(string: album.title.count > 0 ? album.title : "...", attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: UIColor.white])
        titleTextNode.truncationMode = .byTruncatingTail
        titleTextNode.backgroundColor = UIColor.clear
        titleTextNode.placeholderColor = mainColors.lightGray
        titleTextNode.maximumNumberOfLines = 2
        titleTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(imageWith - 10))
        
        
        countTextNode.attributedText = NSAttributedString(string: "\(album.count) video(s)", attributes: [.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: UIColor.white])
        countTextNode.backgroundColor = UIColor.clear
        countTextNode.placeholderColor = mainColors.lightGray
        countTextNode.maximumNumberOfLines = 1
        
        gradientNode.isLayerBacked = true
        gradientNode.isOpaque = false
        gradientNode.cornerRadius = 5
        gradientNode.clipsToBounds = true
        

        photo_url = album.photo_320.count > 0 ? album.photo_320 : album.photo_160
        
        
        imageNode.url = NSURL(string: photo_url)! as URL
        
//        imageNode.imageModificationBlock = { [weak self] image in
//            if vkDataCache.getDataFromCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "photo") == nil {
//                vkDataCache.saveDataInCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "photo", url: self?.photo_url ?? "", image: image)
//            }
//            return image
//        }
        
        imageNode.cornerRadius = 5
        
        addSubnode(imageNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let overlayImage = ASOverlayLayoutSpec(child: imageNode, overlay: gradientNode)
        
        let titleStack = ASStackLayoutSpec(direction: .vertical, spacing: 2, justifyContent: .start, alignItems: .center, children: [titleTextNode, countTextNode])
        
        let titleStackInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: CGFloat.infinity, left: 5, bottom: 5, right: 5), child: titleStack)
        
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: ASOverlayLayoutSpec(child: overlayImage, overlay: titleStackInsets))
    }
}
