//
//  VKSingleVideoAlbumVC.swift
//  OneApp
//
//  Created by Егор Рынкевич on 12/13/18.
//

import UIKit
import AsyncDisplayKit

protocol VKVideoAlbumDelegate: class {
    func selectVideo(video: VKVideo)
}

class VKSingleVideoAlbumVC: UIViewController, ASCollectionDelegate, ASCollectionDataSource, VKVideoAlbumDelegate {
    
    var imgCollectionNode: ASCollectionNode?
    var videosArr: [VKVideo] = []
    var videoDict: [String : [VKVideo]] = [:]
    var videoDateSection : [String] = []
    var choosedVideos: [String] = []
    var choosedVideosObj: [VKVideo] = []
    var imgSize: Int = 0
    var album_id: Int64 = 0
    
    var delegate: VKVideoAlbumsVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgSize = Int((UIScreen.main.bounds.width - 30) / 3 )
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumLineSpacing = CGFloat(5)
        flowLayout.minimumInteritemSpacing = CGFloat(5)
        flowLayout.headerReferenceSize = CGSize(width: 50, height: 50)
        flowLayout.footerReferenceSize = CGSize(width: 0, height: 0)
        flowLayout.itemSize = CGSize(width: imgSize, height: imgSize)
        imgCollectionNode = ASCollectionNode(collectionViewLayout: flowLayout)
        imgCollectionNode?.registerSupplementaryNode(ofKind: UICollectionView.elementKindSectionHeader)
        imgCollectionNode?.backgroundColor = UIColor.clear
        imgCollectionNode?.style.preferredSize = CGSize(width: 100, height: 100)
        
        imgCollectionNode?.delegate = self
        imgCollectionNode?.dataSource = self
        
        view.addSubnode(imgCollectionNode!)
        
        let rightBtn = UIBarButtonItem(title: choosedVideos.count > 0 ? "Add (\(choosedVideos.count))" : "Add", style: .done, target: self, action: #selector(addPhotos))
        rightBtn.isEnabled = choosedVideos.count > 0
        self.navigationItem.setRightBarButton(rightBtn, animated: true)
        
        vkLoadData.loadUserVideosFromAlbum(peer_id: vkLoadData.mainUserID, album_id: album_id) { [weak self] (arr) in
            if arr.count > 0 {
                for item in arr { self?.videosArr.append(item) }
                self?.configVideoDict()
                
                DispatchQueue.main.async {
                    let frame = self?.view.frame
                    if frame != nil {
                        self?.imgCollectionNode?.frame = CGRect(x: /*frame!.origin.x +*/ 10, y: /*frame!.origin.y +*/ 0, width: frame!.width - 20, height: frame!.height - 10)
                        self?.imgCollectionNode?.reloadData()
                    }
                }
            }
        }
        
        view.backgroundColor = mainColors.mainTableView
    }
    
    func configVideoDict() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        
        for video in videosArr {
            let dateStr = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(video.date)))
            if videoDateSection.contains(dateStr) == false { videoDateSection.append(dateStr) }
            
            if var videoValues = videoDict[dateStr] {
                videoValues.append(video)
                videoDict[dateStr] = videoValues
            }
            else { videoDict[dateStr] = [video] }
        }
        
        videoDateSection = videoDateSection.sorted(by: { $0 > $1 })
    }
    
    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        return videoDateSection.count
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, nodeForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> ASCellNode {
        return VKAlbumsPhotoSectionNode(dateStr: videoDateSection[indexPath.section])
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        return videoDict[videoDateSection[section]]?.count ?? 0
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        return ASSizeRange(min: CGSize(width: imgSize, height: imgSize), max: CGSize(width: imgSize, height: imgSize))
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        return {
            let video = self.videoDict[self.videoDateSection[indexPath.section]]![indexPath.row]
            let index = self.getIndexVideoInArr(video_id: video.id, owner_id: video.owner_id)
            let node = VKAlbumsVideoNode(video: video, isSelected: index != -1, chooseNumber: index, nodeSize: self.imgSize)
            node.delegate = self
            return node
        }
    }
    
    func selectVideo(video: VKVideo) {
        let video_id = video.id
        let owner_id = video.owner_id
        let index = getIndexVideoInArr(video_id: video_id, owner_id: owner_id)
        
        if index == -1 {
            choosedVideos.append("\(video_id)_\(owner_id)_\(choosedVideos.count)")
            choosedVideosObj.append(video)
            if let indexPath = getIndexPathForReload(video_id: video_id, owner_id: owner_id) {
                imgCollectionNode?.reloadItems(at: [indexPath])
            }
        }
        else {
            var indexPathArr: [IndexPath] = []
            
            let needRemoveElem = choosedVideos[index].split(separator: "_")
            
            if let indexPath = getIndexPathForReload(video_id: Int64(needRemoveElem[0]) ?? 0, owner_id: Int64(needRemoveElem[1]) ?? 0) {
                indexPathArr.append(indexPath)
            }
            
            choosedVideos.remove(at: index)
            choosedVideosObj.remove(at: index)
            
            for vd in videosArr {
                let indexInArr = getIndexVideoInArr(video_id: vd.id, owner_id: vd.owner_id)
                if indexInArr != -1 {
                    let arr = choosedVideos[indexInArr].split(separator: "_")
                    if String(indexInArr) != arr[2] {
                        choosedVideos[indexInArr] = arr[0] + "_" + arr[1] + "_" + String(indexInArr)
                        
                        if let indexPathToUpdate = getIndexPathForReload(video_id: Int64(arr[0]) ?? 0, owner_id: Int64(arr[1]) ?? 0) {
                            indexPathArr.append(indexPathToUpdate)
                        }
                    }
                }
            }
            imgCollectionNode?.reloadItems(at: indexPathArr)
        }
        
        delegate?.addVideoToArr(choosedVideos: choosedVideos, choosedVideosObject: choosedVideosObj)
        
        self.navigationItem.rightBarButtonItem?.title = choosedVideos.count > 0 ? "Add (\(choosedVideos.count))" : "Add"
        self.navigationItem.rightBarButtonItem?.isEnabled = choosedVideos.count > 0
    }
    
    
    func getIndexPathForReload(video_id: Int64, owner_id: Int64) -> IndexPath? {
        
        let indexInArr = videosArr.firstIndex { (video) -> Bool in
            return video.id == video_id && video.owner_id == owner_id
        }
        let video = videosArr[indexInArr!]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        
        let dateStr = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(video.date)))
        let section = videoDateSection.firstIndex(of: dateStr)
        
        let videosValue = videoDict[dateStr]
        
        if videosValue != nil {
            let indexInArr = videosValue!.firstIndex { (video) -> Bool in
                return video.id == video_id && video.owner_id == owner_id
            }
            if indexInArr != nil && section != nil {
                return IndexPath(row: indexInArr!, section: section!)
            }
        }
        return nil
    }
    
    func getIndexVideoInArr(video_id: Int64, owner_id: Int64) -> Int {
        return choosedVideos.firstIndex(where: { (str) -> Bool in
            let arr = str.split(separator: "_")
            return arr[0] == String(video_id) && arr[1] == String(owner_id)
        }) ?? -1
    }
    
    @objc func addPhotos() {
        delegate?.addVideo()
        self.navigationController?.popViewController(animated: true)
    }
}



class VKAlbumsVideoNode: ASCellNode {
    
    fileprivate let videoNode: VideoNode
    fileprivate let numberTextNode: ASTextNode
    fileprivate let titleTextNode: ASTextNode
    fileprivate let gradientNode: GradientNode
    
    var _video: VKVideo?
    var is_selected: Bool = false
    var numberInsets: CGFloat = 0
    weak var delegate: VKVideoAlbumDelegate?
    
    init(video: VKVideo, isSelected: Bool, chooseNumber: Int, nodeSize: Int) {
        videoNode = VideoNode(videoSrc: video, contentWidth: nodeSize, delegate: nil)
        numberTextNode = ASTextNode()
        titleTextNode = ASTextNode()
        gradientNode = GradientNode()
        super.init()
        automaticallyManagesSubnodes = true
        
        self._video = video
        self.is_selected = isSelected
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineBreakMode = .byWordWrapping
        
        numberTextNode.attributedText = NSAttributedString(string: "\(chooseNumber + 1)", attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.boldSystemFont(ofSize: 36), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: UIColor.white])
        numberTextNode.maximumNumberOfLines = 1
        numberTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(40))
        
        let numberHeight = numberTextNode.calculateSizeThatFits(CGSize(width: nodeSize, height: nodeSize)).height
        numberInsets = (CGFloat(nodeSize) - numberHeight) / 2.0
        
        titleTextNode.attributedText = NSAttributedString(string: "\(video.title)", attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.boldSystemFont(ofSize: 12), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: UIColor.white])
        titleTextNode.truncationMode = .byTruncatingTail
        titleTextNode.maximumNumberOfLines = 2
        titleTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(nodeSize - 10))
        
        gradientNode.isLayerBacked = true
        gradientNode.isOpaque = false
        gradientNode.cornerRadius = 9
        gradientNode.clipsToBounds = true
        
        if isSelected {
            gradientNode.borderWidth = 2
            gradientNode.borderColor = UIColor(red: 143/255, green: 155/255, blue: 224/255, alpha: 1).cgColor
        }
        
        videoNode.imageNode.addTarget(self, action: #selector(choosedVideo), forControlEvents: .touchUpInside)
        
        addSubnode(videoNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let videoInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: -5, left: 0, bottom: 0, right: 0), child: videoNode)
        var overlayImage = ASOverlayLayoutSpec(child: videoInsets, overlay: gradientNode)
        if is_selected == true {
            let numberNodeInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: numberInsets, left: 0, bottom: numberInsets, right: 0), child: numberTextNode)
            overlayImage =  ASOverlayLayoutSpec(child: overlayImage, overlay: numberNodeInsets)
        } else {
            let titleInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: CGFloat.infinity, left: 5, bottom: 3, right: 5), child: titleTextNode)
            overlayImage = ASOverlayLayoutSpec(child: overlayImage, overlay: titleInsets)
        }
        let photoSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: overlayImage)
        photoSpec.style.maxSize = constrainedSize.max
        photoSpec.style.minSize = constrainedSize.min
        return photoSpec
    }
    
    @objc func choosedVideo() {
        if _video != nil { delegate?.selectVideo(video: _video!) }
    }
}
