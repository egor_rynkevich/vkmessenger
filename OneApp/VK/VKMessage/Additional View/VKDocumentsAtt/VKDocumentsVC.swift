//
//  VKDocumentsVC.swift
//  OneApp
//
//  Created by Егор Рынкевич on 12/15/18.
//

import UIKit
import Gallery
import AsyncDisplayKit

protocol VKDocumentsVCDelegate: class {
    func addPhotoDocument(_ images: [UIImage?], _ sourceImg: [Image])
    func addVideoDocument(_ video: Video)
    func addVKDocumnets(_ docs: [VKDocument])
}

class VKDocumentsVC: UIViewController {
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var viewForTableNode: UIView!
    
    var documents: [VKDocument] = []
    var choosedDoc: [VKDocument] = []
    var contentWidth: Int = 0
    var isLoading: Bool = false
    var tableNode: ASTableNode?
    
    var delegate: VKDocumentsVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableNode = ASTableNode()
        self.tableNode!.view.separatorStyle = .none
        self.tableNode!.delegate = self
        self.tableNode!.dataSource = self
        self.tableNode!.backgroundColor = mainColors.mainTableView
        self.tableNode!.leadingScreensForBatching = 4
        
        self.view.backgroundColor = mainColors.mainTableView
        
        self.viewForTableNode.backgroundColor = mainColors.mainTableView
        self.viewForTableNode.addSubnode(tableNode!)
        
        addBtn.setTitle("Upload", for: .normal)
        addBtn.setImage(mainIcons.addUser_s25, for: .normal)
        addBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 15)
        addBtn.addTarget(self, action: #selector(openGallery), for: .touchUpInside)
        
        let rightBtn = UIBarButtonItem(title: "Add", style: .done, target: self, action: #selector(addDocs))
        rightBtn.isEnabled = false
        self.navigationItem.setRightBarButton(rightBtn, animated: true)
        
        vkLoadData.loadUserDocuments(peer_id: vkLoadData.mainUserID, completion: { [weak self] (dataArr) in
            if dataArr.count > 0 {
                DispatchQueue.main.async {
                    for doc in dataArr {
                        self?.documents.append(doc)
                    }
                    let frame = self?.view.frame
                    if frame != nil {
                        self?.tableNode?.frame = CGRect(x: /*frame!.origin.x +*/ 0, y: /*frame!.origin.y +*/ 5, width: frame!.width, height: frame!.height - 10)
                        self?.contentWidth = Int(frame?.width ?? 0)
                        self?.tableNode?.reloadData()
                    }
                }
            }
        })
    }
    
    @objc func openGallery() {
        let gallery = GalleryController()
        gallery.delegate = self
        
        Gallery.Config.Font.Text.bold = UIFont.systemFont(ofSize: 14)
        Gallery.Config.Camera.recordLocation = true
        Gallery.Config.tabsToShow = [.cameraTab, .imageTab, .videoTab]
        Gallery.Config.initialTab = .imageTab
        Gallery.Config.VideoEditor.maximumDuration = 60 * 60 * 24
        Gallery.Config.Grid.FrameView.borderColor = UIColor(red: 143/255, green: 155/255, blue: 224/255, alpha: 1)
        
        self.present(gallery, animated: true, completion: nil)
    }

    @objc func addDocs() {
        delegate?.addVKDocumnets(choosedDoc)
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension VKDocumentsVC: ASTableDelegate, ASTableDataSource {
    
    //MARK: TableNode function
    
    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
        return documents.count
    }
    
    func tableNode(_ tableNode: ASTableNode, nodeBlockForRowAt indexPath: IndexPath) -> ASCellNodeBlock {
        return {
            let node = VKDocumentCellNode(doc: self.documents[indexPath.row], contentWidth: self.contentWidth, msgColor: mainColors.getMessTextColor(true))
            if let _ = self.choosedDoc.firstIndex(where: { (doc) -> Bool in return doc.id == self.documents[indexPath.row].id }) {
                node.borderWidth = 2
                node.borderColor = UIColor(red: 143/255, green: 155/255, blue: 224/255, alpha: 1).cgColor
            }
            
            return node
        }
    }
    
    func tableNode(_ tableNode: ASTableNode, didSelectRowAt indexPath: IndexPath) {
        tableNode.deselectRow(at: indexPath, animated: true)
        
        let index = choosedDoc.firstIndex {(doc) -> Bool in return doc.id == documents[indexPath.row].id}
        
        if index == nil { choosedDoc.append(documents[indexPath.row]) }
        else { choosedDoc.remove(at: index!) }
        
        self.navigationItem.rightBarButtonItem?.title = choosedDoc.count > 0 ? "Add (\(choosedDoc.count))" : "Add"
        self.navigationItem.rightBarButtonItem?.isEnabled = choosedDoc.count > 0
        
        tableNode.reloadRows(at: [indexPath], with: .none)
    }
    
    func tableNode(_ tableNode: ASTableNode, willBeginBatchFetchWith context: ASBatchContext) {
        DispatchQueue.main.async {
            if self.isLoading == false {
                self.isLoading = true
                vkLoadData.loadUserDocuments(peer_id: vkLoadData.mainUserID, count: 1999, offset: 1999, completion: { [weak self] (dataArr) in
                    if dataArr.count > 0 {
                        DispatchQueue.main.async {
                            for doc in dataArr { self?.documents.append(doc) }
                            self?.isLoading = false
                            context.completeBatchFetching(true)
                        }
                    }
                })
            }
        }
    }
    
    func shouldBatchFetch(for tableNode: ASTableNode) -> Bool {
        if self.documents.count < vkLoadData.countUserDocuments { return true }
        
        return false
    }
    
    
    @objc(tableNode:constrainedSizeForRowAtIndexPath:)
    func tableNode(_ tableNode: ASTableNode, constrainedSizeForRowAt indexPath: IndexPath) -> ASSizeRange {
        let min = CGSize(width: UIScreen.main.bounds.size.width, height: 1)
        let max = CGSize(width: UIScreen.main.bounds.size.width, height: CGFloat.greatestFiniteMagnitude)
        return ASSizeRange(min: min, max: max)
    }
    
}

extension VKDocumentsVC: GalleryControllerDelegate {
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        controller.dismiss(animated: true, completion: nil)
        
        delegate?.addVideoDocument(video)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        controller.dismiss(animated: true, completion: nil)
        
        Image.resolve(images: images) { (allImages) in
            self.delegate?.addPhotoDocument(allImages, images)
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        controller.dismiss(animated: true, completion: nil)
        //MARK: TO_DO Create gallery for preview images
        
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

class VKDocumentCellNode: ASCellNode {
    
    fileprivate let fileNameTextNode: ASTextNode
    fileprivate let extTextNode: ASTextNode
    fileprivate let sizeTextNode: ASTextNode
    fileprivate let dateTextNode: ASTextNode
    fileprivate let fileImageNode: ASImageNode
    fileprivate let imageNetNode: ASNetworkImageNode
    var url: String = ""
    var img_id: Int64 = 0
    var img_url: String = ""
    var owner_id: Int64 = 0
    
    init(doc: VKDocument, contentWidth: Int, msgColor: MessageColor) {
        fileNameTextNode = ASTextNode()
        extTextNode = ASTextNode()
        sizeTextNode = ASTextNode()
        dateTextNode = ASTextNode()
        fileImageNode = ASImageNode()
        imageNetNode = ASNetworkImageNode()
        super.init()
        automaticallyManagesSubnodes = true
        
        selectionStyle = .none
        
        url = doc.url
        
        fileNameTextNode.attributedText = NSAttributedString(string: doc.title.count > 0 ? doc.title : "...", attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: msgColor.messageText!])
        fileNameTextNode.truncationMode = .byTruncatingTail
        fileNameTextNode.backgroundColor = UIColor.clear
        fileNameTextNode.placeholderColor = mainColors.lightGray
        fileNameTextNode.maximumNumberOfLines = 1
        fileNameTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 70))
        
        extTextNode.attributedText = NSAttributedString(string: doc.ext.uppercased(), attributes: [.font : UIFont.systemFont(ofSize: 8), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: msgColor.messageIcon!])
        extTextNode.placeholderEnabled = true
        extTextNode.backgroundColor = UIColor.clear
        extTextNode.placeholderColor = mainColors.lightGray
        extTextNode.cornerRadius = 3
        extTextNode.borderWidth = 1
        extTextNode.borderColor = msgColor.messageIcon?.cgColor
        extTextNode.textContainerInset = UIEdgeInsets(top: 2, left: 3, bottom: 2, right: 3)
        
        sizeTextNode.attributedText = NSAttributedString(string: ByteCountFormatter.string(fromByteCount: Int64(doc.size), countStyle: .file), attributes: [.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: msgColor.messageSubText!])
        sizeTextNode.placeholderEnabled = true
        sizeTextNode.backgroundColor = UIColor.clear
        sizeTextNode.placeholderColor = mainColors.lightGray
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy h:mm a"
        let dateStr = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(doc.date)))
        
        dateTextNode.attributedText = NSAttributedString(string: "- \(dateStr)", attributes: [.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: msgColor.messageSubText!])
        dateTextNode.placeholderEnabled = true
        dateTextNode.backgroundColor = UIColor.clear
        dateTextNode.placeholderColor = mainColors.lightGray
        
        fileImageNode.image = mainIcons.file_s40
        fileImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(msgColor.messageSubText!)
        
        img_id = doc.id
        img_url =  doc.photoDocSizeMid?.url ?? doc.photoDocSizeMax?.url ?? doc.photoDocSizeMin?.url ?? ""
        owner_id = doc.owner_id
        imageNetNode.style.preferredSize = CGSize(width: 50, height: 50)
        imageNetNode.cornerRadius = 7
        
        let data = vkDataCache.getDataFromCachedData(sourceID: img_id, ownerID: owner_id, type: "image", tag: "doc_image")
        
        if data == nil { imageNetNode.url = NSURL(string: img_url)! as URL }
        else { imageNetNode.image = UIImage(data: data! as Data) }
        
        imageNetNode.imageModificationBlock = { [weak self] image in
            if vkDataCache.getDataFromCachedData(sourceID: self?.img_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "doc_image") == nil {
                vkDataCache.saveDataInCachedData(sourceID: self?.img_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "doc_image", url: self?.img_url ?? "", image: image)
            }
            return image
        }
        
        addSubnode(fileNameTextNode)
        addSubnode(extTextNode)
        addSubnode(fileImageNode)
        addSubnode(sizeTextNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let sizeDateHStackSpec = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .start, alignItems: .start, children: [sizeTextNode, dateTextNode])
        
        let titleVStackSpec = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .start, alignItems: .start, children: [fileNameTextNode, sizeDateHStackSpec])
        
        var spec = ASLayoutSpec()
        
        if img_url == "" {
            extTextNode.style.layoutPosition = CGPoint(x: 0, y: 17)
            fileImageNode.style.layoutPosition = CGPoint(x: 0, y: 0)
            
            let absoluteSpec = ASAbsoluteLayoutSpec(children: [fileImageNode, extTextNode])
            absoluteSpec.sizing = .sizeToFit
            spec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 5), child: absoluteSpec)
        } else {
            spec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 5), child: imageNetNode)
        }
        
        
        let finalStack = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .start, alignItems: .center, children: [spec, titleVStackSpec])
        let finalInsetSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 5, bottom: 0, right: 5), child: finalStack)
        return finalInsetSpec
    }
    
}
