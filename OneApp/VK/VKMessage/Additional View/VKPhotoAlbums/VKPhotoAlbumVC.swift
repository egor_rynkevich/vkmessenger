//
//  VKPhotoAlbumVC.swift
//  OneApp
//
//  Created by Егор Рынкевич on 12/5/18.
//

import UIKit
import AsyncDisplayKit

protocol VKPhotoAlbumDelegate: class {
    func selectPhoto(photo: VKPhoto)
}

class VKPhotoAlbumVC: UIViewController, ASCollectionDelegate, ASCollectionDataSource, VKPhotoAlbumDelegate {
    
    var imgCollectionNode: ASCollectionNode?
    var photosArr: [VKPhoto] = []
    var photoDict: [String : [VKPhoto]] = [:]
    var photoDateSection : [String] = []
    var choosedPhotos: [String] = []
    var choosedPhotosObj: [VKPhoto] = []
    var imgSize: Int = 0
    var album_id: Int64 = 0
    
    var delegate: VKAlbumsVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgSize = Int((UIScreen.main.bounds.width - 30) / 3 )
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumLineSpacing = CGFloat(5)
        flowLayout.minimumInteritemSpacing = CGFloat(5)
        flowLayout.headerReferenceSize = CGSize(width: 50, height: 50)
        flowLayout.footerReferenceSize = CGSize(width: 0, height: 0)
        flowLayout.itemSize = CGSize(width: imgSize, height: imgSize)
        imgCollectionNode = ASCollectionNode(collectionViewLayout: flowLayout)
        imgCollectionNode?.registerSupplementaryNode(ofKind: UICollectionView.elementKindSectionHeader)
        imgCollectionNode?.backgroundColor = UIColor.clear
        imgCollectionNode?.style.preferredSize = CGSize(width: 100, height: 100)

        imgCollectionNode?.delegate = self
        imgCollectionNode?.dataSource = self
        
        view.addSubnode(imgCollectionNode!)
        
        let rightBtn = UIBarButtonItem(title: choosedPhotos.count > 0 ? "Add (\(choosedPhotos.count))" : "Add", style: .done, target: self, action: #selector(addPhotos))
        rightBtn.isEnabled = choosedPhotos.count > 0
        self.navigationItem.setRightBarButton(rightBtn, animated: true)
        
        vkLoadData.loadUserPhotosFromAlbum(peer_id: vkLoadData.mainUserID, album_id: album_id) { [weak self] (arr) in
            if arr.count > 0 {
                for item in arr { self?.photosArr.append(item) }
                self?.configPhotoDict()
                
                DispatchQueue.main.async {
                    let frame = self?.view.frame
                    if frame != nil {
                        self?.imgCollectionNode?.frame = CGRect(x: /*frame!.origin.x +*/ 10, y: /*frame!.origin.y +*/ 0, width: frame!.width - 20, height: frame!.height - 10)
                        self?.imgCollectionNode?.reloadData()
                    }
                }
            }
        }
        
        view.backgroundColor = mainColors.mainTableView
        
        // Do any additional setup after loading the view.
    }
    
    func configPhotoDict() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        
        for photo in photosArr {
            let dateStr = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(photo.date)))
            if photoDateSection.contains(dateStr) == false { photoDateSection.append(dateStr) }
            
            if var photoValues = photoDict[dateStr] {
                photoValues.append(photo)
                photoDict[dateStr] = photoValues
            }
            else { photoDict[dateStr] = [photo] }
        }
        
        photoDateSection = photoDateSection.sorted(by: { $0 > $1 })
    }
    
    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        return photoDateSection.count
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, nodeForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> ASCellNode {
        return VKAlbumsPhotoSectionNode(dateStr: photoDateSection[indexPath.section])
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        return photoDict[photoDateSection[section]]?.count ?? 0
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        return ASSizeRange(min: CGSize(width: imgSize, height: imgSize), max: CGSize(width: imgSize, height: imgSize))
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        return {
            let photo = self.photoDict[self.photoDateSection[indexPath.section]]![indexPath.row]
            let index = self.getIndexPhotoInArr(photo_id: photo.id, owner_id: photo.owner_id)
            let node = VKAlbumsPhotoNode(photo: photo, isSelected: index != -1, chooseNumber: index, nodeSize: self.imgSize)
            node.delegate = self
            return node
        }
    }
    
    func selectPhoto(photo: VKPhoto) {
        let photo_id = photo.id
        let owner_id = photo.owner_id
        let index = getIndexPhotoInArr(photo_id: photo_id, owner_id: owner_id)
        
        if index == -1 {
            choosedPhotos.append("\(photo_id)_\(owner_id)_\(choosedPhotos.count)")
            choosedPhotosObj.append(photo)
            if let indexPath = getIndexPathForReload(photo_id: photo_id, owner_id: owner_id) {
                imgCollectionNode?.reloadItems(at: [indexPath])
            }
        }
        else {
            var indexPathArr: [IndexPath] = []
            
            let needRemoveElem = choosedPhotos[index].split(separator: "_")
            
            if let indexPath = getIndexPathForReload(photo_id: Int64(needRemoveElem[0]) ?? 0, owner_id: Int64(needRemoveElem[1]) ?? 0) {
                indexPathArr.append(indexPath)
            }
                
            choosedPhotos.remove(at: index)
            choosedPhotosObj.remove(at: index)
                
            for pht in photosArr {
                let indexInArr = getIndexPhotoInArr(photo_id: pht.id, owner_id: pht.owner_id)
                if indexInArr != -1 {
                    let arr = choosedPhotos[indexInArr].split(separator: "_")
                    if String(indexInArr) != arr[2] {
                        choosedPhotos[indexInArr] = arr[0] + "_" + arr[1] + "_" + String(indexInArr)
                
                        if let indexPathToUpdate = getIndexPathForReload(photo_id: Int64(arr[0]) ?? 0, owner_id: Int64(arr[1]) ?? 0) {
                            indexPathArr.append(indexPathToUpdate)
                        }
                    }
                }
            }
            imgCollectionNode?.reloadItems(at: indexPathArr)
        }
        
        delegate?.addPhotoToArr(choosedPhotos: choosedPhotos, choosedPhotosObject: choosedPhotosObj)
        
        self.navigationItem.rightBarButtonItem?.title = choosedPhotos.count > 0 ? "Add (\(choosedPhotos.count))" : "Add"
        self.navigationItem.rightBarButtonItem?.isEnabled = choosedPhotos.count > 0
    }
    
    
    func getIndexPathForReload(photo_id: Int64, owner_id: Int64) -> IndexPath? {
        
        let indexInArr = photosArr.firstIndex { (photo) -> Bool in
            return photo.id == photo_id && photo.owner_id == owner_id
        }
        let photo = photosArr[indexInArr!]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        
        let dateStr = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(photo.date)))
        let section = photoDateSection.firstIndex(of: dateStr)
        
        let photosValue = photoDict[dateStr]
        
        if photosValue != nil {
            let indexInArr = photosValue!.firstIndex { (photo) -> Bool in
                return photo.id == photo_id && photo.owner_id == owner_id
            }
            if indexInArr != nil && section != nil {
                return IndexPath(row: indexInArr!, section: section!)
            }
        }
        return nil
    }

    func getIndexPhotoInArr(photo_id: Int64, owner_id: Int64) -> Int {
        return choosedPhotos.firstIndex(where: { (str) -> Bool in
            let arr = str.split(separator: "_")
            return arr[0] == String(photo_id) && arr[1] == String(owner_id)
        }) ?? -1
    }
    
    @objc func addPhotos() {
        delegate?.addPhoto()
        self.navigationController?.popViewController(animated: true)
    }
}



class VKAlbumsPhotoNode: ASCellNode {
    
    fileprivate let imageNode: ASNetworkImageNode
    fileprivate let titleTextNode: ASTextNode
    fileprivate let gradientNode: GradientNode
    
    var photo_id: Int64 = 0
    var owner_id: Int64 = 0
    var photo_url: String = ""
    var _photo: VKPhoto?
    var is_selected: Bool = false
    var numberInsets: CGFloat = 0
    weak var delegate: VKPhotoAlbumDelegate?
    
    init(photo: VKPhoto, isSelected: Bool, chooseNumber: Int, nodeSize: Int) {
        imageNode = ASNetworkImageNode()
        titleTextNode = ASTextNode()
        gradientNode = GradientNode()
        super.init()
        automaticallyManagesSubnodes = true
        
        self.is_selected = isSelected
        _photo = photo
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineBreakMode = .byWordWrapping
        
        titleTextNode.attributedText = NSAttributedString(string: "\(chooseNumber + 1)", attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.boldSystemFont(ofSize: 36), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: UIColor.white])
        titleTextNode.maximumNumberOfLines = 1
        titleTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(40))
        
        let numberHeight = titleTextNode.calculateSizeThatFits(CGSize(width: nodeSize, height: nodeSize)).height
        numberInsets = (CGFloat(nodeSize) - numberHeight) / 2.0
        
        gradientNode.isLayerBacked = true
        gradientNode.isOpaque = false
        gradientNode.cornerRadius = 5
        gradientNode.clipsToBounds = true
        
        photo_id = photo.id
        owner_id = photo.owner_id
        photo_url = photo.photoDocSizeMid?.url ?? photo.photoDocSizeMax?.url ?? photo.photoDocSizeMin?.url ?? ""
        
        let data = vkDataCache.getDataFromCachedData(sourceID: photo_id, ownerID: owner_id, type: "image", tag: "photo")
        
        if data == nil { imageNode.url = NSURL(string: photo_url)! as URL }
        else { imageNode.image = UIImage(data: data! as Data) }
        
        imageNode.imageModificationBlock = { [weak self] image in
            if vkDataCache.getDataFromCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "photo") == nil {
                vkDataCache.saveDataInCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "photo", url: self?.photo_url ?? "", image: image)
            }
            return image
        }
        
        imageNode.cornerRadius = 5
        if isSelected {
            gradientNode.borderWidth = 2
            gradientNode.borderColor = UIColor(red: 143/255, green: 155/255, blue: 224/255, alpha: 1).cgColor
        }
        
        imageNode.addTarget(self, action: #selector(choosedPhoto), forControlEvents: .touchUpInside)
        
        addSubnode(imageNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        var overlayImage = ASLayoutSpec()
        if is_selected == true {
            let overlayImageGrad = ASOverlayLayoutSpec(child: imageNode, overlay: gradientNode)
            let numberNodeInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: numberInsets, left: 0, bottom: numberInsets, right: 0), child: titleTextNode)
            overlayImage =  ASOverlayLayoutSpec(child: overlayImageGrad, overlay: numberNodeInsets)
        } else {
            overlayImage =  ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: imageNode)
        }
        let photoSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: overlayImage)
        photoSpec.style.maxSize = constrainedSize.max
        photoSpec.style.minSize = constrainedSize.min
        return photoSpec
    }
    
    @objc func choosedPhoto() {
        if _photo != nil { delegate?.selectPhoto(photo: _photo!) }
    }
}

class VKAlbumsPhotoSectionNode: ASCellNode {
    
    fileprivate let titleTextNode: ASTextNode
    
    init(dateStr: String) {
        titleTextNode = ASTextNode()
        super.init()
        automaticallyManagesSubnodes = true
        
        titleTextNode.attributedText = NSAttributedString(string: dateStr, attributes: [.font : UIFont.boldSystemFont(ofSize: 18), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: UIColor(red: 117/255, green: 149/255, blue: 212/255, alpha: 1)])
        titleTextNode.backgroundColor = UIColor.clear
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let sectionSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 14, left: 0, bottom: 0, right: 0), child: titleTextNode)
        return sectionSpec
    }
}
