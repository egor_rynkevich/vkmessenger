//
//  VKReplyForwardMsgView.swift
//  OneApp
//
//  Created by Егор Рынкевич on 1/18/19.
//

import Foundation

enum VKReplyForwardMsgViewType {
    case reply, forward
}

class VKReplyForwardMsgView : UIView {
    
    //var actionType: VKReplyForwardMsgViewType = VKReplyForwardMsgViewType.reply
    var title: String = ""
    var titleLbl: UILabel = UILabel()
    var caption: String = ""
    var captionLbl: UILabel = UILabel()
    var vLine: UIImageView = UIImageView()
    let closeBtn: UIButton = UIButton()
    
    
    func configView(msg: VKMessage, actionType: VKReplyForwardMsgViewType) {
        title = vkDataCache.getUsernameByIDAndType(id: msg.from_id, type: msg.from_id > 0 ? VKUserType.isUser.rawValue : VKUserType.isGroup.rawValue)
        if actionType == VKReplyForwardMsgViewType.reply {
            if msg.text.count > 0 { caption = msg.text
            } else if msg.attachments.count > 0 { caption = "\(msg.attachments.count) attachments"
            } else if msg.geo != nil { caption = "geo"
            } else if msg.fwd_messages.count > 0 { caption = "\(msg.fwd_messages.count) forwarded messages"}
        } else {
            
        }
        
        //backgroundColor = mainColors.online
        
        vLine.frame = CGRect(x: 10, y: 5, width: 2, height: 35)
        vLine.backgroundColor = mainColors.writeRow
        vLine.layer.cornerRadius = 1
        addSubview(vLine)
        
        titleLbl.frame = CGRect(x: 20, y: 3, width: UIScreen.main.bounds.width - 60, height: 17)
        titleLbl.text = title
        titleLbl.textColor = mainColors.writeRow
        titleLbl.font = UIFont.systemFont(ofSize: 15)
        addSubview(titleLbl)
        captionLbl.frame = CGRect(x: 20, y: 25, width: UIScreen.main.bounds.width - 60, height: 15)
        captionLbl.text = caption
        captionLbl.textColor = mainColors.lightGray
        captionLbl.font = UIFont.systemFont(ofSize: 13)
        addSubview(captionLbl)
        closeBtn.frame = CGRect(x: UIScreen.main.bounds.width - 30, y: 15, width: 10, height: 10)
        closeBtn.setImage(mainIcons.cancel_s21, for: .normal)
        closeBtn.tintColor = mainColors.deleteRow
        addSubview(closeBtn)
    }
}
