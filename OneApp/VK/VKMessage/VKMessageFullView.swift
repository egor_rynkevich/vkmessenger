//
//  VKMessageFullView.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 10.09.2018.
//

import UIKit
import RealmSwift
import SwiftyVK
import SwiftyJSON
import SafariServices
import MapKit
import AsyncDisplayKit
import AVKit
import AVFoundation

class VKMessageFullView: UIViewController, ASTableDelegate, ASTableDataSource, MessageCellNodeDelegate {

    private var tableNode: ASTableNode = ASTableNode()
    var msgRef: ThreadSafeReference<VKMessage>?
    var mes: VKMessage?
    var userIDarr: [Int64] = []
    var groupIDarr: [Int64] = []
    var userIDs: String = ""
    var groupIDs: String = ""
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadUserInfo()
        navigationItem.title = "Message"
        
        tableNode.view.separatorStyle = .none
        tableNode.delegate = self
        tableNode.dataSource = self
        tableNode.backgroundColor = mainColors.mainTableView
        
        view.addSubnode(tableNode)
    }
    
    func loadUserInfo() {
        let realm = try! Realm()
        
        if let msg = mes {
            msgRef = ThreadSafeReference(to: msg)
            for att in msg.attachments {
                if att.type == "wall" {
                    guard let wall = att.wall else { return }
                    if wall.owner_id > 0 { userIDarr.append(wall.owner_id) }
                    else { groupIDarr.append(abs(wall.owner_id)) }
                }
            }
            if msg.fwd_messages.count > 0 { getUserIDFromFwdMsg(msg.fwd_messages) }
            
            let allUsers = realm.objects(VKUser.self)
            
            for id in userIDarr {
                let user = allUsers.filter("id = %@", id).first
                if user == nil { userIDs.append("\(id),")}
            }
            
            let allGroups = realm.objects(VKGroup.self)
            
            for id in groupIDarr {
                let group = allGroups.filter("id = %@", id).first
                if group == nil { groupIDs.append("\(id),")}
            }
            
            vkLoadData.loadMainInfoAboutGroup(groupIDs, completion: { [weak self] (result) in
                if result == true {
                    vkDataCache.saveGroupInfoInCache(vkLoadData.groupsInfo, completion: { [weak self] (result) in
                        if result == 1 {
                            vkLoadData.groupsInfo.removeAll()
                            if self?.userIDs.count ?? 0 > 0 {
                                vkLoadData.loadMainInfoAboutUser(self?.userIDs ?? "") { [weak self] (result) in
                                    if result == true{
                                        vkDataCache.saveUserInfoInCache(vkLoadData.usersInfo, completion: { [weak self] (result) in
                                            if result == 1 {
                                                vkLoadData.usersInfo.removeAll()
                                                DispatchQueue.main.async {
                                                    self?.count = 1
                                                    self?.tableNode.reloadData()
                                                }
                                            }
                                        })
                                    }
                                }
                            }
                            else {
                                DispatchQueue.main.async {
                                    self?.count = 1
                                    self?.tableNode.reloadData()
                                }
                            }
                        }
                    })
                }
            })
            
            
        }
    }
    
    func getUserIDFromFwdMsg(_ msgs: List<VKMessage>) {
        for msg in msgs {
            if msg.fwd_messages.count > 0 { getUserIDFromFwdMsg(msg.fwd_messages) }
            else {
                if msg.from_id > 0 { userIDarr.append(msg.from_id) }
                else { groupIDarr.append(abs(msg.from_id)) }
            }
        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableNode.frame = view.bounds
    }

    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
        return count
    }
    
    func tableNode(_ tableNode: ASTableNode, nodeBlockForRowAt indexPath: IndexPath) -> ASCellNodeBlock {
        let mesRf = ThreadSafeReference(to: mes!)
        return {
            let node = MessageFullNode(msgRef: mesRf, tableWidth: UIScreen.main.bounds.size.width - 10, delegate: self)
            return node
        }
    }
    
    @objc(tableNode:constrainedSizeForRowAtIndexPath:)
    func tableNode(_ tableNode: ASTableNode, constrainedSizeForRowAt indexPath: IndexPath) -> ASSizeRange {
        let min = CGSize(width: UIScreen.main.bounds.size.width, height: 1)
        let max = CGSize(width: UIScreen.main.bounds.size.width, height: CGFloat.greatestFiniteMagnitude)
        return ASSizeRange(min: min, max: max)
    }
    
    //MARK: MessageCellNodeDelegate
 
    func openURL(_ url: String, msgStrID: String) {
        let svc = SFSafariViewController(url: URL(string: url)!)
        present(svc, animated: true, completion: nil)
    }
    
    func openMap(lat: Double, long: Double, msgStrID: String) {
        let source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long)))
        source.name = "Source"
        
        let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long)))
        destination.name = "Destination"
        
        MKMapItem.openMaps(with: [source, destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
    }
    
    func openGif(gifUrl: String, gifSize: Int32, videoUrl: String, videoSize: Int32, msgStrID: String) {
        if videoUrl == "" {
            let gifPlayerCont = GifPlayer(nibName: "GifPlayer", bundle: nil)
            gifPlayerCont.gifUrl = URL(string: gifUrl)
            self.present(gifPlayerCont, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "", message: "Choose format (the gif-player offers the ability to rewind in both directions)", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Gif(\(ByteCountFormatter.string(fromByteCount: Int64(gifSize), countStyle: .file)))", style: .default, handler: { [weak self] (action) in
                let gifPlayerCont = GifPlayer(nibName: "GifPlayer", bundle: nil)
                gifPlayerCont.gifUrl = URL(string: gifUrl)
                self?.present(gifPlayerCont, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "MP4(\(ByteCountFormatter.string(fromByteCount: Int64(videoSize), countStyle: .file)))", style: .default, handler: { [weak self] (action) in
                let url = URL(string: videoUrl)
                let player = AVPlayer(url: url!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self?.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openMessageNode(msgID: Int64) {
        
    }
    
    func pressVote() {
        let realm = try! Realm()
        if let msg = realm.objects(VKMessage.self).filter("peer_id = %@ AND id = %@ AND isPreview = %@", mes?.peer_id ?? 0, mes?.id ?? 0, false).first {
            mes = msg
        }
        tableNode.reloadData()
    }
    
    func openProfile(user_id: Int64) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vkUserProfileTVC = storyboard.instantiateViewController(withIdentifier: "VKUserProfileTableViewController") as? VKUserProfileTableViewController {
            let realm = try! Realm()
            if let user = realm.objects(VKUser.self).filter("id = %@", user_id).first {
                vkUserProfileTVC.user.copyValue(user)
                //vkUserProfileTVC.indexPath = indexPath
                self.navigationController?.pushViewController(vkUserProfileTVC, animated: true)
            }
            
        }
    }
}
