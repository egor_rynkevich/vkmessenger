//
//  VKMessageTableViewController.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 11.04.2018.
//

import UIKit
import RealmSwift
import SwiftyVK
import SwiftyJSON
import SafariServices
import MapKit
import AsyncDisplayKit
import AVKit
import AVFoundation
import DifferenceKit
import Gallery

//extension Int64 : Differentiable {}

class VKMessageViewController: UIViewController {
    struct MessageSectionStruct: Differentiable {
        let sectionTitle: String
        var messages: [VKMessage]

        var differenceIdentifier: String {
            return sectionTitle
        }

        func isContentEqual(to source: MessageSectionStruct) -> Bool {
            return sectionTitle == source.sectionTitle
        }
    }

    private typealias MessageSection = ArraySection<MessageSectionStruct, VKMessage>

    private var messagesDiff = [MessageSection]()
    private var messagesInput: [MessageSection] {
        get { return messagesDiff }
        set {
            let changeset = StagedChangeset(source: messagesDiff, target: newValue)
            tableNode.reload(using: changeset) { data in
                self.messagesDiff = data
            }
        }
    }
    
    
    weak var convers : VKConversation?
    var conversRef : ThreadSafeReference<VKConversation>?
    var conversID : String = ""
    var out_read: Int64 = 0
    var name : String = ""
    var userImage : UIImage?
    
    var messages : [VKMessage] = []
    var messSectionDate: [Int64] = []
    var messSectionDateString: [String] = []
    
    var attachments: [VKAttachmentShort] = []
    
    var allMsgs : Results<VKMessage>?
    var peer_type : String = VKUserType.isUser.rawValue
    var peer_id : Int64 = 0
    var peer_local_id : Int64 = 0
    var in_read: Int64 = 0
    
    var count : Int = 0
    var unreadCount : Int = 0
    var offset : Int = 0
    var isLoading: Bool = false
    var inputText: String = "Type here anything..."
    var emptyViewHeight: CGFloat = 0
    var isRecordBtn: Bool = true
    var isRecording = false
    var isStatusBarWasHidden: Bool = false
    var isHaveAttachments: Bool = false
    var isUserTyping: Bool = false
    var isUserOnline: Bool = false
    var attViewHeight: CGFloat = 0
    private let parentAdditionalHeight : CGFloat = 24.0
    var audioRecorder: AVAudioRecorder?
    var tableNode: ASTableNode = ASTableNode()
    var inputTextNode: ASEditableTextNode = ASEditableTextNode()
    var attachmentNode: VKAttachmentNode?
    var attachmentBadgeLbl: UILabel?
    
    @IBOutlet weak var viewForTableNode: UIView!
    @IBOutlet weak var msgInputCntrView: UIView!
    @IBOutlet weak var inputStickerView: UIView!
    
    @IBOutlet weak var textViewParentHeight: NSLayoutConstraint!
    @IBOutlet weak var textViewParentBottom: NSLayoutConstraint!
    @IBOutlet weak var textInputTop: NSLayoutConstraint!
    let textInputTopConstant: Int = 9

    @IBOutlet weak var addAttachmensBtn: UIButton!
    @IBOutlet weak var addStickerBtn: UIButton!
    @IBOutlet weak var sendMsgBtn: UIButton!
    
    var userStatusBGQueue: DispatchQueue?
    
    //var refreshControl = UIRefreshControl()
    var messFrame: CGRect?
    var viewFrame: CGRect?
    
    var tapGestureRecognizer: UITapGestureRecognizer?
    
    //action on message
    
    var actionWithMsgView: UIView?
    var overlay: UIView?
    var isSelectMode: Bool = false
    var selectMessagesID: [Int64] = []
    var selectMsgIndexPath: IndexPath?
    var isEditMode: Bool = false
    var editMsgID: Int64 = 0
    var replyForwardViewHeight: CGFloat = 0
    var replyForwardView: VKReplyForwardMsgView?
    var replyForwardMsgID: Int64 = 0
    var forwardTargetView: VKForwardTargetView?
    //MARK: METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userStatusBGQueue = DispatchQueue.init(label: "USER_STATUS_BG_QUEUE")
        
        vkLoadData.countMessInConv = 0
        initData()
        initTableView()
        
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(longPressGesture:)))
        longPressGesture.delegate = self
        self.tableNode.view.addGestureRecognizer(longPressGesture)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addLocation(notification:)), name: Notification.Name.addLocation, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideAttNode(notification:)), name: NSNotification.Name.updateAttachmentNode, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadInfoInMessageTV), name: NSNotification.Name.reloadInfoInMessageTV, object: nil)
        //view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
    }
    
//    deinit {
//        tableNode.delegate = nil
//        tableNode.dataSource = nil
//    }
    
    func initData() {
        conversID = convers?.conversationID ?? ""
        conversRef = ThreadSafeReference(to: (vkDataCache.bgRealm?.objects(VKConversation.self).filter("conversationID = %@", conversID).first)!)
        
        navigationItem.titleView = setTitle()
        
        
        peer_type = convers?.peer_type ?? VKUserType.isUser.rawValue
        peer_id = convers?.peer_id ?? 0
        peer_local_id = convers?.peer_local_id ?? 0
        conversID = convers?.conversationID ?? ""
        out_read = convers?.out_read ?? 0
        in_read = convers?.in_read ?? 2_000_000_000
        isUserOnline = convers?.userInfo?.online ?? false
        
        unreadCount = convers?.unread_count ?? 0
        
        let data = vkDataCache.getProfileImgFromCache(sourceID: peer_id, ownerID: peer_id, type: "image", tag: "ava")
        
        var profileImg: UIImage?
        let imgSize = 30
        
        if data == nil {
                profileImg = Helper.textToImage(drawText: vkDataCache.getUserShortNameByIDAndType(id: peer_id, type: peer_type), height: imgSize, width: imgSize)
        }
        else {
            profileImg = UIImage(data: data! as Data)
        }
        
        profileImg = profileImg?.resize(targetSize: CGSize(width: imgSize, height: imgSize))
        
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: imgSize, height: imgSize))
        btn.setImage(profileImg, for: .normal)
        btn.layer.cornerRadius = CGFloat(imgSize) / 2
        btn.layer.masksToBounds = true
        
        let v = UIImageView(frame: CGRect(x: 0, y: 0, width: imgSize, height: imgSize))
        v.layer.cornerRadius = CGFloat(imgSize) / 2
        v.layer.shadowOffset = CGSize(width: 0, height: 0)
        v.layer.shadowRadius = 2.7 //CGFloat(imgSize) / 2
        v.layer.shadowOpacity = 0.28
        v.layer.shadowColor = UIColor(red: 59.0 / 255.0, green: 59.0 / 255.0, blue: 59.0 / 255.0, alpha: 1).cgColor
        v.addSubview(btn)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: v)
    
        removeDeletedMsg()
        
        if self.convers?.messages.count ?? 0 >= 40 {
            self.updateValueInTable(40)
        }
        
        updateUserStatus()
    }
    
    func setTitle() -> UIView {
        var titleWidth : CGFloat = 0
        var subtitleWidth : CGFloat = 0
        let titleLabel = UILabel(frame: CGRect(x: 0, y: -2, width: 0, height: 0))
        var soundImgView : UIImageView = UIImageView()
        var mobileImgView : UIImageView = UIImageView()
        
        self.convers = vkDataCache.bgRealm?.objects(VKConversation.self).filter("conversationID = %@", self.conversID).first
        
        /////// title
        
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textColor = mainColors.textBar
        titleLabel.font = UIFont.boldSystemFont(ofSize: 15)
        titleLabel.text = vkDataCache.getUsernameByIDAndType(id: convers?.peer_id ?? 0, type: convers?.peer_type ?? "")
        titleLabel.sizeToFit()
        titleWidth = titleLabel.frame.width
        
        /////// sound
        
        if convers?.push_settings_no_sound == true {
            soundImgView = UIImageView(frame: CGRect(x: titleWidth + 10, y: 0, width: 15, height: 15))
            soundImgView.image = mainIcons.soundless
            soundImgView.tintColor = mainColors.textBar
            titleWidth += 25
        }
        
        /////// subtitle
        
        let subtitleLabel = UILabel(frame: CGRect(x: 0, y: 18, width: 0, height: 0))
        subtitleLabel.backgroundColor = UIColor.clear
        subtitleLabel.textColor = mainColors.subTextBar
        subtitleLabel.font = UIFont.systemFont(ofSize: 11)
        
        if isUserTyping == true {
            subtitleLabel.text = "typing"
            subtitleLabel.textColor = UIColor(red: 73 / 255, green: 162 / 255, blue: 227 / 255, alpha: 1)
        }
        else if convers?.peer_type == VKUserType.isChat.rawValue {
            let count = convers?.chat_settings_members_count ?? 0
            subtitleLabel.text = String(count)
            subtitleLabel.text?.append(count > 1 ? " users" : " user")
        } else if convers?.peer_type == VKUserType.isGroup.rawValue { subtitleLabel.text = "group" }
        else {
            if convers?.userInfo?.online == true {
                subtitleLabel.text = "online"
                subtitleLabel.textColor = mainColors.online
            } else {
                if convers?.userInfo?.deactivated == "banned" || convers?.userInfo?.deactivated == "deleted" { subtitleLabel.text = convers?.userInfo?.deactivated }
                else { subtitleLabel.text = Helper.getLastSeenStr(lastSeen: TimeInterval(convers?.userInfo?.last_seen_time ?? 0)) }
            }
        }
        
        subtitleLabel.sizeToFit()
        subtitleWidth = subtitleLabel.frame.width
        
        //////// mobile
        
        let isMobile = (convers?.userInfo?.online_mobile ?? false) == true || ((convers?.userInfo?.last_seen_platform ?? 7) != 7 && (convers?.userInfo?.last_seen_platform ?? 0) != 0)
        
        if isUserTyping == true {
            let imgArr = [mainIcons.userTyping_1!, mainIcons.userTyping_2!, mainIcons.userTyping_3!, mainIcons.userTyping_4!, mainIcons.userTyping_5!, mainIcons.userTyping_6!]
            let animImg = UIImage.animatedImage(with: imgArr, duration: 0.8)
            mobileImgView = UIImageView(frame: CGRect(x: subtitleWidth + 2, y: 22, width: 21, height: 7))
            mobileImgView.image = animImg
            subtitleWidth += 23
        }
        else if isMobile == true {
            mobileImgView = UIImageView(frame: CGRect(x: subtitleWidth + 2, y: 18, width: 12, height: 12))
            mobileImgView.image = mainIcons.userPhoneOnline
            mobileImgView.tintColor = convers?.userInfo?.online == false ? mainColors.subTextBar : mainColors.online
            subtitleWidth += 14
        }
        
        /////// titleView
        
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: max(titleWidth, subtitleWidth), height: 30))
        titleView.addSubview(titleLabel)
        titleView.addSubview(subtitleLabel)
        
        if convers?.push_settings_no_sound == true { titleView.addSubview(soundImgView) }
        if isMobile == true || isUserTyping == true { titleView.addSubview(mobileImgView) }
        
        let widthDiff = subtitleWidth - titleWidth
        let newX = widthDiff / 2
        
        if widthDiff < 0 {
            subtitleLabel.frame.origin.x = abs(newX)
            mobileImgView.frame.origin.x = subtitleLabel.frame.origin.x + subtitleLabel.frame.width + 2
        } else {
            titleLabel.frame.origin.x = newX
            soundImgView.frame.origin.x = titleLabel.frame.origin.x + titleLabel.frame.width + 10
        }
        
        return titleView
    }
    
    func initTableView() {
        self.tableNode.inverted = true
        self.tableNode.view.separatorStyle = .none
        //self.tableNode.view.refreshControl = self.refreshControl
        self.tableNode.delegate = self
        self.tableNode.dataSource = self
        self.tableNode.backgroundColor = mainColors.mainTableView
        self.tableNode.leadingScreensForBatching = 4
        //self.tableNode.allowsMultipleSelectionDuringEditing = true
        
        
        self.msgInputCntrView.backgroundColor = mainColors.mainNavigationBarLight
        self.view.backgroundColor = mainColors.mainNavigationBarLight
        inputStickerView.backgroundColor = mainColors.messageInputNode
        
        self.viewForTableNode.addSubnode(tableNode)
        
        addAttachmensBtn.setImage(mainIcons.addUser_s25, for: .normal)
        addAttachmensBtn.setTitle(nil, for: .normal)
        addStickerBtn.setImage(mainIcons.sticker_s24, for: .normal)
        sendMsgBtn.setImage(mainIcons.microphone_s24, for: .normal)
        sendMsgBtn.setTitle(nil, for: .normal)
        addAttachmensBtn.tintColor = mainColors.sendMessage
        addStickerBtn.tintColor = mainColors.sendMessage
        sendMsgBtn.tintColor = mainColors.sendMessage
        
        sendMsgBtn.addTarget(self, action: #selector(recordAudio), for: .touchDown)
        sendMsgBtn.addTarget(self, action: #selector(sendMessOutSide), for: .touchUpOutside)
        
        let viewFrame = self.inputStickerView.frame
        
        inputTextNode.frame = CGRect(x: 3, y: 5, width: viewFrame.width - 40, height: 26)
        inputTextNode.cornerRadius = 13
        inputTextNode.backgroundColor = mainColors.messageInputNode
        inputTextNode.textContainerInset = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        inputTextNode.attributedPlaceholderText = NSAttributedString(string: "Type here...", attributes: [.font : UIFont.systemFont(ofSize: 15), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: mainColors.textFooter!])
        inputTextNode.typingAttributes = [NSAttributedString.Key.font.rawValue: UIFont.systemFont(ofSize: 15), NSAttributedString.Key.strokeColor.rawValue: mainColors.text!, NSAttributedString.Key.foregroundColor.rawValue: mainColors.textBar!]
        inputTextNode.style.minHeight = ASDimension(unit: .points, value: 26)
//        inputTextNode.attributedText = NSAttributedString(string: "", attributes: [.font : UIFont.systemFont(ofSize: 15), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: mainColors.textBar!])
        inputTextNode.delegate = self
        inputStickerView.addSubnode(inputTextNode)
        
        vkLoadData.loadMessHistory(peer_id: peer_id, peer_type: peer_type, conversRef: conversRef!, prtID: conversID, count: unreadCount + 40) { [weak self] (result) in
            if result == true {
                if self?.peer_type == VKUserType.isChat.rawValue {
                    vkLoadData.loadUsersIdInChats(String(self?.peer_local_id ?? 0), completion: { (result) in
                        if result == true {
                            vkDataCache.saveUserInfoInCache(vkLoadData.usersInfo, completion: { (result) in
                                if result == 1 {
                                    vkLoadData.usersInfo.removeAll()
                                    DispatchQueue.main.async {
                                        self?.updateValueInTable(40, true)
                                        self?.scrollToUnread()
                                    }
                                }
                            })
                        }
                    })
                } else {
                    DispatchQueue.main.async {
                        self?.updateValueInTable(40, true)
                        self?.scrollToUnread()
                    }
                }
            } else if result == false { DispatchQueue.main.async { self?.updateValueInTable(40, true) } }
        }
    }
    
    func scrollToUnread() {
        if unreadCount != 0 {
            tableNode.scrollToRow(at: IndexPath(row: (messagesDiff[0].elements.count) - 1, section: 0), at: .middle, animated: false)
        }
    }
    
    @objc func reloadInfoInMessageTV(notification: NSNotification) {
        let userInfo = notification.userInfo ?? [:]
        
        if userInfo.count > 0 {
            let user_id: Int64 = userInfo["user_id"] as! Int64
            let type: Int64 = userInfo["type"] as! Int64
            
            if user_id != peer_id { return }
            
            if type == 1 || type == 2 || type == 3 {
                usleep(100000)
                let msg_id: Int64 = userInfo["msg_id"] as! Int64
                DispatchQueue.main.async {
                    self.updateMsgDictAndTableById(msg_id: msg_id, type: type)
                }
            }
            else if type == 4 || type == 13 {
                DispatchQueue.main.async {
                    vkDataCache.bgRealm?.refresh()
                    
                    if type == 13 {
                        self.navigationController?.popViewController(animated: true)
                        //self.updateValueInTable(0, true)
                        return
                    }
                
                    let visRows = self.tableNode.indexPathsForVisibleRows()
                    
                    for visInd in visRows {
                        if visInd.row == 0 && visInd.section == 0 {
                            self.in_read = userInfo["msg_id"] as! Int64
                        }
                    }
                    self.isUserTyping = false
                    //self.updateValueInTable(self.count + 1, true)
                    self.updateMsgDictAndTableById(msg_id: userInfo["msg_id"] as! Int64, type: type)
                }
            }
            else if type == 5 {
                DispatchQueue.main.async {
                    self.updateRowByMsgId(userInfo["msg_id"] as! Int64)
                }
            }
            else if type == 6 || type == 7 {
                DispatchQueue.main.async {
                    let msg_id = userInfo["msg_id"] as! Int64
                    if type == 6 {
                        self.in_read = msg_id
                        self.updateValueInTable(self.count, true)
                    }
                    else {
                        var indexArr: [IndexPath] = []
                        let count = self.messages.count
                        for i in 0..<count {
                            if self.messages[i].id <= msg_id && self.messages[i].id > self.out_read {
                                if let index = self.getIndexPathByMsgId(self.messages[i].id) {
                                    indexArr.append(index)
                                }
                            }
                            else if self.messages[i].id <= self.out_read {break}
                        }
                        self.out_read = msg_id
                        if indexArr.count > 0 { self.tableNode.reloadRows(at: indexArr, with: .none) }
                    }
                }
            }
            else if type == 52 || type == 114 {
                DispatchQueue.main.async {
                    self.navigationItem.titleView = nil
                    self.navigationItem.titleView = self.setTitle()
                }
            }
            else if type == 8 || type == 9 {
                if type == 8 {
                    self.isUserOnline = true
                    DispatchQueue.main.async {
                        self.navigationItem.titleView = nil
                        self.navigationItem.titleView = self.setTitle()
                    }
                } else {
                    self.isUserOnline = false
                    self.updateUserStatus()
                }
            }
            else if type == 61 || type == 62 {
//                var writingUserName = ""
//
//                if type == 62 {
//                    writingUserName = userInfo["writingUserName"] as! String
//                    writingUserName += " is typing"
//                }
//                else { writingUserName = "typing" }
                self.showUsetTyping()
            }
        }
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        if !isStatusBarWasHidden {
            self.tableNode.frame = self.viewForTableNode.bounds
            isStatusBarWasHidden = UIApplication.shared.isStatusBarHidden ? true : false
        } else {
            isStatusBarWasHidden = UIApplication.shared.isStatusBarHidden ? true : false
        }
    }
    
    func showUsetTyping() {
        userStatusBGQueue?.async {
            self.isUserTyping = true
            DispatchQueue.main.async {
                self.navigationItem.titleView = nil
                self.navigationItem.titleView = self.setTitle()
            }
            let time = Date().timeIntervalSince1970
            while self.isUserTyping {
                usleep(300000)
                if (time + 5) < Date().timeIntervalSince1970 {
                    self.isUserTyping = false
                    break
                }
            }
            DispatchQueue.main.async {
                self.navigationItem.titleView = nil
                self.navigationItem.titleView = self.setTitle()
            }
        }
    }
    
    func updateUserStatus() {
        userStatusBGQueue?.async {
            DispatchQueue.main.async {
                self.navigationItem.titleView = nil
                self.navigationItem.titleView = self.setTitle()
            }
            while !self.isUserOnline {
                sleep(20)
                DispatchQueue.main.async {
                    self.navigationItem.titleView = nil
                    self.navigationItem.titleView = self.setTitle()
                }
            }
        }
    }
    
    func updateRowByMsgId(_ msg_id: Int64) {
        if let index = getIndexPathByMsgId(msg_id) { tableNode.reloadRows(at: [index], with: .none) }
    }
    
    func getIndexPathByMsgId(_ msg_id: Int64) -> IndexPath? {
        if let index = messages.firstIndex(where: { (mes) -> Bool in
            return mes.id == msg_id
        }) {
            let date = Helper.getDateForMsgKey(date: messages[index].date)
            let curInd = messSectionDate.firstIndex(of: date)!
            if let dictIndex = self.messagesInput.firstIndex(where: { (elem) -> Bool in
                return elem.model.sectionTitle == messSectionDateString[curInd]
            }) {
                if let mesInd = self.messagesInput[dictIndex].elements.firstIndex(where: { (mes) -> Bool in
                    return mes.id == msg_id
                }) {
                    return IndexPath(row: mesInd, section: dictIndex)
                }
            }
        }
        return nil
    }
    
    func updateMsgDictAndTableById(msg_id: Int64, type: Int64) {
        if type == 2 {
            if let index = messages.firstIndex(where: { (mes) -> Bool in
                return mes.id == msg_id
            }) {
                let date = Helper.getDateForMsgKey(date: messages[index].date)
                let curInd = messSectionDate.firstIndex(of: date)!
                if let dictIndex = self.messagesInput.firstIndex(where: { (elem) -> Bool in
                    return elem.model.sectionTitle == messSectionDateString[curInd]
                }) {
                    if let mesInd = self.messagesInput[dictIndex].elements.firstIndex(where: { (mes) -> Bool in
                        return mes.id == msg_id
                    }) {
                        var deleted = false
                        if messagesInput[dictIndex].elements[mesInd].isDelete {
                            messagesInput[dictIndex].elements.remove(at: mesInd)
                            messages.remove(at: index)
                            self.count -= 1
                            deleted = true
                        }
                        
                        if deleted {
                            if messagesInput[dictIndex].elements.count == 0 {
                                messSectionDate.remove(at: dictIndex)
                                messSectionDateString.remove(at: dictIndex)
                                messagesInput.remove(at: dictIndex)
                            } else if messagesInput[dictIndex].elements.count > mesInd {
                                tableNode.reloadRows(at: [IndexPath(row: mesInd, section: dictIndex)], with: .none)
                            }
                        } else {
                            tableNode.reloadRows(at: [IndexPath(row: mesInd, section: dictIndex)], with: .none)
                        }
                    }
                }
            }
        }
        else {
            vkDataCache.bgRealm?.refresh()
            if let msg = vkDataCache.bgRealm?.objects(VKMessage.self).filter("id = %@ AND isPreview = %@", msg_id, false).first {
                
                if let indexPath = getIndexPathByMsgId(msg.id) {
                    tableNode.reloadRows(at: [indexPath], with: .fade)
                } else {
                    self.count += 1
                    self.messages.insert(msg, at: 0)
                    self.messages.sort { (mes1, mes2) -> Bool in
                        return mes1.date > mes2.date
                    }
                    
                    var msgDict = self.messagesInput
                    var curInd: Int = messSectionDateString.count - 1
                    var date = Helper.getDateForMsgKey(date: msg.date)
                    if msg.id > in_read && msg.from_id != vkLoadData.mainUserID { date *= -1 }
                    if messSectionDate.contains(date) == false {
                        messSectionDate.append(date)
                        messSectionDate = messSectionDate.sorted(by: { $0 < $1 })
                        
                        curInd = messSectionDate.firstIndex(of: date)!
                        messSectionDateString.insert(Helper.getDateForMsgSectionTitle(date: date), at: curInd)
                    } else { curInd = messSectionDate.firstIndex(of: date)! }
                    
                    var index = msgDict.firstIndex { (elem) -> Bool in
                        return elem.model.sectionTitle == messSectionDateString[curInd]
                    }
                    
                    if index != nil {
                        if msgDict[index!].elements.contains(where: { (mess) -> Bool in
                            mess.id == msg.id && mess.conversation_message_id == msg.conversation_message_id
                        }) == false { msgDict[index!].elements.append(msg) }
                    } else {
                        msgDict.append(MessageSection(model: MessageSectionStruct(sectionTitle: messSectionDateString[curInd], messages: [msg]), elements: [msg]))
                    }
                    
                    if index == nil {
                        index = msgDict.firstIndex { (elem) -> Bool in
                            return elem.model.sectionTitle == messSectionDateString[curInd]
                        }
                    }
                    
                    msgDict[index!].elements.sort { (mes1, mes2) -> Bool in
                        if mes1.date == mes2.date { return mes1.conversation_message_id > mes2.conversation_message_id }
                        return mes1.date > mes2.date
                    }
                    
                    msgDict.sort { (sec1, sec2) -> Bool in
                        return sec1.elements[0].date > sec2.elements[0].date
                    }
                    
                    messagesInput = msgDict

                    var indexArr: [IndexPath] = []
                    if let indexPathMid = getIndexPathByMsgId(msg.id) {
                        if msgDict[indexPathMid.section].elements.count == 1 { return }
                        indexArr.append(indexPathMid)
                        if msgDict[indexPathMid.section].elements.count - 1 > indexPathMid.row {
                            indexArr.append(IndexPath(row: indexPathMid.row + 1, section: indexPathMid.section))
                        }
                        if indexPathMid.row != 0 {
                            indexArr.append(IndexPath(row: indexPathMid.row - 1, section: indexPathMid.section))
                        }
                        tableNode.reloadRows(at: indexArr, with: .none)
                    }
                }
                
            }
        }
    }
    
    func updateValueInArrAndDict(msg: VKMessage) {
        if let index = messages.firstIndex(where: { (mes) -> Bool in
            return mes.messageID == msg.messageID
        }) {
            messages[index] = msg
            let date = Helper.getDateForMsgKey(date: messages[index].date)
            let curInd = messSectionDate.firstIndex(of: date)!
            if let dictIndex = self.messagesInput.firstIndex(where: { (elem) -> Bool in
                return elem.model.sectionTitle == messSectionDateString[curInd]
            }) {
                if let mesInd = self.messagesInput[dictIndex].elements.firstIndex(where: { (mes) -> Bool in
                    return mes.messageID == msg.messageID
                }) {
                    messagesInput[dictIndex].elements[mesInd] = msg
                }
            }
        }
    }
    
    func removeDeletedMsg() {
        if let deletedMsgs = convers?.messages.filter("isDelete = %@", true) {
            do {
                let realm = try Realm()
                for msg in deletedMsgs { try realm.write { realm.delete(msg) } }
            } catch { print("Ooops! Something went wrong: \(error)") }
        }
    }
    
    func updateValueInTable(_ getCount: Int, _ reloadAllMsgs: Bool = false) {
        self.getMsg(getCount, reloadAllMsgs)
        self.count = self.messages.count
        self.configMessDict()
    }
    
    func addNewMessage(_ msgID: Int64) {
        vkDataCache.bgRealm?.refresh()
        if let msg = vkDataCache.bgRealm?.objects(VKMessage.self).filter("id = %@", msgID).first {
            
            self.messages.insert(msg, at: 0)
            self.in_read = msgID
            
            var msgDict = self.messagesInput
            var curInd: Int = messSectionDateString.count - 1
            
            let date = Helper.getDateForMsgKey(date: msg.date)

            if messSectionDate.contains(date) == false {
                messSectionDate.append(date)
                messSectionDate = messSectionDate.sorted(by: { $0 < $1 })
                
                curInd = messSectionDate.firstIndex(of: date)!
                messSectionDateString.insert(Helper.getDateForMsgSectionTitle(date: date), at: curInd)
                
            } else { curInd = messSectionDate.firstIndex(of: date)! }
            
            
            let index = msgDict.firstIndex { (elem) -> Bool in
                return elem.model.sectionTitle == messSectionDateString[curInd]
            }
            
            if index != nil {
                    msgDict[index!].elements.insert(msg, at: 0)
            } else {
                msgDict.append(MessageSection(model: MessageSectionStruct(sectionTitle: messSectionDateString[curInd], messages: [msg]), elements: [msg]))
            }
            
            messagesInput = msgDict
        }
    }
    
    func updateAllMsgs() {
        vkDataCache.bgRealm?.refresh()
        self.convers = vkDataCache.bgRealm?.objects(VKConversation.self).filter("peer_id = %@", self.peer_id).first
        self.in_read = self.convers?.in_read ?? 2_000_000_000
        allMsgs = self.convers?.messages.sorted(byKeyPath: "date", ascending: false)
        allMsgs = allMsgs?.filter("isDelete = %@", false)
    }
    
    func getMsgByID(_ id: Int64) -> VKMessage {
        for msg in messages {
            if msg.id == id {
                return msg
            }
        }
        return VKMessage()
    }

    func getMsg(_ cnt : Int, _ reloadAllMsgs: Bool) {
        if self.convers == nil || (self.convers?.messages.count ?? 0) < cnt { self.count = 0 }
        if allMsgs == nil || reloadAllMsgs == true {
            updateAllMsgs()
            if reloadAllMsgs == true { messages.removeAll() }
        }
        let msgCnt = messages.count
        
        for i in msgCnt..<cnt {
            if (allMsgs?.count ?? 0) <= i { return }
            messages.append(allMsgs![i])
        }
    }
    
    func configMessDict() {
        var messagesDiffVar = messagesInput
        var curInd: Int = messSectionDateString.count - 1
        
        for msg in messages {
            var date = Helper.getDateForMsgKey(date: msg.date)
            if msg.id > in_read && msg.from_id != vkLoadData.mainUserID { date *= -1 }
            if messSectionDate.contains(date) == false {
                messSectionDate.append(date)
                messSectionDate = messSectionDate.sorted(by: { $0 < $1 })
                
                curInd = messSectionDate.firstIndex(of: date)!
                messSectionDateString.insert(Helper.getDateForMsgSectionTitle(date: date), at: curInd)
                
            } else { curInd = messSectionDate.firstIndex(of: date)! }
            
            
            let index = messagesDiffVar.firstIndex { (elem) -> Bool in
                return elem.model.sectionTitle == messSectionDateString[curInd]
            }
            
            if index != nil {
                if messagesDiffVar[index!].elements.contains(where: { (mess) -> Bool in
                    mess.id == msg.id && mess.conversation_message_id == msg.conversation_message_id
                }) == false { messagesDiffVar[index!].elements.append(msg) }
            } else {
                messagesDiffVar.append(MessageSection(model: MessageSectionStruct(sectionTitle: messSectionDateString[curInd], messages: [msg]), elements: [msg]))
            }
            
        }
        
        messagesDiffVar.sort { (sec1, sec2) -> Bool in
            return sec1.elements[0].date > sec2.elements[0].date
        }
        
        for i in 0..<messagesDiffVar.count {
            //print("\(i) - \(messagesDiffVar[i].elements.count)")
            messagesDiffVar[i].elements.sort { (mes1, mes2) -> Bool in
                if mes1.date == mes2.date { return mes1.conversation_message_id > mes2.conversation_message_id }
                return mes1.date > mes2.date
            }
        }
        
        messagesInput = messagesDiffVar
    }
    
    //MARK: Send message
    
    @objc func sendMessOutSide() {
        print("record outSide")
        sendMsgAction(Int())
    }
    
    @IBAction func sendMsgAction(_ sender: Any) {
        if isRecordBtn == false {
            sendMessageRecursion(attachmentsShort: attachmentNode?.attachments ?? [], text: inputTextNode.textView.text)
            inputTextNode.textView.text.removeAll()
        }
        else {
            print("record up")
            audioRecorder?.stop()
            isRecording = false
            
            // Get data from image file by path
            do {
                let data = try Data(contentsOf: getAudioFileUrl())
                
                VK.API.Upload.toAudioMessage(Media.document(data: data, type: "audio_message"))
                    .onSuccess { (data) in
                        let json = JSON(data)
                        print(json)
                        
                        let msg = VKMessage()
                        msg.date = Int64(Date().timeIntervalSince1970)
                        msg.random_id = Int64(arc4random_uniform(3000000000))
                        msg.peer_id = self.peer_id
                        msg.from_id = vkLoadData.mainUserID
                        msg.prntID = self.conversID
                        msg.conversation_message_id = self.count > 0 ? (self.messages[0].conversation_message_id + 1) : -1
                        
                        let att = VKAttachments()
                        att.type = "audio_message"
                        att.prntID = msg.messageID
                        att.audio_msg = VKAudioMsg(json: json[0], prnt_id: msg.messageID)
                        
                        msg.attachments.append(att)
                        
                        vkDataCache.addNewMsgInCache(msg) { [weak self] (result) in
                            if result == true {
                                DispatchQueue.main.async {
                                    self?.updateValueInTable((self?.count ?? 0) + 1, true)
                                    
                                    let str = "doc" + String(vkLoadData.mainUserID) + "_" + String(json[0]["id"].int64 ?? 0)
                                    VK.API.Messages.send([.userId : String(self?.peer_id ?? 0), .attachment : str])
                                        .onSuccess{ _ in
                                        }.onError{
                                            print("VKMessageViewController - sendMsg - Request failed with error: \($0)")
                                        }.send()
                                }
                            }
                        }
                    }.onError { (error) in
                        print(error)
                    }.send()
                
                let fileManager = FileManager.default
                
                do { try fileManager.removeItem(atPath: getAudioFileUrl().absoluteString) }
                catch let error as NSError {  print("Ooops! Something went wrong: \(error)") }
            } catch {
                print("Ooops! Something went wrong: \(error)")
            }
        }
    }
    
    func sendMessageRecursion(attachmentsShort: [VKAttachmentShort], text: String, _ count_to_update: Int = 0) {
        if attachmentsShort.count == 0 && text == "" && replyForwardMsgID == 0 {
//            if isEditMode {
//                isEditMode = false
//                return
//            }
            updateValueInTable(count + count_to_update, true)
            adjustUITextViewHeight(textView: inputTextNode.textView)
            return
        }
        var attShortArr = attachmentsShort
        let convers_id: Int64 = messages.count > 0 ? ((messages.last?.conversation_message_id ?? 0) + Int64(count_to_update + 1)) : -1
        var msg = VKMessage()
        msg.date = Int64(Date().timeIntervalSince1970)
        msg.random_id = Int64(arc4random_uniform(3000000000))
        msg.peer_id = peer_id
        msg.from_id = vkLoadData.mainUserID
        msg.prntID = conversID
        msg.conversation_message_id = convers_id
        msg.text = text
        
        for i in 0..<attachmentsShort.count { if (attachmentsShort[i].vk_id.count == 0 && attachmentsShort[i].type != "geo") { return } }
        
        var param: SwiftyVK.Parameters = [:]
        param[Parameter.message] = msg.text
        param[Parameter.peerId] = String(peer_id)
        
        if replyForwardMsgID != 0 {
            //param[.replyTo] = String(replyForwardMsgID)
            param[.forwardMessages] = String(replyForwardMsgID)
            
            let realm = try! Realm()
            if let msgCh = realm.objects(VKMessage.self).filter("id = %@", replyForwardMsgID).first {
                let fwdMsg = VKMessage()
                fwdMsg.copyValue(msgCh)
                msg.fwd_messages.append(fwdMsg)
            }

            hideReplyForwardView()
        }
        
        for i in 0..<attShortArr.count {
            if attShortArr[i].type == "geo" {
                param[Parameter.lat] = String(attShortArr[i].latitude)
                param[Parameter.long] = String(attShortArr[i].longitude)
                let geo = VKGeo()
                geo.coordinates_latitude = Float(attShortArr[i].latitude)
                geo.coordinates_longitude = Float(attShortArr[i].longitude)
                msg.geo = geo
                attShortArr.remove(at: i)
                break
            }
        }
        
        var attArr: [VKAttachmentShort] = []
        let endIndex = attShortArr.count > 10 ? 10 : attShortArr.count
        for i in 0..<endIndex { attArr.append(attShortArr[i]) }
        for _ in 0..<endIndex { attShortArr.remove(at: 0) }
        
        param[Parameter.attachment] = getAttachmentStr(&msg, attArr)
        
        if attShortArr.count == 0 { showAttNode(false) }
        
        if isEditMode {
            msg.id = editMsgID
            editMsgID = 0
            isEditMode = false
            param[.messageId] = String(msg.id)
            
            vkDataCache.updateMsg(msg) { [weak self] (result) in
                if result == true {
                    VK.API.Messages.edit(param)
                        .onSuccess{ _ in
                            DispatchQueue.main.async {
                                self?.updateRowByMsgId(msg.id)
                            }
                        }.onError{
                            print("VKMessageViewController - sendMessageRecursion - edit - Request failed with error: \($0)")
                        }.send()
                    
                    DispatchQueue.main.async {
                        self?.sendMessageRecursion(attachmentsShort: attShortArr, text: "", count_to_update + 1)
                    }
                }
            }
        } else {
            vkDataCache.addNewMsgInCache(msg) { [weak self] (result) in
                if result == true {
                    VK.API.Messages.send(param)
                        .onSuccess{
                            let json = JSON($0)
                            let id = json["response"].int64 ?? 0
                            msg.id = id
                            let realm = try! Realm()
                            do {
                                try realm.write {realm.create(VKMessage.self, value: msg, update: true)}
                                usleep(200_000)
                                DispatchQueue.main.async { self?.updateRowByMsgId(id) }
                            } catch let error as NSError {
                                print("VKMessageViewController - sendMessageRecursion - update msg failed - \(error)")
                            }
                            print(id)
                        }.onError{
                            print("VKMessageViewController - sendMessageRecursion - Request failed with error: \($0)")
                        }.send()
                    
                    DispatchQueue.main.async {
                        self?.sendMessageRecursion(attachmentsShort: attShortArr, text: "", count_to_update + 1)
                    }
                }
            }
        }
        
        
    }
    
    func getAttachmentStr(_ msg: inout VKMessage, _ attachmentsArr: [VKAttachmentShort]) -> String {
        var str: String = ""
        for att in attachmentsArr {
            if att.vk_id != "" {
                let vkAtt = VKAttachments()
                vkAtt.type = att.type
                
                switch att.type {
                case "photo":
                    vkAtt.photo = att.photo!
                    break
                case "video":
                    let video = VKVideo()
                    video.duration = att.duration
                    video.id = Int64(att.vk_id.split(separator: "_")[1]) ?? 0
                    video.owner_id = vkLoadData.mainUserID
                    vkAtt.video = video
                    break
                case "doc":
                    vkAtt.doc = att.doc
                    break
                case "audio":
                    vkAtt.audio = att.audio
                    break
                default:
                    break
                }
                str.append("\(att.type)\(att.vk_id),")
                
                msg.attachments.append(vkAtt)
            }
        }
        
        
        
        return str
    }
}

extension VKMessageViewController: VKPhotosVideosAlbumAttDelegate, VKDocumentsVCDelegate {
    //MARK: Add attachment
    
    func addBadgeToAttBtn() {
        var size: CGFloat = 13
        var width: CGFloat = 20
        var x: CGFloat = 20
        let attCount = attachments.count
        
        if attCount == 0 {
            attachmentBadgeLbl?.removeFromSuperview()
            attachmentBadgeLbl = nil
            return
        }
        
        if attCount > 9 && attCount < 100 { size = 11 }
        else if attCount >= 100 {
            size = 9
            width = 25
            x = 15
        }
        
        if attachmentBadgeLbl == nil {
            attachmentBadgeLbl = UILabel(frame: CGRect(x: x, y: -5, width: width, height: 20))
            attachmentBadgeLbl!.layer.borderColor = UIColor.clear.cgColor
            attachmentBadgeLbl!.layer.borderWidth = 0
            attachmentBadgeLbl!.layer.cornerRadius = attachmentBadgeLbl!.bounds.size.height / 2
            attachmentBadgeLbl!.textAlignment = .center
            attachmentBadgeLbl!.layer.masksToBounds = true
            
            attachmentBadgeLbl!.font = UIFont.systemFont(ofSize: size)
            attachmentBadgeLbl!.textColor = .white
            attachmentBadgeLbl!.backgroundColor = mainColors.deleteRow
            attachmentBadgeLbl!.text = String(attCount)
            attachmentBadgeLbl!.isUserInteractionEnabled = false
            addAttachmensBtn.addSubview(attachmentBadgeLbl!)
        } else {
            attachmentBadgeLbl!.font = UIFont.systemFont(ofSize: size)
            attachmentBadgeLbl!.text = String(attCount)
            attachmentBadgeLbl?.frame = CGRect(x: x, y: -5, width: width, height: 20)
        }
    }
    
    @IBAction func addAttachment(_ sender: Any) {
        
//        if (attachmentNode?.attachments.count ?? 0) >= 10 {
//            ToastView.shared.long(self.view, txt_msg: "You can't add more 10 att")
//            return
//        }
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet, blurStyle: .dark)
        
        alert.addAction(UIAlertAction(title: "Photo or video", style: .default, handler: { [weak self] (action) in
            let gallery = GalleryController()
            gallery.delegate = self
            
            
            //Gallery.Config.Camera.imageLimit = 10 - (self?.attachmentNode?.attachments.count ?? 0)
            
            Gallery.Config.Font.Text.bold = UIFont.systemFont(ofSize: 14)
            Gallery.Config.Camera.recordLocation = true
            Gallery.Config.tabsToShow = [.cameraTab, .imageTab, .videoTab]
            Gallery.Config.initialTab = .imageTab
            Gallery.Config.VideoEditor.maximumDuration = 60 * 60 * 24
            Gallery.Config.Grid.FrameView.borderColor = UIColor(red: 143/255, green: 155/255, blue: 224/255, alpha: 1)
            
            self?.present(gallery, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Photo (VK)", style: .default, handler: { [weak self] (action) in
            let vkAlbumVC = VKAlbumsVC(nibName: "VKAlbumsVC", bundle: nil)
            vkAlbumVC.delegate = self
            self?.navigationController!.pushViewController(vkAlbumVC, animated: true)
        }))
        alert.addAction(UIAlertAction(title: "Video (VK)", style: .default, handler: { [weak self] (action) in
            let vkVideoAlbumVC = VKVideoAlbumsVC(nibName: "VKVideoAlbumsVC", bundle: nil)
            vkVideoAlbumVC.delegate = self
            self?.navigationController!.pushViewController(vkVideoAlbumVC, animated: true)
        }))
        alert.addAction(UIAlertAction(title: "Location", style: .default, handler: { [weak self] (action) in
            self?.navigationController!.pushViewController(AddLocationVC(nibName: "AddLocationVC", bundle: nil), animated: true)
        }))
        alert.addAction(UIAlertAction(title: "Document (VK)", style: .default, handler: { [weak self] (action) in
            let vkDocVC = VKDocumentsVC(nibName: "VKDocumentsVC", bundle: nil)
            vkDocVC.delegate = self
            self?.navigationController!.pushViewController(vkDocVC, animated: true)
        }))

        //        alert.addAction(UIAlertAction(title: "Graffiti", style: .default, handler: { [weak self] (action) in
        //
        //        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func insertImageToAttArray(_ images: [UIImage?], _ sourceImg: [Image]) {
        var addNew: Bool = false
        for image in images {
            let elem = VKAttachmentShort()
            elem.image = image
            elem.type = "photo"
            elem.file_name = sourceImg[images.firstIndex(of: image) ?? 0].asset.originalFilename ?? ""
            elem.setImageType()
            elem.phAssest = sourceImg[images.firstIndex(of: image) ?? 0].asset
            
            if !self.attachments.contains(where: { (att) -> Bool in
                att.image?.cgImage == image?.cgImage
            }) {
                self.attachments.append(elem)
                addNew = true
            }
        }
        if addNew == true { showAttNode() }
    }
    
    func insertVideoToAttArray(_ video: Video) {
        video.fetchThumbnail(size: CGSize(width: 120, height: 120)) { (image) in
            if (image?.size.width ?? 119.0) < 120.0 { return }
            let elem = VKAttachmentShort()
            elem.image = image
            elem.type = "video"
            elem.duration = Int64(video.asset.duration)
            elem.phAssest = video.asset
            
            if !self.attachments.contains(where: { (att) -> Bool in att.image == image }) {
                self.attachments.append(elem)
                self.showAttNode()
            }
        }
    }
    
    func showAttNode(_ isShow: Bool = true) {
        if isShow {
            if self.attViewHeight == 0 {
                self.isHaveAttachments = true
                self.attViewHeight = 75
                
                self.textInputTop.constant += self.attViewHeight
                self.textViewParentHeight.constant += self.attViewHeight
                
                let viewFrame = self.viewForTableNode.frame
                
                self.viewForTableNode.frame = CGRect(x: viewFrame.origin.x, y: viewFrame.origin.y, width: viewFrame.width, height: viewFrame.height - self.attViewHeight)
                
                self.attachmentNode = VKAttachmentNode(att: attachments, contentWidth: 80)
                self.attachmentNode?.frame = CGRect(x: 5, y: 3 + self.replyForwardViewHeight, width: viewFrame.width - 10, height: 70)
                self.msgInputCntrView.addSubnode(self.attachmentNode!)
                
                if isRecordBtn == true {
                    UIView.transition(with: self.sendMsgBtn! as UIView, duration: 0.1, options: .transitionFlipFromBottom, animations: {
                        self.sendMsgBtn.setImage(mainIcons.send_s24, for: .normal)
                    }, completion: nil)
                }
                self.isRecordBtn = false
                
                UIView.animate(withDuration: 0.25) {
                    self.view.setNeedsLayout()
                }
            }
            else {
                self.attachmentNode?.insertNewAtt(self.attachments)
            }
        } else {
            attachments.removeAll()
            attachmentNode?.attachments.removeAll()
            self.isHaveAttachments = false
            
            self.textInputTop.constant -= self.attViewHeight
            self.textViewParentHeight.constant -= self.attViewHeight
            
            let viewFrame = self.viewForTableNode.frame
            
            self.viewForTableNode.frame = CGRect(x: viewFrame.origin.x, y: viewFrame.origin.y, width: viewFrame.width, height: viewFrame.height + self.attViewHeight)
            
            self.attachmentNode?.removeFromSupernode()
            self.attachmentNode = nil
            self.attViewHeight = 0
            
            UIView.animate(withDuration: 0.25) {
                self.view.setNeedsLayout()
            }
        }
        addBadgeToAttBtn()
    }
    
    @objc func hideAttNode(notification: NSNotification) {
        let info = notification.userInfo ?? [:]
        if info.count > 0 {
            let count = info["count"] as! Int
            if count == 0 { showAttNode(false) }
            else {
                attachments = attachmentNode?.attachments ?? []
                addBadgeToAttBtn()
            }
        }
    }
    
    @objc func addLocation(notification: NSNotification) {
        let locationInfo = notification.userInfo ?? [:]
        if locationInfo.count > 0 {
            let lat: Double = locationInfo["lat"] as! Double
            let long: Double = locationInfo["long"] as! Double
            
            let elem = VKAttachmentShort()
            elem.type = "geo"
            elem.latitude = lat
            elem.longitude = long
            
            let index = self.attachmentNode?.attachments.firstIndex(where: { (att) -> Bool in
                att.type == "geo"
            })
            if index != nil { self.attachmentNode?.deleteAtt(self.attachmentNode?.attachments[index!].attID ?? "") }
            
            self.attachments.append(elem)
            self.showAttNode()
        }
        self.view.layoutIfNeeded()
    }
    
    func addPhotosAtt(photos: [VKPhoto]) {
        for photo in photos {
            let elem = VKAttachmentShort()
            elem.type = "photo"
            elem.vk_id = "\(photo.owner_id)_\(photo.id)"
            elem.photo = photo
            if let image = vkDataCache.getDataFromCachedData(sourceID: photo.id, ownerID: photo.owner_id, type: "image", tag: "photo") {
                elem.image = UIImage(data: image as Data)
            }
            self.attachments.append(elem)
        }
        self.showAttNode()
    }
    
    func addVideosAtt(videos: [VKVideo]) {
        for video in videos {
            let elem = VKAttachmentShort()
            elem.type = "video"
            elem.duration = video.duration
            elem.vk_id = "\(video.owner_id)_\(video.id)"
            if let image = vkDataCache.getDataFromCachedData(sourceID: video.id, ownerID: video.owner_id, type: "image", tag: "video") {
                elem.image = UIImage(data: image as Data)
            }
            self.attachments.append(elem)
        }
        self.showAttNode()
    }
    
    func addPhotoDocument(_ images: [UIImage?], _ sourceImg: [Image]) {
        for image in images {
            let elem = VKAttachmentShort()
            elem.image = image
            elem.type = "doc"
            elem.file_name = sourceImg[images.firstIndex(of: image) ?? 0].asset.originalFilename ?? ""
            elem.setImageType()
            elem.phAssest = sourceImg[images.firstIndex(of: image) ?? 0].asset
            self.attachments.append(elem)
        }
        showAttNode()
    }
    
    func addVideoDocument(_ video: Video) {
        video.fetchThumbnail(size: CGSize(width: 120, height: 120)) { (image) in
            if (image?.size.width ?? 119.0) < 120.0 { return }
            let elem = VKAttachmentShort()
            elem.image = image
            elem.file_name = video.asset.originalFilename ?? ""
            elem.type = "doc"
            elem.duration = Int64(video.asset.duration)
            elem.phAssest = video.asset
            
            if !self.attachments.contains(where: { (att) -> Bool in att.image == image }) {
                self.attachments.append(elem)
                self.showAttNode()
            }
        }
    }
    
    func addVKDocumnets(_ docs: [VKDocument]) {
        for doc in docs {
            let elem = VKAttachmentShort()
            elem.type = "doc"
            elem.vk_id = "\(doc.owner_id)_\(doc.id)"
            elem.doc = doc
            if let image = vkDataCache.getDataFromCachedData(sourceID: doc.id, ownerID: doc.owner_id, type: "image", tag: "doc_image") {
                elem.image = UIImage(data: image as Data)
            }
            self.attachments.append(elem)
        }
        self.showAttNode()
    }
    
}

extension VKMessageViewController: GalleryControllerDelegate {
    //MARK: GalleryControllerDelegate
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        controller.dismiss(animated: true, completion: nil)
        
        self.insertVideoToAttArray(video)
        
//        let editor = VideoEditor()
//        editor.edit(video: video) { (editedVideo: Video?, tempPath: URL?) in
//            DispatchQueue.main.async {
//                if let tempPath = tempPath {
//                    let controller = AVPlayerViewController()
//                    controller.player = AVPlayer(url: tempPath)
//
//                    self.present(controller, animated: true, completion: nil)
//                }
//            }
//        }
    }
    
    
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        controller.dismiss(animated: true, completion: nil)
        
        Image.resolve(images: images) { (allImages) in
            self.insertImageToAttArray(allImages, images)
        }
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        controller.dismiss(animated: true, completion: nil)
        //MARK: TO_DO Create gallery for preview images
        
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension VKMessageViewController: ASEditableTextNodeDelegate {
    //MARK: Work with keyboard
    
    @objc func handleKeyboardNotification(notification: Notification) {
        if let userInfo = notification.userInfo {
            let keyboardframe = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            
            if isKeyboardShowing && tapGestureRecognizer == nil {
                tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
                view.addGestureRecognizer(tapGestureRecognizer!)
            }
            
            if emptyViewHeight == 0 && isKeyboardShowing {
                let prevValue = emptyViewHeight
                emptyViewHeight = self.view.frame.height - self.msgInputCntrView!.frame.height - self.msgInputCntrView!.frame.origin.y
                if emptyViewHeight > 100 { emptyViewHeight = prevValue }
            }
            
            self.textViewParentBottom.constant = isKeyboardShowing ? -keyboardframe.height + emptyViewHeight : 0
            let viewFrame = self.viewForTableNode.frame
            let newDiffHeight: CGFloat = keyboardframe.height - emptyViewHeight
            
            self.viewForTableNode.frame = CGRect(x: viewFrame.origin.x, y: viewFrame.origin.y, width: viewFrame.width, height: viewFrame.height + (isKeyboardShowing ? -newDiffHeight : newDiffHeight) )
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func dismissKeyboard() {
        inputTextNode.textView.endEditing(true)
        if tapGestureRecognizer != nil {
            view.removeGestureRecognizer(tapGestureRecognizer!)
            tapGestureRecognizer = nil
        }
    }
    
    func editableTextNodeDidBeginEditing(_ editableTextNode: ASEditableTextNode) { }
    
    func editableTextNodeDidUpdateText(_ editableTextNode: ASEditableTextNode) {
        self.adjustUITextViewHeight(textView: editableTextNode.textView)
    }
    
    private func adjustUITextViewHeight(textView : UITextView) {
        if textView.text.count > 0 || attachments.count > 0 {
            if isRecordBtn == true {
                UIView.transition(with: self.sendMsgBtn! as UIView, duration: 0.1, options: .transitionFlipFromBottom, animations: {
                    self.sendMsgBtn.setImage(mainIcons.send_s24, for: .normal)
                }, completion: nil)
            }
            self.isRecordBtn = false
        }
        else {
            UIView.transition(with: self.sendMsgBtn! as UIView, duration: 0.1, options: .transitionFlipFromTop, animations: {
                self.sendMsgBtn.setImage(mainIcons.microphone_s24, for: .normal)
            }, completion: nil)
            self.isRecordBtn = true
        }
        
        let fixedWidth = textView.text.count > 0 ? inputStickerView.frame.width - 6 : inputStickerView.frame.width - 40
        let oldHeight = inputTextNode.frame.height
        var newSizeHieght = inputTextNode.calculateSizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude)).height
        newSizeHieght = newSizeHieght >= 26 ? newSizeHieght : 26
        let diffHeight = newSizeHieght - oldHeight
        
        if (newSizeHieght + attViewHeight + replyForwardViewHeight) < 290 {
            inputTextNode.frame = CGRect(x: 3, y: 5, width: fixedWidth, height: newSizeHieght)
            
            self.textViewParentHeight.constant = newSizeHieght + parentAdditionalHeight + attViewHeight + replyForwardViewHeight
            let viewFrame = self.viewForTableNode.frame
            
            self.viewForTableNode.frame = CGRect(x: viewFrame.origin.x, y: viewFrame.origin.y, width: viewFrame.width, height: viewFrame.height - diffHeight)
            
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
        
        
    }
}

extension VKMessageViewController: AVAudioRecorderDelegate {
    //MARK: Record audio
    
    @objc func recordAudio() {
        if isRecordBtn == true {
            print("record down")
            AVAudioSession.sharedInstance().requestRecordPermission () {
                /*[unowned self]*/ allowed in
                if allowed {
                    // Microphone allowed, do what you like!
                } else {
                    // User denied microphone. Tell them off!
                    return
                }
            }
            //1. create the session
            let session = AVAudioSession.sharedInstance()
            do {
                // 2. configure the session for recording and playback
                try session.setCategory(AVAudioSession.Category.playAndRecord, mode: .default, options: .defaultToSpeaker)
                try session.setActive(true)
                // 3. set up a high-quality recording session
                let settings = [
                    AVFormatIDKey: kAudioFormatAppleLossless,
                    AVSampleRateKey: 16000,
                    AVEncoderBitRateKey: 16,
                    AVNumberOfChannelsKey: 1,
                    AVEncoderAudioQualityKey: AVAudioQuality.low.rawValue
                    ] as [String : Any]
                // 4. create the audio recording, and assign ourselves as the delegate
                audioRecorder = try AVAudioRecorder(url: getAudioFileUrl(), settings: settings)
                audioRecorder?.delegate = self
                audioRecorder?.record()
                
                //5. Changing record icon to stop icon
                isRecording = true
            }
            catch let error {
                print(error)
                // failed to record!
            }
        }
    }
    
    func getAudioFileUrl() -> URL{
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docsDirect = paths[0]
        let audioUrl = docsDirect.appendingPathComponent("recording.opus")
        return audioUrl
    }
}

extension VKMessageViewController: MessageCellNodeDelegate {
   
    //MARK: MessageCellNodeDelegate
    
    func openURL(_ url: String, msgStrID: String) {
        if !isSelectMode {
            let svc = SFSafariViewController(url: URL(string: url)!)
            present(svc, animated: true, completion: nil)
        } else { selectRowByMsgID(msgStrID: msgStrID) }
    }
    
    func openMap(lat: Double, long: Double, msgStrID: String) {
        if !isSelectMode {
            let source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long)))
            source.name = "Source"
            
            let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long)))
            destination.name = "Destination"
            
            MKMapItem.openMaps(with: [source, destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
        } else { selectRowByMsgID(msgStrID: msgStrID) }
    }
    
    func openGif(gifUrl: String, gifSize: Int32, videoUrl: String, videoSize: Int32, msgStrID: String) {
        if !isSelectMode {
            if videoUrl == "" {
                let gifPlayerCont = GifPlayer(nibName: "GifPlayer", bundle: nil)
                gifPlayerCont.gifUrl = URL(string: gifUrl)
                self.present(gifPlayerCont, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: "", message: "Choose format (the gif-player offers the ability to rewind in both directions)", preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: "Gif(\(ByteCountFormatter.string(fromByteCount: Int64(gifSize), countStyle: .file)))", style: .default, handler: { [weak self] (action) in
                    let gifPlayerCont = GifPlayer(nibName: "GifPlayer", bundle: nil)
                    gifPlayerCont.gifUrl = URL(string: gifUrl)
                    self?.present(gifPlayerCont, animated: true, completion: nil)
                }))
                alert.addAction(UIAlertAction(title: "MP4(\(ByteCountFormatter.string(fromByteCount: Int64(videoSize), countStyle: .file)))", style: .default, handler: { [weak self] (action) in
                    let url = URL(string: videoUrl)
                    let player = AVPlayer(url: url!)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    self?.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        } else { selectRowByMsgID(msgStrID: msgStrID) }
    }
    
    func openMessageNode(msgID: Int64) {
        if !isSelectMode {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let vkMessNode = storyboard.instantiateViewController(withIdentifier: "VKMessageFullView") as? VKMessageFullView {
                let mes = getMsgByID(msgID)
                //let mesRef = ThreadSafeReference(to: mes)
                //vkMessNode.msgRef = mesRef
                vkMessNode.mes = mes
                self.navigationController?.pushViewController(vkMessNode, animated: true)
            }
        } else { selectRowByMsgID(msgID: msgID) }
    }
    
    func openProfile(user_id: Int64) {
        if !isSelectMode {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let vkUserProfileTVC = storyboard.instantiateViewController(withIdentifier: "VKUserProfileTableViewController") as? VKUserProfileTableViewController {
                let realm = try! Realm()
                if let user = realm.objects(VKUser.self).filter("id = %@", user_id).first {
                    vkUserProfileTVC.user.copyValue(user)
                    //vkUserProfileTVC.indexPath = indexPath
                    self.navigationController?.pushViewController(vkUserProfileTVC, animated: true)
                }
                
            }
        }
    }
    
    func pressVote() {
        
    }
    
    func selectRowByMsgID(msgID: Int64 = -1, msgStrID: String = "") {
        let visibleNodes = tableNode.indexPathsForVisibleRows()
        
        for visNode in visibleNodes {
            if messagesInput[visNode.section].elements[visNode.row].id == msgID || messagesInput[visNode.section].elements[visNode.row].messageID == msgStrID {
                selectRow(indexPath: visNode)
                break
            }
        }
    }
}

extension VKMessageViewController: ASTableDelegate, ASTableDataSource {
    
    //MARK: TableNode function
    
    func numberOfSections(in tableNode: ASTableNode) -> Int {
        return messagesDiff.count
    }
    
    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
        return messagesDiff[section].elements.count
    }
    
    func tableNode(_ tableNode: ASTableNode, nodeBlockForRowAt indexPath: IndexPath) -> ASCellNodeBlock {
        let mes = messagesDiff[indexPath.section].elements[indexPath.row]
        var isNeedFooter: Bool = true
        var isHaveRect: Bool = false
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        
        let currnetDate = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(mes.date)))
        
        if indexPath.row != 0 {
            let date = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(messagesDiff[indexPath.section].elements[indexPath.row - 1].date)))
            isNeedFooter = date != currnetDate || messagesDiff[indexPath.section].elements[indexPath.row - 1].from_id != mes.from_id
        }
        
        if indexPath.row != (messagesDiff[indexPath.section].elements.count - 1) {
            let nextDate = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(messagesDiff[indexPath.section].elements[indexPath.row + 1].date)))
            isHaveRect = nextDate == currnetDate && messagesDiff[indexPath.section].elements[indexPath.row + 1].from_id == mes.from_id
        }
        
        
        let mesRef = ThreadSafeReference(to: mes)
        return {
            let node = MessageCellNode(msgRef: mesRef, tableWidth: UIScreen.main.bounds.size.width, lastOutRead: self.out_read, isNeedFooter: isNeedFooter, isNeedRect: isHaveRect, isSelectMode: self.isSelectMode, delegate: self)
            if self.isSelectMode == true { node.moveSelectImageNode(isShow: true) }
            return node
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let viewHeight: CGFloat = 60
        let isNewMess: Bool = messagesDiff[section].model.sectionTitle == "Unread message"
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: viewHeight))
        view.backgroundColor = nil
        view.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineBreakMode = .byWordWrapping
        
        let helpTextNode = ASTextNode()
        helpTextNode.attributedText = NSAttributedString(string: messagesDiff[section].model.sectionTitle, attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: mainColors.text!])
        helpTextNode.maximumNumberOfLines = 1
        
        let textWidth = helpTextNode.calculateSizeThatFits(CGSize(width: tableView.frame.width, height: 30)).width
        
        var colorForText = mainColors.outgoingSubText!
        
        if isNewMess {
            let firstColor = UIColor(red: 115.0 / 255.0, green: 149.0 / 255.0, blue: 246.0 / 255.0, alpha: 1)
            let secondColor = UIColor(red: 117.0 / 255.0, green: 175.0 / 255.0, blue: 249.0 / 255.0, alpha: 1)
            
            let gradient = CAGradientLayer()
            gradient.frame = CGRect(x: 0, y: 0, width: textWidth, height: 20)
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.colors = [firstColor.cgColor, secondColor.cgColor]
            
            colorForText = UIColor(patternImage: UIImage().image(fromLayer: gradient))
        }
        
        let titleTextNode = ASTextNode()
        titleTextNode.attributedText = NSAttributedString(string: messagesDiff[section].model.sectionTitle, attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: colorForText])
        titleTextNode.truncationMode = .byTruncatingTail
        titleTextNode.placeholderColor = mainColors.lightGray
        titleTextNode.maximumNumberOfLines = 1
        
        titleTextNode.frame = CGRect(x: 0, y: 0, width: isNewMess ? UIScreen.main.bounds.width : (textWidth + 28), height: 30)
        titleTextNode.view.transform = CGAffineTransform(rotationAngle: CGFloat(-Float.pi))
        titleTextNode.backgroundColor = mainColors.mainTableView //mainColors.darkMode == true ? mainColors.mainNavigationBarLight : nil
        titleTextNode.textContainerInset = UIEdgeInsets(top: 7, left: 14, bottom: 4, right: 14)
        titleTextNode.view.center = CGPoint(x: UIScreen.main.bounds.width / 2, y: viewHeight / 2)
        titleTextNode.cornerRadius = isNewMess ? 0 : 7
        titleTextNode.view.layer.masksToBounds = true
        
        view.addSubnode(titleTextNode)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableNode(_ tableNode: ASTableNode, didSelectRowAt indexPath: IndexPath) {
        if isSelectMode == true {
            selectRow(indexPath: indexPath)
        }
    }
    
    func tableNode(_ tableNode: ASTableNode, willBeginBatchFetchWith context: ASBatchContext) {
        DispatchQueue.main.async {
            if self.isLoading == false {
                if self.convers?.messages.count ?? 0 >= (self.count + 40) {
                    self.isLoading = true
                    self.updateValueInTable(self.count + 40)
                    self.isLoading = false
                    context.completeBatchFetching(true)
                }
                else {
                    self.isLoading = true
                    guard let conv = vkDataCache.bgRealm?.objects(VKConversation.self).filter("conversationID = %@", self.conversID).first else {return}
                    self.conversRef = ThreadSafeReference(to: (conv))
                    vkLoadData.loadMessHistory(peer_id: self.peer_id, peer_type: self.peer_type, conversRef: self.conversRef!, prtID: self.conversID, startID: self.messages.last?.id ?? 0, offset: 1) { [weak self] (result) in
                        if result == true {
                            DispatchQueue.main.async {
                                self?.updateValueInTable((self?.count ?? 0) + 40)
                                self?.isLoading = false
                                context.completeBatchFetching(true)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func shouldBatchFetch(for tableNode: ASTableNode) -> Bool {
        if self.count < vkLoadData.countMessInConv { return true }
        
        return false
    }
    
    @objc(tableNode:constrainedSizeForRowAtIndexPath:)
    func tableNode(_ tableNode: ASTableNode, constrainedSizeForRowAt indexPath: IndexPath) -> ASSizeRange {
        let min = CGSize(width: UIScreen.main.bounds.size.width, height: 1)
        let max = CGSize(width: UIScreen.main.bounds.size.width, height: CGFloat.greatestFiniteMagnitude)
        return ASSizeRange(min: min, max: max)
    }

}

extension VKMessageViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {

        let actionDelete = UIContextualAction(style: .normal, title: "Delete") { [weak self] (action, view, complition) in
            let alert = UIAlertController(title: "Delete conversation?", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (action) in

            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self?.present(alert, animated: true, completion: nil)
            complition(true)
        }
        actionDelete.image = mainIcons.trash_s20
        actionDelete.backgroundColor = mainColors.writeRow

        return UISwipeActionsConfiguration(actions: [actionDelete])
    }
}

extension VKMessageViewController: UIGestureRecognizerDelegate {
    
    @objc func handleLongPress(longPressGesture:UILongPressGestureRecognizer) {
        
        let p = longPressGesture.location(in: self.tableNode.view)
        let indexPath = self.tableNode.indexPathForRow(at: p)
        
        if indexPath == nil {
            print("Long press on table view, not row.")
        }
        else if (longPressGesture.state == UIGestureRecognizer.State.began) {
            print("Long press on section-row, at \(indexPath!.section)-\(indexPath!.row)")
            dismissKeyboard()
            selectMsgIndexPath = indexPath
            let msg = messagesInput[selectMsgIndexPath!.section].elements[selectMsgIndexPath!.row]
            let generator = UIImpactFeedbackGenerator(style: .heavy /*.medium*/)
            generator.impactOccurred()
            
            actionForward()
            return
            
            overlay = UIView(frame: UIScreen.main.bounds)
            overlay?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            UIApplication.shared.keyWindow?.addSubview(overlay!)

            actionWithMsgView = UIView(frame: CGRect.zero)
            actionWithMsgView!.backgroundColor = mainColors.mainTableView
            actionWithMsgView?.layer.cornerRadius = 11
            self.overlay?.addSubview(actionWithMsgView!)

            let viewWidth  = Int(UIScreen.main.bounds.width - 54)
            var yBtn: Int = 7
            
            let replyBtn = createBtnForActionView(y: yBtn, width: viewWidth, text: "Reply", image: mainIcons.reply_s23)
            replyBtn.addTarget(self, action: #selector(actionReply), for: .touchUpInside)
            self.actionWithMsgView!.addSubview(replyBtn)
            
            yBtn += 44

            if msg.text != "" {
                let copyBtn = createBtnForActionView(y: yBtn, width: viewWidth, text: "Copy", image: mainIcons.copy_s23)
                copyBtn.addTarget(self, action: #selector(actionCopy), for: .touchUpInside)
                self.actionWithMsgView!.addSubview(copyBtn)
                
                yBtn += 44
            }

            let forwardBtn = createBtnForActionView(y: yBtn, width: viewWidth, text: "Forward", image: mainIcons.forward_s23)
            forwardBtn.addTarget(self, action: #selector(actionForward), for: .touchUpInside)
            self.actionWithMsgView!.addSubview(forwardBtn)
            
            yBtn += 44
            
            let res = msg.attachments.filter { $0.type == "sticker" || $0.type == "audio_message" }
            
            if res.count == 0 && (Int64(Date().timeIntervalSince1970) - msg.date) < 86_400 {
                let editBtn = createBtnForActionView(y: yBtn, width: viewWidth, text: "Edit", image: mainIcons.newMessage_s23)
                editBtn.addTarget(self, action: #selector(actionEdit), for: .touchUpInside)
                self.actionWithMsgView!.addSubview(editBtn)
                
                yBtn += 44
            }

            let deleteBtn = createBtnForActionView(y: yBtn, width: viewWidth, text: "Delete", image: mainIcons.trash_s23)
            deleteBtn.addTarget(self, action: #selector(actionDeleteMsg), for: .touchUpInside)
            self.actionWithMsgView!.addSubview(deleteBtn)
            
            yBtn += 44

            let moreBtn = createBtnForActionView(y: yBtn, width: viewWidth, text: "More", image: mainIcons.threeDots_s23)
            moreBtn.addTarget(self, action: #selector(actionSelectSomeMessage), for: .touchUpInside)
            self.actionWithMsgView!.addSubview(moreBtn)
            
            yBtn += 51

            UIView.transition(with: actionWithMsgView!, duration: 0.2, options: .showHideTransitionViews, animations: {
                let y = Int(UIScreen.main.bounds.height / 2) - (yBtn / 2)
                self.actionWithMsgView!.frame = CGRect(x: 27, y: y, width: viewWidth, height: yBtn)
            })

            tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideActionWithMsgView))
            overlay?.addGestureRecognizer(tapGestureRecognizer!)
        }
        
    }
    
    func createBtnForActionView(y: Int, width: Int, text: String, image: UIImage?) -> UIButton {
        let btn = UIButton(frame: CGRect(x: 0, y: y, width: width, height: 44))
        
        btn.setTitle(text, for: .normal)
        btn.setTitleColor(mainColors.actionViewElements, for: .normal)
        btn.titleLabel?.font = .systemFont(ofSize: 14)
        btn.setImage(image, for: .normal)
        btn.tintColor = mainColors.actionViewElements
        btn.titleEdgeInsets = UIEdgeInsets(top: 0, left: 23, bottom: 0, right: 0)
        btn.contentHorizontalAlignment = .left
        btn.contentEdgeInsets = UIEdgeInsets(top: 0, left: 23, bottom: 0, right: 0)
        
        //btn.layer.borderColor = mainColors.actionViewElements?.cgColor
        //btn.layer.borderWidth = 0.5
        
        return btn
    }
    
    @objc func hideActionWithMsgView() {
        self.overlay?.removeFromSuperview()
        self.overlay?.removeGestureRecognizer(self.tapGestureRecognizer!)
        self.tapGestureRecognizer = nil
        self.overlay = nil
        self.actionWithMsgView?.removeFromSuperview()
        self.actionWithMsgView = nil
        self.selectMsgIndexPath = nil
    }
    
    //reply on message
    
    @objc func actionReply() {
        if selectMsgIndexPath != nil {
            var viewFrame = self.viewForTableNode.frame
            let msg = messagesInput[selectMsgIndexPath!.section].elements[selectMsgIndexPath!.row]
            hideActionWithMsgView()
            self.replyForwardViewHeight = 50
            
            if replyForwardMsgID == 0 {
                self.textInputTop.constant += self.replyForwardViewHeight
                self.textViewParentHeight.constant += self.replyForwardViewHeight
                self.viewForTableNode.frame = CGRect(x: viewFrame.origin.x, y: viewFrame.origin.y, width: viewFrame.width, height: viewFrame.height - self.replyForwardViewHeight)
                
                if attachmentNode != nil {
                    viewFrame = self.attachmentNode!.frame
                    self.attachmentNode?.frame = CGRect(x: 5, y: 3 + self.replyForwardViewHeight, width: viewFrame.width, height: viewFrame.height)
                }
            }
            
            if isRecordBtn == true {
                UIView.transition(with: self.sendMsgBtn! as UIView, duration: 0.1, options: .transitionFlipFromBottom, animations: {
                    self.sendMsgBtn.setImage(mainIcons.send_s24, for: .normal)
                }, completion: nil)
            }
            self.isRecordBtn = false
            
            replyForwardMsgID = msg.id
            replyForwardView?.removeFromSuperview()
            replyForwardView = VKReplyForwardMsgView(frame: CGRect(x: 5, y: 5, width: viewFrame.width - 10, height: 40))
            replyForwardView?.configView(msg: msg, actionType: .reply)
            replyForwardView?.closeBtn.addTarget(self, action: #selector(hideReplyForwardView), for: .touchUpInside)
            self.msgInputCntrView.addSubview(self.replyForwardView!)
            inputTextNode.textView.becomeFirstResponder()
            UIView.animate(withDuration: 0.25) {
                self.view.setNeedsLayout()
            }
        }
        
    }
    
    @objc func hideReplyForwardView() {
        var viewFrame = self.viewForTableNode.frame
        self.textInputTop.constant -= self.replyForwardViewHeight
        self.textViewParentHeight.constant -= self.replyForwardViewHeight
        self.viewForTableNode.frame = CGRect(x: viewFrame.origin.x, y: viewFrame.origin.y, width: viewFrame.width, height: viewFrame.height + self.replyForwardViewHeight)
        replyForwardMsgID = 0
        replyForwardView?.removeFromSuperview()
        
        if attachmentNode != nil {
            viewFrame = self.attachmentNode!.frame
            self.attachmentNode?.frame = CGRect(x: 5, y: 3, width: viewFrame.width, height: viewFrame.height)
        }
        
        self.replyForwardViewHeight = 0
        
        UIView.animate(withDuration: 0.25) {
            self.view.setNeedsLayout()
        }
    }
    
    //copy text
    
    @objc func actionCopy() {
        if selectMsgIndexPath != nil {
            let text = messagesInput[selectMsgIndexPath!.section].elements[selectMsgIndexPath!.row].text
            if text == "" { ToastView.shared.short(self.view, txt_msg: "Message don't have a text") }
            else { UIPasteboard.general.string = text }
            hideActionWithMsgView()
        }
    }
    
    //forward message
    
    @objc func actionForward() {
        if selectMsgIndexPath != nil {
            hideActionWithMsgView()
            overlay?.removeFromSuperview()
            overlay = UIView(frame: UIScreen.main.bounds)
            overlay?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            overlay?.tag = 101
            UIApplication.shared.keyWindow?.addSubview(overlay!)
            
            forwardTargetView = VKForwardTargetView(frame: CGRect(x: 0, y: 100, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 100))
            forwardTargetView?.cancelBtn.addTarget(self, action: #selector(closeForwardTargetView), for: .touchUpInside)
            forwardTargetView?.sendBtn.addTarget(self, action: #selector(sendAction), for: .touchUpInside)
            forwardTargetView?.config()
            overlay?.addSubview(forwardTargetView!)
            
        }
    }
    
    @objc func sendAction() {
        let alert = UIAlertController(title: "Input message text (optional/not necessary)", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Send", style: .default, handler: { [weak self] (action) in
            if self == nil { return }
            //            var user_ids = ""
            //            for indexPath in self!.friendsIndexPathForChat {
            //                user_ids.append(String(self!.getUserBy(indexPath: indexPath)?.user.id ?? 0) + ",")
            //            }
            //            let chatName = alert.textFields![0].text
            //
            //            VK.API.Messages.createChat([.title: chatName, .userIDs: "\(user_ids)"]).onSuccess({ [weak self] (data) in
            //                let result = JSON(data)
            //                var chat_id = result["response"].int64 ?? 0
            //                chat_id += 2_000_000_000
            //                self?.loadNeedConvers(peer_id: chat_id)
            //            }).onError({ (error) in
            //                print("createChatAction - \(error)")
            //            }).send()
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.placeholder = "Enter text:"
        })
        self.present(alert, animated: true, completion: nil)
        closeForwardTargetView()
    }
    
    @objc func closeForwardTargetView() {
        self.forwardTargetView?.removeFromSuperview()
        self.forwardTargetView = nil
        self.overlay?.removeFromSuperview()
        for subview in UIApplication.shared.keyWindow?.subviews ?? [] {
            if subview.tag == 101 {
                subview.removeFromSuperview()
            }
        }
        self.overlay = nil
    }
    
    //edit message
    
    @objc func actionEdit() {
        if selectMsgIndexPath != nil {
            let msg = messagesInput[selectMsgIndexPath!.section].elements[selectMsgIndexPath!.row]
            hideActionWithMsgView()
            if msg.text != "" {
                inputTextNode.textView.becomeFirstResponder()
                let textRange = inputTextNode.textView.textRange(from: inputTextNode.textView.beginningOfDocument, to: inputTextNode.textView.endOfDocument)
                inputTextNode.textView.replace(textRange!, withText: msg.text)
            }
            
            parseAttachmentsForActionEdit(msg)
            
            if self.attachments.count > 0 {
                self.showAttNode()
            }
            
            isEditMode = true
            editMsgID = msg.id
        }
    }
    
    func parseAttachmentsForActionEdit(_ msg: VKMessage) {
        
        if msg.geo != nil {
            let elem = VKAttachmentShort()
            elem.type = "geo"
            elem.latitude = Double(msg.geo!.coordinates_latitude)
            elem.longitude = Double(msg.geo!.coordinates_longitude)
            
            self.attachments.append(elem)
        }
        
        for att in msg.attachments {
            let elem = VKAttachmentShort()
            elem.type = att.type
            
            switch att.type {
            case "audio":
                elem.vk_id = "\(att.audio!.owner_id)_\(att.audio!.id)"
                elem.audio = VKAudio()
                elem.audio?.copyValue(att.audio!)
                break
            case "photo":
                elem.vk_id = "\(att.photo!.owner_id)_\(att.photo!.id)"
                elem.photo = VKPhoto()
                elem.photo?.copyValue(att.photo!)
                if let image = vkDataCache.getDataFromCachedData(sourceID: att.photo!.id, ownerID: att.photo!.owner_id, type: "image", tag: "photo") {
                    elem.image = UIImage(data: image as Data)
                }
                break
            case "video":
                elem.vk_id = "\(att.video!.owner_id)_\(att.video!.id)"
                elem.duration = att.video!.duration
                if let image = vkDataCache.getDataFromCachedData(sourceID: att.video!.id, ownerID: att.video!.owner_id, type: "image", tag: "video") {
                    elem.image = UIImage(data: image as Data)
                }
                break
            case "graffiti":
                break
            case "doc":
                elem.vk_id = "\(att.doc!.owner_id)_\(att.doc!.id)"
                elem.doc = VKDocument()
                elem.doc?.copyValue(att.doc!)
                if let image = vkDataCache.getDataFromCachedData(sourceID: att.doc!.id, ownerID: att.doc!.owner_id, type: "image", tag: "doc_image") {
                    elem.image = UIImage(data: image as Data)
                }
                break
            case "wall":
                elem.vk_id = "\(att.wall!.owner_id)_\(att.wall!.id)"
                elem.wall = VKWall()
                elem.wall?.copyValue(att.wall!)
                break
            default: break
            }
            
            
            
            self.attachments.append(elem)
        }
        
    }
    
    //delete message
    
    @objc func actionDeleteMsg() {
        if selectMsgIndexPath != nil {
            let index = selectMsgIndexPath!
            hideActionWithMsgView()
            deleteMessages(indexPaths: [index])
        }
    }
    
    func deleteMessages(indexPaths: [IndexPath]) {
        var msgIDs: [Int64] = []
        var isOnlyMainUserMsgs: Bool = true
        for index in indexPaths {
            let msg = messagesInput[index.section].elements[index.row]
            msgIDs.append(msg.id)
            if msg.from_id != vkLoadData.mainUserID { isOnlyMainUserMsgs = false }
        }
        
        let alert = UIAlertController(title: "Delete?", message: "", preferredStyle: .actionSheet)
        
        if isOnlyMainUserMsgs && vkLoadData.mainUserID != peer_id {
            alert.addAction(UIAlertAction(title: "Delete for all", style: .destructive, handler: { [weak self] (action) in
                self?.deleteMessages(msgIDs: msgIDs, deleteForAll: true)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { [weak self] (action) in
            self?.deleteMessages(msgIDs: msgIDs, deleteForAll: false)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteMessages(msgIDs: [Int64], deleteForAll: Bool) {
        do {
            let realm = try Realm()
            for id in msgIDs {
                let msg = realm.objects(VKMessage.self).filter("id = %@ AND isPreview = false", id).first
                try realm.write { msg?.isDelete = true }
            }
            
            for id in msgIDs { updateMsgDictAndTableById(msg_id: id, type: 2) }
            
            vkLoadData.deleteMessages(msgIDs: msgIDs, group_id: self.peer_id, deleteForAll: deleteForAll) { [weak self] (result) in
                if result == false {
                    do {
                        let realmBG = try Realm()
                        for id in msgIDs {
                            let msg = realmBG.objects(VKMessage.self).filter("id = %@ AND isPreview = false", id).first
                            try realmBG.write { msg?.isDelete = true }
                        }
                        for id in msgIDs { self?.updateMsgDictAndTableById(msg_id: id, type: 3) }
                    } catch let error as NSError { print("\(error)") }
                }
            }
        } catch let error as NSError { print("\(error)") }
    }
    
    //selectMode functions
    
    @objc func actionSelectSomeMessage() {
        hideActionWithMsgView()
        
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapWhenSelectMode(tapGesture:)))
        tableNode.view.addGestureRecognizer(tapGestureRecognizer!)
        
        isSelectMode = true
        moveSelectButtonOnMessNode(isShow: true)
        
        let leftBtn = UIBarButtonItem(image: mainIcons.cancel_s21, style: .plain, target: self, action: #selector(cancelAction))
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationItem.setLeftBarButton(leftBtn, animated: true)
    }
        
    @objc func tapWhenSelectMode(tapGesture:UITapGestureRecognizer) {
    
        let p = tapGesture.location(in: self.tableNode.view)
        let indexPath = self.tableNode.indexPathForRow(at: p)
        
        if indexPath == nil { print("Tap on table view, not row.") }
        else {
            print("Tap on section-row, at \(indexPath!.section)-\(indexPath!.row)")
            selectRow(indexPath: indexPath!)
        }
    }
    
    func selectRow(indexPath: IndexPath) {
        print(indexPath)
        tableNode.deselectRow(at: indexPath, animated: true)
        let mesID = messagesInput[indexPath.section].elements[indexPath.row].id
        let node = tableNode.nodeForRow(at: indexPath) as? MessageCellNode
        if selectMessagesID.contains(mesID) {
            node?.setSelect(isSelect: false)
            selectMessagesID.remove(at: selectMessagesID.firstIndex(of: mesID)!)
        } else {
            node?.setSelect(isSelect: true)
            selectMessagesID.append(mesID)
        }
    }
    
    func moveSelectButtonOnMessNode(isShow: Bool) {
        for i in 0..<tableNode.numberOfSections {
            print("\(i) - \(tableNode.numberOfRows(inSection: i))")
            for j in 0..<tableNode.numberOfRows(inSection: i) {
                (tableNode.nodeForRow(at: IndexPath(row: j, section: i)) as? MessageCellNode)?.moveSelectImageNode(isShow: isShow)
            }
        }
    }
    
    func getAllIndexPaths() -> [IndexPath] {
        var indexPaths: [IndexPath] = []
        
        for i in 0..<tableNode.numberOfSections {
            for j in 0..<tableNode.numberOfRows(inSection: i) {
                indexPaths.append(IndexPath(row: j, section: i))
            }
        }
        return indexPaths
    }
    
    @objc func cancelAction() {
        self.navigationItem.hidesBackButton = false
        self.navigationItem.setHidesBackButton(false, animated: true)
        self.navigationItem.leftBarButtonItem = nil
        isSelectMode = false
        tableNode.view.removeGestureRecognizer(tapGestureRecognizer!)
        tapGestureRecognizer = nil
        moveSelectButtonOnMessNode(isShow: false)
        selectMessagesID.removeAll()
    }
}
    
