//
//  VKWriteDeleteTableViewCell.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 13.07.2018.
//

import UIKit
import RealmSwift

class VKWriteDeleteTableViewCell: UITableViewCell {
    @IBOutlet weak var writeBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    var user_id: Int64 = 0
    var friend_status: Int8 = 0
    var navControll: UINavigationController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(user_id: Int64, friend_status: Int8, nav_controll: UINavigationController?) {
        self.user_id = user_id
        self.friend_status = friend_status
        self.navControll = nav_controll
        
        let write = mainIcons.newMessage_s20
        writeBtn.setImage(write, for: .normal)
        writeBtn.setTitle(" Send message", for: .normal)
        writeBtn.tintColor = mainColors.writeRow
        
        var color: UIColor?
        var title: String = ""
        var img: UIImage?
        
        switch friend_status {
        case 0:
            color = mainColors.online
            title = "Add to friends"
            img = mainIcons.addUser_s20
            break
        case 1:
            color = mainColors.deleteRow
            title = "Cancel folowwing"
            img = mainIcons.trash_s20
            break
        case 2:
            color = mainColors.online
            title = "Approve request"
            img = mainIcons.addUser_s20
            break
        case 3:
            color = mainColors.deleteRow
            title = "Delete"
            img = mainIcons.trash_s20
            break
        default:
            break
        }
        
        deleteBtn.setImage(img, for: .normal)
        deleteBtn.setTitle(" " + title, for: .normal)
        deleteBtn.tintColor = color
    }
    
    @IBAction func writeAction(_ sender: Any) {
        let realm = try! Realm()
        if let _ = realm.objects(VKConversation.self).filter("peer_id = %@", user_id).first {
            self.goToConvers(peer_id: user_id)
        } else {
            vkLoadData.loadVKConversationsByID(peer_ids: String(user_id), completionBlock: { [weak self] (result) in
                if result == true {
                    if self == nil { return }
                    let realmAs = try! Realm()
                    if let _ = realmAs.objects(VKConversation.self).filter("peer_id = %@", self!.user_id).first {
                        self!.goToConvers(peer_id: self!.user_id)
                    }
                }
            })
        }
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deleteOrAddActionInProfile"), object: nil, userInfo: ["user_id": user_id])
    }
    
    func goToConvers(peer_id: Int64) {
        DispatchQueue.main.async {
            let realm = try! Realm()
            if let convers = realm.objects(VKConversation.self).filter("peer_id = %@", peer_id).first {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let vkMessageViewController = storyboard.instantiateViewController(withIdentifier: "VKMessageViewController") as? VKMessageViewController {
                    vkMessageViewController.convers = convers
                    self.navControll?.pushViewController(vkMessageViewController, animated: true)
                }
            }
        }
    }
}
