//
//  VKFriendsTableViewCell.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 13.07.2018.
//

import UIKit

class VKFriendsTableViewCell: UITableViewCell {
    @IBOutlet weak var friendsBtn: UIButton!
    var user_id: Int64 = 0
    var friendsCount: Int = 0
    var friendsMutualCount: Int = 0
    var navControll: UINavigationController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(user_id: Int64, frCnt: Int, frOnlCnt: Int, frMutCnt: Int) {
        self.user_id = user_id
        self.friendsCount = frCnt
        self.friendsMutualCount = frMutCnt
        let str = "Friends" + " (" + String(frCnt) + ", " + String(frOnlCnt) + " " + "online" + ", " + String(frMutCnt) + " " + "mutual" + ")"
        friendsBtn.setTitle(str, for: .normal)
        friendsBtn.tintColor = mainColors.writeRow
    }
    
    @IBAction func friendsAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vkContactController = storyboard.instantiateViewController(withIdentifier: "VKContactsViewControllerID") as? VKContactsViewController {
            vkContactController.userID = user_id
            vkContactController.friendsCount = friendsCount
            vkContactController.friendsMutualCount = friendsMutualCount
            self.navControll?.pushViewController(vkContactController, animated: true)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
