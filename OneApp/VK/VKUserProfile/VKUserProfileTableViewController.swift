//
//  VKUserProfileTableViewController.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 13.07.2018.
//

import UIKit
//import INSPhotoGalleryFramework
import INSPhotoGallery
import RealmSwift
import SpriteKit

enum VKProfileCellType {
    case avatar, userName, bdate, writeDelete, phoneNumberMob, phoneNumberCity, friends, blacklist
}

class VKUserProfileTableViewController: UITableViewController, ImageLoadedListener {
    var needCell: [VKProfileCellType] = []
    var cellHieght: [CGFloat] = []
    var user: VKUser = VKUser()
    var imgData: NSData? = nil
    var indexPath: IndexPath = IndexPath()
    var profilePhotos: [INSCachingImage] = []
    
    private var cellImageLoader : CellImageLoader?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cellImageLoader = CellImageLoader(withImageLoadedListener: self)
        
        tableView.backgroundColor = mainColors.mainTableView
        tableView.separatorColor = mainColors.separator
        tableView.contentInset = UIEdgeInsets(top: ((self.navigationController?.navigationBar.frame.height ?? 0) + (self.navigationController?.navigationBar.frame.origin.y ?? 0)) * (-1), left: 0, bottom: 0, right: 0)
        self.navigationController?.navigationBar.barTintColor = nil
        self.navigationController?.navigationBar.isTranslucent = true
        //self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.topItem?.title = ""
        //print(navigationController?.navigationBar.frame)
        
        if imgData == nil {
            cellImageLoader?.loadImageForIndex(IndexPath(row: 0, section: 0), user.photo_600, "ava", user.id)
        }
        
        registerNib()
        initData()
        
//        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
//        self.tableView.addGestureRecognizer(gestureRecognizer)
        
        vkLoadData.loadAllInfoAboutUser(String(user.id)) { [weak self] (result) in
            if result == true {
                vkDataCache.saveUserInfoInCache(vkLoadData.usersInfo, completion: { _ in })
                DispatchQueue.main.async {
                    self?.user.copyValue((vkLoadData.usersInfo.first?.value)!)
                    if self?.imgData == nil {
                        self?.cellImageLoader?.loadImageForIndex(IndexPath(row: 0, section: 0), self?.user.photo_600 ?? "", "ava", self?.user.id ?? 0)
                    }
                    vkLoadData.usersInfo.removeAll()
                    self?.tableView.reloadData()
                }
                
                
            }
        }
        
        vkLoadData.loadProfileImages(peer_id: self.user.id) { [weak self] (result) in
            if result == true {
                for photo in vkLoadData.profilePhoto {
                    self?.profilePhotos.append(INSCachingImage(imageURL: URL(string: photo.photoDocSizeMid?.url ?? ""), image_id: photo.id))
                }
            }
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.isMovingFromParent || self.isBeingDismissed {
            self.navigationController?.navigationBar.barTintColor = mainColors.mainNavigationBarLight
            navigationController?.navigationBar.isTranslucent = false
        }
    }
    
    func registerNib() {
        var nib = UINib(nibName: "VKAvatarTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "VKAvatarTableViewCell")
        
        nib = UINib(nibName: "VKBDateTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "VKBDateTableViewCell")
        
        nib = UINib(nibName: "VKUserNameTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "VKUserNameTableViewCell")
        
        nib = UINib(nibName: "VKWriteDeleteTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "VKWriteDeleteTableViewCell")
        
        nib = UINib(nibName: "VKPhoneNumberTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "VKPhoneNumberTableViewCell")
        
        nib = UINib(nibName: "VKFriendsTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "VKFriendsTableViewCell")
        
        nib = UINib(nibName: "VKBlackListTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "VKBlackListTableViewCell")
    }

    func initData() {
        //title = user.first_name
        
        needCell.append(.avatar)
        needCell.append(.userName)
        
        if user.bdate.count > 0 { needCell.append(.bdate) }
        
        needCell.append(.writeDelete)
        
        if user.contacts_mobile_phone.count > 0 && (Int(user.contacts_mobile_phone) ?? 0) > 0 { needCell.append(.phoneNumberMob) }
        if user.contacts_home_phone.count > 0 && (Int(user.contacts_home_phone) ?? 0) > 0 { needCell.append(.phoneNumberCity) }
        
        needCell.append(.friends)
        needCell.append(.blacklist)
        
        calcCellHeight()
        
        NotificationCenter.default.addObserver(self, selector: #selector(deleteOrAddAction), name: NSNotification.Name.deleteOrAddActionInProfile, object: nil)
    }
    
    func calcCellHeight() {
        cellHieght.removeAll()
        for type in needCell {
            switch type {
            case .avatar:
                cellHieght.append(240)
                break
            case .bdate, .phoneNumberCity, .phoneNumberMob, .friends, .blacklist, .writeDelete:
                cellHieght.append(50.0)
                break
            default:
                cellHieght.append(70.0)
            }
        }
    }
    
    func avatarLoaded(peer_id: Int64, data: NSData?) {
        DispatchQueue.main.async {
            self.imgData = data
            self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
        }
    }

    func imageLoaded(withIndexPath indexPath: IndexPath, data: NSData?) {
        //NSLog("imageLoaded")
    }
    
    func isNeedToLoadImageWithIndex(_ indexPath : IndexPath, completion: @escaping (_ :Bool) -> Void) {
        DispatchQueue.main.async {
            if self.imgData != nil {
                completion(false)
                return
            }
            
            let cellWithIndex = self.tableView.visibleCells.filter({ (cell) -> Bool in
                let indexPathForCell = self.tableView.indexPath(for: cell)
                return indexPathForCell == indexPath
            })
            
            if (cellWithIndex.count == 0) {
                completion(false)
                return
            }
            
            completion(true)
        }
    }

    @objc func deleteOrAddAction(notification: NSNotification) {
        var userInfo = notification.userInfo ?? [:]
        
        if userInfo["user_id"] != nil && (userInfo["user_id"] as! Int64) == user.id {
            var title: String = ""
            var btnTitle: String = ""
            var isBan: Bool = false
            if userInfo["isBan"] != nil {
                title = "Add " + self.user.first_name + " to blacklist?"
                btnTitle = "Add"
                isBan = true
            } else {
                if user.friend_status == 1 {
                    title = "Cancel following from " + self.user.first_name + "?"
                    btnTitle = "Delete"
                }
                else if user.friend_status == 3 {
                    title = "Delete " + self.user.first_name + " from friends?"
                    btnTitle = "Delete"
                } else {
                    title = "Add " + self.user.first_name + " to friends?"
                    btnTitle = "Add"
                }
            }
            
            let alert = UIAlertController(title: title, message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: btnTitle, style: .default, handler: { [weak self] (action) in
                if self == nil { return }
                
                userInfo["msg"] = ""
                if alert.textFields?.count ?? 0 > 0 {
                    userInfo["msg"] = alert.textFields?[0].text ?? ""
                }
                
                let realm = try! Realm()
                let status: Int8 = self!.user.friend_status
                try! realm.write {
                    if status == 0 { self!.user.friend_status = 1 }
                    if status == 1 || status == 3 || isBan == true { self!.user.friend_status = 0 }
                    if status == 2 { self!.user.friend_status = 3 }
                }
                userInfo["status"] = status
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deleteOrAddFriend"), object: nil, userInfo: userInfo)
                
                if status == 3 || isBan == true {
                    self!.navigationController?.popViewController(animated: true)
                } else {
                    self!.tableView.reloadData()
                }
                
            }))
            
            if user.friend_status == 0 {
                alert.addTextField(configurationHandler: {(textField: UITextField!) in
                    textField.placeholder = "Enter message:"
                })
            }
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
//    @IBAction func handlePan(_ gestureRecognizer: UIPanGestureRecognizer) {
//        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
//
//            let translation = gestureRecognizer.translation(in: self.view)
//            print(translation)
//            if translation.y > -200 {
//                cellHieght[0] = 240 + translation.y
//                if Int(translation.y) % 5 == 0 {
//                    UIView.animate(withDuration: 0.4, animations: {
//                        self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
//                    }) { _ in }
//                    //tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
//                }
//            }
//
//            //let point = translation
//            // note: 'view' is optional and need to be unwrapped
//            //gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x + translation.x, y: gestureRecognizer.view!.center.y + translation.y)
//            //gestureRecognizer.setTranslation(CGPoint.zero, in: self.view)
//        } else if gestureRecognizer.state == .ended {
//            cellHieght[0] = 240
//            UIView.animate(withDuration: 0.4, animations: {
//                //tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
//            }) { _ in
//                self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
//            }
//            //tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
//        }
//    }
    
//    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//        scrollOffset = -scrollView.contentOffset.y - 64
//        print(scrollOffset)
//        cellHieght[0] = 240 + scrollOffset
//        tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
//        //tableView.rectForRow(at: IndexPath(row: 0, section: 0))
//        //tableView.reloadData()
////        if scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y <= yourLimit {
////            heightConstraint.constant = collectionViewHeight - scrollView.contentOffset.y
////            tableView.isScrollEnabled = false
////
////        } else if scrollView.contentOffset.y > yourLimit {
////            heightConstraint.constant = collectionViewHeight
////            tableView.isScrollEnabled = true
////        }
//    }
    

    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return needCell.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHieght[indexPath.row]
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if indexPath.row == 0 {
            if self.profilePhotos.count > 0 {
                let cell = tableView.cellForRow(at: indexPath) as! VKAvatarTableViewCell
                cell.photos = self.profilePhotos
                self.present(cell.getGalleryPreview(), animated: true, completion: nil)
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch needCell[indexPath.row] {
        case .avatar:
            let cell = tableView.dequeueReusableCell(withIdentifier: "VKAvatarTableViewCell", for: indexPath) as! VKAvatarTableViewCell
            cell.configCell(data: imgData == nil ? user.photo100Data : imgData, crop_photo_bottom: user.crop_photo_bottom, crop_photo_top: user.crop_photo_top, viewFrameWidth: tableView.frame.width, viewFrameHeight: cellHieght[0])
            cell.backgroundColor = mainColors.mainTableView
            return cell
            
        case .userName:
            let cell = tableView.dequeueReusableCell(withIdentifier: "VKUserNameTableViewCell", for: indexPath) as! VKUserNameTableViewCell
            cell.configCell(firstName: user.first_name, lastName: user.last_name, isOnline: user.online, isMobOnline: user.online_mobile, lastSeenTime: user.last_seen_time, lastSeenPlatform: user.last_seen_platform)
            cell.backgroundColor = mainColors.mainTableView
            return cell
            
        case .bdate:
            let cell = (tableView.dequeueReusableCell(withIdentifier: "VKBDateTableViewCell", for: indexPath) as! VKBDateTableViewCell)
            cell.configCell(user.bdate)
            cell.backgroundColor = mainColors.mainTableView
            return cell
            
        case .phoneNumberCity, .phoneNumberMob:
            let cell = tableView.dequeueReusableCell(withIdentifier: "VKPhoneNumberTableViewCell", for: indexPath) as! VKPhoneNumberTableViewCell
            cell.configCell(number: (needCell[indexPath.row] == .phoneNumberMob ? user.contacts_mobile_phone : user.contacts_home_phone))
            cell.backgroundColor = mainColors.mainTableView
            return cell
            
        case .writeDelete:
            let cell = tableView.dequeueReusableCell(withIdentifier: "VKWriteDeleteTableViewCell", for: indexPath) as! VKWriteDeleteTableViewCell
            cell.configCell(user_id: user.id, friend_status: user.friend_status, nav_controll: navigationController)
            cell.backgroundColor = mainColors.mainTableView
            return cell
            
        case .friends:
            let cell = tableView.dequeueReusableCell(withIdentifier: "VKFriendsTableViewCell", for: indexPath) as! VKFriendsTableViewCell
            cell.configCell(user_id: user.id, frCnt: user.counters_friends, frOnlCnt: user.counters_online_friends, frMutCnt: user.counters_mutual_friends)
            cell.navControll = self.navigationController
            cell.backgroundColor = mainColors.mainTableView
            return cell
            
        case .blacklist:
            let cell = tableView.dequeueReusableCell(withIdentifier: "VKBlackListTableViewCell", for: indexPath) as! VKBlackListTableViewCell
            cell.configCell()
            cell.user_id = user.id
            cell.backgroundColor = mainColors.mainTableView
            return cell
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


