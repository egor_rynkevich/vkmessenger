//
//  VKBDateTableViewCell.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 13.07.2018.
//

import UIKit

class VKBDateTableViewCell: UITableViewCell {
    @IBOutlet weak var bdTitleLbl: UILabel!
    @IBOutlet weak var bdLbl: UILabel!
    @IBOutlet weak var yearLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(_ bdate: String) {
        bdTitleLbl.text = "Birthday: "
        bdLbl.text = Helper.vkBDtoStr(bdate: bdate)
        
        let dotCount = bdate.reduce(into: 0) { (count, char) in
            if char == "." {
                count += 1
            }
        }
        
        if dotCount == 2 {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy"
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.locale = Locale.current
            let date = dateFormatter.date(from: bdate)
            
            let eleps = Date().timeIntervalSince(date!)
            let formatter = DateComponentsFormatter()
            formatter.allowedUnits = [.year]
            let str = formatter.string(from: eleps)!
            
            yearLbl.text = "(" + str + ")"
        } else {
            yearLbl.text = ""
        }
        
        bdTitleLbl.textColor = mainColors.text
        bdLbl.textColor = mainColors.text
        yearLbl.textColor = mainColors.text
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
