//
//  VKUserNameTableViewCell.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 13.07.2018.
//

import UIKit

class VKUserNameTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var onlineLbl: UILabel!
    @IBOutlet weak var onlineImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(firstName: String, lastName: String, isOnline: Bool, isMobOnline: Bool, lastSeenTime: Int64, lastSeenPlatform: Int8) {
        nameLbl.text = firstName + " " + lastName
        nameLbl.textColor = mainColors.text
        if isOnline == true {
            onlineLbl.text = "online"
            onlineLbl.textColor = mainColors.online
        } else {
            onlineLbl.text = Helper.getLastSeenStr(lastSeen: TimeInterval(lastSeenTime))
            onlineLbl.textColor = mainColors.lightGray
        }
        
        let isMobile = isMobOnline == true || (lastSeenPlatform != 7 && lastSeenPlatform != 0)
        
        if isMobile == true {
            onlineImgView.image = mainIcons.userPhoneOnline
            onlineImgView.tintColor = isOnline == false ? mainColors.lightGray : mainColors.online
        }
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
