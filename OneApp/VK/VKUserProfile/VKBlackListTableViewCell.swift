//
//  VKBlackListTableViewCell.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 14.07.2018.
//

import UIKit

class VKBlackListTableViewCell: UITableViewCell {
    @IBOutlet weak var addBtn: UIButton!
    var user_id: Int64 = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell() {
        addBtn.setTitle("Add to blacklist", for: .normal)
    }

    @IBAction func addAction(_ sender: Any) {
        var userInfo: [String: Any] = [: ]
        userInfo["isBan"] = true
        userInfo["user_id"] = user_id
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deleteActionInProfile"), object: nil, userInfo: userInfo)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
