//
//  VKPhoneNumberTableViewCell.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 13.07.2018.
//

import UIKit

class VKPhoneNumberTableViewCell: UITableViewCell {
    @IBOutlet weak var phoneTextView: UITextView!
    @IBOutlet weak var callBtn: UIButton!
    
    var number: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        callBtn.setImage(mainIcons.call_s30, for: .normal)
        callBtn.setTitle("", for: .normal)
        callBtn.tintColor = mainColors.online
        // Initialization code
    }
    
    func configCell(number: String) {
        phoneTextView.text = number
        phoneTextView.textColor = mainColors.text
        phoneTextView.backgroundColor = mainColors.mainTableView
    }

    @IBAction func callAction(_ sender: Any) {
        if let url = URL(string: "tel://\(phoneTextView.text!)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
