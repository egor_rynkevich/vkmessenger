//
//  VKAvatarTableViewCell.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 13.07.2018.
//

import UIKit
import INSPhotoGallery

class VKAvatarTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var collectionView: UICollectionView!
    var avaImg: UIImage?
    var viewFrameWidth: CGFloat = 0.0
    var viewFrameHeight: CGFloat = 0.0
    var photos: [INSPhotoViewable] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(data: NSData?, crop_photo_bottom: Int, crop_photo_top: Int, viewFrameWidth: CGFloat, viewFrameHeight: CGFloat) {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(UINib.init(nibName: "VKAvatarCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VKAvatarCollectionViewCell")
        
        self.viewFrameWidth = viewFrameWidth
        self.viewFrameHeight = viewFrameHeight
        
        if data != nil {
            self.avaImg = UIImage(data: (data! as Data))//?.withAlignmentRectInsets(UIEdgeInsets(top: CGFloat(-crop_photo_top+10), left: 0, bottom: CGFloat(crop_photo_bottom), right: 0))
            self.collectionView.reloadData()
        } else {
            self.avaImg = nil
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: viewFrameWidth, height: viewFrameHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VKAvatarCollectionViewCell", for: indexPath as IndexPath) as! VKAvatarCollectionViewCell
        
        cell.imgView.image = avaImg
        
        return cell
    }

    func getGalleryPreview() -> INSPhotosViewController {
        let cell = collectionView.cellForItem(at: IndexPath(row: 0, section: 0)) as! VKAvatarCollectionViewCell
        let currentPhoto = photos[0]
        let galleryPreview = INSPhotosViewController(photos: photos, initialPhoto: currentPhoto, referenceView: cell)
        
        galleryPreview.referenceViewForPhotoWhenDismissingHandler = { [weak self] photo in
            if let index = self?.photos.index(where: { (item) -> Bool in item === photo }) {
                let indexPath = IndexPath(row: index, section: 0)
                return self?.collectionView.cellForItem(at: indexPath) as? VKAvatarCollectionViewCell
            }
            return nil
        }
        //let frame = galleryPreview.view.frame
        //let view = UIView(frame: CGRect(x: 0, y: frame.height - 30, width: frame.width, height: 30))
        //view.backgroundColor = mainColors.deleteRow
        //galleryPreview.overlayView.view().addSubview(view)
        return galleryPreview
    }
    
}
