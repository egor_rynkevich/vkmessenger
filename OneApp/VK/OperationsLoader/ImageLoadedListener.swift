//
//  ImageLoadedListener.swift
//  OperationsLoader
//
//  Created by NCHD on 7/11/18.
//  Copyright © 2018 NCHD. All rights reserved.
//

import Foundation

protocol ImageLoadedListener : class {
    func isNeedToLoadImageWithIndex(_ indexPath : IndexPath, completion: @escaping (_ :Bool) -> Void)
    func imageLoaded(withIndexPath indexPath : IndexPath, data: NSData?)
    func avatarLoaded(peer_id: Int64, data: NSData?)
}
