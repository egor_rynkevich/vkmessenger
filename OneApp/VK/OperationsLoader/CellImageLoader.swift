//
//  CellImageLoader.swift
//  OperationsLoader
//
//  Created by NCHD on 7/11/18.
//  Copyright © 2018 NCHD. All rights reserved.
//

import Foundation

class CellImageLoader {
    
    private let queueManager = QueueManager()
    private weak var imageLoadedListener : ImageLoadedListener?
    
    private var loadingImages : Set<String> = []
    
    init(withImageLoadedListener imageLoadedListener : ImageLoadedListener) {
        self.imageLoadedListener = imageLoadedListener
    }
    
    func loadImageForIndex(_ indexPath : IndexPath, _ imageURL: String, _ tag: String, _ peer_id: Int64) {
        let str = String(indexPath.section) + "_" + String(indexPath.row)
        if imageURL != "" && loadingImages.insert(str).inserted {
            //NSLog("loadImageForIndex \(indexPath)")
            
            let loadImageOperation = LoadImageOperation(withIndexPath: indexPath, imageURL: imageURL, tag: tag, peer_id: peer_id, imageLoadedListener: self.imageLoadedListener)
            
            loadImageOperation.completionBlock = { [weak self] in
                let str = String(loadImageOperation.indexPath.section) + "_" + String(loadImageOperation.indexPath.row)
                self?.loadingImages.remove(str)
            }
            
            self.queueManager.addOperation(loadImageOperation)
        }
    }
    
    func cancelLoadingImageForIndex(_ indexPath : IndexPath) {
        queueManager.cancelOperation(withIndexPath: indexPath)
    }
    
    func cancelLoadingAllImage() {
        queueManager.cancelAllOperation()
        loadingImages.removeAll()
    }
    
}
