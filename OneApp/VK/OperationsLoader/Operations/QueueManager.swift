//
//  LoadImagesOperationsQueue.swift
//  OperationsLoader
//
//  Created by NCHD on 7/11/18.
//  Copyright © 2018 NCHD. All rights reserved.
//

import Foundation

class QueueManager {
    
    private let operationQueue: OperationQueue = OperationQueue()
    
    init() {
        operationQueue.maxConcurrentOperationCount = 1
    }
    
    func addOperation(_ operation: Operation) {
        operationQueue.addOperation(operation)
    }
    
    func cancelOperation(withIndexPath indexPath : IndexPath) {
        for operation in operationQueue.operations {
            if let loadImageOperation = operation as? LoadImageOperation {
                if loadImageOperation.indexPath == indexPath {
                    NSLog("cancelOperation \(indexPath)")
                    loadImageOperation.cancel()
                }
            }
        }
    }
    
    func cancelAllOperation() {
        for operation in operationQueue.operations {
            if let loadImageOperation = operation as? LoadImageOperation {
                NSLog("cancelOperation \(loadImageOperation.indexPath)")
                loadImageOperation.cancel()
            }
        }
    }
}
