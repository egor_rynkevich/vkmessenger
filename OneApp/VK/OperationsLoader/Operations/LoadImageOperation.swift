//
//  LoadImageOperation.swift
//  OperationsLoader
//
//  Created by NCHD on 7/11/18.
//  Copyright © 2018 NCHD. All rights reserved.
//

import UIKit
import RealmSwift

class LoadImageOperation: ImageOperation {

    var indexPath: IndexPath
    var imageURL: String
    var peer_id: Int64
    var tag: String
    weak var imageLoadedListener : ImageLoadedListener?
    
    init(withIndexPath indexPath: IndexPath, imageURL: String, tag: String, peer_id: Int64, imageLoadedListener : ImageLoadedListener?) {
        self.indexPath = indexPath
        self.imageURL = imageURL
        self.peer_id = peer_id
        self.tag = tag
        self.imageLoadedListener = imageLoadedListener
    }
    
    override func main() {
        guard
            isCancelled == false
            else {
            //NSLog("Operation \(indexPath) cancelled")
            finish(true)
            return
        }
        
        if imageLoadedListener != nil {
            imageLoadedListener?.isNeedToLoadImageWithIndex(indexPath, completion: { (isNeeded) in
                DispatchQueue.global().async {
                    if isNeeded {
                        self.executing(true)
                        //NSLog("Operation \(self.indexPath) is running...")
                        
                        if self.imageURL == "" {
                            self.executing(false)
                            self.finish(true)
                            return
                        }
                        
                        if self.tag == "ava" {
                            
                            if self.imageURL.contains("deactivated") == true {
                                self.imageLoadedListener?.avatarLoaded(peer_id: self.peer_id, data: mainIcons.deactivatedNSData)
                                self.executing(false)
                                self.finish(true)
                                return
                            }
                            
                            
                            let predicate = NSPredicate(format: "cachedDataID = %@ AND type = %@ AND tag = 'ava' AND url = %@",argumentArray:[self.peer_id, "image", self.imageURL])
                            let realmAsync = try! Realm()
                            let image = realmAsync.objects(VKCachedData.self).filter(predicate).first
                            
                            if image == nil {
                                usleep(200_000)
                                DispatchQueue.global().async {
                                    vkDataCache.loadImage(imageID: self.peer_id, type: "image", tag: "ava", url: self.imageURL) { (result) in
                                        if result != nil {
                                            self.imageLoadedListener?.avatarLoaded(peer_id: self.peer_id, data: result)
                                        }
                                        else {
                                            self.imageLoadedListener?.avatarLoaded(peer_id: self.peer_id, data: nil)
                                        }
                                    }
                                }
                            }
                            else {
                                self.imageLoadedListener?.avatarLoaded(peer_id: self.peer_id, data: image?.data)
                            }
                        } else {
                            let predicate = NSPredicate(format: "cachedDataID = %@ AND type = %@ AND tag = %@ AND url = %@",argumentArray:[self.peer_id, "image", self.tag, self.imageURL])
                            let realmAsync = try! Realm()
                            let image = realmAsync.objects(VKCachedData.self).filter(predicate).first
                            
                            if image == nil {
                                usleep(200_000)
                                DispatchQueue.global().async {
                                    vkDataCache.loadImage(imageID: self.peer_id, type: "image", tag: self.tag, url: self.imageURL) { (result) in
                                        if result != nil {
                                            self.imageLoadedListener?.imageLoaded(withIndexPath: self.indexPath, data: result)
                                        }
                                        else {
                                            self.imageLoadedListener?.imageLoaded(withIndexPath: self.indexPath, data: nil)
                                        }
                                    }
                                }
                            }
                            else {
                                self.imageLoadedListener?.imageLoaded(withIndexPath: self.indexPath, data: image?.data)
                            }
                        }
                        
                        
                        
                        //NSLog("Operation \(self.indexPath) is finishing...")
                    }
                    self.executing(false)
                    self.finish(true)
                }
            })
        }
        else {
            self.executing(false)
            self.finish(true)
        }
    }
    
}
