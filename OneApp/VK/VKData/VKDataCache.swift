//
//  VKDataCache.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 07.05.2018.
//

import Foundation
import RealmSwift

class VKDataCache{
    var bgRealm: Realm?
    var isRealmInit : Bool = false
    let BG_SERIAL_QUEUE_ID = "VK_QUEUE"
    let BG_IMAGE_QUEUE_ID = "VK_IMAGE_QUEUE"
    let vkBGQueue: DispatchQueue?
    let vkBGImageQueue: DispatchQueue?
    
    var conversations: Results<VKConversation>!
    var cachedData: Results<VKCachedData>!
    //var conversRef: ThreadSafeReference<Results<VKConversation>>?
    
    var cachedUser: [Int64 : VKUser] = [:]
    var cachedGroup: [Int64 : VKGroup] = [:]
    var cachedChat: [Int64 : VKChat] = [:]
    var cachedProfileImg: [Int64 : NSData?] = [:]
    
    init() {
        let realmURL = Realm.Configuration.defaultConfiguration.fileURL ?? URL(string: "")
        let realmURLs = [
            realmURL,
            realmURL?.appendingPathExtension("lock"),
            realmURL?.appendingPathExtension("note"),
            realmURL?.appendingPathExtension("management")
        ]
        for URL in realmURLs {
            do {
                try FileManager.default.removeItem(at: URL!)
            } catch {
                // handle error
            }
        }
        
        vkBGQueue = DispatchQueue.init(label: BG_SERIAL_QUEUE_ID)
        vkBGImageQueue = DispatchQueue.init(label: BG_IMAGE_QUEUE_ID)
        
        bgRealm = try! Realm()
        
        conversations = bgRealm?.objects(VKConversation.self).sorted(byKeyPath: "lastMsgDate", ascending: false)
        cachedData = bgRealm?.objects(VKCachedData.self)
    }
    
    class func getRealmConfig() -> Realm.Configuration {
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 1,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 1) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
        })
        
        return config
    }
    
    //MARK: Work with VKConversation table
    
    func saveConversationsInCache(_ vkConversations: [VKConversation], completion: @escaping (_:Bool) -> Void) {
        DispatchQueue.main.async { self.bgRealm?.refresh() }
        vkBGQueue?.async {
            autoreleasepool {
                let realm = try! Realm()
                let conversFrmCache = realm.objects(VKConversation.self)
                for convers in vkConversations {
                    do {
                        let conversCh = conversFrmCache.filter("peer_id = %@", convers.peer_id).first
                        if conversCh != nil {
                            
                            if convers.isEqual(conversCh) == false {
                                convers.updateData(realm, conversCh!)
                            }
                        } else {
                            try realm.write {
                                realm.create(VKConversation.self, value: convers, update: true)
                            }
                        }
                    }
                    catch let error as NSError {
                        print("realm didn't write convers - \(error)")
                        completion(false)
                    }
                }
                completion(true)
                //return
                //realm.refresh()
                //let convers = realm.objects(VKConversation.self)
                //self.conversRef = ThreadSafeReference(to: convers)
            }
        }
    }
    
    func getVKConversations(count: Int, isNeedReload: Bool) -> [VKConversation] {
        var conversations : [VKConversation] = []
        
        if self.conversations == nil {
            return conversations
        }
        
        if self.conversations.count < count || isNeedReload == true {
            bgRealm?.refresh()
            self.conversations = bgRealm?.objects(VKConversation.self).sorted(byKeyPath: "lastMsgDate", ascending: false)
        }
        
        for i in 0..<count {
            
            if self.conversations.count <= i {
                return conversations
            }
            
            conversations.append(self.conversations[i])
        }
        return conversations
    }
    
    //MARK: Work with VKUser table
    
    func saveUserInfoInCache(_ usersInfo: [Int64 : VKUser], completion: @escaping (_:Int) -> Void) {
        DispatchQueue.main.async { self.bgRealm?.refresh() }
        vkBGQueue?.async {
            autoreleasepool {
                let realm = try! Realm()
                let conversations = realm.objects(VKConversation.self)
                let usersInfoCache = realm.objects(VKUser.self)
                
                for (key, value) in usersInfo{
                    let convers = conversations.filter("peer_id = %@", key).first
                    let haveConvInDB : Bool = convers == nil ? false : true
                    var needSaveInConvers: Bool = false
                    
                    value.prntID = haveConvInDB == true ? convers!.conversationID : "-"
                    
                    do {
                        let usInf = usersInfoCache.filter("id = %@", value.id).first
                        
                        if usInf != nil {                            
                            if (haveConvInDB && convers!.userInfo == nil) || value.isEqual(usInf) == false {
                                value.updateData(realm: realm, cachedUser: usInf)
                                needSaveInConvers = true
                            }
                        } else {
                            try realm.write{
                                realm.create(VKUser.self, value:value, update: true)
                                needSaveInConvers = true
                            }
                        }
                        
                        if needSaveInConvers == true && haveConvInDB == true {
                            try realm.write {
                                convers!.userInfo = realm.object(ofType: VKUser.self, forPrimaryKey: value.VKUserID )
                            }
                        }
                    }
                    catch let error as NSError {
                        print("realm didn't write user info - \(error)")
                        completion(0)
                    }
                }
                completion(1)
                return
            }
        }
    }
    
    func changeFriendStatus(user_id: Int64, isBan: Bool = false) {
        DispatchQueue.main.async { self.bgRealm?.refresh() }
        vkBGQueue?.async {
            let realm = try! Realm()
            if let user = realm.objects(VKUser.self).filter("id = %@", user_id).first {
                try! realm.write{
                    if isBan == true {
                        user.blacklisted_by_me = true
                    }
                    user.is_friend = false
                    user.friend_status = 0
                }
            }
        }
    }
    
    func changeFriendsCounts(friends_count: Int) {
        DispatchQueue.main.async { self.bgRealm?.refresh() }
        vkBGQueue?.async {
            let realm = try! Realm()
            if let user = realm.objects(VKUser.self).filter("isMainUser = true").first {
                try! realm.write { user.counters_friends = friends_count }
            }
        }
    }
    
    func loadUserFriends() -> [VKUser] {
        let time = CFAbsoluteTimeGetCurrent()
        self.bgRealm?.refresh()
        var friends: [VKUser] = []
        let users = self.bgRealm?.objects(VKUser.self).filter("is_friend = true")
        
        if users != nil {
            for user in users! {
                let userCopy = VKUser()
                userCopy.copyValue(user)
                friends.append(userCopy)
            }
        }
        
        friends.sort(by: { (user1, user2) -> Bool in
            user1.last_name < user2.last_name
        })
        
        print("loadUserFriends - \(CFAbsoluteTimeGetCurrent() - time)")
        return friends
    }
    
    //MARK: Work with VKGroup table
    
    func saveGroupInfoInCache(_ groupsInfo: [Int64 : VKGroup], completion: @escaping (_:Int) -> Void) {
        DispatchQueue.main.async { self.bgRealm?.refresh() }
        vkBGQueue?.async {
            autoreleasepool {
                let realm = try! Realm()
                
                let conversations = realm.objects(VKConversation.self)
                let groupsInfoCache = realm.objects(VKGroup.self)
                
                for (key, value) in groupsInfo {
                    let convers = conversations.filter("peer_id = %@", key).first
                    let haveConvInDB : Bool = convers == nil ? false : true
                    var needSaveInConvers: Bool = false
                    
                    do {
                        let grpInf = groupsInfoCache.filter("id = %@", value.id).first
                        
                        if grpInf != nil {
                            let isInfoEqual = value.isEqual(grpInf)
                            
                            if (haveConvInDB && convers!.groupInfo == nil) || isInfoEqual == false {
                                value.updateData(realm: realm, cachedGroup: grpInf)
                                needSaveInConvers = true
                            }
                        } else {
                            try realm.write {
                                value.prntID = haveConvInDB == true ? convers!.conversationID : "-"
                                realm.create(VKGroup.self, value:value, update: true)
                                needSaveInConvers = true
                            }
                        }
                        
                        if needSaveInConvers == true && haveConvInDB == true {
                            try realm.write {
                                convers!.groupInfo = realm.object(ofType: VKGroup.self, forPrimaryKey: value.VKGroupID)
                            }
                        }
                    }
                    catch let error as NSError {
                        print("realm didn't write group info - \(error)")
                        completion(0)
                    }
                }
                completion(1)
                //return
            }
        }
    }
    
    //MARK: Work with VKChat table
    
    func saveChatInfoInCache(_ chatsInfo: [Int64 : VKChat], completion: @escaping (_:Int) -> Void) {
        DispatchQueue.main.async { self.bgRealm?.refresh() }
        vkBGQueue?.async {
            autoreleasepool {
                let realm = try! Realm()
                
                let conversations = realm.objects(VKConversation.self)
                let chatsInfoCache = realm.objects(VKChat.self)
                
                for (key, value) in chatsInfo {
                    let convers = conversations.filter("peer_id = %@", key).first
                    let haveConvInDB : Bool = convers == nil ? false : true
                    var needSaveInConvers: Bool = false
                    
                    do {
                        let chtInf = chatsInfoCache.filter("id = %@", value.id).first
                        
                        if chtInf != nil {
                            let isInfoEqual = value.isEqual(chtInf)
                            
                            if (haveConvInDB && convers!.chatInfo == nil) || isInfoEqual == false {
                                value.updateData(realm: realm, cachedChat: chtInf)
                                needSaveInConvers = true
                            }
                        } else {
                            try realm.write {
                                value.prntID = haveConvInDB == true ? convers!.conversationID : "-"
                                realm.create(VKChat.self, value:value, update: true)
                                needSaveInConvers = true
                            }
                        }
                        
                        if needSaveInConvers == true && haveConvInDB == true {
                            try realm.write {
                                convers!.chatInfo = realm.object(ofType: VKChat.self, forPrimaryKey: value.VKChatID)
                            }
                        }
                    }
                    catch let error as NSError {
                        print("realm didn't write chat info - \(error)")
                        completion(0)
                    }
                }
                completion(1)
                //return
            }
        }
    }
    
    //MARK: Work with Message table
    
    func saveMsgInCache(_ msgs: [VKMessage], _ convRef: ThreadSafeReference<VKConversation>, completion: @escaping (_:Bool) -> Void ){
        DispatchQueue.main.async { self.bgRealm?.refresh() }
        vkBGQueue?.async {
                let realm = try! Realm()
                
                guard let convers = realm.resolve(convRef) else {
                    return
                }
                
                let msgsCache = realm.objects(VKMessage.self)
                
                for msg in msgs {
                    let msgCh = msgsCache.filter("peer_id = %@ AND id = %@ AND isPreview = %@", msg.peer_id, msg.id, false).first
                    do {
                        if msgCh != nil && (msgCh?.date) != 0 {
                            if msg.isEqual(msgCh) == false {
                                msg.updateData(realm, msgCh!)
                            }
                        } else {
                            try realm.write({ () -> Void in
                                if convers.last_message == nil {
                                    let msgPrev = VKMessage()
                                    msgPrev.copyValue(msg)
                                    msgPrev.isPreview = true
                                    msgPrev.prntID = convers.conversationID
                                    convers.last_message = msgPrev
                                    convers.lastMsgDate = msgPrev.date
                                } else {
                                    msg.prntID = convers.conversationID
                                    realm.create(VKMessage.self, value:msg, update: true)
                                    let realmMsg = realm.object(ofType: VKMessage.self, forPrimaryKey: msg.messageID)
                                    convers.messages.append(realmMsg!)
                                }
                            })
                        }
                        
                    }
                    catch let error as NSError {
                        print("realm didn't write msg - \(error)")
                        completion(false)
                    }
                }
                completion(true)
            
        }
    }
    
    func addNewMsgInCache(_ msg : VKMessage, completion: @escaping (_:Bool) -> Void) {
        DispatchQueue.main.async { self.bgRealm?.refresh() }
        vkBGQueue?.async{
            let realm = try! Realm()
            
            let convCh = realm.object(ofType: VKConversation.self, forPrimaryKey: msg.prntID)
            
            let msgCh = convCh?.messages.filter("peer_id = %@ AND isPreview = %@ AND random_id = %@", msg.peer_id, false, msg.random_id).first
            do {
                if msgCh != nil && (msgCh?.date) != 0 {
                    if msg.isEqual(msgCh) == false {
                        msg.updateData(realm, msgCh!)
                        msg.updateData(realm, (convCh?.last_message)!)
                        
                    }
                } else {
                    try realm.write({ () -> Void in
                        realm.create(VKMessage.self, value:msg, update: true)
                        let realmMsg = realm.object(ofType: VKMessage.self, forPrimaryKey: msg.messageID)
                        convCh?.messages.append(realmMsg!)
                    })
                }
                completion(true)
            }
            catch let error as NSError {
                print("realm didn't write msg - \(error)")
                completion(false)
            }
        }
    }
    
    func updateMsg(_ msg : VKMessage, completion: @escaping (_:Bool) -> Void) {
        DispatchQueue.main.async { self.bgRealm?.refresh() }
        vkBGQueue?.async{
            let realm = try! Realm()
            
            let convCh = realm.object(ofType: VKConversation.self, forPrimaryKey: msg.prntID)
            
            let msgCh = convCh?.messages.filter("peer_id = %@ AND isPreview = %@ AND id = %@", msg.peer_id, false, msg.id).first
            do {
                if msgCh != nil && (msgCh?.date) != 0 {
                    msg.date = msgCh?.date ?? msg.date
                    msg.random_id = msgCh?.random_id ?? msg.random_id
                    if msg.isEqual(msgCh) == false {
                        msg.updateData(realm, msgCh!)
                        msg.updateData(realm, (convCh?.last_message)!)
                        
                    }
                } else {
                    try realm.write({ () -> Void in
                        realm.create(VKMessage.self, value:msg, update: true)
                        let realmMsg = realm.object(ofType: VKMessage.self, forPrimaryKey: msg.messageID)
                        convCh?.messages.append(realmMsg!)
                    })
                }
                completion(true)
            }
            catch let error as NSError {
                print("realm didn't write msg - \(error)")
                completion(false)
            }
        }
    }
    
    //MARK: Work with poll
    
    func updatePoll(poll_id: Int64, answerIDs: [Int64], isDelete: Bool) {
        let realm = try! Realm()
        if let poll = realm.objects(VKPoll.self).filter("id = %@", poll_id).first {
            try! realm.write {
                if isDelete == true {
                    poll.votes -= 1
                    for answer in poll.answers {
                        if poll.answer_ids.contains(answer.id) {
                            answer.votes -= 1
                            answer.rate = Float(answer.votes) / Float(poll.votes) * Float(100)
                        }
                    }
                    
                    poll.answer_ids.removeAll()
                } else {
                    poll.votes += 1
                    
                    for id in answerIDs { poll.answer_ids.append(id) }
                    
                    for answer in poll.answers {
                        if poll.answer_ids.contains(answer.id) {
                            answer.votes += 1
                            answer.rate = Float(answer.votes) / Float(poll.votes) * Float(100)
                        }
                    }
                }
            }
            realm.refresh()
        }
    }
    
    //MARK: Work with CachedData table
    
    func getProfileImageFromCache(convers: VKConversation, completion: @escaping (_:NSData?) -> Void) {
            do {
                var url: String = ""
                switch convers.peer_type {
                case VKUserType.isChat.rawValue:
                    url = convers.chatInfo?.photo_100 ?? ""
                    break
                case VKUserType.isUser.rawValue:
                    url = convers.userInfo?.photo_100 ?? ""
                    break
                case VKUserType.isGroup.rawValue:
                    url = convers.groupInfo?.photo_100 ?? ""
                    break
                default:
                    break
                }
                
                if url == "" { return }
                
                let predicate = NSPredicate(format: "cachedDataID = %@ AND type = %@ AND tag = %@ AND url = %@",argumentArray:[convers.peer_id, "image", "ava", url])
                let realmAsync = try Realm()
                let image = realmAsync.objects(VKCachedData.self).filter(predicate).first
                
                if image != nil{
                    switch convers.peer_type {
                    case VKUserType.isChat.rawValue:
                        let chatCh = realmAsync.objects(VKChat.self).filter("id = %@", convers.peer_id).first
                        if chatCh?.photo100Data == nil { try realmAsync.write { chatCh?.photo100Data = image?.data } }
                        break
                    case VKUserType.isUser.rawValue:
                        let userCh = realmAsync.objects(VKUser.self).filter("id = %@", convers.peer_id).first
                        if userCh?.photo100Data == nil { try realmAsync.write { userCh?.photo100Data = image?.data } }
                        break
                    case VKUserType.isGroup.rawValue:
                        let groupCh = realmAsync.objects(VKGroup.self).filter("id = %@", convers.peer_id).first
                        if groupCh?.photo100Data == nil { try realmAsync.write { groupCh?.photo100Data = image?.data } }
                        break
                    default:
                        break
                    }
                    completion(image?.data ?? nil)
                }
                else {
                    bgRealm?.refresh()
                    vkBGImageQueue?.async{
                        self.loadImage(imageID: convers.peer_id, type: "image", tag: "ava", url: url) { (result) in
                        if result != nil {
                            completion(result)
                        }
                        else {
                            completion(nil)
                        }
                    }
                    }
                }
            } catch let error as NSError{
                print("image load faild - \(error)")
                completion(nil)
            }
            
        
    }
    
    func getImageFromCache(_ imageID: Int64, _ type: String, _ tag: String, _ url: String, completion: @escaping (_:NSData?) -> Void) {
        
            do {
                let predicate = NSPredicate(format: "cachedDataID = %@ AND type = %@ AND tag = %@ AND url = %@",argumentArray:[ imageID, type, tag, url])
                let realmAsync = try Realm()
                let image = realmAsync.objects(VKCachedData.self).filter(predicate).first
                
                if image != nil{
                    completion(image?.data ?? nil)
                }
                else {
                    vkBGImageQueue?.async{
                        self.loadImage(imageID: imageID, type: type, tag: tag, url: url) { (result) in
                            if result != nil {
                                completion(result)
                            }
                            else {
                                completion(nil)
                            }
                        }
                    }
                }
            } catch let error as NSError{
                print("image load faild - \(error)")
                completion(nil)
            }
        
    }
    
    func loadImage(imageID: Int64, type: String, tag: String, url: String, completion: @escaping (_:NSData?) -> Void) {
        do {
            let urlData = URL(string: url)
            let imageData: Data = try Data(contentsOf : urlData!)
            //DispatchQueue.global(qos: .background).async {
                do {
                    let realmAsync = try Realm()
                    let cache = VKCachedData(value: [imageID, imageID, type, tag, url, imageData, Int(Date().timeIntervalSince1970)])
                    
                    if tag == "ava" {
                        if imageID > 2_000_000_000 {
                            let chatCh = realmAsync.objects(VKChat.self).filter("id = %@", imageID).first
                            if chatCh?.photo100Data == nil {
                                try realmAsync.write {
                                    chatCh?.photo100Data = imageData as NSData
                                }
                            }
                        } else if imageID < 0 {
                            let groupCh = realmAsync.objects(VKGroup.self).filter("id = %@", imageID).first
                            if groupCh?.photo100Data == nil {
                                try realmAsync.write {
                                    groupCh?.photo100Data = imageData as NSData
                                }
                            }
                        } else {
                            let userCh = realmAsync.objects(VKUser.self).filter("id = %@", imageID).first
                            if userCh?.photo100Data == nil {
                                try realmAsync.write {
                                    userCh?.photo100Data = imageData as NSData
                                }
                            }
                        }
                    }
                    
                    try realmAsync.write{
                        realmAsync.add(cache, update: true)
                    }
                    realmAsync.refresh()
                    completion(imageData as NSData)
                }
                catch let error as NSError{
                    print("image load faild - \(error)")
                    completion(nil)
                }
            //}
        }
        catch let error as NSError{
            print("image load faild - \(error)")
            completion(nil)
        }
    }
    
    func saveNSDataInCachedData(sourceID: Int64, ownerID: Int64, type: String, tag: String, url: String, data: NSData) {
        do {
            let realmAsync = try Realm()
            let cache = VKCachedData(value: [sourceID, ownerID, type, tag, url, data, Int(Date().timeIntervalSince1970)])
            try realmAsync.write{
                realmAsync.add(cache, update: true)
            }
            realmAsync.refresh()
        } catch let error as NSError{
            print("saveNSDataInCachedData faild - \(error)")
        }
    }
    
    func saveDataInCachedData(sourceID: Int64, ownerID: Int64, type: String, tag: String, url: String, image: UIImage) {
        let data = image.jpegData(compressionQuality: 1)! as NSData //  .pngData()! as NSData
        saveNSDataInCachedData(sourceID: sourceID, ownerID: ownerID, type: type, tag: tag, url: url, data: data)
    }
    
    func getDataFromCachedData(sourceID: Int64, ownerID: Int64, type: String, tag: String) -> NSData? {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "cachedDataID = %@ AND cachedDataOwnerID = %@ AND type = %@ AND tag = %@" ,argumentArray:[sourceID, ownerID, type, tag])
        let data = realm.objects(VKCachedData.self).filter(predicate).first
        return data?.data
    }
    
    func removeDataFromCachedData(sourceID: Int64, ownerID: Int64, type: String, tag: String) {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "cachedDataID = %@ AND cachedDataOwnerID = %@ AND type = %@ AND tag = %@" ,argumentArray:[sourceID, ownerID, type, tag])
        let data = realm.objects(VKCachedData.self).filter(predicate).first
        
        if data != nil {
            try! realm.write { realm.delete(data!) }
        }
    }
    
    func getUsernameByIDAndType(id: Int64, type: String) -> String {
        //bgRealm?.refresh()
        var username = ""
        switch type {
        case VKUserType.isChat.rawValue:
            //if let chat = self.cachedChat[id] { username = chat.title }
            //else {
                do {
                    let realm = try Realm()
                    if let newChat = realm.objects(VKChat.self).filter("id = %@", id).first {
                        let chatCopy = VKChat()
                        chatCopy.copyValue(newChat)
                        //self.cachedChat[id] = chatCopy
                        username = chatCopy.title
                    }
                } catch let error as NSError { print(error) }
            //}
            return username
        case VKUserType.isGroup.rawValue:
            //if let group = self.cachedGroup[id] { username = group.name }
            //else {
                do {
                    let realm = try Realm()
                    if let newGroup = realm.objects(VKGroup.self).filter("id = %@", id).first {
                        let groupCopy = VKGroup()
                        groupCopy.copyValue(newGroup)
                        //self.cachedGroup[id] = groupCopy
                        username = groupCopy.name
                    }
                } catch let error as NSError { print(error) }
            //}
            return username
        case VKUserType.isUser.rawValue:
            //if let user = self.cachedUser[id] { username = user.first_name + " " + user.last_name }
            //else {
                do {
                    let realm = try Realm()
                    if let newUser = realm.objects(VKUser.self).filter("id = %@", id).first {
                        let userCopy = VKUser()
                        userCopy.copyValue(newUser)
                        //self.cachedUser[id] = userCopy
                        username = userCopy.first_name + " " + userCopy.last_name
                    }
                } catch let error as NSError { print(error) }
            //}
            return username
        default:
            return username
        }
        
    }
    
    func getProfileImgFromCache(sourceID: Int64, ownerID: Int64, type: String, tag: String) -> NSData? {
        var imgData: NSData? = cachedProfileImg[ownerID] ?? nil
        
        if imgData == nil {
            imgData = getDataFromCachedData(sourceID: sourceID, ownerID: ownerID, type: type, tag: tag)
            if imgData != nil { cachedProfileImg[ownerID] = imgData }
        }
        
        return imgData
    }
    
    func getProfileURLFromCache(sourceID: Int64, ownerID: Int64, type: String, tag: String) -> String {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "cachedDataID = %@ AND cachedDataOwnerID = %@ AND type = %@ AND tag = %@" ,argumentArray:[sourceID, ownerID, type, tag])
        let data = realm.objects(VKCachedData.self).filter(predicate).first
        return data?.url ?? ""
    }
    
    func getUserShortNameByIDAndType(id: Int64, type: String) -> String {
        //bgRealm?.refresh()
        var username = ""
        switch type {
        case VKUserType.isChat.rawValue:
            //if let chat = self.cachedChat[id] { username = String(chat.title.prefix(1)) }
            //else {
                do {
                    let realm = try Realm()
                    if let newChat = realm.objects(VKChat.self).filter("id = %@", id).first {
                        let chatCopy = VKChat()
                        chatCopy.copyValue(newChat)
                        //self.cachedChat[id] = chatCopy
                        username = String(chatCopy.title.prefix(1))
                    }
                    
                } catch let error as NSError { print(error) }
            //}
            return username
        case VKUserType.isGroup.rawValue:
            //if let group = self.cachedGroup[id] { username = String(group.name.prefix(1)) }
            //else {
                do {
                    let realm = try Realm()
                    if let newGroup = realm.objects(VKGroup.self).filter("id = %@", id).first {
                        let groupCopy = VKGroup()
                        groupCopy.copyValue(newGroup)
                        //self.cachedGroup[id] = groupCopy
                        username = String(groupCopy.name.prefix(1))
                       
                    }
                } catch let error as NSError { print(error) }
            //}
            return username
        case VKUserType.isUser.rawValue:
            //if let user = self.cachedUser[id] { username = String(user.first_name.prefix(1) + user.last_name.prefix(1)) }
            //else {
                do {
                    let realm = try Realm()
                    if let newUser = realm.objects(VKUser.self).filter("id = %@", id).first {
                        let userCopy = VKUser()
                        userCopy.copyValue(newUser)
                        //self.cachedUser[id] = userCopy
                        username = String(userCopy.first_name.prefix(1) + userCopy.last_name.prefix(1))
                        
                    }
                } catch let error as NSError { print(error) }
            //}
            return username
        default:
            return username
        }
        
    }
}
