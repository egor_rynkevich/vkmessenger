//
//  VKData.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 04.04.2018.
//

import RealmSwift
import UIKit
import SwiftyJSON
import DifferenceKit

class VKCachedData: Object {
    @objc dynamic var cachedDataID: Int64 = 0
    @objc dynamic var cachedDataOwnerID: Int64 = 0
    @objc dynamic var type: String = ""
    @objc dynamic var tag: String = ""
    @objc dynamic var url: String = ""
    @objc dynamic var data: NSData?
    @objc dynamic var createDate: Int64 = 0
    
    @objc dynamic var cacheID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "cacheID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKCachedData {
            if self.cachedDataID == object.cachedDataID &&
                self.cachedDataOwnerID == object.cachedDataOwnerID &&
                self.createDate == object.createDate &&
                self.tag == object.tag &&
                self.type == object.type &&
                self.url == object.url {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
}

enum VKUserType: String {
    case isUser = "user"
    case isGroup = "group"
    case isChat = "chat"
    case isEmail = "email"
}

enum CantWriteReason: Int {
    case UserBlockedOrDeleted = 18
    case UserInBlackList = 900
    case UserBlockMsgFromGroup = 901
    case UserBlockSendHimMsgInPrivacySettings = 902
    case InGroupDisableMsg = 915
    case InGroupBlockMsg = 916
    case DontHaveAccessToChat = 917
    case DontHaveAccessToEmail = 918
    case DontHaveAccessToGroup = 203
}

class VKUser: Object{
    @objc dynamic var id : Int64 = 0
    @objc dynamic var first_name : String = ""
    @objc dynamic var last_name : String = ""
    
    @objc dynamic var online: Bool = false
    @objc dynamic var online_mobile: Bool = false
    @objc dynamic var last_seen_time: Int64 = 0
    @objc dynamic var last_seen_platform: Int8 = 0
    @objc dynamic var photo_100: String = ""
    @objc dynamic var photo100Data: NSData? = nil
    @objc dynamic var crop_photo_bottom: Int = 0
    @objc dynamic var crop_photo_top: Int = 0
    @objc dynamic var photo_600: String = ""
    
    @objc dynamic var deactivated: String = ""
    @objc dynamic var bdate: String = ""
    @objc dynamic var blacklisted: Bool = false
    @objc dynamic var blacklisted_by_me: Bool = false
    @objc dynamic var can_write_private_message: Bool = true
    @objc dynamic var contacts_mobile_phone: String = ""
    @objc dynamic var contacts_home_phone: String = ""
    @objc dynamic var counters_friends: Int = 0
    @objc dynamic var counters_online_friends: Int = 0
    @objc dynamic var counters_mutual_friends: Int = 0
    @objc dynamic var friend_status: Int8 = 0 // 0 - not friend, 1 - send request to user, 2 - have incoming request from user, 3 - friend
    @objc dynamic var has_mobile: Bool = false
    @objc dynamic var is_friend: Bool = false
    

    @objc dynamic var isMainUser: Bool = false
    
    @objc dynamic var VKUserID = UUID().uuidString
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        id = json["id"].int64 ?? 0
        first_name = json["first_name"].string ?? ""
        last_name = json["last_name"].string ?? ""
        deactivated = json["deactivated"].string ?? ""
        bdate = json["bdate"].string ?? ""
        blacklisted = (json["blacklisted"].int ?? 0) == 1 ? true : false
        blacklisted_by_me = (json["blacklisted_by_me"].int ?? 0) == 1 ? true : false
        can_write_private_message = (json["can_write_private_message"].int ?? 0) == 1 ? true : false
        contacts_mobile_phone = json["mobile_phone"].string ?? ""
        contacts_home_phone = json["home_phone"].string ?? ""
        counters_friends = json["counters"]["friends"].int ?? 0
        counters_online_friends = json["counters"]["online_friends"].int ?? 0
        counters_mutual_friends = json["counters"]["mutual_friends"].int ?? 0
        friend_status = json["friend_status"].int8 ?? 0
        has_mobile = (json["has_mobile"].int ?? 0) == 1 ? true : false
        is_friend = (json["is_friend"].int ?? 0) == 1 ? true : false
        online = (json["online"].int ?? 0) == 0 ? false : true
        online_mobile = (json["online_mobile"].int ?? 0) == 0 ? false : true
        last_seen_time = json["last_seen"]["time"].int64 ?? 0
        last_seen_platform = json["last_seen"]["platform"].int8 ?? 0
        photo_100 = json["photo_100"].string ?? ""
        
        let sizes = json["crop_photo"]["photo"]["sizes"].array ?? []
        
        let obj = /*(json["crop_photo"]["crop"]["y"].float ?? 0.0) == 0.0 && (json["crop_photo"]["crop"]["y2"].float ?? 100.0) == 100.0 ?*/ "rect" /*: "crop"*/
        
        for size in sizes {
            let height = size["height"].int ?? 0
            let type = size["type"].string ?? ""
            if height >= 580  || type == "r" {
                photo_600 = size["url"].string ?? ""
                crop_photo_bottom = Int((100 - (json["crop_photo"][obj]["y2"].float ?? 0)) * Float(height / 100))
                crop_photo_top = Int((json["crop_photo"][obj]["y"].float ?? 0) * Float(height / 100))
                break
            }
        }
        
        prntID = prnt_id
    }
    
    override static func primaryKey() -> String? {
        return "VKUserID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKUser {
            if self.id == object.id &&
                self.first_name == object.first_name &&
                self.last_name == object.last_name &&
                self.deactivated == object.deactivated &&
                self.online == object.online &&
                self.online_mobile == object.online_mobile &&
                self.last_seen_time == object.last_seen_time &&
                self.last_seen_platform == object.last_seen_platform &&
                self.bdate == object.bdate &&
                self.blacklisted == object.blacklisted &&
                self.blacklisted_by_me == object.blacklisted_by_me &&
                self.can_write_private_message == object.can_write_private_message &&
                self.contacts_mobile_phone == object.contacts_mobile_phone &&
                self.contacts_home_phone == object.contacts_home_phone &&
                self.counters_online_friends == object.counters_online_friends &&
                self.counters_friends == object.counters_friends &&
                self.counters_mutual_friends == object.counters_mutual_friends &&
                self.friend_status == object.friend_status &&
                self.has_mobile == object.has_mobile &&
                self.is_friend == object.is_friend &&
                self.photo_100 == object.photo_100 {
                
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
   
    func updateData(realm: Realm, cachedUser: VKUser?) {
        do {
            try realm.write({ () -> Void in
                if cachedUser != nil {
                    isMainUser = cachedUser!.isMainUser
                    if cachedUser!.counters_friends != 0 {
                        counters_friends = counters_friends == 0 ? cachedUser!.counters_friends : counters_friends
                    }
                    if cachedUser!.counters_online_friends != 0 {
                        counters_online_friends = counters_online_friends == 0 ? cachedUser!.counters_online_friends : counters_online_friends
                    }
                    prntID = cachedUser!.prntID == "-" ? prntID : cachedUser!.prntID
                    VKUserID = cachedUser!.VKUserID
                    //realm.delete(cachedUser!)
                } else {
                    prntID = "-"
                }
                realm.create(VKUser.self, value: self, update: true)
            })
        } catch let error as NSError {
            print("update userinfo failed - \(error)")
        }
    }
    
    func copyValue(_ userSource: VKUser) {
        self.id = userSource.id
        self.first_name = userSource.first_name
        self.last_name = userSource.last_name
        self.deactivated = userSource.deactivated
        self.online = userSource.online
        self.online_mobile = userSource.online_mobile
        self.last_seen_time = userSource.last_seen_time
        self.last_seen_platform = userSource.last_seen_platform
        self.bdate = userSource.bdate
        self.blacklisted = userSource.blacklisted
        self.blacklisted_by_me = userSource.blacklisted_by_me
        self.can_write_private_message = userSource.can_write_private_message
        self.contacts_mobile_phone = userSource.contacts_mobile_phone
        self.contacts_home_phone = userSource.contacts_home_phone
        self.counters_friends = userSource.counters_friends
        self.counters_online_friends = userSource.counters_online_friends
        self.counters_mutual_friends = userSource.counters_mutual_friends
        self.friend_status = userSource.friend_status
        self.has_mobile = userSource.has_mobile
        self.is_friend = userSource.is_friend
        self.photo_100 = userSource.photo_100
        self.photo_600 = userSource.photo_600
        self.crop_photo_bottom = userSource.crop_photo_bottom
        self.crop_photo_top = userSource.crop_photo_top
        self.photo100Data = userSource.photo100Data
        
        self.isMainUser = userSource.isMainUser
        
        self.VKUserID = userSource.VKUserID
        
        self.prntID = userSource.prntID
    }
}

class VKGroup: Object {
    @objc dynamic var id : Int64 = 0
    @objc dynamic var name : String = ""
    @objc dynamic var deactivated: String = ""
    @objc dynamic var is_admin: Bool = false
    @objc dynamic var photo_100: String = ""
    @objc dynamic var photo_200: String = ""
    @objc dynamic var is_messages_blocked : Bool = false
    @objc dynamic var can_message: Bool = true // can write msg
    
    @objc dynamic var photo100Data: NSData? = nil
    
    @objc dynamic var VKGroupID = UUID().uuidString
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        id = json["id"].int64 ?? 0
        name = json["name"].string ?? ""
        deactivated = json["deactivated"].string ?? ""
        is_admin = (json["is_admin"].int ?? 0) == 1 ? true : false
        can_message = (json["can_message"].int ?? 0) == 1 ? true : false
        is_messages_blocked = (json["is_messages_blocked"].int ?? 0) == 1 ? true : false
        photo_100 = json["photo_100"].string ?? ""
        photo_200 = json["photo_200"].string ?? ""
        
        prntID = prnt_id
    }
    
    override static func primaryKey() -> String? {
        return "VKGroupID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKGroup {
            if self.id == object.id &&
                self.name == object.name &&
                self.deactivated == object.deactivated &&
                self.is_admin == object.is_admin &&
                self.is_messages_blocked == object.is_messages_blocked &&
                self.can_message == object.can_message &&
                self.photo_100 == object.photo_100 &&
                self.photo_200 == object.photo_200 {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
    
    func updateData(realm: Realm, cachedGroup: VKGroup?) {
        do {
            try realm.write({ () -> Void in
                if cachedGroup != nil {
                    prntID = cachedGroup!.prntID
                    VKGroupID = cachedGroup!.VKGroupID
                    //realm.delete(cachedGroup!)
                } else {
                    prntID = "-"
                }
                realm.create(VKGroup.self, value: self, update: true)
            })
        } catch let error as NSError {
            print("update groupinfo failed - \(error)")
        }
    }
    
    func copyValue(_ groupSource: VKGroup) {
        self.id = groupSource.id
        self.name = groupSource.name
        self.deactivated = groupSource.deactivated
        self.is_admin = groupSource.is_admin
        self.is_messages_blocked = groupSource.is_messages_blocked
        self.can_message = groupSource.can_message
        self.photo_100 = groupSource.photo_100
        self.photo_200 = groupSource.photo_200
        self.photo100Data = groupSource.photo100Data
        
        self.VKGroupID = groupSource.VKGroupID
        self.prntID = groupSource.prntID
    }
    
}

class VKChat: Object {
    @objc dynamic var id : Int64 = 0
    @objc dynamic var type : String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var admin_id: Int64 = 0
    @objc dynamic var photo_100: String = ""
    @objc dynamic var photo_200: String = ""
    let users = List<Int64>()
    @objc dynamic var push_settings_sound: Bool = true
    @objc dynamic var push_settings_disabled_until: Int = 0
    @objc dynamic var left: Int8 = 0
    @objc dynamic var kicked: Int8 = 0
    
    @objc dynamic var photo100Data: NSData? = nil
    
    @objc dynamic var VKChatID = UUID().uuidString
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        id = json["id"].int64 ?? 0
        type = json["type"].string ?? ""
        title = json["title"].string ?? ""
        admin_id = json["admin_id"].int64 ?? 0
        push_settings_sound = (json["push_settings"]["sound"].int ?? 1) == 1 ? true : false
        push_settings_disabled_until = json["push_settings"]["disabled_until"].int ?? 0
        left = json["left"].int8 ?? 0
        kicked = json["kicked"].int8 ?? 0
        photo_100 = json["photo_100"].string ?? ""
        photo_200 = json["photo_200"].string ?? ""
        
        let usersJSON = json["users"].array ?? []
        
        for user in usersJSON {
            users.append(user.int64 ?? 0)
        }
        
        prntID = prnt_id
    }
    
    override static func primaryKey() -> String? {
        return "VKChatID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKChat {
            if self.id == object.id &&
                self.type == object.type &&
                self.title == object.title &&
                self.admin_id == object.admin_id &&
                self.push_settings_sound == object.push_settings_sound &&
                self.push_settings_disabled_until == object.push_settings_disabled_until &&
                self.left == object.left &&
                self.kicked == object.kicked &&
                self.photo_100 == object.photo_100 &&
                self.photo_200 == object.photo_200 {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
    
    func setUsers(_ list: List<Int64>) {
        self.users.removeAll()
    
        for i in 0..<list.count {
            self.users.append(list[i])
        }
    }
    
    func updateData(realm: Realm, cachedChat: VKChat?) {
        do {
            try realm.write({ () -> Void in
                if cachedChat != nil {
                    prntID = cachedChat!.prntID
                    VKChatID = cachedChat!.VKChatID
                    //realm.delete(cachedChat!)
                } else {
                    prntID = "-"
                }
                realm.create(VKChat.self, value: self, update: true)
            })
        } catch let error as NSError {
            print("update chatinfo failed - \(error)")
        }
    }
    
    func copyValue(_ chatSource: VKChat) {
        self.id = chatSource.id
        self.type = chatSource.type
        self.title = chatSource.title
        self.admin_id = chatSource.admin_id
        self.push_settings_disabled_until = chatSource.push_settings_disabled_until
        self.push_settings_sound = chatSource.push_settings_sound
        self.left = chatSource.left
        self.kicked = chatSource.kicked
        self.photo_100 = chatSource.photo_100
        self.photo_200 = chatSource.photo_200
        self.photo100Data = chatSource.photo100Data
        
        self.VKChatID = chatSource.VKChatID
        self.prntID = chatSource.prntID
        
        self.setUsers(chatSource.users)
    }
    
}

class VKConversation: Object, Differentiable {
    @objc dynamic var peer_id: Int64 = 0
    @objc dynamic var peer_type: String = ""
    @objc dynamic var peer_local_id: Int64 = 0
    @objc dynamic var in_read: Int64 = 0
    @objc dynamic var out_read: Int64 = 0
    @objc dynamic var unread_count: Int = 0
    @objc dynamic var important: Int = 0
    @objc dynamic var unanswered: Int = 0
    
    @objc dynamic var push_settings_disabled_until: Int64 = -1
    @objc dynamic var push_settings_no_sound: Bool = false
    @objc dynamic var push_settings_disabled_forever: Bool = false
    
    @objc dynamic var can_write_allowed: Bool = true
    @objc dynamic var can_write_reason: Int8 = 0
    
    
    @objc dynamic var chat_settings_members_count: Int = 0
    @objc dynamic var chat_settings_title: String = ""
    @objc dynamic var chat_settings_state: String = "" // in - in chat, kicked, left
    @objc dynamic var chat_settings_photo100: String = ""
    @objc dynamic var chat_settings_photo200: String = ""
    @objc dynamic var chat_settings_active_ids: String = ""
    @objc dynamic var chat_settings_is_group_channel: Bool = false

    @objc dynamic var last_message: VKMessage? = nil
    @objc dynamic var userInfo: VKUser? = nil
    @objc dynamic var groupInfo: VKGroup? = nil
    @objc dynamic var chatInfo: VKChat? = nil
    @objc dynamic var lastMsgDate: Int64 = 0
    
    let messages = List<VKMessage>()
    
    
    @objc dynamic var conversationID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "conversationID"
    }
    
    var differenceIdentifier: Int64 {
        return peer_id
    }
    
    func isContentEqual(to source: VKConversation) -> Bool {
        return unread_count == source.unread_count
    }
    
    convenience init(conversationJSON: JSON, messageJSON: JSON) {
        self.init()
        
        peer_id = conversationJSON["peer"]["id"].int64 ?? 0
        peer_type = conversationJSON["peer"]["type"].string ?? ""
        peer_local_id = conversationJSON["peer"]["local_id"].int64 ?? 0 //Для чатов — id - 2000000000, для сообществ — -id, для e-mail — -(id+2000000000).
        in_read = conversationJSON["in_read"].int64 ?? 0
        out_read = conversationJSON["out_read"].int64 ?? 0
        unread_count = conversationJSON["unread_count"].int ?? 0
        important = conversationJSON["important"].int ?? 0
        unanswered = conversationJSON["unanswered"].int ?? 0
        
        push_settings_disabled_until = conversationJSON["push_settings"]["disabled_until"].int64 ?? 0
        push_settings_no_sound = conversationJSON["push_settings"]["no_sound"].bool ?? false
        push_settings_disabled_forever = conversationJSON["push_settings"]["disabled_forever"].bool ?? false
        
        can_write_reason = conversationJSON["can_write"]["reason"].int8 ?? 0
        can_write_allowed = conversationJSON["can_write"]["allowed"].bool ?? true
        
        chat_settings_members_count = conversationJSON["chat_settings"]["members_count"].int ?? 0
        chat_settings_title = conversationJSON["chat_settings"]["title"].string ?? ""
        chat_settings_state = conversationJSON["chat_settings"]["state"].string ?? ""
        chat_settings_photo100 = conversationJSON["chat_settings"]["photo"]["photo_100"].string ?? ""
        chat_settings_photo200 = conversationJSON["chat_settings"]["photo"]["photo_200"].string ?? ""
        chat_settings_is_group_channel = conversationJSON["chat_settings"]["is_group_channel"].bool ?? false
        
        let active_ids = conversationJSON["chat_settings"]["active_ids"].array ?? []
        for id in active_ids {
            chat_settings_active_ids.append(String(id.int64 ?? 0) + ",")
        }

        let mess = messageJSON.dictionary ?? [:]
        
        if mess.count > 0{
            last_message = VKMessage(json: messageJSON, prnt_id: conversationID)
            last_message?.isPreview = true
            lastMsgDate = last_message?.date ?? 0
        }
    }
    

    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKConversation {
            if self.peer_id == object.peer_id &&
                self.peer_type == object.peer_type &&
                self.peer_local_id == object.peer_local_id &&
                self.in_read == object.in_read &&
                self.out_read == object.out_read &&
                self.unread_count == object.unread_count &&
                self.important == object.important &&
                self.unanswered == object.unanswered &&
                self.push_settings_no_sound == object.push_settings_no_sound &&
                self.push_settings_disabled_until == object.push_settings_disabled_until &&
                self.push_settings_disabled_forever == object.push_settings_disabled_forever &&
                self.can_write_allowed == object.can_write_allowed &&
                self.can_write_reason == object.can_write_reason &&
                self.chat_settings_state == object.chat_settings_state &&
                self.chat_settings_title == object.chat_settings_title &&
                self.chat_settings_photo100 == object.chat_settings_photo100 &&
                self.chat_settings_photo200 == object.chat_settings_photo200 &&
                self.chat_settings_members_count == object.chat_settings_members_count &&
                self.chat_settings_is_group_channel == object.chat_settings_is_group_channel {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
    
    func updateData(_ realm: Realm, _ cachedConvers: VKConversation) {
        do {
            try realm.write {
                if cachedConvers.last_message != nil {
                    cachedConvers.last_message?.delete(realm)
                    realm.delete(cachedConvers.last_message!)
                }
                
                cachedConvers.last_message = last_message
                cachedConvers.lastMsgDate = last_message?.date ?? 0
                cachedConvers.copyValue(self)
                realm.add(cachedConvers, update: true)
            }
        } catch let error as NSError {
            print("update convers failed - \(error)")
        }
    }
    
    func delete(_ realm: Realm) {
        last_message?.delete(realm)
        
        if last_message != nil {
            realm.delete(last_message!)
        }
        
        for msg in messages {
            msg.delete(realm)
            realm.delete(msg)
        }
    }
    
    func copyValue(_ conversSource: VKConversation) {
        self.peer_id = conversSource.peer_id
        self.peer_type = conversSource.peer_type
        self.peer_local_id = conversSource.peer_local_id
        self.in_read = conversSource.in_read
        self.out_read = conversSource.out_read
        self.unread_count = conversSource.unread_count
        self.important = conversSource.important
        self.unanswered = conversSource.unanswered
        self.push_settings_no_sound = conversSource.push_settings_no_sound
        self.push_settings_disabled_until = conversSource.push_settings_disabled_until
        self.push_settings_disabled_forever = conversSource.push_settings_disabled_forever
        self.can_write_allowed = conversSource.can_write_allowed
        self.can_write_reason = conversSource.can_write_reason
        self.chat_settings_state = conversSource.chat_settings_state
        self.chat_settings_title = conversSource.chat_settings_title
        self.chat_settings_photo100 = conversSource.chat_settings_photo100
        self.chat_settings_photo200 = conversSource.chat_settings_photo200
        self.chat_settings_members_count = conversSource.chat_settings_members_count
        self.chat_settings_active_ids = conversSource.chat_settings_active_ids
        self.chat_settings_is_group_channel = conversSource.chat_settings_is_group_channel
        self.lastMsgDate = conversSource.lastMsgDate
        
        if conversSource.userInfo != nil {
            self.userInfo = VKUser()
            self.userInfo!.copyValue(conversSource.userInfo!)
        }
        
        if conversSource.groupInfo != nil {
            self.groupInfo = VKGroup()
            self.groupInfo!.copyValue(conversSource.groupInfo!)
        }
        
        if conversSource.chatInfo != nil {
            self.chatInfo = VKChat()
            self.chatInfo!.copyValue(conversSource.chatInfo!)
        }
    }

}


class VKMessage: Object, Differentiable {
    //Main variable
    @objc dynamic var id: Int64 = 0
    @objc dynamic var conversation_message_id: Int64 = 0
    @objc dynamic var date: Int64 = 0
    @objc dynamic var peer_id: Int64 = 0
    @objc dynamic var from_id: Int64 = 0
    @objc dynamic var text: String = ""
    @objc dynamic var random_id: Int64 = 0
    
    let attachments = List<VKAttachments>()
    
    @objc dynamic var important: Bool = false
    @objc dynamic var geo: VKGeo? = nil
    
    let fwd_messages = List<VKMessage>()
    
    @objc dynamic var update_time: Int64 = 0
    @objc dynamic var is_hidden: Bool = false
    @objc dynamic var isPreview: Bool = false
    
    @objc dynamic var isDelete: Bool = false
    
    //Additional variable
    @objc dynamic var action_type: String = ""
    @objc dynamic var action_member_id: Int64 = 0
    @objc dynamic var action_text: String = ""
    @objc dynamic var action_email: String = ""
    @objc dynamic var action_photo50: String = ""
    @objc dynamic var action_photo200: String = ""
    @objc dynamic var action_photo_id: Int64 = 0
    
    @objc dynamic var prntID: String = ""
    
    var differenceIdentifier: String {
        return messageID
    }
    
    func isContentEqual(to source: VKMessage) -> Bool {
        return messageID == source.messageID
    }
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        // main var
        id = json["id"].int64 ?? 0
        conversation_message_id = json["conversation_message_id"].int64 ?? 0
        text = json["text"].string ?? ""
        date = json["date"].int64 ?? 0
        from_id = json["from_id"].int64 ?? 0
        peer_id = json["peer_id"].int64 ?? 0
        important = (json["important"].int8 ?? 0) == 0 ? false : true
        random_id = json["random_id"].int64 ?? 0
        update_time = json["update_time"].int64 ?? 0
        
        is_hidden = (json["is_hidden"].int ?? 0) == 0 ? false : true
        
        // additional var for chat
        action_type = json["action"]["type"].string ?? ""
        action_member_id = json["action"]["member_id"].int64 ?? 0  // < 0 if email
        action_text = json["action"]["text"].string ?? "" // chat text
        action_email = json["action"]["email"].string ?? ""
        action_photo50 = json["action"]["photo"]["photo_50"].string ?? ""
        action_photo200 = json["action"]["photo"]["photo_200"].string ?? ""
        
        let attachJSON = json["attachments"].array ?? []
        for item in attachJSON{
            attachments.append(VKAttachments(json: item, prnt_id: messageID))
        }
        
        let fwdMessJSON = json["fwd_messages"].array ?? []
        for item in fwdMessJSON{
            fwd_messages.append(VKMessage(json: item, prnt_id: messageID))
        }
        
        let geoJSON = json["geo"]
        if geoJSON != JSON.null{
            geo = VKGeo(json: geoJSON, prnt_id: messageID)
        }
        
    }
    
    @objc dynamic var messageID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "messageID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKMessage {
            if self.id == object.id &&
                self.text == object.text &&
                self.date == object.date &&
                self.from_id == object.from_id &&
                self.peer_id == object.peer_id &&
                self.random_id == object.random_id &&
                self.important == object.important &&
                self.action_email == object.action_email &&
                self.action_text == object.action_text &&
                self.action_type == object.action_type &&
                self.action_member_id == object.action_member_id &&
                self.action_photo50 == object.action_photo50 &&
                self.isPreview == object.isPreview &&
                self.is_hidden == object.is_hidden &&
                self.update_time == object.update_time &&
                self.isDelete == object.isDelete {
                
                if self.geo?.isEqual(object.geo!) ?? true == false {
                    return false
                }
                
                if self.attachments.count != object.attachments.count {
                    return false
                }
                
                for i in 0..<attachments.count {
                    var isFound: Bool = false
                    
                    for j in 0..<object.attachments.count {
                        if attachments[i].type == "link" {
                            if attachments[i].link?.url == object.attachments[j].link?.url {
                                isFound = true
                            }
                        } else if attachments[i].type == "sticker" {
                            if attachments[i].sticker?.product_id == object.attachments[j].sticker?.product_id &&
                                attachments[i].sticker?.stickerID == object.attachments[j].sticker?.stickerID {
                                isFound = true
                            }
                        } else if attachments[i].type == "pretty_cards" {
                            if attachments[i].pretty_cards.count > 0 {
                                if attachments[i].pretty_cards[0].card_id == object.attachments[j].pretty_cards[0].card_id {
                                    isFound = true
                                }
                            }
                        } else {
                            if attachments[i].type == object.attachments[j].type &&
                                attachments[i].obj_id == object.attachments[j].obj_id {
                                isFound = true
                            }
                        }
                        
                        if isFound == true {
                            if attachments[i].isEqual(object.attachments[j]) == false {
                                return false
                            } else {
                                break
                            }
                        }
                    }
                }

                if self.fwd_messages.count != object.fwd_messages.count {
                    return false
                }
                for i in 0..<fwd_messages.count {
                    if fwd_messages[i].isEqual(object.fwd_messages[i]) == false {
                        return false
                    }
                }

                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
    
    func delete(_ realm: Realm){
        if geo != nil {
            realm.delete(geo!)
        }
        
        for att in attachments {
            att.delete(realm)
            realm.delete(att)
        }
    }
    
    func copyValue(_ object: VKMessage) {
        self.id = object.id
        self.conversation_message_id = object.conversation_message_id
        self.text = object.text
        self.date = object.date
        self.from_id = object.from_id
        self.peer_id = object.peer_id
        self.important = object.important
        self.random_id = object.random_id
        self.action_email = object.action_email
        self.action_text = object.action_text
        self.action_type = object.action_type
        self.action_member_id = object.action_member_id
        self.action_photo50 = object.action_photo50
        self.action_photo200 = object.action_photo200
        self.isPreview = object.isPreview
        self.is_hidden = object.is_hidden
        self.update_time = object.update_time
        
        self.isDelete = object.isDelete
        
        self.geo = object.geo
        self.geo?.prntID = messageID
        
        //self.attachments.removeAll()
//        for att in object.attachments {
//            att.prntID = messageID
//            self.attachments.append(att)
//        }
//        self.fwd_messages.removeAll()
//        for fwd_msg in object.fwd_messages {
//            fwd_msg.prntID = messageID
//            self.fwd_messages.append(fwd_msg)
//        }
    }
    
    func updateData(_ realm: Realm, _ cachedMsg: VKMessage) {
        do {
            try realm.write {
                if cachedMsg.isPreview == true {
                    self.isPreview = true
                }
                self.messageID = cachedMsg.messageID
                self.prntID = cachedMsg.prntID
                //cachedMsg.delete(realm)
                //cachedMsg.copyValue(self)
                
                realm.create(VKMessage.self, value: self, update: true)
                
                //realm.add(cachedMsg, update: true)
                
//                if self.isPreview == true {
//                    self.isPreview = false
//                }
            }
        } catch let error as NSError {
            print("update msg failed - \(error)")
        }
    }
}

class VKGeo: Object{
    @objc dynamic var type: String = ""
    @objc dynamic var coordinates_latitude: Float = 0.0
    @objc dynamic var coordinates_longitude: Float = 0.0
    @objc dynamic var isHavePlace: Bool = false
    @objc dynamic var place_id: Int64 = 0
    @objc dynamic var place_title: String = ""
    @objc dynamic var place_latitude: Float = 0.0
    @objc dynamic var place_longitude: Float = 0.0
    @objc dynamic var place_created: Int64 = 0  //created date
    @objc dynamic var place_icon: String = "" //url icon
    @objc dynamic var place_country: String = ""
    @objc dynamic var place_city: String = ""
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        
        type = json["type"].string ?? ""
        coordinates_latitude = json["coordinates"]["latitude"].float ?? 0.0
        coordinates_longitude = json["coordinates"]["longitude"].float ?? 0.0
        
        let placeJSON = json["place"]
        if placeJSON != JSON.null{
            isHavePlace = true
            place_id = placeJSON["id"].int64 ?? 0
            place_title = placeJSON["title"].string ?? ""
            place_latitude  = placeJSON["latitude"].float ?? 0
            place_longitude  = placeJSON["longitude"].float ?? 0
            place_created = placeJSON["created"].int64 ?? 0
            place_icon = placeJSON["icon"].string ?? ""
            place_country = placeJSON["country"].string ?? ""
            place_city = placeJSON["city"].string ?? ""
        }
    }
    
    @objc dynamic var geoID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "geoID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKGeo {
            if self.type == object.type &&
                self.coordinates_latitude == object.coordinates_latitude &&
                self.coordinates_longitude == object.coordinates_longitude &&
                self.place_id == object.place_id &&
                self.place_title == object.place_title &&
                self.place_latitude == object.place_latitude &&
                self.place_longitude == object.place_longitude &&
                self.place_created == object.place_created &&
                self.place_icon == object.place_icon &&
                self.place_country == object.place_country &&
                self.place_city == object.place_city {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
}

class VKAttachments: Object {
    @objc dynamic var type: String = ""
    @objc dynamic var obj_id: Int64 = 0
    @objc dynamic var audio: VKAudio? = nil
    @objc dynamic var photo: VKPhoto? = nil
    @objc dynamic var video: VKVideo? = nil
    @objc dynamic var graffiti: VKGraffiti? = nil
    @objc dynamic var audio_msg: VKAudioMsg? = nil
    @objc dynamic var doc: VKDocument? = nil
    @objc dynamic var link: VKLink? = nil
    @objc dynamic var market: VKMarket? = nil
    @objc dynamic var market_album: VKMarketAlbum? = nil
    @objc dynamic var wall: VKWall? = nil
    @objc dynamic var sticker: VKSticker? = nil
    @objc dynamic var gift: VKGift? = nil
    @objc dynamic var note: VKNote? = nil
    @objc dynamic var poll: VKPoll? = nil
    @objc dynamic var album: VKAlbum? = nil
    @objc dynamic var page: VKPage? = nil
    let pretty_cards = List<VKCards>()
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        
        type = json["type"].string ?? ""
        
        switch type {
        case "audio":
            audio = VKAudio(json: json[type], prnt_id: prntID)
            obj_id = audio?.id ?? 0
            break
        case "photo":
            photo = VKPhoto(json: json[type], prnt_id: prntID)
            obj_id = photo?.id ?? 0
            break
        case "video":
            video = VKVideo(json: json[type], prnt_id: prntID)
            obj_id = video?.id ?? 0
            break
        case "audio_message":
            audio_msg = VKAudioMsg(json: json[type], prnt_id: prntID)
            obj_id = audio_msg?.id ?? 0
            break
        case "graffiti":
            graffiti = VKGraffiti(json: json[type], prnt_id: prntID)
            obj_id = graffiti?.id ?? 0
            break
        case "doc":
            doc = VKDocument(json: json[type], prnt_id: prntID)
            obj_id = doc?.id ?? 0
            break
        case "link":
            link = VKLink(json: json[type], prnt_id: prntID)
            break
        case "market":
            market = VKMarket(json: json[type], prnt_id: prntID)
            obj_id = market?.id ?? 0
            break
        case "market_album":
            market_album = VKMarketAlbum(json: json[type], prnt_id: prntID)
            obj_id = market_album?.id ?? 0
            break
        case "wall":
            wall = VKWall(json: json[type], prnt_id: prntID)
            obj_id = wall?.id ?? 0
            break
        case "sticker":
            sticker = VKSticker(json: json[type], prnt_id: prntID)
            break
        case "gift":
            gift = VKGift(json: json[type], prnt_id: prntID)
            obj_id = gift?.id ?? 0
            break
        case "note":
            note = VKNote(json: json[type], prnt_id: prntID)
            obj_id = note?.id ?? 0
            break
        case "poll":
            poll = VKPoll(json: json[type], prnt_id: prntID)
            obj_id = poll?.id ?? 0
            break
        case "album":
            album = VKAlbum(json: json[type], prnt_id: prntID)
            obj_id = album?.id ?? 0
            break
        case "page":
            page = VKPage(json: json[type], prnt_id: prntID)
            obj_id = page?.id ?? 0
            break
        case "pretty_cards":
            let items = json[type]["cards"].array ?? []
            
            for item in items {
                pretty_cards.append(VKCards(json: item, prnt_id: prntID))
            }
            
            break
        default:
            break
        }
    }
    
    @objc dynamic var attID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "attID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKAttachments {
            switch type {
            case "audio":
                return audio?.isEqual(object.audio) ?? false
            case "photo":
                return photo?.isEqual(object.photo) ?? false
            case "video":
                return video?.isEqual(object.video) ?? false
            case "audio_message":
                return audio_msg?.isEqual(object.audio_msg) ?? false
            case "graffiti":
                return graffiti?.isEqual(object.graffiti) ?? false
            case "doc":
                return doc?.isEqual(object.doc) ?? false
            case "link":
                return link?.isEqual(object.link) ?? false
            case "market":
                return market?.isEqual(object.market) ?? false
            case "market_album":
                return market_album?.isEqual(object.market_album) ?? false
            case "wall":
                return wall?.isEqual(object.wall) ?? false
            case "sticker":
                return sticker?.isEqual(object.sticker) ?? false
            case "gift":
                return gift?.isEqual(object.gift) ?? false
            case "note":
                return note?.isEqual(object.note) ?? false
            case "poll":
                return poll?.isEqual(object.poll) ?? false
            case "album":
                return album?.isEqual(object.album) ?? false
            case "page":
                return page?.isEqual(object.page) ?? false
            case "pretty_cards":
                if pretty_cards.count != object.pretty_cards.count {
                    return false
                }
                
                for i in 0..<pretty_cards.count {
                    if pretty_cards[i].isEqual(object.pretty_cards[i]) == false {
                        return false
                    }
                }
                return true
            default:
                break
            }
            return false
        } else {
            return false
        }
    }
    
    func delete(_ realm: Realm){
        switch type {
        case "audio":
            if audio != nil { realm.delete(audio!) }
            break
        case "photo":
            if photo != nil {
                photo?.delete(realm)
                realm.delete(photo!)
            }
            break
        case "video":
            if video != nil { realm.delete(video!) }
            break
        case "audio_message":
            if audio_msg != nil { realm.delete(audio_msg!) }
            break
        case "graffiti":
            if graffiti != nil { realm.delete(graffiti!) }
            break
        case "doc":
            if doc != nil {
                doc?.delete(realm)
                realm.delete(doc!)
            }
            break
        case "link":
            if link != nil {
                link?.delete(realm)
                realm.delete(link!)
            }
            break
        case "market":
            if market != nil {
                market?.delete(realm)
                realm.delete(market!)
            }
            break
        case "market_album":
            if market_album != nil {
                market_album?.delete(realm)
                realm.delete(market_album!)
            }
            break
        case "wall":
            if wall != nil {
                wall?.delete(realm)
                realm.delete(wall!)
            }
            break
        case "sticker":
            if sticker != nil { realm.delete(sticker!) }
            break
        case "gift":
            if gift != nil { realm.delete(gift!) }
            break
        case "note":
            realm.delete(note ?? VKNote())
            break
        case "poll":
            if poll != nil {
                poll?.delete(realm)
                realm.delete(poll!)
            }
            break
        case "album":
            if album != nil {
                album?.delete(realm)
                realm.delete(album!)
            }
            break
        case "page":
            if page != nil { realm.delete(page!) }
            break
        case "pretty_cards":
            for card in pretty_cards {
                realm.delete(card)
            }
            break
        default:
            break
        }
    }

    
}

class VKPhoto: Object {
    @objc dynamic var id: Int64 = 0
    @objc dynamic var owner_id: Int64 = 0
    @objc dynamic var album_id: Int64 = 0
    @objc dynamic var user_id: Int64 = 0
    @objc dynamic var text: String = ""
    @objc dynamic var date: Int64 = 0
    
    @objc dynamic var photoDocSizeMin: VKSize? = nil
    @objc dynamic var photoDocSizeMid: VKSize? = nil
    @objc dynamic var photoDocSizeMax: VKSize? = nil

    @objc dynamic var width: Int16 = 0
    @objc dynamic var height: Int16 = 0
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        
        id = json["id"].int64 ?? 0
        owner_id = json["owner_id"].int64 ?? 0
        album_id = json["album_id"].int64 ?? 0
        user_id = json["user_id"].int64 ?? 0
        text = json["text"].string ?? ""
        date = json["date"].int64 ?? 0
        
        let photoJSON = json["sizes"]
        if photoJSON != JSON.null{
            let items = photoJSON.array ?? []
            
            for item in items{
                let size = VKSize(json: item, prnt_id: prntID)
                
                if size.type == "s" { photoDocSizeMin = size }
                else if size.type == "y" || size.type == "x" {
                    if photoDocSizeMid == nil { photoDocSizeMid = size }
                    else if size.type == "y" { photoDocSizeMid = size }
                }
                else if size.type == "z" || size.type == "w" {
                    if photoDocSizeMax == nil { photoDocSizeMax = size }
                    else if size.type == "w" { photoDocSizeMax = size }
                }
            }
        }
        

        width = json["width"].int16 ?? 0
        height = json["height"].int16 ?? 0
    }
    
    @objc dynamic var photoID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "photoID"
    }
    
    func copyValue(_ object: VKPhoto) {
        self.id = object.id
        self.text = object.text
        self.date = object.date
        self.owner_id = object.owner_id
        self.album_id = object.album_id
        self.user_id = object.user_id
        self.width = object.width
        self.height = object.height
        self.prntID = object.prntID
        self.photoID = object.photoID
        
        if object.photoDocSizeMin != nil {
            let size = VKSize()
            size.copyValue(object.photoDocSizeMin!)
            self.photoDocSizeMin = size
        }
        
        if object.photoDocSizeMid != nil {
            let size = VKSize()
            size.copyValue(object.photoDocSizeMid!)
            self.photoDocSizeMid = size
        }
        
        if object.photoDocSizeMax != nil {
            let size = VKSize()
            size.copyValue(object.photoDocSizeMax!)
            self.photoDocSizeMax = size
        }
        
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKPhoto {
            if self.id == object.id &&
                self.owner_id == object.owner_id &&
                self.album_id == object.album_id &&
                self.user_id == object.user_id &&
                self.text == object.text &&
                self.date == object.date &&
                self.width == object.width &&
                self.height == object.height {
                
                if photoDocSizeMin != nil && object.photoDocSizeMin != nil {
                    if photoDocSizeMin?.isEqual(object.photoDocSizeMin) ?? false == false {
                        return false
                    }
                }
                
                if photoDocSizeMid != nil && object.photoDocSizeMid != nil {
                    if photoDocSizeMid?.isEqual(object.photoDocSizeMid) ?? false == false {
                        return false
                    }
                }
                
                if photoDocSizeMax != nil && object.photoDocSizeMax != nil {
                    if photoDocSizeMax?.isEqual(object.photoDocSizeMax) ?? false == false {
                        return false
                    }
                }
                
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
    }
    
    func delete(_ realm: Realm){
        if photoDocSizeMin != nil { realm.delete(photoDocSizeMin!) }
        if photoDocSizeMid != nil { realm.delete(photoDocSizeMid!) }
        if photoDocSizeMax != nil { realm.delete(photoDocSizeMax!) }
    }
}

class VKVideo: Object {
    @objc dynamic var id: Int64 = 0
    @objc dynamic var title: String = ""
    @objc dynamic var owner_id: Int64 = 0  //владелец
    @objc dynamic var description_vk: String = ""
    @objc dynamic var duration: Int64 = 0  // in seconds
    @objc dynamic var photo_130: String = ""
    @objc dynamic var photo_320: String = ""
    @objc dynamic var photo_640: String = ""
    @objc dynamic var photo_800: String = ""
    @objc dynamic var date: Int64 = 0
    @objc dynamic var adding_date: Int64 = 0
    @objc dynamic var views: Int64 = 0
    @objc dynamic var comments: Int64 = 0
    @objc dynamic var player: String = ""
    @objc dynamic var platform: String = ""
    @objc dynamic var can_add: Bool = false
    @objc dynamic var is_private: Bool = false
    @objc dynamic var access_key: String = ""
    @objc dynamic var processing: Bool = false  // if video adaptationing
    @objc dynamic var live: Bool = false
    @objc dynamic var upcoming: Bool = false
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        
        id = json["id"].int64 ?? 0
        title = json["title"].string ?? ""
        owner_id = json["owner_id"].int64 ?? 0
        description_vk = json["description"].string ?? ""
        duration = json["duration"].int64 ?? 0
        photo_130 = json["photo_130"].string ?? ""
        photo_320 = json["photo_320"].string ?? ""
        photo_640 = json["photo_640"].string ?? ""
        photo_800 = json["photo_800"].string ?? ""
        date = json["date"].int64 ?? 0
        adding_date = json["adding_date"].int64 ?? 0
        views = json["views"].int64 ?? 0
        comments = json["comments"].int64 ?? 0
        player = json["player"].string ?? ""
        platform = json["platform"].string ?? ""
        can_add = (json["can_add"].int8 ?? 0) == 0 ? false : true
        is_private = (json["is_private"].int8 ?? 0) == 0 ? false : true
        access_key = json["access_key"].string ?? ""
        processing = (json["processing"].int8 ?? 0) == 0 ? false : true
        live = (json["live"].int8 ?? 0) == 0 ? false : true
        upcoming = (json["upcoming"].int8 ?? 0) == 0 ? false : true
    }
    
    @objc dynamic var videoID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "videoID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKVideo {
            if self.id == object.id &&
                self.title == object.title &&
                self.owner_id == object.owner_id &&
                self.description_vk == object.description_vk &&
                self.duration == object.duration &&
                self.photo_320 == object.photo_320 &&
                self.date == object.date &&
                self.adding_date == object.adding_date &&
                self.views == object.views &&
                self.comments == object.comments &&
                self.access_key == object.access_key &&
                self.processing == object.processing &&
                self.live == object.live &&
                self.upcoming == object.upcoming {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
}

class VKAudio: Object {
    @objc dynamic var id: Int64 = 0
    @objc dynamic var title: String = ""
    @objc dynamic var artist: String = ""
    @objc dynamic var owner_id: Int64 = 0  //владелец
    @objc dynamic var duration: Int64 = 0  // in seconds
    @objc dynamic var url: String = ""
    @objc dynamic var lyrics_id: Int64 = 0
    @objc dynamic var album_id: Int64 = 0
    @objc dynamic var genre_id: Int64 = 0
    @objc dynamic var date: Int64 = 0
    @objc dynamic var no_search: Bool = false
    @objc dynamic var is_hq: Bool = false
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        
        id = json["id"].int64 ?? 0
        title = json["title"].string ?? ""
        artist = json["artist"].string ?? ""
        owner_id = json["owner_id"].int64 ?? 0
        duration = json["duration"].int64 ?? 0
        url = json["url"].string ?? ""
        lyrics_id = json["lyrics_id"].int64 ?? 0
        album_id = json["album_id"].int64 ?? 0
        genre_id = json["genre_id"].int64 ?? 0
        date = json["date"].int64 ?? 0
        no_search = (json["no_search"].int8 ?? 0) == 0 ? false : true
        is_hq = (json["is_hq"].int8 ?? 0) == 0 ? false : true
    }
    
    @objc dynamic var audioID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "audioID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKAudio {
            if self.id == object.id &&
                self.title == object.title &&
                self.artist == object.artist &&
                self.owner_id == object.owner_id &&
                self.duration == object.duration &&
                self.lyrics_id == object.lyrics_id &&
                self.album_id == object.album_id &&
                self.genre_id == object.genre_id &&
                self.date == object.date &&
                self.no_search == object.no_search &&
                self.is_hq == object.is_hq {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
    
    func copyValue(_ object: VKAudio) {
        self.id = object.id
        self.title = object.title
        self.artist = object.artist
        self.owner_id = object.owner_id
        self.duration = object.duration
        self.url = object.url
        self.lyrics_id = object.lyrics_id
        self.album_id = object.album_id
        self.genre_id = object.genre_id
        self.date = object.date
        self.no_search = object.no_search
        self.is_hq = object.is_hq
        self.prntID = object.prntID
    }
}

class VKSize: Object {
    @objc dynamic var url: String = ""
    @objc dynamic var width: Int16 = 0
    @objc dynamic var height: Int16 = 0
    @objc dynamic var type: String = ""
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        
        url = json["url"].string ?? json["src"].string ?? ""
        width = json["width"].int16 ?? 0
        height = json["height"].int16 ?? 0
        type = json["type"].string ?? ""
    }
    
    @objc dynamic var sizeID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "sizeID"
    }
    
    func copyValue(_ object: VKSize) {
        self.url = object.url
        self.type = object.type
        self.width = object.width
        self.height = object.height
        self.prntID = object.prntID
        self.sizeID = object.sizeID
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKSize {
            if self.url == object.url &&
                self.width == object.width &&
                self.height == object.height &&
                self.type == object.type {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
}

class VKGraffiti: Object {
    @objc dynamic var id: Int64 = 0
    @objc dynamic var url: String = ""
    @objc dynamic var width: Int16 = 0
    @objc dynamic var height: Int16 = 0
    @objc dynamic var owner_id: Int64 = 0
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        
        id = json["id"].int64 ?? 0
        url = json["url"].string ?? ""
        width = json["width"].int16 ?? 0
        height = json["height"].int16 ?? 0
        owner_id = json["owner_id"].int64 ?? 0
    }
    
    @objc dynamic var graffitiID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "graffitiID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKGraffiti {
            if self.id == object.id &&
                self.width == object.width &&
                self.height == object.height &&
                self.owner_id == object.owner_id {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
}

class VKAudioMsg: Object {
    @objc dynamic var id: Int64 = 0
    @objc dynamic var duration: Int16 = 0
    let waveform = List<Int>()
    @objc dynamic var link_ogg: String = ""
    @objc dynamic var link_mp3: String = ""
    @objc dynamic var access_key: String = ""
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        id = json["id"].int64 ?? 0
        duration = json["duration"].int16 ?? 0
        
        let items = json["waveform"].array ?? []
        for item in items{
            waveform.append(item.int ?? 0)
        }
        
        link_ogg = json["link_ogg"].string ?? ""
        link_mp3 = json["link_mp3"].string ?? ""
        access_key = json["access_key"].string ?? ""
        
        let prevJSON = json["preview"]["audio_msg"]
        
        if prevJSON != JSON.null {
            link_ogg = prevJSON["link_ogg"].string ?? ""
            link_mp3 = prevJSON["link_mp3"].string ?? ""
            duration = prevJSON["duration"].int16 ?? 0
            
            let items = prevJSON["waveform"].array ?? []
            for item in items{
                waveform.append(item.int ?? 0)
            }
        }
    }
    
    @objc dynamic var audioMsgID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "audioMsgID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKAudioMsg {
            if self.duration == object.duration &&
                self.id == object.id &&
                self.link_mp3 == object.link_mp3 &&
                self.link_ogg == object.link_ogg &&
                self.access_key == object.access_key {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
}

class VKDocument: Object {
    @objc dynamic var id: Int64 = 0
    @objc dynamic var title: String = ""
    @objc dynamic var owner_id: Int64 = 0  //владелец
    @objc dynamic var size: Int32 = 0
    @objc dynamic var ext: String = ""
    @objc dynamic var url: String = ""
    @objc dynamic var date: Int64 = 0
    @objc dynamic var type: Int8 = 0
    /*1 — текстовые документы;
     2 — архивы;
     3 — gif;
     4 — изображения;
     5 — аудио;
     6 — видео;
     7 — электронные книги;
     8 — неизвестно.
     */
    //preview
    @objc dynamic var isHavePreview: Bool = false
    
    @objc dynamic var photoDocSizeMin: VKSize? = nil
    @objc dynamic var photoDocSizeMid: VKSize? = nil
    @objc dynamic var photoDocSizeMax: VKSize? = nil
    
    @objc dynamic var gifVideoPreviewUrl: String = ""
    @objc dynamic var gifVideoPreviewSize: Int32 = 0
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        
        // main var
        id = json["id"].int64 ?? 0
        date = json["date"].int64 ?? 0
        title = json["title"].string ?? ""
        owner_id = json["owner_id"].int64 ?? 0
        size = json["size"].int32 ?? 0
        ext = json["ext"].string ?? ""
        url = json["url"].string ?? ""
        type = json["type"].int8 ?? 0
        
        let previewJSON = json["preview"]
        
        if previewJSON != JSON.null {
            isHavePreview = true
            
            let photoJSON = previewJSON["photo"]["sizes"]
            if photoJSON != JSON.null {
                let items = photoJSON.array ?? []
                
                for item in items{
                    let size = VKSize(json: item, prnt_id: prnt_id)
                    
                    if size.type == "s" /*|| size.type == "o"*/ || size.type == "m" { photoDocSizeMin = size }
                    else if size.type == "y" || size.type == "x" {
                        if photoDocSizeMid == nil { photoDocSizeMid = size }
                        else if size.type == "y" { photoDocSizeMid = size }
                    }
                    else if size.type == "z" || size.type == "w" {
                        if photoDocSizeMax == nil { photoDocSizeMax = size }
                        else if size.type == "w" { photoDocSizeMax = size }
                    }
                }
            }
            
            gifVideoPreviewUrl = previewJSON["video"]["src"].string ?? ""
            gifVideoPreviewSize = previewJSON["video"]["file_size"].int32 ?? 0
        }
    }
    
    @objc dynamic var documentID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "documentID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKDocument {
            if self.id == object.id &&
                self.title == object.title &&
                self.owner_id == object.owner_id &&
                self.size == object.size &&
                self.ext == object.ext &&
                self.date == object.date &&
                self.type == object.type &&
                self.isHavePreview == object.isHavePreview {
                
                if isHavePreview == true {
                    if photoDocSizeMin != nil && object.photoDocSizeMin != nil {
                        if photoDocSizeMin?.isEqual(object.photoDocSizeMin) ?? false == false {
                            return false
                        }
                    }
                    
                    if photoDocSizeMid != nil && object.photoDocSizeMid != nil {
                        if photoDocSizeMid?.isEqual(object.photoDocSizeMid) ?? false == false {
                            return false
                        }
                    }
                    
                    if photoDocSizeMax != nil && object.photoDocSizeMax != nil {
                        if photoDocSizeMax?.isEqual(object.photoDocSizeMax) ?? false == false {
                            return false
                        }
                    }
                }
                
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
    }
    
    func copyValue(_ object: VKDocument) {
        self.url = object.url
        self.id = object.id
        self.title = object.title
        self.owner_id = object.owner_id
        self.prntID = object.prntID
        self.size = object.size
        self.ext = object.ext
        self.date = object.date
        self.type = object.type
        self.isHavePreview = object.isHavePreview
        self.gifVideoPreviewUrl = object.gifVideoPreviewUrl
        self.gifVideoPreviewSize = object.gifVideoPreviewSize
        
        if object.photoDocSizeMin != nil {
            self.photoDocSizeMin = VKSize()
            self.photoDocSizeMin?.copyValue(object.photoDocSizeMin!)
        }
        
        if object.photoDocSizeMid != nil {
            self.photoDocSizeMid = VKSize()
            self.photoDocSizeMid?.copyValue(object.photoDocSizeMid!)
        }
        
        if object.photoDocSizeMax != nil {
            self.photoDocSizeMax = VKSize()
            self.photoDocSizeMax?.copyValue(object.photoDocSizeMax!)
        }
        
    }
    
    func delete(_ realm: Realm){
        if photoDocSizeMin != nil { realm.delete(photoDocSizeMin!) }
        if photoDocSizeMid != nil { realm.delete(photoDocSizeMid!) }
        if photoDocSizeMax != nil { realm.delete(photoDocSizeMax!) }
    }
}

class VKProductPrice: Object {
    @objc dynamic var amount: Int = 0// Int * 100
    @objc dynamic var text: String = ""
    @objc dynamic var currency_id: Int64 = 0
    @objc dynamic var currency_name: String = ""
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        
        amount = json["amount"].int ?? 0
        text = json["text"].string ?? ""
        
        let currJSON = json["currency"]
        
        if currJSON != JSON.null{
            currency_id = currJSON["id"].int64 ?? 0
            currency_name = currJSON["name"].string ?? ""
        }
    }
    
    @objc dynamic var priceID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "priceID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKProductPrice {
            if self.amount == object.amount &&
                self.text == object.text &&
                self.currency_id == object.currency_id &&
                self.currency_name == object.currency_name {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
    }
}

class VKLink: Object {
    @objc dynamic var url: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var caption: String = ""
    @objc dynamic var description_vk: String = ""
    @objc dynamic var photo: VKPhoto? = nil
    @objc dynamic var product: VKProductPrice? = nil
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        
        url = json["url"].string ?? ""
        title = json["title"].string ?? ""
        caption = json["caption"].string ?? ""
        description_vk = json["description"].string ?? ""
        
        let photoJSON = json["photo"]
        if photoJSON != JSON.null{
            photo = VKPhoto(json: photoJSON, prnt_id: prnt_id)
        }
        
        let productJSON = json["product"]["price"]
        if productJSON != JSON.null{
            product = VKProductPrice(json: productJSON, prnt_id: prnt_id)
        }
    }
    
    @objc dynamic var linkID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "linkID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKLink {
            if self.url == object.url &&
                self.title == object.title &&
                self.caption == object.caption &&
                self.description_vk == object.description_vk {
                
                if photo?.isEqual(object.photo ?? VKPhoto()) ?? false == false {
                    return false
                }
                
                if product?.isEqual(object.product ?? VKProductPrice()) ?? true == false {
                    return false
                }
                
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
    
    func delete(_ realm: Realm){
        if product != nil {
            realm.delete(product!)
        }
        if photo != nil {
            realm.delete(photo!)
        }
    }
}

class VKMarketCategory: Object {
    @objc dynamic var id: Int64 = 0
    @objc dynamic var name: String = ""
    @objc dynamic var section_id: Int64 = 0
    @objc dynamic var section_name: String = ""
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        
        id = json["id"].int64 ?? 0
        name = json["name"].string ?? ""
        
        if json["section"] != JSON.null{
            section_id = json["section"]["id"].int64 ?? 0
            section_name = json["section"]["name"].string ?? ""
        }
    }
    
    @objc dynamic var marketCategoryID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "marketCategoryID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKMarketCategory {
            if self.id == object.id &&
                self.name == object.name &&
                self.section_id == object.section_id &&
                self.section_name == object.section_name {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
}

class VKMarket: Object {
    @objc dynamic var id: Int64 = 0
    @objc dynamic var owner_id: Int64 = 0
    @objc dynamic var title: String = ""
    @objc dynamic var description_vk: String = ""
    @objc dynamic var price: VKProductPrice? = nil
    @objc dynamic var category: VKMarketCategory? = nil
    @objc dynamic var thumb_photo: String = ""
    @objc dynamic var date: Int64 = 0
    @objc dynamic var availability: Int8 = 0  // 0 - is available, 1 - deleted, 2 - isn't available
    
    //optional @objc dynamic var if extended = 1
    let photos = List<VKPhoto>()
    @objc dynamic var can_comment: Bool = false
    @objc dynamic var can_repost: Bool = false
    @objc dynamic var likes_user_likes: Bool = false  // 1 - user likes this market
    @objc dynamic var likes_count: Int32 = 0
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        
        id = json["id"].int64 ?? 0
        date = json["date"].int64 ?? 0
        title = json["title"].string ?? ""
        owner_id = json["owner_id"].int64 ?? 0
        description_vk = json["description"].string ?? ""
        thumb_photo = json["thumb_photo"].string ?? ""
        availability = json["availability"].int8 ?? 0
        
        can_comment = (json["can_comment"].int8 ?? 0) == 0 ? false : true
        can_repost = (json["can_repost"].int8 ?? 0) == 0 ? false : true
        likes_user_likes = (json["likes"]["user_likes"].int8 ?? 0) == 0 ? false : true
        likes_count = json["likes"]["count"].int32 ?? 0
        
        let priceJSON = json["price"]
        
        if priceJSON != JSON.null{
            price = VKProductPrice(json: priceJSON, prnt_id: prnt_id)
        }
        
        let categoryJSON = json["category"]
        
        if categoryJSON != JSON.null{
            category = VKMarketCategory(json: categoryJSON, prnt_id: prnt_id)
        }
        
        let items = json["photo"].array ?? []
        
        for item in items{
            let photo = VKPhoto(json: item, prnt_id: prnt_id)
            photos.append(photo)
        }
    }
    
    @objc dynamic var marketID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "marketID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKMarket {
            if self.id == object.id &&
                self.owner_id == object.owner_id &&
                self.title == object.title &&
                self.description_vk == object.description_vk &&
                self.thumb_photo == object.thumb_photo &&
                self.date == object.date &&
                self.availability == object.availability &&
                self.can_comment == object.can_comment &&
                self.can_repost == object.can_repost &&
                self.likes_user_likes == object.likes_user_likes &&
                self.likes_count == object.likes_count {
                
                if price?.isEqual(object.price ?? VKProductPrice()) ?? true == false {
                    return false
                }
                
                if category?.isEqual(object.category ?? VKMarketCategory()) ?? true == false {
                    return false
                }
                
                if photos.count != object.photos.count {
                    return false
                }
                
                for i in 0..<photos.count {
                    
                    for j in 0..<object.photos.count {
                        if photos[i].id == object.photos[j].id {
                            if photos[i].isEqual(object.photos[j]) == false {
                                return false
                            } else {
                                break
                            }
                        }
                    }
                }
                
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
    
    func delete(_ realm: Realm){
        if price != nil {
            realm.delete(price!)
        }
        if category != nil {
            realm.delete(category!)
        }
        
        for pht in photos {
            realm.delete(pht)
        }
    }
}

class VKMarketAlbum: Object {
    @objc dynamic var id: Int64 = 0
    @objc dynamic var owner_id: Int64 = 0
    @objc dynamic var title: String = ""
    @objc dynamic var photo: VKPhoto? = nil
    @objc dynamic var count: Int16 = 0
    @objc dynamic var updated_time: Int64 = 0
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        
        id = json["id"].int64 ?? 0
        title = json["title"].string ?? ""
        owner_id = json["owner_id"].int64 ?? 0
        count = json["count"].int16 ?? 0
        updated_time = json["updated_time"].int64 ?? 0
        
        let photoJSON = json["photo"]
        
        if photoJSON != JSON.null {
            photo = VKPhoto(json: photoJSON, prnt_id: prnt_id)
        }
    }
    
    @objc dynamic var marketAlbumID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "marketAlbumID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKMarketAlbum {
            if self.id == object.id &&
                self.owner_id == object.owner_id &&
                self.title == object.title &&
                self.count == object.count &&
                self.updated_time == object.updated_time {
                
                if photo?.isEqual(object.photo ?? VKPhoto()) ?? true == false {
                    return false
                }
                
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
    
    func delete(_ realm: Realm){
        if photo != nil {
            realm.delete(photo!)
        }
    }
}

class VKSticker: Object{
    @objc dynamic var product_id: Int64 = 0
    @objc dynamic var sticker_id: Int64 = 0
    @objc dynamic var image_url_mid: String = ""
    @objc dynamic var image_width_mid: Int = 0
    @objc dynamic var image_height_mid: Int = 0
    @objc dynamic var image_url_max: String = ""
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        
        // main var
        product_id = json["product_id"].int64 ?? 0
        sticker_id = json["sticker_id"].int64 ?? 0
        
        let items = json["images"].array ?? []
        for item in items{
            if (item["width"].int ?? 0) > 200 && (item["width"].int ?? 0) < 270 {
                image_url_mid = item["url"].string ?? ""
                image_width_mid = item["width"].int ?? 0
                image_height_mid = item["height"].int ?? 0
            }
            else if (item["width"].int ?? 0) >= 270 { image_url_max = item["url"].string ?? "" }
        }
    }
    
    @objc dynamic var stickerID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "stickerID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKSticker {
            if self.product_id == object.product_id &&
                self.sticker_id == object.sticker_id {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
    }
}

class VKGift: Object {
    @objc dynamic var id: Int64 = 0
    @objc dynamic var thumb_256: String = ""
    @objc dynamic var thumb_96: String = ""
    @objc dynamic var thumb_48: String = ""
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        
        id = json["id"].int64 ?? 0
        thumb_48 = json["thumb_48"].string ?? ""
        thumb_96 = json["thumb_96"].string ?? ""
        thumb_256 = json["thumb_256"].string ?? ""
    }
    
    @objc dynamic var giftID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "giftID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKGift {
            if self.id == object.id &&
                self.thumb_96 == object.thumb_96 {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
}

class VKNote: Object {
    @objc dynamic var id: Int64 = 0
    @objc dynamic var owner_id: Int64 = 0
    @objc dynamic var title: String = ""
    @objc dynamic var text: String = ""
    @objc dynamic var date: Int64 = 0
    @objc dynamic var comments: Int = 0
    @objc dynamic var view_url: String = ""
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        
        id = json["id"].int64 ?? 0
        owner_id = json["owner_id"].int64 ?? 0
        title = json["title"].string ?? ""
        text = json["text"].string ?? ""
        date = json["date"].int64 ?? 0
        comments = json["comments"].int ?? 0
        view_url = json["view_url"].string ?? ""
    }
    
    @objc dynamic var noteID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "noteID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKNote {
            if self.id == object.id &&
                self.owner_id == object.owner_id &&
                self.title == object.title &&
                self.text == object.text &&
                self.date == object.date &&
                self.comments == object.comments &&
                self.view_url == object.view_url {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
}

class VKPoll: Object {
    @objc dynamic var id: Int64 = 0
    @objc dynamic var owner_id: Int64 = 0
    @objc dynamic var created: Int64 = 0
    @objc dynamic var question: String = ""
    @objc dynamic var votes: Int = 0
    let answer_ids = List<Int64>()
    let answers = List<VKPollAnswer>()
    @objc dynamic var anonymous: Bool = false
    @objc dynamic var multiple: Bool = false
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        id = json["id"].int64 ?? 0
        owner_id = json["owner_id"].int64 ?? 0
        created = json["created"].int64 ?? 0
        owner_id = json["owner_id"].int64 ?? 0
        question = json["question"].string ?? ""
        votes = json["votes"].int ?? 0
        
        let answerIDsJSON = json["answer_ids"].array ?? []
        if answerIDsJSON.count == 0 {
            if let answer_id = json["answer_id"].int64 { if answer_id != 0 { answer_ids.append(answer_id) } }
        } else {
            for item in answerIDsJSON {
                answer_ids.append(item.int64 ?? 0)
            }
        }
        
        
        anonymous = (json["anonymous"].int8 ?? 0) == 0 ? false : true
        multiple = (json["multiple"].int8 ?? 0) == 0 ? false : true
        
        let items = json["answers"].array ?? []
        for item in items {
            answers.append(VKPollAnswer(json: item, prnt_id: pollID))
        }
        
        prntID = prnt_id
    }
    
    @objc dynamic var pollID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "pollID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKPoll {
            if self.id == object.id &&
                self.owner_id == object.owner_id &&
                self.created == object.created &&
                self.question == object.question &&
                self.votes == object.votes &&
                self.anonymous == object.anonymous {
                
                if answers.count != object.answers.count {
                    return false
                }
                
                for i in 0..<answers.count {
                    for j in 0..<object.answers.count {
                        if answers[i].id == object.answers[j].id {
                            if answers[i].isEqual(object.answers[j]) == false {
                                return false
                            } else {
                                break
                            }
                        }
                    }
                }
                
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
    
    func delete(_ realm: Realm){
        for ans in answers {
            realm.delete(ans)
        }
    }
}

class VKPollAnswer: Object {
    @objc dynamic var id: Int64 = 0
    @objc dynamic var text: String = ""
    @objc dynamic var votes: Int = 0
    @objc dynamic var rate: Float = 0
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        id = json["id"].int64 ?? 0
        text = json["text"].string ?? ""
        votes = json["votes"].int ?? 0
        rate = json["rate"].float ?? 0.0
        
        prntID = prnt_id
    }
    
    @objc dynamic var pollAnswerID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "pollAnswerID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKPollAnswer {
            if self.id == object.id &&
                self.text == object.text &&
                self.votes == object.votes &&
                self.rate == object.rate {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
}

class VKPage: Object {
    @objc dynamic var id:Int64 = 0
    @objc dynamic var group_id:Int64 = 0
    @objc dynamic var creator_id:Int64 = 0
    @objc dynamic var title:String = ""
    @objc dynamic var edited:Int64 = 0
    @objc dynamic var created:Int64 = 0
    @objc dynamic var editor_id:Int64 = 0
    @objc dynamic var views:Int = 0
    @objc dynamic var who_can_view:Int8 = 0
    /*2 — просматривать страницу могут все;
     1 — только участники сообщества;
     0 — только руководители сообщества.*/
    @objc dynamic var parent:String = ""
    @objc dynamic var parent2:String = ""
    @objc dynamic var source:String = ""
    @objc dynamic var html:String = ""
    @objc dynamic var view_url:String = ""
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        
        id = json["id"].int64 ?? 0
        group_id = json["owner_id"].int64 ?? 0
        creator_id = json["creator_id"].int64 ?? 0
        title = json["title"].string ?? ""
        created = json["created"].int64 ?? 0
        edited = json["edited"].int64 ?? 0
        editor_id = json["editor_id"].int64 ?? 0
        views = json["views"].int ?? 0
        who_can_view = json["who_can_view"].int8 ?? 0
        parent = json["parent"].string ?? ""
        parent2 = json["parent2"].string ?? ""
        source = json["source"].string ?? ""
        html = json["html"].string ?? ""
        view_url = json["view_url"].string ?? ""
        
        prntID = prnt_id
    }
    
    @objc dynamic var pageID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "pageID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKPage {
            if self.id == object.id &&
                self.group_id == object.group_id &&
                self.creator_id == object.creator_id &&
                self.title == object.title &&
                self.edited == object.edited &&
                self.created == object.created &&
                self.editor_id == object.editor_id &&
                self.views == object.views &&
                self.who_can_view == object.edited &&
                self.parent == object.parent &&
                self.parent2 == object.parent2 &&
                self.source == object.source &&
                self.html == object.html &&
                self.view_url == object.view_url {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
    }
}

class VKAlbum: Object {
    @objc dynamic var id: Int64 = 0
    @objc dynamic var thumb: VKPhoto? = nil
    @objc dynamic var owner_id: Int64 = 0
    @objc dynamic var thumb_id: Int64 = 0
    @objc dynamic var title: String = ""
    @objc dynamic var description_vk: String = ""
    @objc dynamic var created: Int64 = 0
    @objc dynamic var updated: Int64 = 0
    @objc dynamic var size: Int = 0
    
    @objc dynamic var photoDocSizeMin: VKSize? = nil
    @objc dynamic var photoDocSizeMid: VKSize? = nil
    @objc dynamic var photoDocSizeMax: VKSize? = nil
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        id = json["id"].int64 ?? 0
        owner_id = json["owner_id"].int64 ?? 0
        created = json["created"].int64 ?? 0
        owner_id = json["owner_id"].int64 ?? 0
        thumb_id = json["thumb_id"].int64 ?? 0
        title = json["title"].string ?? ""
        description_vk = json["description"].string ?? ""
        updated = json["updated"].int64 ?? 0
        size = json["size"].int ?? 0
        
        
        
        let thumbJSON = json["thumb"]
        if thumbJSON != JSON.null{
            thumb = VKPhoto(json: thumbJSON, prnt_id: prnt_id)
        } else {
            let photoJSON = json["sizes"]
            if photoJSON != JSON.null{
                let items = photoJSON.array ?? []
                
                for item in items{
                    let size = VKSize(json: item, prnt_id: prnt_id)
                    
                    if size.type == "s" { photoDocSizeMin = size }
                    else if size.type == "y" || size.type == "x" {
                        if photoDocSizeMid == nil { photoDocSizeMid = size }
                        else if size.type == "y" { photoDocSizeMid = size }
                    }
                    else if size.type == "z" || size.type == "w" {
                        if photoDocSizeMax == nil { photoDocSizeMax = size }
                        else if size.type == "w" { photoDocSizeMax = size }
                    }
                }
            }
        }
        
        prntID = prnt_id
    }
    
    @objc dynamic var albumID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "albumID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKAlbum {
            if self.id == object.id &&
                self.thumb == object.thumb &&
                self.owner_id == object.owner_id &&
                self.title == object.title &&
                self.description_vk == object.description_vk &&
                self.created == object.created &&
                self.updated == object.updated &&
                self.size == object.size {
                
                if thumb?.isEqual(object.thumb ?? VKPhoto()) ?? true == false {
                    return false
                }
                
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
    
    func delete(_ realm: Realm){
        if thumb != nil {
            realm.delete(thumb!)
        }
    }
}

class VKCards: Object {
    @objc dynamic var card_id: String = ""
    @objc dynamic var link_url: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var image_url_mid: String = ""
    @objc dynamic var image_url_max: String = ""
    @objc dynamic var price: String = ""
    @objc dynamic var price_old: String = ""
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        card_id = json["card_id"].string ?? ""
        link_url = json["link_url"].string ?? ""
        title = json["title"].string ?? ""
        price = json["price"].string ?? ""
        price_old = json["price_old"].string ?? ""
        
        let items = json["images"].array ?? []
        for item in items{
            if (item["width"].int ?? 0) > 200 && (item["width"].int ?? 0) < 500 { image_url_mid = item["url"].string ?? "" }
            else if (item["width"].int ?? 0) >= 500 { image_url_max = item["url"].string ?? "" }
        }
        
        prntID = prnt_id
    }
    
    @objc dynamic var cardsID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "cardsID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKCards {
            if self.card_id == object.card_id &&
                self.link_url == object.link_url &&
                self.title == object.title &&
                self.price == object.price &&
                self.price_old == object.price_old {
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
    }
    
    func copyValue(_ cardSource: VKCards) {
        self.card_id = cardSource.card_id
        self.cardsID = cardSource.cardsID
        self.image_url_max = cardSource.image_url_max
        self.image_url_mid = cardSource.image_url_mid
        self.link_url = cardSource.link_url
        self.price = cardSource.price
        self.price_old = cardSource.price_old
        self.prntID = cardSource.prntID
        self.title = cardSource.title
    }
}

class VKWall: Object {
    @objc dynamic var id: Int64 = 0
    @objc dynamic var owner_id: Int64 = 0
    @objc dynamic var from_id: Int64 = 0
    @objc dynamic var date: Int64 = 0
    @objc dynamic var text: String = ""
    @objc dynamic var reply_owner_id: Int64 = 0
    @objc dynamic var reply_post_id: Int64 = 0
    @objc dynamic var friends_only: Bool = false // 1 - wall only for friends
    @objc dynamic var comments_count: Int = 0
    @objc dynamic var likes_count: Int = 0
    @objc dynamic var likes_user_likes: Bool = false // 1 - user liked this wall
    @objc dynamic var reposts_count: Int = 0
    @objc dynamic var reposts_user_reposted: Bool = false // 1 - user reposted this wall
    @objc dynamic var views_count: Int = 0
    
    @objc dynamic var geo: VKGeo? = nil
    
    let attachments = List<VKAttachments>()
    
    @objc dynamic var prntID: String = ""
    
    convenience init(json: JSON, prnt_id: String) {
        self.init()
        prntID = prnt_id
        
        id = json["id"].int64 ?? 0
        owner_id = json["owner_id"].int64 ?? json["to_id"].int64 ?? 0
        from_id = json["from_id"].int64 ?? 0
        date = json["date"].int64 ?? 0
        text = json["text"].string ?? ""
        reply_owner_id = json["reply_owner_id"].int64 ?? 0
        reply_post_id = json["reply_post_id"].int64 ?? 0
        friends_only = (json["friends_only"].int8 ?? 0) == 0 ? false : true
        comments_count = json["comments_count"].int ?? 0
        likes_count = json["likes_count"].int ?? 0
        likes_user_likes = (json["likes_user_likes"].int8 ?? 0) == 0 ? false : true
        reposts_count = json["reposts_count"].int ?? 0
        reposts_user_reposted = (json["reposts_user_reposted"].int8 ?? 0) == 0 ? false : true
        views_count = json["views_count"].int ?? 0
        
        let geoJSON = json["geo"]
        
        if geoJSON != JSON.null {
            geo = VKGeo(json: geoJSON, prnt_id: prnt_id)
        }
        
        let items = json["attachments"].array ?? []
        
        for item in items {
            attachments.append(VKAttachments(json: item, prnt_id: prnt_id))
        }
    }
    
    @objc dynamic var wallID = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "wallID"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? VKWall {
            if self.id == object.id &&
                self.owner_id == object.owner_id &&
                self.from_id == object.from_id &&
                self.date == object.date &&
                self.text == object.text &&
                self.reply_owner_id == object.reply_owner_id &&
                self.reply_post_id == object.reply_post_id &&
                self.friends_only == object.friends_only &&
                self.comments_count == object.comments_count &&
                self.likes_count == object.likes_count &&
                self.likes_user_likes == object.likes_user_likes &&
                self.reposts_count == object.reposts_count &&
                self.reposts_user_reposted == object.reposts_user_reposted &&
                self.views_count == object.views_count {
                
                if self.geo?.isEqual(object.geo ?? VKGeo()) ?? true == false {
                    return false
                }
                
                if self.attachments.count != object.attachments.count {
                    return false
                }
                
                for i in 0..<attachments.count {
                    var isFound: Bool = false
                    
                    for j in 0..<object.attachments.count {
                        if attachments[i].type == "link" {
                            if attachments[i].link?.url == object.attachments[j].link?.url {
                                isFound = true
                            }
                        } else if attachments[i].type == "sticker" {
                            if attachments[i].sticker?.product_id == object.attachments[j].sticker?.product_id &&
                                attachments[i].sticker?.stickerID == object.attachments[j].sticker?.stickerID {
                                isFound = true
                            }
                        } else if attachments[i].type == "pretty_cards" {
                            if attachments[i].pretty_cards.count > 0 {
                                if attachments[i].pretty_cards[0].card_id == object.attachments[j].pretty_cards[0].card_id {
                                    isFound = true
                                }
                            }
                        } else {
                            if attachments[i].type == object.attachments[j].type &&
                                attachments[i].obj_id == object.attachments[j].obj_id {
                                isFound = true
                            }
                        }
                        
                        if isFound == true {
                            if attachments[i].isEqual(object.attachments[j]) == false {
                                return false
                            } else {
                                break
                            }
                        }
                    }
                }
                
                return true
            }
            else {
                return false
            }
        } else {
            return false
        }
        
    }
    
    func copyValue(_ wallSource: VKWall) {
        self.id = wallSource.id
        self.owner_id = wallSource.owner_id
        self.from_id = wallSource.from_id
        self.date = wallSource.date
        self.text = wallSource.text
        self.reply_owner_id = wallSource.reply_owner_id
        self.reply_post_id = wallSource.reply_post_id
    }
    
    func delete(_ realm: Realm){
        for att in attachments {
            att.delete(realm)
            realm.delete(att)
        }
        if geo != nil {
            realm.delete(geo!)
        }
    }
}
