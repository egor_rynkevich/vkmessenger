//
//  MainData.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 07.04.2018.
//

import Foundation
import SwiftyVK
import SwiftyJSON
import RealmSwift
import Alamofire

class VKLoadData {
    
    var countConvers: Int = 0
    var countUnreadConvers: Int = 0
    // for user
    var usersInfo: [Int64 : VKUser] = [:]
    var friendsInfo: [Int64 : VKUser] = [:]
    var searchUserInfo: [VKUser] = []
    var allSearchUsersCount: Int = 0
    var requestToFriendCount: Int = 0
    var requestToFriendArr: [Int64] = []
    var profilePhoto: [VKPhoto] = []
    
    // for chat and group
    var groupsInfo: [Int64 : VKGroup] = [:]
    var chatsInfo: [Int64 : VKChat] = [:]
    
    // for message
    var countMessInConv: Int = 0
    
    var countUserDocuments: Int = 0
    
    var countryInfo: [String : Int] = [:]
    var cityInfo: [Int : VKSearchCityView.CityInfo] = [:]

    var mainUserID : Int64 = 0
    var mainUser: VKUser = VKUser()
    var BG_LONGPOLL_QUEUE_ID = "VK_LONGPOLL"
    var vkBGLongPollQueue: DispatchQueue?
    var BG_UPLOAD_FILE_QUEUE_ID = "VK_UPLOADFILE"
    var vkBGUploadFileQueue: DispatchQueue?
    
    init() {
        do {          
            vkBGLongPollQueue = DispatchQueue.init(label: BG_LONGPOLL_QUEUE_ID)
            vkBGUploadFileQueue = DispatchQueue.init(label: BG_UPLOAD_FILE_QUEUE_ID)
            longPollInit()
            
            print(Realm.Configuration.defaultConfiguration.fileURL!)
            
            let realm = try Realm()
            
            var main_user = realm.objects(VKUser.self).filter("isMainUser = true").first
            
            if main_user == nil {
                loadMainInfoAboutUser("") { [weak self] (result) in
                    if result == true {
                        DispatchQueue.main.async {
                            if self == nil { return }
                            self!.mainUser.isMainUser = true
                            self!.mainUserID = self!.mainUser.id
                            main_user = VKUser()
                            main_user?.copyValue(self!.mainUser)
                            //self.mainUser.copyValue(main_user!)
                            do {
                                try realm.write({ () -> Void in
                                    realm.create(VKUser.self, value: main_user!, update: true)
                                })
                            } catch let error as NSError {
                                print("init main data - failed save mainuser - \(error)")
                            }
                            self!.usersInfo.removeAll()
                        }
                    } else {
                        print("fail loaded main user")
                    }
                }
            } else {
                mainUserID = main_user?.id ?? 0
                mainUser.copyValue(main_user!)
            }
        } catch let error as NSError {
            print("real didn't init - \(error)")
        }
    }
    
    func longPollInit() {
        vkBGLongPollQueue?.async {
            VK.sessions.default.longPoll.start(version: .third) {
                print($0)
                for event in $0 {
                    VKLongPollDecode.decodeEvent(event)
                }
                
            }
        }
    }
    
    //MARK: Work with Converstions
    
    func loadVKConversations(startID: Int64 = 0, offset: Int = 0, count: Int = 30, view: UIView, completionBlock: @escaping (_:Bool) -> Void) {
        DispatchQueue.main.async { UIApplication.shared.isNetworkActivityIndicatorVisible = true }
        VK.API.Messages.getConversations([.startMessageId: startID == 0 ? "" : String(startID), .offset: String(offset), .count: String(count), .filter: "all"])
            .onSuccess{ [weak self] data in
                self?.parseVKConversations(data, completion: completionBlock)
                print("Data loaded")
                DispatchQueue.main.async { UIApplication.shared.isNetworkActivityIndicatorVisible = false }
        }
            .onError{ error in
                print("MainData - loadVKConversations - Request failed with error: \(error)")
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    ToastView.shared.long(view, txt_msg: "\(error)", isError: true)
                }
                completionBlock(false)
            }
            .send()
    }
    
    func loadVKConversationsByID(peer_ids: String, completionBlock: @escaping (_:Bool) -> Void) {
        VK.API.Messages.getConversationsById([.peerIds:  peer_ids])
            .onSuccess{ [weak self] data in
                self?.parseVKConversations(data, true, completion: completionBlock)
                print("Data loaded")
            }
            .onError{
                print("MainData - loadVKConversationsByID - Request failed with error: \($0)")
            }
            .send()
    }
    
    func reloadVKConversations(completionBlock: @escaping (_ result: Bool) -> Void){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        VK.API.Messages.getConversations([.count: "30"])
            .onSuccess{ [weak self] data in
                self?.parseVKConversations(data, completion: completionBlock)
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            }
            .onError{
                print("Request failed with error: \($0)")
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            }
            .send()
    }
    
    func parseVKConversations(_ param: Data, _ initByID: Bool = false, completion: @escaping (_:Bool) -> Void){
        var user_ids = ""
        var group_ids = ""
        var chat_ids = ""
        let json = JSON(param)
        countConvers = json["count"].int ?? countConvers
        countUnreadConvers = json["unread_count"].int ?? countUnreadConvers
        let items = json["items"]
        var conversToSave: [VKConversation] = []
        
        for (_, value) in items {
            let jsonConvers = initByID == false ? value["conversation"] : value
            let convers = VKConversation(conversationJSON: jsonConvers, messageJSON: value["last_message"])
            
            if convers.peer_type == "chat" {
                chat_ids.append(String(convers.peer_local_id) + ",")
                user_ids.append(convers.chat_settings_active_ids)
                user_ids.append(String(convers.last_message?.from_id ?? 0) + ",")
            }
            else if convers.peer_type == "user" {
                user_ids.append(String(convers.peer_local_id) + ",")
            }
            else if convers.peer_type == "group" {
                group_ids.append(String(convers.peer_local_id) + ",")
            }
            
            conversToSave.append(convers)
        }
        
        var isLoadUsers: Int = -1
        var isLoadGroups: Int = -1
        var isLoadChats: Int = -1
        
        vkDataCache.saveConversationsInCache(conversToSave) { [weak self] (result) in
            if result == true {
                DispatchQueue.main.async { UIApplication.shared.isNetworkActivityIndicatorVisible = true }
                self?.loadMainInfoAboutUser(user_ids) { [weak self] (result) in
                    if result == true {
                        vkDataCache.saveUserInfoInCache(self?.usersInfo ?? [:], completion: { [weak self] (result) in
                            if result == 1 {
                                self?.usersInfo.removeAll()
                                isLoadUsers = 1
                                if isLoadUsers == 1 && isLoadGroups == 1 && isLoadChats == 1 {
                                    DispatchQueue.main.async { UIApplication.shared.isNetworkActivityIndicatorVisible = false }
                                    completion(true)
                                }
                            }
                            else if result == 0 {
                                DispatchQueue.main.async { UIApplication.shared.isNetworkActivityIndicatorVisible = false }
                                completion(false)
                            }
                        })
                    } else {
                        DispatchQueue.main.async { UIApplication.shared.isNetworkActivityIndicatorVisible = false }
                        completion(false)
                    }
                }
                
                self?.loadMainInfoAboutGroup(group_ids, completion: { [weak self] (result) in
                    if result == true {
                        vkDataCache.saveGroupInfoInCache(self?.groupsInfo ?? [:], completion: { [weak self] (result) in
                            if result == 1 {
                                self?.groupsInfo.removeAll()
                                isLoadGroups = 1
                                if isLoadUsers == 1 && isLoadGroups == 1 && isLoadChats == 1 {
                                    DispatchQueue.main.async { UIApplication.shared.isNetworkActivityIndicatorVisible = false }
                                    completion(true)
                                }
                            }
                            else if result == 0 {
                                DispatchQueue.main.async { UIApplication.shared.isNetworkActivityIndicatorVisible = false }
                                completion(false)
                            }
                        })
                    } else {
                        DispatchQueue.main.async { UIApplication.shared.isNetworkActivityIndicatorVisible = false }
                        completion(false)
                    }
                })
                
                self?.loadInfoAboutChat(chat_ids, completion: { [weak self] (result) in
                    if result == true {
                        vkDataCache.saveChatInfoInCache(self?.chatsInfo ?? [:], completion: { [weak self] (result) in
                            if result == 1 {
                                self?.chatsInfo.removeAll()
                                isLoadChats = 1
                                if isLoadUsers == 1 && isLoadGroups == 1 && isLoadChats == 1 {
                                    DispatchQueue.main.async { UIApplication.shared.isNetworkActivityIndicatorVisible = false }
                                    completion(true)
                                }
                            }
                            else if result == 0 {
                                DispatchQueue.main.async { UIApplication.shared.isNetworkActivityIndicatorVisible = false }
                                completion(false)
                            }
                        })
                    } else {
                        DispatchQueue.main.async { UIApplication.shared.isNetworkActivityIndicatorVisible = false }
                        completion(false)
                    }
                })
            }
        }
        
        
//        vkDataCache.saveConversationsInCache(conversToSave) { [weak self] (result) in
//            if result == true {
//                self?.loadMainInfoAboutUser(user_ids) { [weak self] (result) in
//                    if result == true {
//                        vkDataCache.saveUserInfoInCache(self?.usersInfo ?? [:], completion: { [weak self] (result) in
//                            if result == true {
//                                self?.usersInfo.removeAll()
//                                self?.loadMainInfoAboutGroup(group_ids, completion: { [weak self] (result) in
//                                    if result == true {
//                                        vkDataCache.saveGroupInfoInCache(self?.groupsInfo ?? [:], completion: { [weak self] (result) in
//                                            if result == true {
//                                                self?.groupsInfo.removeAll()
//                                                self?.loadInfoAboutChat(chat_ids, completion: { [weak self] (result) in
//                                                    if result == true {
//                                                        vkDataCache.saveChatInfoInCache(self?.chatsInfo ?? [:], completion: { [weak self] (result) in
//                                                            if result == true {
//                                                                self?.chatsInfo.removeAll()
//                                                                completion(true)
//                                                            }
//                                                        })
//                                                    }
//                                                })
//                                            }
//                                        })
//                                    }
//                                })
//                            }
//                        })
//                    }
//                }
//            }
//        }
        //completion(false)
    }
    
    func deleteConversation(peer_id: Int64, peer_type: String, completion: @escaping (_:Bool) -> Void) {
        var param: SwiftyVK.Parameters = [:]
        param[peer_type == "user" ? Parameter.userId : Parameter.peerId] = String(peer_id)
        
        VK.API.Messages.deleteConversation(param).onSuccess { _ in
                completion(true)
            }.onError { (error) in
                print("MainData - deleteConversation - Request failed with error: \(error)")
                completion(false)
            }.send()
    }
    
    func readConversation(peer_id: Int64, completion: @escaping (_:Bool) -> Void) {
        VK.API.Messages.markAsRead([.peerId: String(peer_id)]).onSuccess { _ in
            completion(true)
            }.onError { (error) in
                print("MainData - readConversation - Request failed with error: \(error)")
                completion(false)
            }.send()
    }
    
    //MARK: Work with Group
    
    func loadMainInfoAboutGroup(_ group_ids: String, completion: @escaping (_:Bool) -> Void){
        if group_ids.count != 0 {
            VK.API.Groups.getById([.fields : "photo_100", .groupIds : group_ids])
                .onSuccess{ [weak self] data in
                    let jsonGroup = JSON(data)
                    
                    for (_, value) in jsonGroup{
                        let groupInfo = VKGroup(json: value, prnt_id: "")
                        groupInfo.id *= -1
                        self?.groupsInfo[groupInfo.id] = groupInfo
                    }
                    completion(true)
                    
                }.onError{
                    print("MainData - LoadInfoAboutGroup - Request failed with error: \($0)")
                    completion(false)
                }.send()
        }
        else {
            completion(true)
        }
    }
    
    //MARK: Work with User
    
    func loadMainInfoAboutUser(_ user_ids: String, completion: @escaping (_:Bool) -> Void){
        VK.API.Users.get([.fields : "photo_100, online, last_seen, counters, bdate, blacklisted, blacklisted_by_me, friend_status, is_friend", .userIDs : user_ids, .nameCase : "nom"])
            .onSuccess{ [weak self] data in
                let jsonUser = JSON(data)
                
                if user_ids == "" && (self?.mainUserID ?? 1) == 0{
                    self?.mainUser = VKUser(json: jsonUser[0], prnt_id: "")
                }
                else {
                    for (_, value) in jsonUser{
                        let userInfo = VKUser(json: value, prnt_id: "")
                        self?.usersInfo[userInfo.id] = userInfo
                    }
                }
                
                completion(true)
            }
            .onError{
                print("MainData - LoadInfoAboutUser - Request failed with error: \($0)")
                completion(false)
            }
            .send()
    }
    
    func loadAllInfoAboutUser(_ user_ids: String, completion: @escaping (_:Bool) -> Void) {
        let fields: String = "photo_100, photo_max_orig, crop_photo, deactivated, bdate, blacklisted, blacklisted_by_me, can_write_private_message, contacts, online, last_seen, friend_status, has_mobile, is_friend, counters"
        
        VK.API.Users.get([.fields : fields, .userIDs : user_ids, .nameCase : "nom"])
            .onSuccess{ [weak self] data in
                let jsonUser = JSON(data)
                
                for (_, value) in jsonUser{
                    let userInfo = VKUser(json: value, prnt_id: "")
                    
                    self?.usersInfo[userInfo.id] = userInfo
                }
                completion(true)
            }
            .onError{
                print("MainData - loadAllInfoAboutUser - Request failed with error: \($0)")
                completion(false)
            }
            .send()
    }
    
    
    func recursionloadUserFriends(user_id: Int64, friends_count: Int, offset: Int, needSave: Bool, completionBlock: @escaping (_:Bool) -> Void) {
        if friends_count < 5000 {
            loadUserFriends(user_id: user_id, count: friends_count, offset: offset) { [weak self] (result) in
                if result == true {
                    if needSave == true {
                        vkDataCache.saveUserInfoInCache(self?.friendsInfo ?? [:], completion: { (result) in
                            if result == 1 {
                                completionBlock(true)
                            }
                            else if result == 0 { completionBlock(false) }
                        })
                    } else {
                        completionBlock(true)
                    }
                } else if result == false {
                    completionBlock(false)
                }
            }
        } else {
            loadUserFriends(user_id: user_id, count: 5_000, offset: offset) { [weak self] (result) in
                if result == true {
                    let newOffset: Int = offset + 5_000
                    let count: Int = friends_count - 5_000
                    self?.recursionloadUserFriends(user_id: user_id, friends_count: count, offset: newOffset, needSave: needSave, completionBlock: completionBlock)
                } else if result == false {
                    completionBlock(false)
                }
            }
        }
    }
    
    func loadUserFriends(user_id: Int64, count: Int, offset: Int, completionBlock: @escaping (_:Bool) -> Void) {
        let fields: String = "photo_100, crop_photo, deactivated, bdate, can_write_private_message, contacts, online, last_seen, friend_status, has_mobile, is_friend"
        VK.API.Friends.get([.userId: "\(user_id)", .order: "hints", .count: "\(count)", .offset: "\(offset)", .fields: fields])
            .onSuccess { [weak self] (data) in
                let json = JSON(data)
                let items = json["items"].array ?? []
                
                for item in items {
                    self?.friendsInfo[item["id"].int64 ?? 0] = VKUser(json: item, prnt_id: "")
                }
                completionBlock(true)
                
            }.onError { (error) in
                print("MainData - loadUserFriends - Request failed with error: \(error)")
                completionBlock(false)
            }.send()
    }
    
    func deleteUserFromFriends(user_id: Int64, completionBlock: @escaping (_:Bool) -> Void) {
        completionBlock(true)
//        VK.API.Friends.delete([.userId: String(user_id)]).onSuccess { (data) in
//                vkDataCache.changeFriendStatus(user_id: user_id)
//                completionBlock(true)
//            }.onError { (error) in
//                completionBlock(false)
//        }.send()
    }
    
    func banPeer(peer_id: Int64, completionBlock: @escaping (_:Bool) -> Void) {
        VK.API.Account.banUser([.ownerId: String(peer_id)]).onSuccess { _ in
            vkDataCache.changeFriendStatus(user_id: peer_id, isBan: peer_id < 0 ? true : false)
            completionBlock(true)
            }.onError { (error) in
                completionBlock(false)
            }.send()
    }
    
    func addToFriends(user_id: Int64, text: String, follow: Bool = false, completionBlock: @escaping (_:Bool) -> Void) {
        var param: SwiftyVK.Parameters = [:]
        param[Parameter.userId] = String(user_id)
        if text.count > 0 { param[Parameter.text] = text }
        if follow == true { param[Parameter.follow] = "1" }
        
        completionBlock(true)
        
//        VK.API.Friends.add(param).onSuccess { _ in
//                completionBlock(true)
//            }.onError { (error) in
//                completionBlock(false)
//            }.send()
    }
    
    func loadProfileImages(peer_id: Int64, completionBlock: @escaping (_:Bool) -> Void) {
        VK.API.Photos.get([.ownerId: String(peer_id), .albumId: "profile", .rev: "1", .photoSizes: "1"])
            .onSuccess { [weak self](data) in
                let json = JSON(data)
                let items = json["items"].array ?? []
                self?.profilePhoto.removeAll()
                for item in items {
                    self?.profilePhoto.append(VKPhoto(json: item, prnt_id: ""))
                }
                completionBlock(true)
            }.onError { (error) in
                print("MainData - loadProfileImages - Request failed with error: \(error)")
                completionBlock(false)
        }.send()
    }
    
    func loadSuggestionsUsers(offset: Int = 0, completionBlock: @escaping (_:Bool) -> Void) {
        let fields: String = "photo_100, deactivated, bdate, online, last_seen, friend_status"
        VK.API.Friends.getSuggestions([.offset: String(offset), .count: "40", .fields: fields])
            .onSuccess { [weak self] (data) in
                let json = JSON(data)
                self?.allSearchUsersCount = json["count"].int ?? 0
                let items = json["items"].array ?? []
                self?.searchUserInfo.removeAll()
                for item in items {
                    self?.searchUserInfo.append(VKUser(json: item, prnt_id: ""))
                }
                completionBlock(true)
                
            }.onError { (error) in
                print("MainData - loadSuggestionsUsers - Request failed with error: \(error)")
                completionBlock(false)
            }.send()
    }
    
    func loadRequestsUsers(offset: Int = 0, isOut: Bool, isNeedViewed: Bool, completionBlock: @escaping (_:Bool) -> Void) {
        var param: SwiftyVK.Parameters = [:]
        param[Parameter.offset] = String(offset)
        param[Parameter.sort] = "0"
        param[Parameter.out] = isOut == true ? "1" : "0"
        param[Parameter.needViewed] = isNeedViewed == true ? "1" : "0"
        if isNeedViewed == true { param[Parameter.count] = "40" }
        
        VK.API.Friends.getRequests(param).chain { [weak self] (response) in
                let json = JSON(response)
                let items = json["items"].array ?? []
            
                if isOut == false && isNeedViewed == false {
                    self?.requestToFriendCount = json["count"].int ?? 0
                    for id in items { self?.requestToFriendArr.append(id.int64 ?? 0) }
                }
                else { self?.allSearchUsersCount = json["count"].int ?? 0 }
            
                var user_ids: String = ""
            
                for id in items { user_ids.append(String(id.int64 ?? 0) + ",") }
            
                return VK.API.Users.get([.fields : "photo_100, crop_photo, deactivated, bdate, can_write_private_message, contacts, online, last_seen, friend_status, has_mobile", .userIDs : user_ids, .nameCase : "nom"])
            }
            .onSuccess { [weak self] (data) in
                let jsonUser = JSON(data)
                
                self?.searchUserInfo.removeAll()
                for (_, value) in jsonUser { self?.searchUserInfo.append(VKUser(json: value, prnt_id: "")) }
                
                completionBlock(true)
                
            }.onError { (error) in
                print("MainData - loadRequestsUsers - Request failed with error: \(error)")
                completionBlock(false)
            }.send()
    }
    
    func searchingUsers(offset: Int = 0, q: String, parameters: VKSearchPeopleViewController.SearchParameters, completionBlock: @escaping (_:Bool) -> Void) {
        var param: SwiftyVK.Parameters = [:]
        param[Parameter.offset] = String(offset)
        param[Parameter.sort] = "0"
        param[Parameter.count] = "40"
        param[Parameter.fields] = "photo_100, deactivated, bdate, online, last_seen, friend_status"
        param[Parameter.q] = q
        if parameters.age_from > 0 { param[Parameter.ageFrom] = String(parameters.age_from) }
        if parameters.age_to > 0 { param[Parameter.ageTo] = String(parameters.age_to) }
        if parameters.city > 0 { param[Parameter.city] = String(parameters.city) }
        if parameters.country > 0 { param[Parameter.country] = String(parameters.country) }
        if parameters.sex > 0 { param[Parameter.sex] = String(parameters.sex) }
        if parameters.status > 0 { param[Parameter.status] = String(parameters.status) }
        
        VK.API.Users.search(param)
            .onSuccess { [weak self] (data) in
                let json = JSON(data)
                self?.allSearchUsersCount = json["count"].int ?? 0
                let items = json["items"].array ?? []
                self?.searchUserInfo.removeAll()
                for item in items {
                    self?.searchUserInfo.append(VKUser(json: item, prnt_id: ""))
                }
                completionBlock(true)
            }.onError { (error) in
                print("MainData - searchingUsers - Request failed with error: \(error)")
                completionBlock(false)
        }.send()
    }
    
    //MARK: Work with Chat
    
    func loadInfoAboutChat(_ chat_ids: String, completion: @escaping (_:Bool) -> Void) {
        if chat_ids.count != 0 {
            VK.API.Messages.getChat([.chatIds : chat_ids])
                .onSuccess{ [weak self] data in
                    let jsonChat = JSON(data)
                    
                    for (_, value) in jsonChat{
                        let chatInfo = VKChat(json: value, prnt_id: "")
                        chatInfo.id += 2_000_000_000
                        self?.chatsInfo[chatInfo.id] = chatInfo
                    }
                    completion(true)
                }
                .onError{
                    print("MainData - LoadInfoAboutChat - Request failed with error: \($0)")
                    completion(false)
                }
                .send()
        } else {
            completion(true)
        }
    }
    
    func loadUsersIdInChats(_ chat_ids: String, completion: @escaping (_:Bool) -> Void) {
        if chat_ids.count != 0 {
            VK.API.Messages.getChat([.chatIds : chat_ids])
                .onSuccess{ [weak self] data in
                    let jsonChats = JSON(data).array ?? []
                    var user_ids = ""
                    for value in jsonChats {
                        let users = value["users"].array ?? []
                        for id in users {
                            user_ids.append(String(id.int64 ?? 0) + ",")
                        }
                    }
                    self?.loadMainInfoAboutUser(user_ids, completion: completion)
                }
                .onError{
                    print("MainData - loadUsersIdInChats- Request failed with error: \($0)")
                    completion(false)
                }
                .send()
        } else {
            completion(true)
        }
    }
    
    func loadInfoAboutChatForLongPoll(_ chat_id: String, completion: @escaping (_:Bool) -> Void) {
        VK.API.Messages.getChat([.chatId : chat_id])
            .onSuccess{ 
                let jsonChat = JSON($0)
                let chat_id = (jsonChat["id"].int64 ?? 0) + 2_000_000_000
                let photo_100 = jsonChat["photo_100"].string ?? ""
                do {
                    let realm = try Realm()
                    
                    guard let chat = realm.objects(VKChat.self).filter("id = %@", chat_id).first else {
                        completion(false)
                        return
                    }
                    try realm.write {
                        chat.title = jsonChat["title"].string ?? chat.title
                        if photo_100 != chat.photo_100 {
                            chat.photo_100 = photo_100
                        }
                    }
                } catch let error as NSError {
                    print(error)
                    completion(false)
                }
                completion(true)
            }
            .onError{
                print("MainData - loadInfoAboutChatForLongPoll- Request failed with error: \($0)")
                completion(false)
            }
            .send()
    }
    
    //MARK: Work with Message

    func loadMessHistory(peer_id: Int64, peer_type: String, conversRef: ThreadSafeReference<VKConversation>, prtID: String, startID: Int64 = -1, offset: Int = 0, count: Int = 40,  completionBlock: @escaping (_:Bool) -> Void){
        if peer_type == VKUserType.isUser.rawValue {
            VK.API.Messages.getHistory([.userId: "\(peer_id)", .startMessageId: startID == -1 ? "" : "\(startID)", .offset: "\(offset)", .count : "\(count)"])
                .onSuccess{
                    self.parseVKMessHistory($0, conversRef, prtID,  completionBlock: { (result) in
                        if result == true {
                            completionBlock(true)
                        } else {
                            completionBlock(false)
                        }
                    })
                    
                }
                .onError{
                    print("MainData - loadMessHistoryForUser - Request failed with error: \($0)")
                    completionBlock(false)
                }
                .send()
        }
        else if peer_type == VKUserType.isGroup.rawValue || peer_type == VKUserType.isChat.rawValue {
            VK.API.Messages.getHistory([.peerId: "\(peer_id)", .startMessageId: startID == -1 ? "" : "\(startID)", .offset: "\(offset)", .count : "\(count)"])
                .onSuccess{ [weak self] data in
                    self?.parseVKMessHistory(data, conversRef, prtID,  completionBlock: { (result) in
                        if result == true {
                            completionBlock(true)
                        } else {
                            completionBlock(false)
                        }
                    })
                }
                .onError{
                    print("MainData - loadMessHistoryForGroup - Request failed with error: \($0)")
                    completionBlock(false)
                }
                .send()
        }
    }
    
    func parseVKMessHistory(_ param: Data, _ conversRef: ThreadSafeReference<VKConversation>, _ prtID: String, completionBlock: @escaping (_:Bool) -> Void){
        var msgs : [VKMessage] = []
        
        let json = JSON(param)
        countMessInConv = json["count"].int ?? 0
        let items = json["items"]
        for (_, value) in items {
            msgs.append(VKMessage(json: value,  prnt_id: prtID))
        }
        
        vkDataCache.saveMsgInCache(msgs, conversRef) { (result) in
            if result == true {
                completionBlock(true)
            } else {
                completionBlock(false)
            }
        }
    }
    
    func deleteMessages(msgIDs: [Int64], group_id: Int64, deleteForAll: Bool, completionBlock: @escaping (_:Bool) -> Void) {
        var param: SwiftyVK.Parameters = [:]
        param[Parameter.deleteForAll] = deleteForAll ? "1" : "0"
        if group_id < 0 { param[Parameter.groupId] = "\(((-1) * group_id))" }
        
        var str = ""
        for msgID in msgIDs {
            str += "\(msgID),"
        }
        param[Parameter.messageIds] = str
        
        VK.API.Messages.delete(param)
            .onSuccess({ (data) in
                let json = JSON(data)
                print(json)
                completionBlock(true)
            })
            .onError{
                print("VKLoadData - deleteMessages - Request failed with error: \($0)")
                completionBlock(false)
            }
            .send()
    }
    
    //MARK: Work with Database
    
    func loadAllCountry(completionBlock: @escaping (_:Bool) -> Void) {
        VK.API.Database.getCountries([.needAll: "1", .count: "999"])
            .onSuccess{ [weak self] (data) in
                let json = JSON(data)
                let items = json["items"].array ?? []
                self?.countryInfo.removeAll()
                for item in items { self?.countryInfo[item["title"].string ?? ""] = item["id"].int ?? 0 }
                completionBlock(true)
            }
            .onError{
                print("VKLoadData - loadAllCountry - Request failed with error: \($0)")
                completionBlock(false)
            }
            .send()
    }
    
    func loadCity(country_id: Int, q: String, completionBlock: @escaping (_:Bool) -> Void) {
        VK.API.Database.getCities([.countryId: String(country_id), .q: q, .count: "999"])
            .onSuccess{ [weak self] (data) in
                let json = JSON(data)
                let items = json["items"].array ?? []
                self?.cityInfo.removeAll()
                for item in items { self?.cityInfo[item["id"].int ?? 0] = VKSearchCityView.CityInfo(id: item["id"].int ?? 0, title: item["title"].string ?? "", area: item["area"].string ?? "", region: item["region"].string ?? "") }
                completionBlock(true)
            }
            .onError{
                print("VKLoadData - loadCity - Request failed with error: \($0)")
                completionBlock(false)
            }
            .send()
    }
    
    //MARK: Work with poll
    func voteInPoll(owner_id: Int64, poll_id: Int64, answersIDs: [Int64], isDelete: Bool, completion: @escaping (_:Bool) -> Void) {
        if answersIDs.count < 1 { completion(false) }
        if isDelete == false {
            VK.API.Polls.addVote([.ownerId: String(owner_id), .pollId: String(poll_id), .answerIds: answersIDs.map { String($0) }.joined(separator: ",") ])
                .onSuccess { (data) in
                    let json = JSON(data)
                    let response = json["response"].int ?? 0
                    
                    if response == 1 { completion(true) }
                    else { completion(false) }
                    
                }.onError { (error) in
                    print("VKLoadData - addVoteInPoll - Request failed with error: \(error)")
                    completion(false)
            }.send()
        } else {
            VK.API.Polls.deleteVote([.ownerId: String(owner_id), .pollId: String(poll_id), .answerId: String(answersIDs[0]) ])
                .onSuccess { (data) in
                    let json = JSON(data)
                    let response = json["response"].int ?? 0
                    
                    if response == 1 { completion(true) }
                    else { completion(false) }
                    
                }.onError { (error) in
                    print("VKLoadData - deleteVoteInPoll - Request failed with error: \(error)")
                    completion(false)
                }.send()
        }
    }
    
    
    //MARK: Work with upload file to server
    func uploadImageToServerForMes(peer_id: String?, data: Data, attID: String, completion: @escaping (_ photo: JSON?) -> Void) {
        DispatchQueue.main.async {
            VK.API.Photos.getMessagesUploadServer([.peerId: peer_id]).onSuccess({ (result) in
                let response = try JSON(data: result)
                
                if let url = response["upload_url"].string {
                    
                    let url = try! URLRequest(url: URL(string:url)!, method: .post, headers: nil)
                    
                    self.vkBGUploadFileQueue?.sync {
                    Alamofire.upload(multipartFormData: { (multipartFormData) in
                        multipartFormData.append(data, withName: "photo", fileName: "image.jpg", mimeType: "image/jpeg")
                    }, with: url,
                       encodingCompletion: { (encodingResult) in
                        switch encodingResult {
                        case .failure(let encodingError):
                            print(encodingError)
                        case .success(let upload, false, _):
                            upload.uploadProgress(closure: { (progressFetch) in
                                //print("uploadFile - \(progressFetch.fractionCompleted)")
                                var progressInfo: [String: String] = [:]
                                progressInfo["attID"] = attID
                                progressInfo["progress"] = String(progressFetch.fractionCompleted)
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateUploadAttachment"), object: nil, userInfo: progressInfo)
                            })
                            
                            upload.validate()
                            upload.responseJSON { response in
                                // проверим ответ процедуры загрузки картинки
                                guard response.result.isSuccess else {
                                    print ("Error upload file: \(String(describing: response.result.error))")
                                    completion(nil)
                                    return
                                }
                                
                                let responseJSON = response.result.value as? [String: AnyObject]
                                
                                guard let hash = responseJSON?["hash"] as? String,
                                    let photo = responseJSON?["photo"] as? String,
                                    let server = responseJSON?["server"] as? Int64 else {
                                        print("Bad response")
                                        completion(nil)
                                        return
                                }
                                
                                APIScope.Photos.saveMessagesPhoto([ .photo: photo, .server: String(server), .hash: hash ]).onSuccess({ (data) in
                                    let json = JSON(data)
                                    completion(json)
                                }).onError({ (error) in
                                    print(error)
                                }).send()
                            }
                        default: break
                        }
                    }
                    )
                    }
                }
            }).onError({ (error) in
                print(error)
            }).send()
        }
    }
    
    func uploadVideoToServerForMes(peer_id: String?, data: Data, attID: String, completion: @escaping (_ photo: JSON?) -> Void) {
        DispatchQueue.main.async {
            VK.API.Video.save([.isPrivate: "1"]).onSuccess({ (result) in
                let response = try JSON(data: result)
                if let url = response["upload_url"].string {
                    let url = try! URLRequest(url: URL(string:url)!, method: .post, headers: nil)
                    
                    self.vkBGUploadFileQueue?.sync {
                        Alamofire.upload(multipartFormData: { (multipartFormData) in
                            multipartFormData.append(data, withName: "video_file"/*, fileName: "image.jpg", mimeType: "image/jpeg"*/)
                        }, with: url,
                           encodingCompletion: { (encodingResult) in
                            switch encodingResult {
                            case .failure(let encodingError):
                                print(encodingError)
                            case .success(let upload, false, _):
                                upload.uploadProgress(closure: { (progressFetch) in
                                    //print("uploadFile - \(progressFetch.fractionCompleted)")
                                    var progressInfo: [String: String] = [:]
                                    progressInfo["attID"] = attID
                                    progressInfo["progress"] = String(progressFetch.fractionCompleted)
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateUploadAttachment"), object: nil, userInfo: progressInfo)
                                })
                                
                                upload.validate()
                                upload.responseJSON { response in
                                    guard response.result.isSuccess else {
                                        print ("Error upload file: \(String(describing: response.result.error))")
                                        completion(nil)
                                        return
                                    }
                                    
                                    let responseJSON = response.result.value as? [String: AnyObject]
                                    let videoID = (responseJSON?["video_id"] as? Int64) ?? 0
                                    
                                    let json = JSON(["video_id" : videoID])
                                    completion(json)
                                }
                            default: break
                            }
                        }
                        )
                    }
                }
            }).onError({ (error) in
                print(error)
            }).send()
        }
    }
    
    func uploadDocFileToServer(peer_id: String, data: Data, type: String, file_name: String, attID: String, completion: @escaping (_ doc: JSON?) -> Void) {
        // type: image | video
        DispatchQueue.main.async {
            VK.API.Docs.getUploadServer([:]).onSuccess({ (result) in
                let response = try JSON(data: result)
                
                if let url = response["upload_url"].string {
                    
                    let url = try! URLRequest(url: URL(string:url)!, method: .post, headers: nil)
                    var file_type = file_name.getPathExtension().lowercased()
                    file_type = file_type == "jpg" ? "jpeg" : file_type
                    let mimeType = "\(type)/\(file_type)"
                    self.vkBGUploadFileQueue?.sync {
                        Alamofire.upload(multipartFormData: { (multipartFormData) in
                            multipartFormData.append(data, withName: "file", fileName: file_name, mimeType: mimeType)
                        }, with: url,
                           encodingCompletion: { (encodingResult) in
                            switch encodingResult {
                            case .failure(let encodingError):
                                print(encodingError)
                            case .success(let upload, false, _):
                                upload.uploadProgress(closure: { (progressFetch) in
                                    //print("uploadFile - \(progressFetch.fractionCompleted)")
                                    var progressInfo: [String: String] = [:]
                                    progressInfo["attID"] = attID
                                    progressInfo["progress"] = String(progressFetch.fractionCompleted)
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateUploadAttachment"), object: nil, userInfo: progressInfo)
                                })
                                
                                upload.validate()
                                upload.responseJSON { response in
                                    // проверим ответ процедуры загрузки картинки
                                    guard response.result.isSuccess else {
                                        print ("Error upload file: \(String(describing: response.result.error))")
                                        completion(nil)
                                        return
                                    }
                                    
                                    let responseJSON = response.result.value as? [String: AnyObject]
                                    
                                    guard let file = responseJSON?["file"] as? String else {
                                            print("Bad response \(file_name)")
                                            completion(nil)
                                            return
                                    }
                                    
                                    APIScope.Docs.save([.file: file, .title: file_name])
                                        .onSuccess({ (data) in
                                        let json = JSON(data)
                                        completion(json)
                                    }).onError({ (error) in
                                        print(error)
                                    }).send()
                                }
                            default:
                                print("dermo")
                                break
                            }
                        }
                        )
                    }
                }
            }).onError({ (error) in
                print(error)
            }).send()
        }
    }
    
    //MARK: work with photos album
    
    func loadUserPhotoAlbums(peer_id: Int64, completion: @escaping (_ photo: [VKAlbum]) -> Void) {
        VK.API.Photos.getAlbums([.ownerId: String(peer_id), .needSystem : "1", .needCovers : "1", .photoSizes: "1"])
            .onSuccess { (data) in
                let json = JSON(data)
                var albums: [VKAlbum] = []
                let items = json["items"].array ?? []
                
                for item in items { albums.append(VKAlbum(json: item, prnt_id: "")) }
                
                completion(albums)
            }.onError { (error) in
                print("vkLoadData - loadUserPhotoAlbums - \(error)")
                completion([])
            }.send()
    }
    
    func loadUserPhotosFromAlbum(peer_id: Int64, album_id: Int64, completion: @escaping (_ photo: [VKPhoto]) -> Void) {
        VK.API.Photos.get([.ownerId: String(peer_id), .albumId: String(album_id), .rev: "1"])
            .onSuccess { (data) in
                let json = JSON(data)
                var photos: [VKPhoto] = []
                let items = json["items"].array ?? []
                
                for item in items { photos.append(VKPhoto(json: item, prnt_id: "")) }
                
                completion(photos)
            }.onError { (error) in
                print("vkLoadData - loadUserPhotosFromAlbum - \(error)")
                completion([])
            }.send()
    }
    
    //MARK: work with video album
    
    func loadUserVideoAlbums(peer_id: Int64, completion: @escaping (_ photo: [VKVideoAlbum]) -> Void) {
        VK.API.Video.getAlbums([.ownerId: String(peer_id), .needSystem : "1", .extended : "1"])
            .onSuccess { (data) in
                let json = JSON(data)
                var albums: [VKVideoAlbum] = []
                let items = json["items"].array ?? []
                
                for item in items { albums.append(VKVideoAlbum(json: item)) }
                
                completion(albums)
            }.onError { (error) in
                print("vkLoadData - loadUserVideoAlbums - \(error)")
                completion([])
            }.send()
    }
    
    func loadUserVideosFromAlbum(peer_id: Int64, album_id: Int64, completion: @escaping (_ photo: [VKVideo]) -> Void) {
        VK.API.Video.get([.ownerId: String(peer_id), .albumId: String(album_id)])
            .onSuccess { (data) in
                let json = JSON(data)
                var videos: [VKVideo] = []
                let items = json["items"].array ?? []
                
                for item in items { videos.append(VKVideo(json: item, prnt_id: "")) }
                
                completion(videos)
            }.onError { (error) in
                print("vkLoadData - loadUserVideosFromAlbum - \(error)")
                completion([])
            }.send()
    }
    
    //MARK: work with user documents
    
    func loadUserDocuments(peer_id: Int64, count: Int = 1999, offset: Int = 0, completion: @escaping (_ documents: [VKDocument]) -> Void) {
        VK.API.Docs.get([.ownerId: String(peer_id), .count: String(count), .offset: String(offset)])
            .onSuccess { (data) in
                let json = JSON(data)
                var documents: [VKDocument] = []
                let items = json["items"].array ?? []
                self.countUserDocuments = json["count"].int ?? 0
                for item in items { documents.append(VKDocument(json: item, prnt_id: "")) }
                
                completion(documents)
            }.onError { (error) in
                print("vkLoadData - loadUserDocuments - \(error)")
                completion([])
            }.send()
    }
}
