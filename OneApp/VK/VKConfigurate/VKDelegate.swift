//
//  VKDelegate.swift
//  SocialNetworksBrowser
//
//  Created by NCHD on 4/2/18.
//  Copyright © 2018 EgoSecure. All rights reserved.
//

import Foundation
import SwiftyVK
import UIKit

class VKDelegate : SwiftyVKDelegate {

    let appId = "6399630"
    let scopes: Scopes = [.messages,.offline,.friends,.wall,.photos,.audio,.video,.docs,.market,.email]
    
    init() {
        VK.setUp(appId: appId, delegate: self)
        VK.sessions.default.config.apiVersion = "5.92"
        VK.sessions.default.config.language = .en
        VK.sessions.default.config.attemptsMaxLimit = 2
        VK.sessions.default.config.attemptTimeout = 15
    }
    
    func vkNeedsScopes(for sessionId: String) -> Scopes {
        return scopes
    }
    
    func vkNeedToPresent(viewController: VKViewController) {
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            rootController.present(viewController, animated: true)
        }
    }
    
    func vkTokenCreated(for sessionId: String, info: [String : String]) {
        
    }
    
    func vkTokenUpdated(for sessionId: String, info: [String : String]) {
        
    }
    
    func vkTokenRemoved(for sessionId: String) {
        
    }
    
}
