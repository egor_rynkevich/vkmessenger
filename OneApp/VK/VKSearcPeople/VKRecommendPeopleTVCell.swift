//
//  VKRecommendPeoleTVCell.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 03.08.2018.
//

import UIKit

protocol VKRecommendPeopleAbbBtnDelegate: class {
    func vkRecommendPeopleAbbBtnClicked(user_id: Int64, completion: @escaping (_ :Bool) -> Void)
}

class VKRecommendPeopleTVCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var onlineImgView: UIImageView!
    @IBOutlet weak var widthIsOnlineImgCnstr: NSLayoutConstraint!
    
    var user_id: Int64 = 0
    
    weak var delegate: VKRecommendPeopleAbbBtnDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configCell(data: NSData?, userName: String, user_id: Int64, isOnline: Bool, isMobile: Bool, isFriend: Bool) {
        nameLbl.text = userName
        nameLbl.textColor = mainColors.text
        self.user_id = user_id
        
        addBtn.isHidden = false
        addBtn.setImage(isFriend == true ? mainIcons.checkInCircle_s25 : mainIcons.addUser_s25, for: .normal)
        addBtn.setTitle(isFriend == true ? "" : " Add", for: .normal)
        addBtn.tintColor = mainColors.writeRow
        addBtn.isEnabled = !isFriend
        
        if data != nil {
            imgView.image = UIImage(data: data! as Data)
        } else {
            imgView.image = mainIcons.userProfile_s50
            imgView.tintColor = UIColor(white: 0.8, alpha: 1)
        }
        
        if isOnline == true ||
            isMobile == true {
            let image = isMobile == true ? mainIcons.userPhoneOnline : mainIcons.userOnline
            let edgeInsets = isMobile == true ? UIEdgeInsets(top: 1, left: 0, bottom: 1, right: 0) : UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            onlineImgView.image = image?.resizableImage(withCapInsets: edgeInsets, resizingMode: .stretch).withRenderingMode(.alwaysTemplate)
            
            onlineImgView.tintColor = mainColors.online
            onlineImgView.backgroundColor = mainColors.mainTableView
            onlineImgView.layer.borderColor = mainColors.mainTableView?.cgColor
            
            if isMobile == false {
                onlineImgView.layer.masksToBounds = true
                onlineImgView.layer.cornerRadius = 6
                onlineImgView.layer.borderWidth = 2
                widthIsOnlineImgCnstr.constant = 13
            } else {
                onlineImgView.layer.masksToBounds = true
                onlineImgView.layer.cornerRadius = 3
                onlineImgView.layer.borderWidth = 1
                widthIsOnlineImgCnstr.constant = 11
                onlineImgView.contentMode = .scaleAspectFill
            }
        }
        else{
            onlineImgView.image = UIImage()
            onlineImgView.backgroundColor = nil
            onlineImgView.layer.borderWidth = 0
        }
    }

    @IBAction func addBtnAction(_ sender: Any) {
        addBtn.isHidden = true
        delegate?.vkRecommendPeopleAbbBtnClicked(user_id: user_id, completion: { [weak self] (result) in
            if result == false {
                self?.addBtn.isHidden = false
            } else if result == true {
                self?.addBtn.isHidden = false
                self?.addBtn.setImage(mainIcons.checkInCircle_s25, for: .normal)
                self?.addBtn.setTitle("", for: .normal)
                self?.addBtn.isEnabled = false
            }
        })
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
