//
//  VKSearchCountryView.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 10.08.2018.
//

import UIKit

protocol VKSearchCountryCellSelect: class {
    func vkSearchCountryCellSelect(title: String, id: Int)
}

class VKSearchCountryView: UIView {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var countryDict: [String : Int] = [:]
    var countryTitle: [String] = []
    var selectedCountry: String = ""
    var searchCountryTitle: [String] = []
    var isSearch: Bool = false
    var searchBar: UISearchBar = UISearchBar()
    
    weak var delegate: VKSearchCountryCellSelect?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("VKSearchCountryView", owner: self, options: nil)
        addSubview(view)
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func configView(country_id: Int) {
        countryLbl.text = "Country"
        countryLbl.textColor = mainColors.text
        
        closeBtn.setImage(mainIcons.close_s20, for: .normal)
        closeBtn.tintColor = mainColors.lightGray
        
        view.layer.borderColor = mainColors.darkGray?.cgColor
        view.backgroundColor = mainColors.mainTableView
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = mainColors.mainTableView
        tableView.separatorColor = mainColors.separator
        
        vkLoadData.loadAllCountry { [weak self] (result) in
            if result == true {
                self?.countryDict = vkLoadData.countryInfo
                for (key,value) in (self?.countryDict ?? [:]) {
                    self?.countryTitle.append(key)
                    if value == country_id { self?.selectedCountry = key }
                }
                self?.countryTitle = self?.countryTitle.sorted(by: { $0 < $1 }) ?? []
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                    self?.initSearchBar()
                }
            }
        }
    }

}

extension VKSearchCountryView: UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    func initSearchBar() {
        searchBar.searchBarStyle = UISearchBar.Style.prominent
        searchBar.placeholder = " Search..."
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
        searchBar.changeColorMode(mainColors.mainTableView)
        //searchBar.barTintColor = mainColors.mainTableView
        tableView.tableHeaderView = searchBar
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        isSearch = true
        searchCountryTitle.removeAll()
        searchBar.showsCancelButton = true
        
        if searchText.count == 0 {
            searchCountryTitle = countryTitle
        } else {
            searchCountryTitle = countryTitle.reduce(into: []) { (searchCountryTitle, title) in if title.contains(searchText) { searchCountryTitle.append(title) } }
        }
        
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchCountryTitle.removeAll()
        isSearch = false
        searchBar.showsCancelButton = false
        searchBar.text = nil
        searchBar.resignFirstResponder()
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearch == true ? searchCountryTitle.count : countryTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        let title = isSearch == true ? searchCountryTitle[indexPath.row] : countryTitle[indexPath.row]
        cell.textLabel?.text = title
        cell.textLabel?.textColor = mainColors.text
        if selectedCountry == title { cell.accessoryType = .checkmark } else { cell.accessoryType = .none }
        cell.backgroundColor = mainColors.mainTableView
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let title = isSearch == true ? searchCountryTitle[indexPath.row] : countryTitle[indexPath.row]
        let id = countryDict[title] ?? 0
        delegate?.vkSearchCountryCellSelect(title: title, id: id)
    }
}
