//
//  VKSearchParametersView.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 09.08.2018.
//

import UIKit

@IBDesignable class VKSearchParametersView: UIView {

  
    @IBOutlet weak var view: UIView!
    
    @IBOutlet weak var clearBtn: UIButton!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var countyBtn: UIButton!
    @IBOutlet weak var cityBtn: UIButton!
    @IBOutlet weak var statusBtn: UIButton!
    @IBOutlet weak var resultBtn: UIButton!
    
    @IBOutlet weak var sexCntrl: UISegmentedControl!
    
    @IBOutlet weak var ageSlider: RangeSlider!
    
    @IBOutlet weak var paramLbl: UILabel!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var sexLbl: UILabel!
    @IBOutlet weak var ageLbl: UILabel!
    @IBOutlet weak var ageRangeLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("VKSearchParametersView", owner: self, options: nil)
        addSubview(view)
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    func configView(param: VKSearchPeopleViewController.SearchParameters) {
        paramLbl.text = "Parameters"
        paramLbl.textColor = mainColors.text
        countryLbl.text = "Country"
        countryLbl.textColor = mainColors.text
        cityLbl.text = "City"
        cityLbl.textColor = mainColors.text
        sexLbl.text = "Sex"
        sexLbl.textColor = mainColors.text
        ageLbl.text = "Age"
        ageLbl.textColor = mainColors.text
        statusLbl.text = "Family status"
        statusLbl.textColor = mainColors.text
        
        sexCntrl.setTitle("Any", forSegmentAt: 0)
        sexCntrl.setTitle("Female", forSegmentAt: 1)
        sexCntrl.setTitle("Male", forSegmentAt: 2)
        sexCntrl.selectedSegmentIndex = Int(param.sex)
        sexCntrl.tintColor = mainColors.writeRow
        
        clearBtn.setTitle("Clear", for: .normal)
    
        clearBtn.isEnabled = !param.isEqual(param: VKSearchPeopleViewController.SearchParameters())
        
        closeBtn.setImage(mainIcons.close_s20, for: .normal)
        closeBtn.tintColor = mainColors.lightGray
        
        ageSlider.lowerValue = Double(param.age_from)
        ageSlider.upperValue = param.age_to == 0 ? 104.0 : Double(param.age_to + 4)
        setAgeRangeLbl()
        
        btnConfig(btn: countyBtn, title: param.countryTitle == "" ? "Country" : param.countryTitle, img: mainIcons.down)
        btnConfig(btn: cityBtn, title: param.cityTitle == "" ? "City" : param.cityTitle, img: mainIcons.down)
        cityBtn.isEnabled = param.city != 0 ? true : false
        btnConfig(btn: statusBtn, title: param.statusTitle == "" ? "Any" : param.statusTitle, img: mainIcons.down)

        resultBtn.setTitle("Show result", for: .normal) 
        
        view.layer.borderColor = mainColors.darkGray?.cgColor
        view.backgroundColor = mainColors.mainTableView
    }
    
    func btnConfig(btn: UIButton, title: String, img: UIImage?) {
        btn.setTitle(title, for: .normal)
        btn.setImage(img, for: .normal)
        btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: view.frame.width - 60, bottom: 0, right: 0)
        btn.titleEdgeInsets = UIEdgeInsets(top: 0, left: -15, bottom: 0, right: 0)
        btn.tintColor = mainColors.darkGray
        btn.setTitleColor(mainColors.darkGray, for: .normal)
        btn.layer.borderColor = mainColors.darkGray?.cgColor
        btn.layer.borderWidth = 1
    }
    
    func setAgeRangeLbl() {
        let lowerVal = Int(ageSlider.lowerValue)
        let upperVal = Int(ageSlider.upperValue) - 4
        var result = ""
        
        if lowerVal == upperVal || lowerVal == (upperVal + 1) { result = String(lowerVal) }
        else if lowerVal == 0 && upperVal == 100 { result = "Any" }
        else { result = String(lowerVal) + "-" + String(upperVal) }
        
        ageRangeLbl.text = result
        ageRangeLbl.textColor = mainColors.text
    }
}
