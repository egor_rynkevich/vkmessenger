//
//  VKSearchStatusView.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 11.08.2018.
//

import UIKit

protocol VKSearchStatusCellSelect: class {
    func vkSearchStatusCellSelect(title: String, id: Int)
}

class VKSearchStatusView: UIView {
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var status_id: Int = 0
    
    let status: [Int: String] = [0 : "Any", 1 : "не женат (не замужем)", 2 : "встречается", 3 : "помолвлен(-а)", 4 : "женат (замужем)", 5 : "всё сложно", 6 : "в активном поиске", 7 : "влюблен(-а)", 8 : "в гражданском браке"]
    
    weak var delegate: VKSearchStatusCellSelect?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("VKSearchStatusView", owner: self, options: nil)
        addSubview(view)
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func configView(status_id: Int) {
        self.status_id = status_id
        
        statusLbl.text = "Status"
        statusLbl.textColor = mainColors.text
        
        closeBtn.setImage(mainIcons.close_s20, for: .normal)
        closeBtn.tintColor = mainColors.lightGray
        
        view.layer.borderColor = mainColors.darkGray?.cgColor
        view.backgroundColor = mainColors.mainTableView
        tableView.separatorColor = mainColors.separator
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = mainColors.mainTableView
    }
    
}

extension VKSearchStatusView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return status.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = status[indexPath.row] ?? ""
        cell.textLabel?.textColor = mainColors.text
        if status_id == indexPath.row { cell.accessoryType = .checkmark } else { cell.accessoryType = .none }
        cell.backgroundColor = mainColors.mainTableView
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        delegate?.vkSearchStatusCellSelect(title: status[indexPath.row] ?? "", id: indexPath.row)
    }
}
