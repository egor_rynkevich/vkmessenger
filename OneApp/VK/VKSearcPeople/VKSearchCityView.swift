//
//  VKSearchCityView.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 11.08.2018.
//

import UIKit

protocol VKSearchCityCellSelect: class {
    func vkSearchCityCellSelect(title: String, id: Int)
}

class VKSearchCityView: UIView {
    
    struct CityInfo {
        var id: Int = 0
        var title: String = ""
        var area: String = ""
        var region: String = ""
    }
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var cityDict: [Int : CityInfo] = [:]
    var citysID: [Int] = []
    var country_id: Int = 0
    var city_id: Int = 0
    var searchBar: UISearchBar!
    var searchingText: String = ""
    
    weak var delegate: VKSearchCityCellSelect?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("VKSearchCityView", owner: self, options: nil)
        addSubview(view)
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func configView(country_id: Int, city_id: Int) {
        self.city_id = city_id
        self.country_id = country_id
        
        cityLbl.text = "City"
        cityLbl.textColor = mainColors.text
        
        closeBtn.setImage(mainIcons.close_s20, for: .normal)
        closeBtn.tintColor = mainColors.lightGray
        
        view.layer.borderColor = mainColors.darkGray?.cgColor
        view.backgroundColor = mainColors.mainTableView
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = mainColors.mainTableView
        tableView.separatorColor = mainColors.separator
        
        vkLoadData.loadCity(country_id: country_id, q: "") { [weak self] (result) in
            if result == true {
                self?.cityDict = vkLoadData.cityInfo
                self?.citysID.removeAll()
                for (key, _) in (self?.cityDict ?? [:]) { self?.citysID.append(key) }
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                    self?.initSearchBar()
                }
            }
        }
    }
    
}

extension VKSearchCityView: UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    func initSearchBar() {
        searchBar = UISearchBar()
        searchBar.searchBarStyle = UISearchBar.Style.prominent
        searchBar.placeholder = " Search..."
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
        searchBar.changeColorMode(mainColors.mainTableView)
        tableView.tableHeaderView = searchBar
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchBar.showsCancelButton = true
        searchingText = searchText
        vkLoadData.loadCity(country_id: country_id, q: searchingText) { (result) in
            if result == true && self.searchingText == searchText {
                self.cityDict = vkLoadData.cityInfo
                self.citysID.removeAll()
                for (key, _) in self.cityDict { self.citysID.append(key) }
                DispatchQueue.main.async { self.tableView.reloadData() }
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = nil
        searchBar.resignFirstResponder()
        
        searchingText = ""
        vkLoadData.loadCity(country_id: country_id, q: searchingText) { (result) in
            if result == true {
                self.cityDict = vkLoadData.cityInfo
                self.citysID.removeAll()
                for (key, _) in self.cityDict { self.citysID.append(key) }
                DispatchQueue.main.async { self.tableView.reloadData() }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return citysID.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "cellIdentifier")
        }
        let city = cityDict[citysID[indexPath.row]]
        
        cell?.textLabel?.text = city?.title
        cell?.textLabel?.textColor = mainColors.text
        if city_id == city?.id { cell?.accessoryType = .checkmark } else { cell?.accessoryType = .none }
        
        if (city?.area ?? "") != "" || (city?.region ?? "") != "" { cell?.detailTextLabel?.text = (city?.area ?? "") + ", " + (city?.region ?? "") } else { cell?.detailTextLabel?.text = nil }
        cell?.backgroundColor = mainColors.mainTableView
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        delegate?.vkSearchCityCellSelect(title: cityDict[citysID[indexPath.row]]?.title ?? "", id: cityDict[citysID[indexPath.row]]?.id ?? 0)
    }
}
