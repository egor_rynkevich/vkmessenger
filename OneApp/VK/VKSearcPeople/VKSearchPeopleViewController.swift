//
//  VKSearchPeopleViewController.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 03.08.2018.
//

import UIKit

class VKSearchPeopleViewController: UIViewController, UITabBarDelegate, UITableViewDelegate, UITableViewDataSource, VKRecommendPeopleAbbBtnDelegate, VKIORequestPeopleCellDelegate, UISearchBarDelegate, VKSearchCountryCellSelect, VKSearchCityCellSelect, VKSearchStatusCellSelect {

    enum SearchPeopleCellType: Int8 {
        case recommend = 0
        case input = 1
        case output = 2
        case search = 3
    }

    struct SearchParameters {
        var city: Int = 0
        var cityTitle: String = ""
        var country: Int = 0
        var countryTitle: String = ""
        var sex: Int8 = 0
        var status: Int8 = 0
        var statusTitle: String = ""
        var age_from: Int8 = 0
        var age_to: Int8 = 0
        
        func isEqual(param: SearchParameters) -> Bool {
            return self.city == param.city &&
            self.cityTitle == param.cityTitle &&
            self.country == param.country &&
            self.countryTitle == param.countryTitle &&
            self.sex == param.sex &&
            self.status == param.status &&
            self.statusTitle == param.statusTitle &&
            self.age_from == param.age_from &&
            self.age_to == param.age_to
        }
    }
    
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var tableView: UITableView!
    
    private var searchParamView: VKSearchParametersView!
    private var countryView: VKSearchCountryView!
    private var cityView: VKSearchCityView!
    private var statusView: VKSearchStatusView!
    
    private var cellType: Int8 = SearchPeopleCellType.recommend.rawValue
    private var users: [VKUser] = []
    private var allUsersCount: Int = 0
    
    private var searchBar: UISearchBar = UISearchBar()
    private var searchText: String = ""
    private var parameters: SearchParameters = SearchParameters()
    private var isAddSearchBar: Bool = false
    
    private var cellImageLoader : CellImageLoader?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "New people"
        cellImageLoader = CellImageLoader(withImageLoadedListener: self)
        
        registerNib()
        initTableView()
        initTabBar()
    }
    
    //MARK: Init UI
    
    func initTabBar() {
        let recommendBarItem = UITabBarItem(title: "Suggestions", image: mainIcons.recommend_s30, tag: 0)
        let inputBarItem = UITabBarItem(title: "Input", image: mainIcons.userWithInputArrow_s30, tag: 1)
        let outputBarItem = UITabBarItem(title: "Output", image: mainIcons.userWithOutputArrow_s30, tag: 2)
        let globalBarItem = UITabBarItem(title: "Search", image: mainIcons.earth_s30, tag: 3)
        inputBarItem.badgeColor = mainColors.deleteRow
        
        if vkLoadData.requestToFriendCount > 0 {
            inputBarItem.badgeValue = String(vkLoadData.requestToFriendCount)
        }
        
        tabBar.setItems([recommendBarItem, inputBarItem, outputBarItem, globalBarItem], animated: true)
        tabBar.selectedItem = recommendBarItem
        tabBar.delegate = self
        tabBar.isTranslucent = false
        tabBar.barTintColor = mainColors.sectionHeader
        tabBar.tintColor = mainColors.writeRow
        loadData()
    }
    
    func initTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableView.backgroundColor = mainColors.mainTableView
        tableView.separatorColor = mainColors.separator
        
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.beginRefreshing()
        
        tableView.refreshControl?.addTarget(self, action: #selector(loadData), for: .valueChanged)
        
        tableView.addInfiniteScroll { [weak self] (tableView) in
            self?.loadData(offset: self?.users.count ?? 0, isReload: false, isInfinite: true)
        }
    }
    
    func registerNib() {
        var nib = UINib(nibName: "VKRecommendPeoleTVCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "VKRecommendPeoleTVCell")
        
        nib = UINib(nibName: "VKIORequestPeopleTVCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "VKIORequestPeopleTVCell")
        
        
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        cellType = Int8(item.tag)
        allUsersCount = 0
        cellImageLoader?.cancelLoadingAllImage()
        cellImageLoader = CellImageLoader(withImageLoadedListener: self)
        tableView.tableHeaderView = nil
        isAddSearchBar = false
        
        users.removeAll()
        
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableView.keyboardDismissMode = .onDrag
        tableView.reloadData()
        tableView.refreshControl?.beginRefreshing()
        
        loadData()
    }
    
    //MARK: SEARCHBAR
    
    func initSearchView() {
        if isAddSearchBar == false {
            searchBar = UISearchBar(frame: CGRect(x: 25, y: 0, width: tableView.frame.width - 30, height: 56))
            searchBar.searchBarStyle = UISearchBar.Style.prominent
            searchBar.placeholder = " Search..."
            searchBar.sizeToFit()
            searchBar.isTranslucent = false
            searchBar.backgroundImage = UIImage()
            searchBar.delegate = self
            searchBar.changeColorMode(mainColors.mainTableView)
            
            let parametersBtn = UIButton(frame: CGRect(x: 5, y: 13, width: 30, height: 30))
            parametersBtn.tintColor = mainColors.writeRow
            parametersBtn.setImage(mainIcons.filter_s20, for: .normal)
            parametersBtn.addTarget(self, action: #selector(clickedParametersBtn), for: .touchUpInside)
            
            let searchView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 56))
            searchView.addSubview(searchBar)
            searchView.addSubview(parametersBtn)
            tableView.tableHeaderView = searchView
            isAddSearchBar = true
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange textSearched: String) {
        searchBar.showsCancelButton = true
        searchText = textSearched
        searchingUsers(offset: 0, isReload: true, isInfinite: false, q: textSearched)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = nil
        searchBar.resignFirstResponder()
        searchText = ""
        searchingUsers(offset: 0, isReload: true, isInfinite: false, q: searchText)
    }
    
    func searchingUsers(offset: Int, isReload: Bool, isInfinite: Bool, q: String) {
        vkLoadData.searchingUsers(offset: offset, q: q, parameters: parameters) { [weak self] (result) in
            if result == true && q == (self?.searchText ?? "") {
                if self == nil { return }
                self!.cellImageLoader?.cancelLoadingAllImage()
                self!.cellImageLoader = CellImageLoader(withImageLoadedListener: self!)
                self!.configData(isReload: isReload, isInfinite: isInfinite)
            }
        }
    }
    
    //MARK: SearchParametersView
    
    @objc func clickedParametersBtn() {
        searchParamView = VKSearchParametersView(frame: CGRect(x: 5, y: self.view.frame.height + self.view.frame.origin.y + 10, width: self.tableView.frame.width - 10, height: 0))
        searchParamView.configView(param: parameters)
        UIApplication.shared.keyWindow?.addSubview(searchParamView)
        
        searchParamView.closeBtn.addTarget(self, action: #selector(closeSearchParamView), for: .touchUpInside)
        searchParamView.ageSlider.addTarget(self, action: #selector(changeAgeSliderValue), for: .valueChanged)
        searchParamView.clearBtn.addTarget(self, action: #selector(clearAction), for: .touchUpInside)
        searchParamView.countyBtn.addTarget(self, action: #selector(clickedCountryBtn), for: .touchUpInside)
        searchParamView.cityBtn.addTarget(self, action: #selector(clickedCityBtn), for: .touchUpInside)
        searchParamView.statusBtn.addTarget(self, action: #selector(clickedStatusBtn), for: .touchUpInside)
        
        searchParamView.resultBtn.addTarget(self, action: #selector(searchWithParam), for: .touchUpInside)
        
        UIView.animate(withDuration: 0.5) {
            self.searchParamView.frame = CGRect(x: 5, y: 30, width: self.view.frame.width - 10, height: self.view.frame.height + 50)
        }
    }
    
    @objc func closeSearchParamView() {
        UIView.animate(withDuration: 0.5, animations: {
            self.searchParamView.frame = CGRect(x: 5, y: UIApplication.shared.keyWindow?.bounds.height ?? 2000, width: self.view.frame.width - 10, height: self.searchParamView.frame.height)
        }) { (result) in if result == true { self.searchParamView.removeFromSuperview() } }
    }
    
    @objc func clearAction() {
        parameters = SearchParameters()
        UIView.animate(withDuration: 0.2) {
            self.searchParamView.configView(param: self.parameters)
        }
    }
    
    @objc func changeAgeSliderValue() {
        searchParamView.setAgeRangeLbl()
        searchParamView.clearBtn.isEnabled = true
        
        parameters.age_from = Int8(searchParamView.ageSlider.lowerValue)
        parameters.age_to = Int8(searchParamView.ageSlider.upperValue) - 4
    }
    
    @objc func searchWithParam() {
        parameters.sex = Int8(searchParamView.sexCntrl.selectedSegmentIndex)
        closeSearchParamView()
        loadData()
    }
    
    //MARK: SearchCountryView
    
    @objc func clickedCountryBtn() {
        countryView = VKSearchCountryView(frame: CGRect(x: 5, y: self.view.frame.height + self.view.frame.origin.y + 10, width: self.tableView.frame.width - 10, height: 0))
        self.countryView.configView(country_id: parameters.country)
        self.countryView.delegate = self
        self.searchParamView.addSubview(countryView)
        
        self.countryView.closeBtn.addTarget(self, action: #selector(closeCountryView), for: .touchUpInside)
        
        let frame = self.searchParamView.frame
        UIView.animate(withDuration: 0.5) {
            self.countryView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height + 15)
        }
    }
    
    @objc func closeCountryView() {
        UIView.animate(withDuration: 0.5, animations: {
            self.countryView.frame = CGRect(x: 5, y: UIApplication.shared.keyWindow?.bounds.height ?? 2000, width: self.view.frame.width - 10, height: self.searchParamView.frame.height + 15)
        }) { (result) in if result == true { self.countryView.removeFromSuperview() } }
    }
    
    func vkSearchCountryCellSelect(title: String, id: Int) {
        parameters.country = id
        parameters.countryTitle = title
        searchParamView.countyBtn.setTitle(title, for: .normal)
        searchParamView.cityBtn.isEnabled = true
        searchParamView.clearBtn.isEnabled = true
        closeCountryView()
    }

    //MARK: SearchCityView
    
    @objc func clickedCityBtn() {
        cityView = VKSearchCityView(frame: CGRect(x: 5, y: self.view.frame.height + self.view.frame.origin.y + 10, width: self.tableView.frame.width - 10, height: 0))
        self.cityView.configView(country_id: parameters.country, city_id: parameters.city)
        self.cityView.delegate = self
        self.searchParamView.addSubview(cityView)
        
        self.cityView.closeBtn.addTarget(self, action: #selector(closeCityView), for: .touchUpInside)
        
        let frame = self.searchParamView.frame
        UIView.animate(withDuration: 0.5) {
            self.cityView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height + 15)
        }
    }
    
    @objc func closeCityView() {
        UIView.animate(withDuration: 0.5, animations: {
            self.cityView.frame = CGRect(x: 5, y: UIApplication.shared.keyWindow?.bounds.height ?? 2000, width: self.view.frame.width - 10, height: self.searchParamView.frame.height + 15)
        }) { (result) in if result == true { self.cityView.removeFromSuperview() } }
    }
    
    func vkSearchCityCellSelect(title: String, id: Int) {
        parameters.city = id
        parameters.cityTitle = title
        searchParamView.cityBtn.setTitle(title, for: .normal)
        searchParamView.clearBtn.isEnabled = true
        closeCityView()
    }
    
    //MARK: SearchStatusView
    
    @objc func clickedStatusBtn() {
        statusView = VKSearchStatusView(frame: CGRect(x: 5, y: self.view.frame.height + self.view.frame.origin.y + 10, width: self.tableView.frame.width - 10, height: 0))
        self.statusView.configView(status_id: Int(parameters.status))
        self.statusView.delegate = self
        self.searchParamView.addSubview(statusView)
        
        self.statusView.closeBtn.addTarget(self, action: #selector(closeStatusView), for: .touchUpInside)
        
        let frame = self.searchParamView.frame
        UIView.animate(withDuration: 0.5) {
            self.statusView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height + 15)
        }
    }
    
    @objc func closeStatusView() {
        UIView.animate(withDuration: 0.5, animations: {
            self.statusView.frame = CGRect(x: 5, y: UIApplication.shared.keyWindow?.bounds.height ?? 2000, width: self.view.frame.width - 10, height: self.searchParamView.frame.height + 15)
        }) { (result) in if result == true { self.statusView.removeFromSuperview() } }
    }
    
    func vkSearchStatusCellSelect(title: String, id: Int) {
        parameters.status = Int8(id)
        parameters.statusTitle = title
        searchParamView.statusBtn.setTitle(title, for: .normal)
        searchParamView.clearBtn.isEnabled = true
        closeStatusView()
    }
    
    
    //MARK: Work with data
    
    @objc func loadData(offset: Int = 0, isReload: Bool = true, isInfinite: Bool = false) {
        if self.allUsersCount == self.users.count && isInfinite == true {
            DispatchQueue.main.async { self.tableView.finishInfiniteScroll() }
            return
        }
        switch cellType {
        case SearchPeopleCellType.recommend.rawValue:
            vkLoadData.loadSuggestionsUsers(offset: offset) { [weak self] (result) in
                if result == true {
                    self?.configData(isReload: isReload, isInfinite: isInfinite)
                }
            }
            break
        case SearchPeopleCellType.input.rawValue:
            vkLoadData.loadRequestsUsers(offset: offset, isOut: false, isNeedViewed: true) { [weak self] (result) in
                if result == true {
                    self?.configData(isReload: isReload, isInfinite: isInfinite)
                }
            }
            break
        case SearchPeopleCellType.output.rawValue:
            vkLoadData.loadRequestsUsers(offset: offset, isOut: true, isNeedViewed: true) { [weak self] (result) in
                if result == true {
                    self?.configData(isReload: isReload, isInfinite: isInfinite)
                }
            }
            break
        case SearchPeopleCellType.search.rawValue:
            initSearchView()
            searchingUsers(offset: offset, isReload: isReload, isInfinite: isInfinite, q: searchText)
            break
        default:
            break
        }
    }
    
    func configData(isReload: Bool, isInfinite: Bool) {
        if isReload == true { self.users.removeAll() }
        
        self.allUsersCount = vkLoadData.allSearchUsersCount
        for user in vkLoadData.searchUserInfo {
            self.users.append(user)
        }
        
        DispatchQueue.main.async {
            if isInfinite == true { self.tableView.finishInfiniteScroll() }
            else { self.tableView.refreshControl?.endRefreshing() }
            self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
            self.tableView.reloadData()
        }
    }
    
    func setProfileImgBy(user_id: Int64, data: NSData?) {
        for usr in users {
            if user_id == usr.id {
                users[users.index(of: usr)!].photo100Data = data
                break
            }
        }
    }
    
    func vkRecommendPeopleAbbBtnClicked(user_id: Int64, completion: @escaping (Bool) -> Void) {
        vkLoadData.addToFriends(user_id: user_id, text: "") { [weak self] (result) in
            if result == true {
                self?.changeUserStatus(user_id: user_id)
                if (self?.cellType ?? 4) == SearchPeopleCellType.recommend.rawValue {
                    DispatchQueue.main.async { self?.removeUserFromTable(user_id: user_id) }
                }
                completion(true)
            } else if result == false { completion(false) }
        }
    }
    
    func vkIORequestPeopleAddRemoveBtnClicked(user_id: Int64, isAdd: Bool, completion: @escaping (Bool) -> Void) {
        if isAdd == true {
            vkLoadData.addToFriends(user_id: user_id, text: "") { [weak self] (result) in
                if result == true {
                    DispatchQueue.main.async {
                        self?.removeUserFromTable(user_id: user_id)
                        self?.rejectAction(user_id: user_id)
                        
                        var userInfo: [String : Any] = [:]
                        
                        userInfo["user_id"] = user_id
                        userInfo["status"] = Int8(2)
                        userInfo["msg"] = ""
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deleteOrAddFriend"), object: nil, userInfo: userInfo)
                    }
                    completion(true)
                } else if result == false { completion(false) }
            }
        } else  {
            vkLoadData.deleteUserFromFriends(user_id: user_id) { [weak self] (result) in
                if result == true {
                    self?.removeUserFromTable(user_id: user_id)
                    completion(true)
                } else if result == false { completion(false) }
            }
        }
        
    }
    
    func vkIORequestPeopleRejectBtnClicked(user_id: Int64, completion: @escaping (Bool) -> Void) {
        vkLoadData.addToFriends(user_id: user_id, text: "", follow: true) { [weak self] (result) in
            if result == true {
                self?.rejectAction(user_id: user_id)
                completion(true)
            } else if result == false { completion(false) }
        }
    }
    
    func changeUserStatus(user_id: Int64) {
        for user in self.users {
            if user_id == user.id {
                self.users[self.users.index(of: user)!].friend_status = 1
                break
            }
        }
    }
    
    func removeUserFromTable(user_id: Int64) {
        for user in self.users {
            if user_id == user.id {
                tableView.beginUpdates()
                let index = IndexPath(row: self.users.index(of: user) ?? 0, section: 0)
                users.remove(at: self.users.index(of: user) ?? 0)
                allUsersCount -= 1
                tableView.deleteRows(at: [index], with: .left)
                tableView.endUpdates()
                tableView.reloadData()
                break
            }
        }
    }
    
    func rejectAction(user_id: Int64) {
        vkLoadData.requestToFriendCount -= 1
        vkLoadData.requestToFriendArr.remove(at: vkLoadData.requestToFriendArr.index(of: user_id)!)
        self.tabBar.selectedItem?.badgeValue = vkLoadData.requestToFriendCount != 0 ? String(vkLoadData.requestToFriendCount) : nil
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateBabgeOnBarButton"), object: nil, userInfo: nil)
    }

}

extension VKSearchPeopleViewController: ImageLoadedListener {
    func avatarLoaded(peer_id: Int64, data: NSData?) {
        if data != nil {
            DispatchQueue.main.async {
                self.setProfileImgBy(user_id: peer_id, data: data)
                self.tableView.reloadData()
            }
        }
    }
    
    func imageLoaded(withIndexPath indexPath: IndexPath, data: NSData?) { }
    
    func isNeedToLoadImageWithIndex(_ indexPath : IndexPath, completion: @escaping (_ :Bool) -> Void) {
        DispatchQueue.main.async {
            if indexPath.row < self.users.count {
                if self.users[indexPath.row].photo100Data != nil {
                    completion(false)
                    return
                }
                
                let cellWithIndex = self.tableView.visibleCells.filter({ (cell) -> Bool in
                    let indexPathForCell = self.tableView.indexPath(for: cell)
                    return indexPathForCell == indexPath
                })
                
                if (cellWithIndex.count == 0) {
                    completion(false)
                    return
                }
                
                completion(true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if allUsersCount > 0 {
            var str: String = String(allUsersCount)
            if cellType == SearchPeopleCellType.recommend.rawValue { str.append(" suggestions users") }
            else if cellType == SearchPeopleCellType.input.rawValue { str.append(" input requests") }
            else if cellType == SearchPeopleCellType.output.rawValue { str.append(" output requests") }
            else if cellType == SearchPeopleCellType.search.rawValue { str.append(" results") }
            return str
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = mainColors.sectionHeader
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = mainColors.text
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vkUserProfileTVC = storyboard.instantiateViewController(withIdentifier: "VKUserProfileTableViewController") as? VKUserProfileTableViewController {
            vkUserProfileTVC.user.copyValue(users[indexPath.row])
            vkUserProfileTVC.indexPath = indexPath
            self.navigationController?.pushViewController(vkUserProfileTVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < users.count {
            let user = users[indexPath.row]
            if user.photo100Data == nil {
                cellImageLoader?.loadImageForIndex(indexPath, user.photo_100, "ava", user.id)
            }
            
            switch cellType {
            case SearchPeopleCellType.recommend.rawValue, SearchPeopleCellType.search.rawValue:
                let cell = tableView.dequeueReusableCell(withIdentifier: "VKRecommendPeoleTVCell", for: indexPath) as! VKRecommendPeopleTVCell
                cell.configCell(data: user.photo100Data, userName: user.first_name + " " + user.last_name, user_id: user.id, isOnline: user.online, isMobile: user.online_mobile, isFriend: (user.friend_status == 3 || user.friend_status == 1))
                cell.delegate = self
                cell.backgroundColor = mainColors.mainTableView
                return cell
            case SearchPeopleCellType.input.rawValue, SearchPeopleCellType.output.rawValue:
                let isInput: Bool = cellType == SearchPeopleCellType.input.rawValue ? true : false
                let isReject: Bool = isInput == true && vkLoadData.requestToFriendArr.contains(user.id)
                let cell = tableView.dequeueReusableCell(withIdentifier: "VKIORequestPeopleTVCell", for: indexPath) as! VKIORequestPeopleTVCell
                cell.configCell(data: user.photo100Data, userName: user.first_name + " " + user.last_name, user_id: user.id, isOnline: user.online, isMobile: user.online_mobile, isAdd: isInput, isNeedRejectBtn: isReject)
                cell.delegate = self
                cell.backgroundColor = mainColors.mainTableView
                return cell
            default:
                return UITableViewCell()
            }
        }
        
        return UITableViewCell()
    }
}
