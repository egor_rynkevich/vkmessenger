//
//  VKIORequestPeopleTVCell.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 03.08.2018.
//

import UIKit

protocol VKIORequestPeopleCellDelegate: class {
    func vkIORequestPeopleAddRemoveBtnClicked(user_id: Int64, isAdd: Bool, completion: @escaping (_ :Bool) -> Void)
    func vkIORequestPeopleRejectBtnClicked(user_id: Int64, completion: @escaping (_ :Bool) -> Void)
}


class VKIORequestPeopleTVCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var addRemoveBtn: UIButton!
    @IBOutlet weak var rejectBtn: UIButton!
    @IBOutlet weak var onlineImgView: UIImageView!
    @IBOutlet weak var widthIsOnlineImgCnstr: NSLayoutConstraint!
    
    var user_id: Int64 = 0
    var isAdd: Bool = true
    
    weak var delegate: VKIORequestPeopleCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(data: NSData?, userName: String, user_id: Int64, isOnline: Bool, isMobile: Bool, isAdd: Bool, isNeedRejectBtn: Bool) {
        nameLbl.text = userName
        nameLbl.textColor = mainColors.text
        self.user_id = user_id
        self.isAdd = isAdd
        
        if data != nil {
            imgView.image = UIImage(data: data! as Data)
        } else {
            imgView.image = mainIcons.userProfile_s50
            imgView.tintColor = UIColor(white: 0.8, alpha: 1)
        }
        
        let img = isAdd == true ? mainIcons.addUser_s25 : mainIcons.trash_s25
        let text = isAdd == true ? " Add" : " Remove"
        let color = isAdd == true ? mainColors.writeRow : mainColors.deleteRow

        addRemoveBtn.setImage(img, for: .normal)
        addRemoveBtn.setTitle(text, for: .normal)
        addRemoveBtn.tintColor = color
        addRemoveBtn.isHidden = false
        
        if isNeedRejectBtn == false {
            rejectBtn.isHidden = true
        } else {
            rejectBtn.setImage(mainIcons.trash_s25, for: .normal)
            rejectBtn.setTitle(" Reject", for: .normal)
            rejectBtn.tintColor = mainColors.deleteRow
            rejectBtn.isHidden = false
        }
        
        if isOnline == true ||
            isMobile == true {
            let image = isMobile == true ? mainIcons.userPhoneOnline : mainIcons.userOnline
            let edgeInsets = isMobile == true ? UIEdgeInsets(top: 1, left: 0, bottom: 1, right: 0) : UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            onlineImgView.image = image?.resizableImage(withCapInsets: edgeInsets, resizingMode: .stretch).withRenderingMode(.alwaysTemplate)
            
            onlineImgView.tintColor = mainColors.online
            onlineImgView.backgroundColor = mainColors.mainTableView
            onlineImgView.layer.borderColor = mainColors.mainTableView?.cgColor
            
            if isMobile == false {
                onlineImgView.layer.masksToBounds = true
                onlineImgView.layer.cornerRadius = 6
                onlineImgView.layer.borderWidth = 2
                widthIsOnlineImgCnstr.constant = 13
            } else {
                onlineImgView.layer.masksToBounds = true
                onlineImgView.layer.cornerRadius = 3
                onlineImgView.layer.borderWidth = 1
                widthIsOnlineImgCnstr.constant = 11
                onlineImgView.contentMode = .scaleAspectFill
            }
        }
        else{
            onlineImgView.image = UIImage()
            onlineImgView.backgroundColor = nil
            onlineImgView.layer.borderWidth = 0
        }
    }

    @IBAction func addRemoveBtnAction(_ sender: Any) {
        addRemoveBtn.isHidden = true
        delegate?.vkIORequestPeopleAddRemoveBtnClicked(user_id: user_id, isAdd: isAdd, completion: { [weak self] (result) in
            if result == false {
                self?.addRemoveBtn.isHidden = false
            }
        })
    }
    
    @IBAction func rejectBtnAction(_ sender: Any) {
        rejectBtn.isHidden = true
        delegate?.vkIORequestPeopleRejectBtnClicked(user_id: user_id, completion: { [weak self] (result) in
            if result == false {
                self?.rejectBtn.isHidden = false
            }
        })
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
