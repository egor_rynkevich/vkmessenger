//
//  VKContactsTableViewCell.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 27.06.2018.
//

import UIKit
import SwipeCellKit

class VKContactsTableViewCell: SwipeTableViewCell {
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var fNameLbl: UILabel!
    @IBOutlet weak var widthFNameLblCnstr: NSLayoutConstraint!
    @IBOutlet weak var lNameLbl: UILabel!
    @IBOutlet weak var onlineImgView: UIImageView!
    @IBOutlet weak var widthOnlineImgViewCnstr: NSLayoutConstraint!
    @IBOutlet weak var bdateCakeImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(user: VKUser) {
        backgroundColor = mainColors.mainTableView
        
        if user.photo100Data != nil {
            profileImgView.image = UIImage(data: user.photo100Data! as Data)
            
        } else {
            profileImgView.image = mainIcons.userProfile_s50
            profileImgView.tintColor = mainColors.lightGray
        }
        
        fNameLbl.text = user.first_name
        fNameLbl.textColor = mainColors.text
        fNameLbl.sizeToFit()
        widthFNameLblCnstr.constant = fNameLbl.frame.width
        
        lNameLbl.text = user.last_name
        lNameLbl.font = UIFont.boldSystemFont(ofSize: 17.0)
        lNameLbl.textColor = mainColors.text
        
        if user.online == true ||
            user.online_mobile == true {
            let isMob = user.online_mobile
            let image = isMob == true ? mainIcons.userPhoneOnline : mainIcons.userOnline
            let edgeInsets = isMob == true ? UIEdgeInsets(top: 1, left: 0, bottom: 1, right: 0) : UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            onlineImgView.image = image?.resizableImage(withCapInsets: edgeInsets, resizingMode: .stretch).withRenderingMode(.alwaysTemplate)
            
            onlineImgView.tintColor = mainColors.online
            onlineImgView.backgroundColor = mainColors.mainTableView
            onlineImgView.layer.borderColor = mainColors.mainTableView?.cgColor
            
            if isMob == false {
                onlineImgView.layer.masksToBounds = true
                onlineImgView.layer.cornerRadius = 6
                onlineImgView.layer.borderWidth = 2
                widthOnlineImgViewCnstr.constant = 12
            } else {
                onlineImgView.layer.masksToBounds = true
                onlineImgView.layer.cornerRadius = 2
                onlineImgView.layer.borderWidth = 1
                widthOnlineImgViewCnstr.constant = 9
                onlineImgView.contentMode = .scaleAspectFill
            }
        }
        else{
            onlineImgView.image = UIImage()
            onlineImgView.backgroundColor = nil
            onlineImgView.layer.borderWidth = 0
        }
        
        if Helper.haveBDateToday(bdate: user.bdate) == true {
            bdateCakeImgView.image = mainIcons.cake
            bdateCakeImgView.tintColor = mainColors.deleteRow
        } else {
            bdateCakeImgView.image = nil
        }
        
        
    }

}
