//
//  VKContactsViewController.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 25.06.2018.
//

import UIKit
import SwiftyVK
import RealmSwift
import SwiftyJSON
import SwipeCellKit

class VKContactsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, SwipeTableViewCellDelegate, UISearchBarDelegate, ImageLoadedListener {
    
    struct User {
        var user: VKUser = VKUser()
        var isChoose: Bool = false
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentCtrl: UISegmentedControl!
    @IBOutlet weak var segmentView: UIView!
    @IBOutlet weak var createChatTopConstr: NSLayoutConstraint!
    @IBOutlet weak var viewForSegmentBottomConstr: NSLayoutConstraint!
    @IBOutlet weak var createChatBtn: UIButton!
    
    var friendsCount: Int = 0
    var friendsMutualCount: Int = 0
    var userID: Int64 = 0
    
    private var friendsOnlineCount: Int = 0
    
    private var segmentIndex: Int = 0
    private var friendsArr: [VKUser] = []
    
    private var friendsDict: [String : [User]] = [:]
    private var friendsSectionTitles: [String] = []
    
    private var friendsIndexPathForChat: [IndexPath] = []
    private var isCreateChat: Bool = false
    private var isChangeConstr: Bool = false
    
    private var searchBar: UISearchBar = UISearchBar()
    private var friendsArrForSearch: [VKUser] = []
    private var isSearch: Bool = false
    private var isAddSearchBar: Bool = false
    
    private var cellImageLoader : CellImageLoader?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cellImageLoader = CellImageLoader(withImageLoadedListener: self)
        initData()
        // Do any additional setup after loading the view.
    }
    
    func initData() {
        //title = "Friends"
        
        self.navigationController?.navigationBar.barTintColor = mainColors.mainNavigationBarLight
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        segmentCtrl.layer.cornerRadius = 15
        segmentCtrl.layer.borderWidth = 2.0
        segmentCtrl.tintColor = mainColors.writeRow
        segmentCtrl.layer.borderColor = segmentCtrl.tintColor.cgColor
        segmentCtrl.layer.masksToBounds = true
        
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: mainColors.lightGray!]
        segmentCtrl.setTitleTextAttributes(titleTextAttributes, for: .normal)
        segmentCtrl.setTitleTextAttributes(titleTextAttributes, for: .selected)
        
        if friendsMutualCount > 0 {
            segmentCtrl.insertSegment(withTitle: "", at: 2, animated: true)
        }
        
        segmentView.backgroundColor = mainColors.mainTableView
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.beginRefreshing()
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableView.backgroundColor = mainColors.mainTableView
        tableView.separatorColor = mainColors.separator
        tableView.sectionIndexColor = mainColors.writeRow
        tableView.sectionHeaderHeight = 20.0
        
        if userID == vkLoadData.mainUser.id {
            createChatBtn.setImage(mainIcons.createChat_s30, for: UIControl.State.normal)
            createChatBtn.tintColor = mainColors.lightGray
            createChatBtn.backgroundColor = mainColors.mainTableView
        } else {
            createChatTopConstr.constant = -40
            createChatBtn.tintColor = UIColor.white
        }
        
        addUserBtn()
        
        loadFriends()
        
        tableView.refreshControl?.addTarget(self, action: #selector(loadFriends), for: .valueChanged)

        segmentCtrl.addTarget(self, action: #selector(selectedSegmentValue), for: .valueChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(deleteOrAddAction), name: NSNotification.Name.deleteOrAddFriend, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addUserBtn), name: NSNotification.Name.updateBabgeOnBarButton, object: nil)
    }
    
    //MARK: LOAD AND MAKE DATA OPERATIONS
    
    @objc func loadFriends() {
        vkLoadData.friendsInfo.removeAll()
        
        if userID == vkLoadData.mainUserID {
            friendsArr = vkDataCache.loadUserFriends()
            configData()
        }
        
        vkLoadData.recursionloadUserFriends(user_id: userID, friends_count: friendsCount, offset: 0, needSave: (userID == vkLoadData.mainUserID)) { [weak self] (result) in
            if result == true {
                DispatchQueue.main.async {
                    self?.tableView.refreshControl?.endRefreshing()
                    
                    self?.makeFrindsArr()
                    self?.configData()
                }
            } else if result == false {
                DispatchQueue.main.async {
                    self?.tableView.refreshControl?.endRefreshing()
                    self?.tableView.reloadData()
                }
            }
        }
    }
    
    func makeFrindsArr() {
        friendsArr.removeAll()
        for (_, user) in vkLoadData.friendsInfo {
            if user.deactivated != "deleted" && user.deactivated != "banned" {
                friendsArr.append(user)
            }
        }
        friendsArr.sort(by: { (user1, user2) -> Bool in
            user1.last_name < user2.last_name
        })
    }
    
    func makeFriendDict() {
        friendsDict.removeAll()
        friendsSectionTitles.removeAll()
        friendsOnlineCount = 0
        
        let arr = isSearch == false ? friendsArr : friendsArrForSearch
        
        for user in arr {
            
            if segmentIndex == 1 && user.online == false { continue }
            else if segmentIndex == 2 && user.is_friend != true { continue }
            
            let friendKey = String(user.last_name.prefix(1))
            if var friendValues = friendsDict[friendKey] {
                friendValues.append(User(user: user, isChoose: false))
                friendsDict[friendKey] = friendValues
            } else {
                friendsDict[friendKey] = [User(user: user, isChoose: false)]
            }
            
            if user.online == true { friendsOnlineCount += 1 }
        }
        
        friendsSectionTitles = [String](friendsDict.keys)
        friendsSectionTitles = friendsSectionTitles.sorted(by: { $0 < $1 })
    }
    
    func configData() {
        
        if isAddSearchBar == false {
            searchBar.searchBarStyle = UISearchBar.Style.prominent
            searchBar.placeholder = " Search..."
            searchBar.sizeToFit()
            searchBar.isTranslucent = false
            searchBar.backgroundImage = UIImage()
            searchBar.delegate = self
            isAddSearchBar = true
            searchBar.changeColorMode(mainColors.mainTableView)
            tableView.tableHeaderView = searchBar
        }
        
        makeFriendDict()
        
        updateSegmentTitle()
        
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.reloadData()
    }
    
    func getUserBy(indexPath: IndexPath) -> User? {
        if self.friendsDict.count > 0 && friendsSectionTitles.count > indexPath.section {
            let key = friendsSectionTitles[indexPath.section]
            if let friendValues = friendsDict[key] {
                if friendValues.count > indexPath.row {
                    return friendValues[indexPath.row]
                }
            }
            
        }
        return nil
    }
    
    //MARK: UI OPERATIONS
    
    func updateSegmentTitle() {
        DispatchQueue.main.async {
            self.segmentCtrl.setTitle("\(self.friendsCount) friends", forSegmentAt: 0)
            self.segmentCtrl.setTitle("\(self.friendsOnlineCount) friends online", forSegmentAt: 1)
            if self.friendsMutualCount > 0 { self.segmentCtrl.setTitle("\(self.friendsMutualCount) mutual friends", forSegmentAt: 2) }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange textSearched: String) {
        friendsArrForSearch.removeAll()
        searchBar.showsCancelButton = true
        isSearch = true
        if textSearched.count == 0 {
            friendsArrForSearch = friendsArr
        } else {
            for user in friendsArr {
                if user.last_name.lowercased().range(of: textSearched.lowercased()) != nil
                    || user.first_name.lowercased().range(of: textSearched.lowercased()) != nil {
                    friendsArrForSearch.append(user)
                }
            }
        }
        
        makeFriendDict()

        tableView.reloadSectionIndexTitles()
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = nil
        searchBar.resignFirstResponder()
        isSearch = false
        friendsArrForSearch.removeAll()
        
        makeFriendDict()

        tableView.reloadData()
    }
    
    @IBAction func createChatBtn(_ sender: Any) {
        viewForSegmentBottomConstr.constant = -40
        createChatTopConstr.constant = -40
        
        let leftBtn = UIBarButtonItem(image: mainIcons.cancel_s21, style: .plain, target: self, action: #selector(cancelAction))
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationItem.setLeftBarButton(leftBtn, animated: true)
        
        let rightBtn = UIBarButtonItem(title: "Create", style: .done, target: self, action: #selector(createChatAction))
        rightBtn.isEnabled = false
        self.navigationItem.setRightBarButton(rightBtn, animated: true)
        
        isCreateChat = true
        isChangeConstr = true
        
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
            self.tableView.reloadSectionIndexTitles()
        }) { [weak self] (completed) in
            if completed == true {
                self?.isChangeConstr = false
                self?.tableView.reloadSectionIndexTitles()
            }
            
        }
    }
    
    @objc func addUserBtn() {
        let btn = Helper.getBtnWithBadge(img: mainIcons.addNewPeople_s22!, count: vkLoadData.requestToFriendCount, isLeft: false)
        
        btn.addTarget(self, action: #selector(searchPeople), for: .touchUpInside)

        let rightBarButtomItem = UIBarButtonItem(customView: btn)
        
        self.navigationItem.rightBarButtonItem = rightBarButtomItem //UIBarButtonItem(image: #imageLiteral(resourceName: "addNewPeopleIcon").resize(targetSize: CGSize(width: 22, height: 22)), style: .plain, target: self, action: #selector(searchPeople))
    }
    
    @objc func selectedSegmentValue(target: UISegmentedControl) {
        if target == self.segmentCtrl {
            self.segmentIndex = target.selectedSegmentIndex
            
            makeFriendDict()

            self.tableView.reloadData()
        }
    }

    @objc func searchPeople() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vkSearchPeopleVC = storyboard.instantiateViewController(withIdentifier: "VKSearchPeopleViewController") as? VKSearchPeopleViewController {
            //vkUserProfileTVC.user.copyValue((getUserBy(indexPath: indexPath)?.user)!)
            //vkUserProfileTVC.indexPath = indexPath
            self.navigationController?.pushViewController(vkSearchPeopleVC, animated: true)
        }
    }
    
    @objc func createChatAction() {
        if self.friendsIndexPathForChat.count == 1 {
            if let user_id = self.getUserBy(indexPath: self.friendsIndexPathForChat[0])?.user.id {
                self.loadNeedConvers(peer_id: user_id)
            }
        } else {
            let alert = UIAlertController(title: "Input chat name", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Create", style: .default, handler: { [weak self] (action) in
                if self == nil { return }
                var user_ids = ""
                for indexPath in self!.friendsIndexPathForChat {
                    user_ids.append(String(self!.getUserBy(indexPath: indexPath)?.user.id ?? 0) + ",")
                }
                let chatName = alert.textFields![0].text
                
                VK.API.Messages.createChat([.title: chatName, .userIDs: "\(user_ids)"]).onSuccess({ [weak self] (data) in
                    let result = JSON(data)
                    var chat_id = result["response"].int64 ?? 0
                    chat_id += 2_000_000_000
                    self?.loadNeedConvers(peer_id: chat_id)
                }).onError({ (error) in
                    print("createChatAction - \(error)")
                }).send()
                
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            alert.addTextField(configurationHandler: {(textField: UITextField!) in
                textField.placeholder = "Enter name:"
            })
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func cancelAction() {
        viewForSegmentBottomConstr.constant = 0
        createChatTopConstr.constant = 0
        
        let inserts = UIEdgeInsets(top: 0,left: -11,bottom: 0,right: 0)
        let img = mainIcons.back_s21?.withAlignmentRectInsets(inserts)
        let item = UIBarButtonItem(image: img, style: .plain, target: self, action: #selector(backAction))
        
        self.navigationItem.leftBarButtonItem = item
        
        addUserBtn()
        
        isCreateChat = false
        friendsIndexPathForChat.removeAll()
        
        for (key, arr) in friendsDict {
            var index = 0
            for _ in arr {
                friendsDict[key]![index].isChoose = false
                index += 1
            }
        }
        
        tableView.reloadRows(at: tableView.indexPathsForVisibleRows!, with: .none )
        
        isChangeConstr = true
        
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
            self.tableView.reloadSectionIndexTitles()
        }) { (completed) in
            if completed == true {
                self.isChangeConstr = false
                self.tableView.reloadSectionIndexTitles()
            }
            
        }
    }

    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadNeedConvers(peer_id: Int64) {
        let realm = try! Realm()
        if let _ = realm.objects(VKConversation.self).filter("peer_id = %@", peer_id).first {
            self.goToConvers(peer_id: peer_id)
        } else {
            vkLoadData.loadVKConversationsByID(peer_ids: String(peer_id), completionBlock: { [weak self] (result) in
                if result == true {
                    let realmAs = try! Realm()
                    if let _ = realmAs.objects(VKConversation.self).filter("peer_id = %@", peer_id).first {
                        self?.goToConvers(peer_id: peer_id)
                    }
                }
            })
        }
    }
    
    func goToConvers(peer_id: Int64) {
        DispatchQueue.main.async {
            self.cancelAction()
            let realm = try! Realm()
            if let convers = realm.objects(VKConversation.self).filter("peer_id = %@", peer_id).first {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let vkMessageViewController = storyboard.instantiateViewController(withIdentifier: "VKMessageViewController") as? VKMessageViewController {
                    vkMessageViewController.convers = convers
                    self.navigationController?.pushViewController(vkMessageViewController, animated: true)
                }
            }
        }
    }
    
    func addUser(user_id: Int64, msg: String, isHaveRequest: Bool) {
        vkLoadData.addToFriends(user_id: user_id, text: msg) { [weak self] (result) in
            if result == true && isHaveRequest == true {
                vkLoadData.loadMainInfoAboutUser(String(user_id), completion: { (result) in
                    if result == true {
                        if let user = vkLoadData.usersInfo.first?.value {
                            if self == nil { return }
                            self!.friendsArr.append(user)
                            vkLoadData.usersInfo.removeAll()
                            self!.friendsArr.sort(by: { (user1, user2) -> Bool in
                                user1.last_name < user2.last_name
                            })
                            self!.makeFriendDict()
                            
                            self!.friendsCount += 1
                            self!.friendsOnlineCount += user.online == true ? 1 : 0
                            vkDataCache.changeFriendsCounts(friends_count: self!.friendsCount)
                            vkDataCache.saveUserInfoInCache([user.id : user], completion: { _ in })
                            
                            self!.updateSegmentTitle()
                        }
                    }
                })
            }
        }
    }
    
    // MARK: DELETE OPERATIONS
    
    @objc func deleteOrAddAction(notification: NSNotification) {
        let userInfo = notification.userInfo ?? [:]
        var status: Int8 = 0
        if userID == vkLoadData.mainUser.id {
            if userInfo["status"] != nil {
                status = userInfo["status"] as! Int8
            }
            let user_id = userInfo["user_id"] as! Int64
            
            if status == 0 || status == 2 {
                addUser(user_id: user_id, msg: userInfo["msg"] as! String, isHaveRequest: status == 2)
            } else {
                var isBan: Bool = false
                if userInfo["isBan"] != nil {
                    isBan = userInfo["isBan"] as! Bool
                }
                deleteUserBy(user_id: user_id, isBan: isBan)
            }
        }
    }
    
    func deleteUserBy(user_id: Int64, isBan: Bool = false) {
        var section: Int = 0
        var index: Int = 0
        
        for (key, val) in friendsDict {
            section = friendsSectionTitles.index(of: key) ?? 0
            index = 0
            for usr in val {
                if user_id == usr.user.id {
                    let indexPath: IndexPath = IndexPath(row: index, section: section)
                    deleteUserBy(indexPath: indexPath, isBan: isBan)
                    return
                }
                index += 1
            }
        }
        
        removeFromFriendArrBy(user_id: user_id)
        requestToDeleteUser(user_id: user_id, isBan: isBan)
        self.updateSegmentTitle()
    }
    
    func removeFromFriendArrBy(user_id: Int64) {
        for usr in friendsArr {
            if user_id == usr.id {
                friendsArr.remove(at: friendsArr.index(of: usr)!)
                break
            }
        }
        
        if isSearch == true && friendsArrForSearch.count > 0 {
            for usr in friendsArrForSearch {
                if user_id == usr.id {
                    friendsArrForSearch.remove(at: friendsArrForSearch.index(of: usr)!)
                    break
                }
            }
        }
    }
    
    func requestToDeleteUser(user_id: Int64, isBan: Bool = false) {
        if isBan == true {
            //vkLoadData.banPeer(peer_id: user_id) { _ in }
        } else {
            //vkLoadData.deleteUserFromFriends(user_id: user_id) { _ in  }
        }
        vkDataCache.changeFriendsCounts(friends_count: self.friendsCount)
    }
    
    func deleteUserBy(indexPath: IndexPath, isBan: Bool = false) {
        self.tableView.beginUpdates()
        let user_id: Int64 = getUserBy(indexPath: indexPath)?.user.id ?? 0
        
        if userID == vkLoadData.mainUser.id {
            let key = friendsSectionTitles[indexPath.section]
            if let friendsVal = friendsDict[key] {
                let user = friendsVal[indexPath.row]
                if user.user.online == true {
                    self.friendsOnlineCount -= 1
                }
                self.friendsCount -= 1
                
                friendsDict[key]?.remove(at: indexPath.row)
                if (friendsDict[key]?.count ?? 1) == 0 {
                    friendsDict.removeValue(forKey: key)
                    friendsSectionTitles.remove(at: friendsSectionTitles.index(of: key)!)
                    
                    let indexSet = IndexSet(arrayLiteral: indexPath.section)
                    tableView.deleteSections(indexSet, with: .left)
                }
                
                removeFromFriendArrBy(user_id: user_id)
            }
            
            self.updateSegmentTitle()
            self.tableView.deleteRows(at: [indexPath], with: .left)
        }
        
        requestToDeleteUser(user_id: user_id, isBan: isBan)
        
        self.tableView.endUpdates()
        self.tableView.reloadSectionIndexTitles()
    }
    
    //MARK: CHOOSING OPERATIONS
    
    func setUserIsChoose(indexPath: IndexPath, isChoose: Bool) {
        if self.friendsDict.count > 0 {
            let key = friendsSectionTitles[indexPath.section]
            if friendsDict[key] != nil {
                friendsDict[key]![indexPath.row].isChoose = isChoose
            }
        }
    }
    
    func choosingUserForChat(tableView: UITableView, indexPath: IndexPath) {
        if tableView.cellForRow(at: indexPath)?.accessoryType.rawValue == UITableViewCell.AccessoryType.none.rawValue {
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            setUserIsChoose(indexPath: indexPath, isChoose: true)
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            if friendsIndexPathForChat.contains(indexPath) == false {
                friendsIndexPathForChat.append(indexPath)
                self.navigationItem.rightBarButtonItem?.title = "Create (\(friendsIndexPathForChat.count))"
                
            }
        } else {
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
            setUserIsChoose(indexPath: indexPath, isChoose: false)
            if let index = friendsIndexPathForChat.index(of: indexPath) {
                friendsIndexPathForChat.remove(at: index)
                if friendsIndexPathForChat.count != 0 {
                    self.navigationItem.rightBarButtonItem?.title = "Create (\(friendsIndexPathForChat.count))"
                } else {
                    self.navigationItem.rightBarButtonItem?.title = "Create"
                    self.navigationItem.rightBarButtonItem?.isEnabled = false
                }
                
            }
        }
    }
    
    // MARK: - ImageLoadedListener
    
    func avatarLoaded(peer_id: Int64, data: NSData?) {
        if data != nil {
            DispatchQueue.main.async {
                self.setProfileImgBy(user_id: peer_id, data: data)
                self.tableView.reloadData()
            }
        }
    }
    
    func imageLoaded(withIndexPath indexPath: IndexPath, data: NSData?) { }
    
    func isNeedToLoadImageWithIndex(_ indexPath : IndexPath, completion: @escaping (_ :Bool) -> Void) {
        DispatchQueue.main.async {
            if self.getUserBy(indexPath: indexPath)?.user.photo100Data != nil {
                completion(false)
                return
            }
            
            let cellWithIndex = self.tableView.visibleCells.filter({ (cell) -> Bool in
                let indexPathForCell = self.tableView.indexPath(for: cell)
                return indexPathForCell == indexPath
            })
            
            if (cellWithIndex.count == 0) {
                completion(false)
                return
            }
            
            completion(true)
        }
    }
    
    func setProfileImgBy(user_id: Int64, data: NSData?) {
        var index: Int = 0
        
        for (key, val) in friendsDict {
            index = 0
            for usr in val {
                if user_id == usr.user.id {
                    friendsDict[key]![index].user.photo100Data = data
                    return
                }
                index += 1
            }
        }
        
        for usr in friendsArr {
            if user_id == usr.id {
                friendsArr[friendsArr.index(of: usr)!].photo100Data = data
                break
            }
        }
        
        if isSearch == true && friendsArrForSearch.count > 0 {
            for usr in friendsArrForSearch {
                if user_id == usr.id {
                    friendsArrForSearch[friendsArrForSearch.index(of: usr)!].photo100Data = data
                    break
                }
            }
        }
    }
}

extension VKContactsViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return friendsSectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return friendsSectionTitles[section]
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = mainColors.sectionHeader
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = mainColors.text
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if isChangeConstr == false {
            return friendsSectionTitles
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let key = friendsSectionTitles[section]
        if let friendsValues = friendsDict[key] {
            return friendsValues.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "VKContactsTableViewCellID", for: indexPath) as? VKContactsTableViewCell {
            let user = getUserBy(indexPath: indexPath)
            if user != nil {
                cell.configCell(user: user!.user)
                cell.accessoryType = user!.isChoose == true ? .checkmark : .none
                if user?.user.photo100Data == nil {
                    cellImageLoader?.loadImageForIndex(indexPath, user?.user.photo_100 ?? "", "ava", user?.user.id ?? 0)
                }
            } else {
                cell.accessoryType = .none
            }
            cell.delegate = self
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if isCreateChat == true {
            choosingUserForChat(tableView: tableView, indexPath: indexPath)
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let vkUserProfileTVC = storyboard.instantiateViewController(withIdentifier: "VKUserProfileTableViewController") as? VKUserProfileTableViewController {
                vkUserProfileTVC.user.copyValue((getUserBy(indexPath: indexPath)?.user)!)
                vkUserProfileTVC.indexPath = indexPath
                self.navigationController?.pushViewController(vkUserProfileTVC, animated: true)
            }
        }

    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        if let user = self.getUserBy(indexPath: indexPath) {
            guard orientation == .right else {
                if user.user.contacts_mobile_phone.count > 5 {
                    let callAction = SwipeAction(style: .default, title: "Call") { (rowAction, indexPath) in
                        if let url = URL(string: "tel://\(user.user.contacts_mobile_phone)"), UIApplication.shared.canOpenURL(url) {
                            if #available(iOS 10, *) {
                                UIApplication.shared.open(url)
                            } else {
                                UIApplication.shared.openURL(url)
                            }
                        }
                    }
                    callAction.backgroundColor = mainColors.online
                    callAction.image = mainIcons.call_s25
                    return [callAction]
                }
                return nil
            }
            
            let writeAction = SwipeAction(style: .default, title: "Write") { (rowAction, indexPath) in
                self.loadNeedConvers(peer_id: user.user.id)
            }
            writeAction.backgroundColor = mainColors.writeRow
            writeAction.image = mainIcons.newMessage_s25
            
            if user.user.is_friend == true {
                let deleteAction = SwipeAction(style: .destructive, title: "Delete") { (rowAction, indexPath) in
                    let title = "Delete " + user.user.first_name + " " + user.user.last_name + " from friends?"
                    let alert = UIAlertController(title: title, message: "", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (action) in
                        self.deleteUserBy(indexPath: indexPath)
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                deleteAction.backgroundColor = mainColors.deleteRow
                deleteAction.image = mainIcons.trash_s25
                
                return [writeAction, deleteAction]
            } else {
                let addAction = SwipeAction(style: .destructive, title: "Add") { (rowAction, indexPath) in
                    let title = "Add " + user.user.first_name + " " + user.user.last_name + " to friends?"
                    let alert = UIAlertController(title: title, message: "", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Add", style: .default, handler: { (action) in
                        let msg = alert.textFields?[0].text ?? ""
                        self.addUser(user_id: user.user.id, msg: msg, isHaveRequest: user.user.friend_status == 2)
                    }))
                    alert.addTextField(configurationHandler: {(textField: UITextField!) in
                        textField.placeholder = "Enter message:"
                    })
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                addAction.backgroundColor = mainColors.online
                addAction.image = mainIcons.addUser_s25
                
                return [writeAction, addAction]
            }
            
            
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .selection
        options.transitionStyle = .border
        return options
    }
}
