//
//  VKLongPollDecode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 28.05.2018.
//

import Foundation
import SwiftyJSON
import SwiftyVK
import RealmSwift

enum MessageFlags: Int {
    case unread = 1
    case outbox = 2
    case replied = 4 ////// ??????
    case important = 8
    case friends = 32
    case spam = 64
    case deleted = 128
    case hidden = 65536
    case deletedForAll = 131072
}

enum ChatFlags: Int {
    case important = 1
    case unanswered = 2
}


class VKLongPollDecode {
    class func decodeEvent(_ event: LongPollEvent) {

            switch event {
            case let .type1(data):
                changeMsgFlags(data, 1)
                break
            case let .type2(data):
                changeMsgFlags(data, 2)
                break
            case let .type3(data):
                changeMsgFlags(data, 3)
                break
            case let .type4(data):
                addNewMsg(data)
                break
            case let .type5(data):
                changeMsg(data)
                break
            case let .type6(data):
                changeMsgStatus(data, 6)
                break
            case let .type7(data):
                changeMsgStatus(data, 7)
                break
            case let .type8(data):
                changeOnlineStatus(data, 8)
                break
            case let .type9(data):
                changeOnlineStatus(data, 9)
                break
            case let .type10(data),
                 let .type11(data),
                 let .type12(data):
                changeGroupMsgFlags(data)
                break
            case let .type13(data):
                deleteAllMsgToLocalID(data)
                break
            case let .type14(data):
                let json = JSON(data)
                print("restoring delete msg in peer_id to local_id")
                print(json)
                break
            case let .type52(data):
                print("change chat info")
                changeChatFlags(data)
                break
            case let .type61(data):
                isUserWriting(data, 61)
                break
            case let .type62(data):
                isUserWriting(data, 62)
                break
            case let .type70(data):
                let json = JSON(data)
                print("user call with call_id")
                print(json)
                break
            case let .type80(data):
                let json = JSON(data)
                print(json)
                
                break
            case let .type114(data):
                print("change push settings")
                changePushSettings(data)
                break
            case .forcedStop:

                break
            default:
                break
            }
    }
    
    class func changeMsgFlags(_ data: Data, _ type: Int) {
        let json = JSON(data)
        print(json)
        let msg_id = json[0].int64 ?? 0
        let flag = json[1].int ?? 0
        let peer_id = json[2].int64 ?? 0
        var isRemoveMsg: Bool = false
        var userInfo: [String: Int64] = [:]
        var minusUnreadMsg: Bool = false
        
        userInfo["type"] = Int64(type)
        userInfo["msg_id"] = msg_id
        userInfo["user_id"] = peer_id
        
        do {
            let realm = try Realm()
            let msgs = realm.objects(VKMessage.self).filter("id = %@", msg_id)

            if type == 3 &&
                ((flag & MessageFlags.deleted.rawValue) == MessageFlags.deleted.rawValue
                    || (flag & MessageFlags.spam.rawValue) == MessageFlags.spam.rawValue) {
                
                if msgs.count != 0 {
                    if msgs.count == 1 { restoreMsg(newMsg: msgs.first!) }
                    else {
                        for msg in msgs {
                            if !msg.isPreview {
                                restoreMsg(newMsg: msg)
                                return
                            }
                        }
                    }
                } else {
                    VK.API.Messages.getById([.messageIds : String(msg_id), .previewLength : "0"])
                        .onSuccess{
                            let json = JSON($0)
                            let newMsg = VKMessage(json: json["items"][0], prnt_id: "")
                            restoreMsg(newMsg: newMsg)
                        }.onError{
                            print("VKMLongPollDecode - changeMsgFlags - Request failed with error: \($0)")
                        }.send()
                }
            }
            else if let convers = realm.objects(VKConversation.self).filter("peer_id = %@", peer_id).first {
                for msg in msgs {
                    var flagBuf = flag
                    try realm.write {
                        if (flagBuf & MessageFlags.deletedForAll.rawValue) == MessageFlags.deletedForAll.rawValue {
                            
                            if msg.id > convers.in_read && minusUnreadMsg == false { minusUnreadMsg = true }
                            if msg.isPreview == true { realm.delete(msg) }
                            else { msg.isDelete = true }
                            isRemoveMsg = true
                            flagBuf = 0
                        }
                        if (flagBuf & MessageFlags.hidden.rawValue) == MessageFlags.hidden.rawValue { msg.is_hidden = type == 3 ? false : true }
                        if (flagBuf & MessageFlags.deleted.rawValue) == MessageFlags.deleted.rawValue ||
                            (flag & MessageFlags.spam.rawValue) == MessageFlags.spam.rawValue {
                            if type == 2 {
                                if msg.id > convers.in_read && minusUnreadMsg == false { minusUnreadMsg = true }
                                if msg.isPreview == true { realm.delete(msg) }
                                else { msg.isDelete = true }
                                isRemoveMsg = true
                                flagBuf = 0
                            }
                        }
                        if (flagBuf & MessageFlags.important.rawValue) == MessageFlags.important.rawValue { msg.important = type == 3 ? false : true }
                        if (flagBuf & MessageFlags.outbox.rawValue) == MessageFlags.outbox.rawValue && type == 3 {
                            msg.from_id = vkLoadData.mainUserID
                        }
                        if (flagBuf & MessageFlags.unread.rawValue) == MessageFlags.unread.rawValue {
                            convers.in_read = type == 3 ? convers.in_read : msg.id
                        }
                    }
                }

                if isRemoveMsg == true {
                    if minusUnreadMsg == true { try realm.write { convers.unread_count -= 1 } }
                    
                    if convers.last_message == nil {
                        if let msg = convers.messages.filter("isDelete = %@", false).sorted(byKeyPath: "date", ascending: false).first {
                            let previewMsg = VKMessage()
                            previewMsg.prntID = convers.conversationID
                            previewMsg.copyValue(msg)
                            previewMsg.isPreview = true
                            try realm.write {
                                if convers.last_message != nil { realm.delete(convers.last_message!) }
                                convers.last_message = previewMsg
                                convers.lastMsgDate = previewMsg.date
                            }
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
                        }
                        else {
                            let convRef = ThreadSafeReference(to: convers)
                            vkLoadData.loadMessHistory(peer_id: convers.peer_id, peer_type: convers.peer_type, conversRef: convRef, prtID: convers.conversationID, count: 1) { (result) in
                                if result == true {
                                    do {
                                        let realmAs = try Realm()
                                        if let conversAsync = realmAs.objects(VKConversation.self).filter("peer_id = %@", peer_id).first {
                                            if conversAsync.last_message == nil {
                                                try realmAs.write { realmAs.delete(conversAsync) }
                                                userInfo["type"] = 13
                                            }
                                        }
                                    } catch let error as NSError { print("\(error)") }

                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
                                }
                            }
                        }
                    }
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInMessageTV"), object: nil, userInfo: userInfo)
            }

        }
        catch let error as NSError { print("\(error)") }
    }
    
    class func restoreMsg(newMsg: VKMessage) {
        do {
            var userInfo: [String: Int64] = [:]
            var newMsgPreview = VKMessage()
            if newMsg.isPreview == true { newMsgPreview = newMsg }
            else { newMsgPreview.copyValue(newMsg) }
            
            let realm = try Realm()
            let peer_id = newMsg.peer_id
            if let convers = realm.objects(VKConversation.self).filter("peer_id = %@", peer_id).first {
                let convMessages = convers.messages.filter("isDelete = %@", false)
                if convMessages.count > 0 {
                    if newMsg.date > (convMessages.last?.date ?? 0) && newMsg.date < (convMessages.first?.date ?? 0) {
                        
                        try realm.write {
                            newMsg.prntID = convers.conversationID
                            newMsg.isDelete = false
                            convers.messages.append(newMsg)
                            if newMsg.id > convers.in_read && newMsg.from_id != vkLoadData.mainUserID {
                                convers.unread_count += 1
                            }
                        }
                    }
                    else if newMsg.date > (convMessages.first?.date ?? 0) {
                        
                        try realm.write {
                            newMsg.prntID = convers.conversationID
                            newMsgPreview.prntID = convers.conversationID
                            newMsgPreview.isPreview = true
                            newMsgPreview.isDelete = false
                            newMsg.isDelete = false
                            convers.messages.append(newMsg)
                            realm.delete(convers.last_message!)
                            convers.last_message = newMsgPreview
                            convers.lastMsgDate = newMsgPreview.date
                            if newMsg.id > convers.in_read && newMsg.from_id != vkLoadData.mainUserID {
                                convers.unread_count += 1
                            }
                        }
                    }
                }
                else {
                    
                    try realm.write {
                        newMsg.prntID = convers.conversationID
                        newMsgPreview.prntID = convers.conversationID
                        newMsgPreview.isPreview = true
                        if newMsg.isPreview == false { convers.messages.append(newMsg) }
                        newMsg.isDelete = false
                        newMsgPreview.isDelete = false
                        realm.delete(convers.last_message!)
                        convers.last_message = newMsgPreview
                        convers.lastMsgDate = newMsgPreview.date
                        if newMsg.id > convers.in_read && newMsg.from_id != vkLoadData.mainUserID {
                            convers.unread_count += 1
                        }
                    }
                }
                
                userInfo["user_id"] = peer_id
                userInfo["type"] = 3
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
                
                userInfo["msg_id"] = newMsg.id
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInMessageTV"), object: nil, userInfo: userInfo)
            }
        }  catch let error as NSError { print("\(error)") }
    }

    class func addNewMsg(_ data: Data) {
        let json = JSON(data)
        print(json)
        let msg_id = json[0].int64 ?? 0
        let flag = json[1].int ?? 0
        let peer_id = json[2].int64 ?? 0
        let timestamp = json[3].int64 ?? 0
        let chatName = json[4].string ?? ""
        let text = json[5].string ?? ""
        let attch = json[6].dictionary ?? [:]
        var msg = VKMessage()
        var userInfo: [String: Int64] = [:]
        var isUnread: Bool = false
        var isNeedChatName: Bool = false
        
        userInfo["type"] = Int64(4)
        userInfo["user_id"] = peer_id
        
        msg.id = msg_id
        msg.isPreview = true
        
        if peer_id > 2000000000 {
            msg.from_id = Int64(attch["from"]?.string ?? "0")!
            isNeedChatName = true
        }
        msg.peer_id = peer_id
        msg.date = timestamp
        msg.text = text

        parseAttachment(msg: &msg, attch: attch)

        if (flag & MessageFlags.hidden.rawValue) == MessageFlags.hidden.rawValue {
            msg.is_hidden = true
        }
        if (flag & MessageFlags.outbox.rawValue) == MessageFlags.outbox.rawValue {
            msg.from_id = vkLoadData.mainUserID
        }
        
        if (flag & MessageFlags.unread.rawValue) == MessageFlags.unread.rawValue {
            isUnread = true
        }

        do {
            let realm = try Realm()
            if let convers = realm.objects(VKConversation.self).filter("peer_id = %@", peer_id).first {
                try realm.write {
                    if convers.last_message != nil { realm.delete(convers.last_message!) }
                    convers.last_message = msg
                    if msg.from_id != vkLoadData.mainUserID {
                        convers.unread_count += 1
                    }
                    if isUnread == false {
                        if msg.from_id != vkLoadData.mainUserID { convers.out_read = msg_id }
                        else { convers.in_read = msg_id }
                    }
                    if isNeedChatName == true {
                        convers.chat_settings_title = chatName
                        convers.chatInfo?.title = chatName
                        if vkDataCache.cachedChat[peer_id] != nil {
                            vkDataCache.cachedChat[peer_id]?.title = chatName
                        }
                    }
                    
                    convers.lastMsgDate = timestamp
                }
                realm.refresh()
                
                if msg.action_type == "chat_invite_user" {
                    vkLoadData.loadMainInfoAboutUser("\(msg.action_member_id)", completion: { (result) in
                        if result == true {
                            vkDataCache.saveUserInfoInCache(vkLoadData.usersInfo, completion: { (result) in
                                if result == 1 {
                                    vkLoadData.usersInfo.removeAll()
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
                                }
                            })
                        }
                    })
                }
                else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
                }
            } else {
                vkLoadData.loadVKConversations(startID: msg_id, offset: 0, count: 1, view: UIView()) { (result) in
                    if result == true {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
                    }
                }
            }
        } catch let error as NSError {
            print(error)
        }

        VK.API.Messages.getById([.messageIds : String(msg_id), .previewLength : "0"])
            .onSuccess{
                let json = JSON($0)
                let msgJson = json["items"][0]

                do {
                    let realm = try Realm()
                    if let convers = realm.objects(VKConversation.self).filter("peer_id = %@", peer_id).first {
                        let msg = VKMessage(json: msgJson, prnt_id: convers.conversationID)
                        try realm.write {
                            
                            if let index = convers.messages.firstIndex(where: { (mes) -> Bool in
                                mes.id == msg.id
                            }) {
                                msg.messageID = convers.messages[index].messageID
                                realm.create(VKMessage.self, value: msg, update: true)
                            } else { convers.messages.append(msg) }
                            /// notif in msgTblVw
                            userInfo["msg_id"] = msg.id
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInMessageTV"), object: nil, userInfo: userInfo)
                        }
                    }
                } catch let error as NSError {
                    print(error)
                }
            }.onError{
                print("VKMLongPollDecode - addNewMsg - Request failed with error: \($0)")
            }.send()
        
    }
    
    private class func parseAttachment(msg: inout VKMessage, attch: [String : JSON]) {
        for i in 1...10 {
            let attach_type = "attach" + String(i) + "_type"
            //let attach = "attach" + String(i)
            
            if attch[attach_type] != nil {
                let att = VKAttachments()
                att.type = attch[attach_type]?.string ?? ""
                switch attach_type {
                case "photo":
                    att.photo = VKPhoto()
                    break
                case "video":
                    att.video = VKVideo()
                    break
                case "audio":
                    att.audio = VKAudio()
                    break
                case "doc":
                    att.doc = VKDocument()
                    break
                case "wall":
                    att.wall = VKWall()
                    break
                case "sticker":
                    att.sticker = VKSticker()
                    break
                case "link":
                    att.link = VKLink()
                    break
                default:
                    break
                }
                msg.attachments.append(att)
            } else { break }
            
        }
        
        if attch["geo"] != nil {
            msg.geo = VKGeo()
        }
        
        if attch["geo_provider"] != nil {
            msg.geo = VKGeo()
        }
        
        if attch["source_act"] != nil {
            msg.action_type = attch["source_act"]?.string ?? ""
        }

        if attch["source_mid"] != nil {
            msg.action_member_id = Int64(attch["source_mid"]?.string ?? "0")!
        }
        
        if attch["source_text"] != nil {
            msg.action_text = attch["source_text"]?.string ?? ""
        }
    }
    
    class func changeMsg(_ data: Data) {
        let json = JSON(data)
        print(json)
        let msg_id = json[0].int64 ?? 0
        let rlm = try! Realm()
        let msgsArr = rlm.objects(VKMessage.self).filter("id = %@", msg_id)
        if msgsArr.count > 0 {
            VK.API.Messages.getById([.messageIds : String(msg_id), .previewLength : "0"])
                .onSuccess{
                    let json = JSON($0)
                    let msgJson = json["items"][0]
                    
                    do {
                        let realm = try Realm()
                        let msgs = realm.objects(VKMessage.self).filter("id = %@", msg_id)
                        let newMsg = VKMessage(json: msgJson, prnt_id: "")
                        var haveMsgPrev = false
                        for msg in msgs {
                            newMsg.updateData(realm, msg)
                            if msg.isPreview == true { haveMsgPrev = true }
                        }
                        
                        var userInfo: [String: Int64] = [:]
                        userInfo["type"] = Int64(5)
                        userInfo["user_id"] = newMsg.peer_id
                        userInfo["msg_id"] = msg_id
                        if haveMsgPrev == true {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
                        }
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInMessageTV"), object: nil, userInfo: userInfo)
                    } catch let error as NSError { print(error) }
                }.onError{
                    print("VKMLongPollDecode - changeMsg - Request failed with error: \($0)")
                }.send()
        }
    }
    
    class func changeOnlineStatus(_ data: Data, _ type: Int) {
        do {
            let json = JSON(data)
            let user_id = (json[0].int64 ?? 0) * (-1)
            let flag = json[1].int8 ?? 0
            let lastTime = json[2].int64 ?? 0
            var userInfo: [String: Int64] = [:]
            let realm = try Realm()
            
            guard let user = realm.objects(VKUser.self).filter("id = %@", user_id).first else { return }
            
            try realm.write {
                user.last_seen_time = lastTime
                if type == 8 {
                    let isMobile = (flag == 6 || flag == 7) ? false : true
                    user.last_seen_platform = flag
                    user.online = true
                    user.online_mobile = isMobile
                } else if type == 9 {
                    user.last_seen_platform = flag == 0 ? 7 : user.last_seen_platform
                    user.online = false
                    user.online_mobile = false
                }
            }
            userInfo["type"] = Int64(type)
            userInfo["user_id"] = user_id
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInMessageTV"), object: nil, userInfo: userInfo)
            // MARK: need add code for update status in user chat
        } catch let error as NSError {
            print("\(error)")
        }
    }

    class func changeGroupMsgFlags(_ data: Data) {
        let json = JSON(data)
        print(json)
        let peer_id = json[0].int64 ?? 0
        let flags = json[1].int ?? 0
        var needSendNotification: Bool = false
        
        do {
            let realm = try Realm()
            guard let convers = realm.objects(VKConversation.self).filter("peer_id = %@", peer_id).first else { return }
            
            try realm.write {
                if (flags & ChatFlags.important.rawValue) == ChatFlags.important.rawValue { convers.important = 1 }
                else if (flags & ChatFlags.unanswered.rawValue) == ChatFlags.unanswered.rawValue {
                    convers.unanswered = 1
                    needSendNotification = true
                }
            }
            
            if needSendNotification == true {
                var userInfo: [String: Int64] = [:]
                userInfo["type"] = Int64(10) // 10 || 11 || 12
                userInfo["user_id"] = peer_id
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
            }
        }
        catch let error as NSError {
            print("\(error)")
        }
    }

    class func changeMsgStatus(_ data: Data, _ type: Int) { // type = 6 - readed all in msg to local_id, type = 7 - readed all out msg to local_id
        let json = JSON(data)
        print(json)
        let peer_id = json[0].int64 ?? 0
        let local_id = json[1].int64 ?? 0

        do {
            let realm = try Realm()
            
            if let convers = realm.objects(VKConversation.self).filter("peer_id = %@", peer_id).first {
                var countReadedMsg = 0;
                
                if type == 6 {
                    var msgs: Results<VKMessage>
                    
                    msgs = realm.objects(VKMessage.self).filter("id <= %@ AND id > %@ AND from_id != %@ AND isPreview = false", local_id, convers.in_read, vkLoadData.mainUserID)
                    countReadedMsg = msgs.count
                }
                
                try realm.write {
                    if (local_id - convers.in_read) >= countReadedMsg { convers.unread_count = 0 }
                    else { convers.unread_count -= countReadedMsg }
                    
                    if type == 6 { convers.in_read = local_id }
                    else { convers.out_read = local_id }
                }
                
                var userInfo: [String: Int64] = [:]
                userInfo["type"] = 6 // 6 || 7
                userInfo["user_id"] = peer_id
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
                
                userInfo["type"] = Int64(type)
                userInfo["msg_id"] = local_id
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInMessageTV"), object: nil, userInfo: userInfo)
            }
        }
        catch let error as NSError { print("\(error)") }
    }
    
    class func deleteAllMsgToLocalID(_ data: Data) {
        let json = JSON(data)
        print(json)
        let peer_id = json[0].int64 ?? 0
        let local_id = json[1].int64 ?? 0
        
        do {
            let realm = try Realm()
            var predicate: NSPredicate = NSPredicate()
            predicate = NSPredicate(format: "id <= %@ AND peer_id = %@",argumentArray:[ local_id, peer_id])

            let msgsToDelete = realm.objects(VKMessage.self).filter(predicate)
            
            try realm.write {
                for msg in msgsToDelete { realm.delete(msg) }
                if let convers = realm.objects(VKConversation.self).filter("peer_id = %@", peer_id).first { realm.delete(convers) }
            }
            
            var userInfo: [String: Int64] = [:]
            userInfo["type"] = 13
            userInfo["user_id"] = peer_id
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
            
        } catch let error as NSError {
            print(error)
        }
    }
    
    class func changeChatFlags(_ data: Data) {
        let json = JSON(data)
        print(json)
        let type_id = json[0].int64 ?? 0
        let peer_id = json[1].int64 ?? 0
        var userInfo: [String: Int64] = [:]
        userInfo["type"] = 52
        userInfo["user_id"] = peer_id
        
        switch type_id {
        case 1, 2: // change chat title, chat photo
            vkLoadData.loadInfoAboutChatForLongPoll(String(peer_id - 2000000000)) { (result) in
                if result == true {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInMessageTV"), object: nil, userInfo: userInfo)
                }
            }
            break
        case 6, 7, 8, 9: // conect new user, user leave chat, kick user, user lose admin right
            guard let user_id = json[2].int64 else {return}
            
            do {
                let realm = try Realm()
                guard let chat = realm.objects(VKChat.self).filter("id = %@", peer_id).first else { return }
                guard let convers = realm.objects(VKConversation.self).filter("peer_id = %@", peer_id).first else { return }
                
                try realm.write {
                    switch type_id {
                    case 6:
                        chat.users.append(user_id)
                        convers.chat_settings_members_count += 1
                        break
                    case 7, 8:
                        if let index = chat.users.index(of: user_id) {
                            chat.users.remove(at: index)
                            convers.chat_settings_members_count -= 1
                        }
                        break
                    default:
                        break
                    }
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInMessageTV"), object: nil, userInfo: userInfo)
                //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
            } catch let error as NSError {
                print(error)
            }
            // MARK: need add code
            break
        default:
            break
        }
    }

    class func isUserWriting(_ data: Data, _ type: Int) {
        let json = JSON(data)
        var userInfo: [String: Any] = [:]
        let user_id = json[0].int64 ?? 0
        
        userInfo["type"] = Int64(type)
        
        if type == 61 {
            let flags = json[1].int ?? 0
        
            if flags == 1 {
                userInfo["user_id"] = user_id
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInMessageTV"), object: nil, userInfo: userInfo)
            }
        } else if type == 62 {
            let chat_id = json[1].int64 ?? 0
            
            userInfo["user_id"] = chat_id
            
            do {
                let realm = try Realm()
                let user = realm.objects(VKUser.self).filter("id = %@", user_id).first
                
                if user != nil {
                    userInfo["writingUserName"] = (user?.first_name ?? "") + " " + (user?.last_name ?? "")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInMessageTV"), object: nil, userInfo: userInfo)
                } else {
                    vkLoadData.loadMainInfoAboutUser(String(user_id)) { (isLoad) in
                        if isLoad == true {
                            vkDataCache.saveUserInfoInCache(vkLoadData.usersInfo, completion: { (isSave) in
                                vkLoadData.usersInfo.removeAll()
                                if isSave == 1 {
                                    do {
                                        let realmAsync = try Realm()
                                        let userAsync = realmAsync.objects(VKUser.self).filter("id = %@", user_id).first
                                        userInfo["writingUserName"] = (userAsync?.first_name ?? "") + " " + (userAsync?.last_name ?? "")
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInMessageTV"), object: nil, userInfo: userInfo)
                                    } catch let error as NSError {
                                        print(error)
                                    }
                                }
                            })
                        }
                    }
                }
                
            } catch let error as NSError {
                print(error)
            }
        }
    }
    
    class func changeConversCount(_ data: Data) {
        let json = JSON(data)
        let count = json[0].int ?? 0
        
        var userInfo: [String: Int64] = [:]
        
        userInfo["type"] = 80
        vkLoadData.countUnreadConvers = count
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
    }

    class func changePushSettings(_ data: Data) {
        let json = JSON(data)
        let peer_id = json[0]["peer_id"].int64 ?? 0
        let sound = json[0]["sound"].int ?? 0
        let disabled_until = json[0]["disabled_until"].int64 ?? 0
        var userInfo: [String: Int64] = [:]
        
        userInfo["type"] = 114
        userInfo["user_id"] = peer_id
        
        do {
            let realm = try Realm()
            let convers: VKConversation?
            
            convers = realm.objects(VKConversation.self).filter("peer_id = %@", peer_id).first
            
            if convers != nil {
                try realm.write {
                    convers!.push_settings_no_sound = sound == 0 ? true : false
                    convers!.push_settings_disabled_until = disabled_until
                    convers!.push_settings_disabled_forever = disabled_until == -1 ? true : false
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInDialogRow"), object: nil, userInfo: userInfo)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadInfoInMessageTV"), object: nil, userInfo: userInfo)
                }
            }
        }
        catch let error as NSError {
            print("\(error)")
        }
    }
}
