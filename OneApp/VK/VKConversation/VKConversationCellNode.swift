//
//  VKConversationCellNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 10/2/18.
//

import UIKit
import AsyncDisplayKit
import RealmSwift
import SwipeCellKit

protocol VKConversationCellNodeDelegate {
    func userIsWriting(writingStr: String)
    func stopProcessWriting()
}

class VKConversationCellNode: ASCellNode, VKConversationCellNodeDelegate {
    fileprivate let profileImageNode: ASNetworkImageNode
    fileprivate let nameTextNode: ASTextNode
    fileprivate let statusImageNode: ASImageNode
    fileprivate let dateTextNode: ASTextNode
    fileprivate let msgTextNode: ASTextNode
    fileprivate let unreadTextNode: ASTextNode
    fileprivate let unreadBackImageNode: ASImageNode
    fileprivate let msgStatusImageNode: ASImageNode
    fileprivate let soundlessImageNode: ASImageNode
    
    fileprivate let sepNode: ASDisplayNode
    
    var isOnline: Bool = false
    var isOutput: Bool = false
    var isNoSound: Bool = false
    var isHaveUnread: Bool = false
    var peer_id: Int64 = 0
    var photo_url: String = ""
    var stackWidth: CGFloat = 0.0
    let secondsInDay: TimeInterval = 60 * 60 * 24
    var isTyping: Bool = false
    var isNeedStopingWriting: Bool = false
    
    init(convRef: ThreadSafeReference<VKConversation>, tableWidth: CGFloat) {
        profileImageNode = ASNetworkImageNode()
        nameTextNode = ASTextNode()
        statusImageNode = ASImageNode()
        dateTextNode = ASTextNode()
        msgTextNode = ASTextNode()
        unreadTextNode = ASTextNode()
        unreadBackImageNode = ASImageNode()
        msgStatusImageNode = ASImageNode()
        soundlessImageNode = ASImageNode()
        sepNode = ASDisplayNode()
        super.init()
        automaticallyManagesSubnodes = true
        
        backgroundColor = mainColors.mainTableView
        stackWidth = tableWidth - 107
        selectionStyle = .none
        let realm = try! Realm()
        
        if let convers = realm.resolve(convRef) {
            
            //let time = CFAbsoluteTimeGetCurrent()
            
            if convers.userInfo == nil && convers.groupInfo == nil && convers.chatInfo == nil {
                
                try! realm.write {
                    if convers.peer_type == VKUserType.isUser.rawValue {
                        convers.userInfo = realm.objects(VKUser.self).filter("id = %@", convers.peer_id).first
                    }
                    else if convers.peer_type == VKUserType.isGroup.rawValue {
                        convers.groupInfo = realm.objects(VKGroup.self).filter("id = %@", convers.peer_id).first
                    }
                    else if convers.peer_type == VKUserType.isChat.rawValue {
                        convers.chatInfo = realm.objects(VKChat.self).filter("id = %@", convers.peer_id).first
                    }
                }
                
            }
            
            ////////////// DATE
            var dateStr = ""
            let dateFormatter = DateFormatter()
            let elapsedTimeInSec = Date().timeIntervalSince(Date(timeIntervalSince1970: TimeInterval((convers.lastMsgDate))))
            let secondsFromDayBegin = Date().timeIntervalSince(Calendar.current.startOfDay(for: Date()))

            if elapsedTimeInSec > 7 * secondsInDay {
                dateFormatter.dateFormat = "dd.MM.yy"
                dateStr = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval((convers.lastMsgDate))))
            }
            else if elapsedTimeInSec > (secondsInDay + secondsFromDayBegin) {
                dateFormatter.dateFormat = "EEE"
                dateStr = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval((convers.lastMsgDate))))
            }
            else if elapsedTimeInSec <= secondsFromDayBegin {
                dateFormatter.dateFormat = "h:mm a"
                dateStr = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval((convers.lastMsgDate))))
            }
            else {
                dateStr = "Yesterday"
            }

            dateTextNode.attributedText = NSAttributedString(string: dateStr, attributes: [.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: mainColors.conversationMsgText!])
            dateTextNode.backgroundColor = UIColor.clear
            dateTextNode.placeholderColor = mainColors.lightGray
            
            let dateWidth = dateTextNode.calculateSizeThatFits(CGSize(width: tableWidth, height: 30)).width

            
            /////////////// INFO ABOUT USER
            
            nameTextNode.backgroundColor = UIColor.clear
            nameTextNode.maximumNumberOfLines = 1
            
            if (convers.userInfo != nil && convers.peer_type == VKUserType.isUser.rawValue) ||
                (convers.groupInfo != nil && convers.peer_type == VKUserType.isGroup.rawValue) ||
                (convers.chatInfo != nil && convers.peer_type == VKUserType.isChat.rawValue) {
                
                ////////soundless
                
                isNoSound = convers.push_settings_no_sound || convers.push_settings_disabled_until > Int64(Date.timeIntervalBetween1970AndReferenceDate)
                var soundlessImgWidth: CGFloat = 0.0
                
                if isNoSound == true {
                    soundlessImageNode.image = mainIcons.soundless
                    soundlessImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(mainColors.deleteRow!)
                    soundlessImageNode.style.preferredSize = CGSize(width: 12, height: 12)
                    
                    soundlessImgWidth = 12.0
                }
                
                //////// user name
                
                nameTextNode.attributedText = NSAttributedString(string: vkDataCache.getUsernameByIDAndType(id: convers.peer_id, type: convers.peer_type), attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.usernameText!, .foregroundColor: mainColors.usernameText!])
                nameTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(tableWidth - 113 - soundlessImgWidth - dateWidth))
                nameTextNode.truncationMode = .byTruncatingTail
                
                
                //////// profileImg
                
                switch convers.peer_type {
                case VKUserType.isUser.rawValue:
                    photo_url = convers.userInfo?.photo_100 ?? ""
                    break
                case VKUserType.isChat.rawValue:
                    photo_url = convers.chatInfo?.photo_100 ?? ""
                    break
                case VKUserType.isGroup.rawValue:
                    photo_url = convers.groupInfo?.photo_100 ?? ""
                    break
                default:
                    break
                    
                }
                peer_id = convers.peer_id
                
                var data = vkDataCache.getProfileImgFromCache(sourceID: peer_id, ownerID: peer_id, type: "image", tag: "ava")
                
                if vkDataCache.getProfileURLFromCache(sourceID: peer_id, ownerID: peer_id, type: "image", tag: "ava") != photo_url {
                    vkDataCache.removeDataFromCachedData(sourceID: peer_id, ownerID: peer_id, type: "image", tag: "ava")
                    data = nil
                }
                
                if data == nil {
                    //profileImageNode.defaultImage = convers.peer_type == UserType.isChat.rawValue ? mainIcons.chat_s50 : mainIcons.userProfile_s50
                    //profileImageNode.tintColor = mainColors.lightGray
                    if photo_url != "" { profileImageNode.url = NSURL(string: photo_url)! as URL }
                    else {
                        profileImageNode.image = Helper.textToImage(drawText: vkDataCache.getUserShortNameByIDAndType(id: convers.peer_id, type: convers.peer_type), height: 60, width: 60)
                    }
                }
                else {
                    profileImageNode.defaultImage = nil
                    profileImageNode.image = UIImage(data: data! as Data)
                }
                
                profileImageNode.imageModificationBlock = { [weak self] image in
                    if vkDataCache.getProfileImgFromCache(sourceID: self?.peer_id ?? 0, ownerID: self?.peer_id ?? 0, type: "image", tag: "ava") == nil {
                        vkDataCache.saveDataInCachedData(sourceID: self?.peer_id ?? 0, ownerID: self?.peer_id ?? 0, type: "image", tag: "ava", url: self?.photo_url ?? "", image: image)
                    }
                    return image
                }
                
                profileImageNode.cornerRadius = 28
                profileImageNode.style.preferredSize = CGSize(width: 56, height: 56)
                
            }
            else{
                nameTextNode.attributedText = NSAttributedString(string: "Unknown", attributes: [.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: mainColors.text!])
                nameTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(tableWidth - 62))
                profileImageNode.style.preferredSize = CGSize(width: 56, height: 56)
                //profileImg.image = UIImage()
            }
            

            ///////////////// COUNT UNREAD MESSAGE
            
            isHaveUnread = convers.unread_count > 0
            var unreadWidth: CGFloat = 0.0
            
            if isHaveUnread == true {
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = .center
                paragraphStyle.lineBreakMode = .byWordWrapping
                
                unreadTextNode.backgroundColor = UIColor.clear
                unreadTextNode.maximumNumberOfLines = 1
                unreadTextNode.attributedText = NSAttributedString(string: String(convers.unread_count), attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.boldSystemFont(ofSize: 13), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: isNoSound == true ? mainColors.conversationMsgText! : mainColors.unreadLabel!])
                unreadTextNode.style.minHeight = ASDimension(unit: .points, value: 20)
                unreadTextNode.style.minWidth = ASDimension(unit: .points, value: 20)
                
                unreadTextNode.textContainerInset = UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5)
                
                unreadBackImageNode.backgroundColor = isNoSound == true ? mainColors.conversationMsgText?.withAlphaComponent(0.15) : mainColors.unreadLabel?.withAlphaComponent(0.15)
                unreadBackImageNode.cornerRadius = 10
                unreadWidth = unreadTextNode.calculateSizeThatFits(CGSize(width: tableWidth, height: 30)).width
                
                sepNode.backgroundColor = isNoSound == true ? mainColors.conversationMsgText : mainColors.unreadLabel
                sepNode.cornerRadius = 1.5
                sepNode.style.width = ASDimension(unit: .points, value: 3)
                sepNode.style.minHeight = ASDimension(unit: .points, value: 30)
                //sepNode.style.flexGrow = 1
                sepNode.style.flexShrink = 1
            }
            
            
            
            ////////////// isUNREAD MESSAGE
            
            isOutput = convers.last_message?.from_id == vkLoadData.mainUserID
            
            var readWidth : CGFloat = 0
            
            if isOutput == true {
                let isRead = (convers.last_message?.id ?? 0) <= convers.out_read
                msgStatusImageNode.image = isRead ? mainIcons.readMessage_s12 : mainIcons.unreadMessage_s12
                msgStatusImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(isRead ? mainColors.readMessage! : mainColors.textFooter!)
                msgStatusImageNode.style.preferredSize = CGSize(width: 12, height: 12)
                readWidth = 12
            }

            
            ////////////// MESSAGE BODY
            
            var text: String = convers.last_message?.text == "" ? getAttachmentNames(msg: convers.last_message ?? VKMessage()) : (convers.last_message?.text ?? "")
            
            if convers.peer_type == VKUserType.isChat.rawValue && (convers.last_message?.action_type ?? "" ) != "" {
                text = checkActionType(from_id: convers.last_message?.from_id ?? 0, member_id: convers.last_message?.action_member_id ?? 0, actionType: convers.last_message?.action_type ?? "", actionText: convers.last_message?.action_text ?? "")
            }
            else if convers.peer_type == VKUserType.isChat.rawValue {
                text = vkDataCache.getUsernameByIDAndType(id: convers.last_message?.from_id ?? 0, type: "user") + "\n" + text.replacingOccurrences(of: "\n", with: " ")
            } else {
                text = text.replacingOccurrences(of: "\n", with: " ")
            }

            
            msgTextNode.attributedText = NSAttributedString(string: text, attributes: [.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: mainColors.conversationMsgText!])
            msgTextNode.backgroundColor = UIColor.clear
            msgTextNode.maximumNumberOfLines = 2
            msgTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(tableWidth - 103 - unreadWidth - readWidth))
            
            ////////////// USER IS ONLINE
            
            isOnline = convers.userInfo?.online == true || convers.userInfo?.online_mobile == true

            if isOnline == true {
                let isMob = convers.userInfo?.online_mobile
                let image = isMob == true ? mainIcons.userPhoneOnline : mainIcons.userOnline
                let edgeInsets = isMob == true ? UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0) : UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                statusImageNode.image = image?.resizableImage(withCapInsets: edgeInsets, resizingMode: .stretch).withRenderingMode(.alwaysTemplate)
                
                statusImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(mainColors.online!)
                statusImageNode.backgroundColor = mainColors.mainTableView
                statusImageNode.borderColor = mainColors.mainTableView?.cgColor
                statusImageNode.borderWidth = isMob == true ? 2 : 1.7
                statusImageNode.cornerRadius = isMob == true ? 3 : 6.5
                statusImageNode.style.preferredSize = isMob == true ? CGSize(width: 12, height: 16) : CGSize(width: 13, height: 13)
            }
            
            //print("config conversNode - \(CFAbsoluteTimeGetCurrent() - time)")
        }
        
        addSubnode(profileImageNode)
        addSubnode(nameTextNode)
        addSubnode(statusImageNode)
        addSubnode(dateTextNode)
        addSubnode(msgTextNode)
        addSubnode(unreadTextNode)
        addSubnode(msgStatusImageNode)
        addSubnode(soundlessImageNode)
    }
       
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let nameSoundStack = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .start, alignItems: .center, children: [nameTextNode])
        if isNoSound == true { nameSoundStack.children?.append(soundlessImageNode) }
        
        let nameDateStack = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .spaceBetween, alignItems: .center, children: [nameSoundStack, dateTextNode])
        nameDateStack.style.minWidth = ASDimension(unit: .points, value: stackWidth)
        //let height = nameDateStack.calculateLayoutThatFits(constrainedSize).size.height

        let isReadMsgStack = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .start, alignItems: .start, children: [msgTextNode])
        if isOutput == true { isReadMsgStack.children?.insert(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 3, left: 0, bottom: 0, right: 0), child: msgStatusImageNode), at: 0) }
        
        let msgUnreadStack = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .spaceBetween, alignItems: .center, children: [isReadMsgStack])
        if isHaveUnread == true {
            msgUnreadStack.children?.append(ASBackgroundLayoutSpec(child: unreadTextNode, background: unreadBackImageNode))
        }
        msgUnreadStack.style.minWidth = ASDimension(unit: .points, value: stackWidth)
        //msgUnreadStack.style.minHeight = ASDimension(unit: .points, value: constrainedSize.max.height - height - 20)
        let contentStack = ASStackLayoutSpec(direction: .vertical, spacing: 2.8, justifyContent: .start, alignItems: .stretch, children: [nameDateStack, msgUnreadStack])
        
        let finalStack = ASStackLayoutSpec(direction: .horizontal, spacing: 17, justifyContent: .start, alignItems: .stretch, children: [contentStack])
        
        if isOnline == true {
            let overlay = ASOverlayLayoutSpec(child: profileImageNode, overlay: ASInsetLayoutSpec(insets: UIEdgeInsets(top: .infinity, left: 2, bottom: 1, right: CGFloat.infinity), child: statusImageNode))
            
            finalStack.children?.insert(overlay, at : 0)
        } else { finalStack.children?.insert(profileImageNode, at : 0) }
        
        
        let finalInsetSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 14, left: isHaveUnread ? 12 : 15, bottom: 14, right: isHaveUnread ? 17 : 15), child: finalStack)
        finalInsetSpec.style.height = ASDimension(unit: .points, value: 84)
        
        if isHaveUnread == true {
            let isHaveUndeadStack = ASStackLayoutSpec(direction: .horizontal, spacing: 0, justifyContent: .start, alignItems: .start, children: [sepNode, finalInsetSpec])
            return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: isHaveUndeadStack)
        }
        else {
            return finalInsetSpec
        }
    }

    
    func getAttachmentNames(msg: VKMessage) -> String {
        var str = ""
        var attNamesDict: [String: Int] = [:]
        
        if msg.geo != nil { str.append(str.count > 0 ? ", geo" : "geo") }
        if msg.fwd_messages.count > 0 { str.append(str.count > 0 ? ", forward messages" : "forward message") }
        
        for att in msg.attachments {
            if let _ = attNamesDict[att.type] { attNamesDict[att.type]! += 1 }
            else { attNamesDict[att.type] = 1 }
        }
        
        for (key, value) in attNamesDict { str.append(str.count > 0 ? ", \(value) \(key)" : "\(value) \(key)")  }
        
        return str
    }
    
    func checkActionType(from_id: Int64, member_id: Int64, actionType: String, actionText: String) -> String {
        var msg: String = ""
        let fromUsername: String = vkDataCache.getUsernameByIDAndType(id: from_id, type: VKUserType.isUser.rawValue)
        switch actionType {
        case "chat_photo_update":
            msg = fromUsername + " updated the group chat photo"
            break
        case "chat_photo_remove":
            msg = fromUsername + " removed the group chat photo"
            break
        case "chat_create":
            msg = fromUsername + " created this group chat"
            break
        case "chat_title_update":
            msg = fromUsername + " changed the group chat name to \"" + actionText + "\""
            break
        case "chat_invite_user":
            msg = fromUsername + " invited " + vkDataCache.getUsernameByIDAndType(id: member_id, type: VKUserType.isUser.rawValue)
            break
        case "chat_kick_user":
            msg = from_id == member_id ? fromUsername + "left this chat" :  fromUsername +  " kicked " + vkDataCache.getUsernameByIDAndType(id: member_id, type: VKUserType.isUser.rawValue) + " out"
            break
        default:
            break
        }
        
        return msg
    }
    
    func userIsWriting(writingStr: String) {
        if isTyping == false {
            let typingStatusBGQueue = DispatchQueue.init(label: "TYPING_STATUS_BG_QUEUE")
            typingStatusBGQueue.async {
                let prevText = self.msgTextNode.attributedText
                self.isTyping = true
                let atr = [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: mainColors.unreadLabel!]
                var writingDot = ""
                for _ in 0...15 {
                    
                    if self.isNeedStopingWriting == true {
                        self.isTyping = false
                        self.isNeedStopingWriting = false
                        self.msgTextNode.attributedText = prevText
                        return
                    }
                    
                    if writingDot.count == 3 { writingDot = "" }
                    else { writingDot.append(".") }
                    self.msgTextNode.attributedText = NSAttributedString(string: writingStr + writingDot, attributes: atr)
                    usleep(300000)
                }
                self.msgTextNode.attributedText = prevText
                self.isTyping = false
            }
        }
    }
    
    func stopProcessWriting() {
        isNeedStopingWriting = true
    }
}


