//
//  VKConversationTableViewController.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 10/2/18.
//

import UIKit
import SwiftyVK
import SwiftyJSON
import SwipeCellKit
import RealmSwift
import DifferenceKit
import AsyncDisplayKit

class VKConversationTableViewController: UIViewController, UIActionSheetDelegate, ASTableDelegate, ASTableDataSource {
    
    struct ChangingInfoInConvers : Differentiable {
        var lastMessDate: Int64
        var isNoSound: Bool
        
        var differenceIdentifier: Int64 {
            return lastMessDate
        }
        
        func isContentEqual(to source: ChangingInfoInConvers) -> Bool {
            return lastMessDate == source.lastMessDate &&
                isNoSound == source.isNoSound
        }
    }
    
    struct VKConversationView: Differentiable {
        //var changingInfo: ChangingInfoInConvers
        var lastMessDate: Int64
        var isNoSound: Bool
        var convers: VKConversation
        
        var differenceIdentifier: Int64 {
            return lastMessDate
        }
        
        func isContentEqual(to source: VKConversationView) -> Bool {
            return lastMessDate == source.lastMessDate &&
            isNoSound == source.isNoSound
        }
    }
    
    private var conversDiff = [VKConversationView]()
    private var conversInput: [VKConversationView] {
        get { return conversDiff }
        set {
            let changeset = StagedChangeset(source: conversDiff, target: newValue)
            //print(changeset)
            tableNode.reload(using: changeset) { data in
                self.conversDiff = data
            }
        }
    }
    
    var conversations : [VKConversation] = []
    var nodeDict: [Int64 : VKConversationCellNode] = [:]
    var delegateDict: [Int64: VKConversationCellNodeDelegate] = [:]
    var count: Int = 0
    var lastMsgID: Int64 = 0
    var tableNode: ASTableNode = ASTableNode()
    var isLoading : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.black]// tintColor = UIColor.black
        self.navigationController?.navigationBar.barTintColor = mainColors.mainNavigationBarLight
        
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationItem.rightBarButtonItem?.image = mainIcons.search_s21
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.black
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: mainIcons.search_s21, style: .plain, target: nil, action: nil)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: mainIcons.newMessage_s20, style: .plain, target: nil, action: nil)
        //self.navigationController?.navigationBar.tintColor = UIColor.black
        //addContactBtn()
        
        vkLoadData.loadRequestsUsers(isOut: false, isNeedViewed: false) { [weak self] (result) in
            if result == true {
                if vkLoadData.searchUserInfo.count > 0 {
                    DispatchQueue.main.async {
                        self?.tabBarController?.tabBar.items?[2].badgeValue = vkLoadData.requestToFriendCount == 0 ? nil : String(vkLoadData.requestToFriendCount)
                    }
                }
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func addContactBtn() {
        let btn = Helper.getBtnWithBadge(img: mainIcons.contact_s21!, count: vkLoadData.requestToFriendCount, isLeft: true)
        btn.addTarget(self, action: #selector(onContactBtn), for: .touchUpInside)
        let btnLeft = UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItem = btnLeft
    }
    
    func initTableView() {
        tableNode.view.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableNode.backgroundColor = mainColors.mainTableView
        tableNode.view.separatorColor = mainColors.separator
        tableNode.delegate = self
        tableNode.dataSource = self
        tableNode.view.refreshControl = UIRefreshControl()
        
        view.addSubnode(tableNode)
        
        //tableNode.view.refreshControl?.beginRefreshing()
        self.conversations = vkDataCache.getVKConversations(count: 30, isNeedReload: false)
        count = self.conversations.count
        if count > 1 {
            tableNode.view.refreshControl?.endRefreshing()
        }
        

        
        vkLoadData.loadVKConversations(view: self.view) { [weak self] (result) in
            if result == true {
                DispatchQueue.main.async { self?.initConversDiff(needCount: 30, isNeedReload: true) }
            }
        }
        
        tableNode.view.refreshControl?.addTarget(self, action: #selector(reloadConversations), for: .valueChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadInfoInConversationRow), name: NSNotification.Name.reloadInfoInDialogRow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addContactBtn), name: NSNotification.Name.updateBabgeOnBarButton, object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        let frame = self.view.frame
        self.view.frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.width, height: (tabBarController?.tabBar.frame.origin.y ?? (frame.height + frame.origin.y)) - frame.origin.y)
        self.tableNode.frame = self.view.bounds
        self.tableNode.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
    }
    
    @objc func reloadInfoInConversationRow(notification: NSNotification) {
        let userInfo = notification.userInfo ?? [:]
        
        if userInfo.count > 0 {
            let user_id: Int64 = userInfo["user_id"] as! Int64
            let type: Int64 = userInfo["type"] as! Int64
            
            if type == 1 || type == 2 || type == 3 {
                DispatchQueue.main.async {
                    vkDataCache.bgRealm?.refresh()
                    self.initConversDiff(needCount: self.conversDiff.count, isNeedReload: true)
                    //self.tableNode.reloadData()
                }
            }
            else if type == 4 || type == 13 {
                DispatchQueue.main.async {
                    if type == 13 {
                        self.count -= 1
                    }
                    vkDataCache.bgRealm?.refresh()
                    self.delegateDict[user_id]?.stopProcessWriting()
                    usleep(300000)
                    self.initConversDiff(needCount: self.conversDiff.count, isNeedReload: true)
                }
            }
            else if type == 5 || type == 6 || type == 8 || type == 9 || type == 10 || type == 52 || type == 114 {
                DispatchQueue.main.async {
                    vkDataCache.bgRealm?.refresh()
                    let visibleRows = self.tableNode.indexPathsForVisibleRows()
                    for iPath in visibleRows {
                        if user_id == self.conversations[iPath.row].peer_id {
                            self.tableNode.reloadRows(at: [iPath], with: .none)
                        }
                    }
                }
            }
            else if type == 61 || type == 62 {
                var writingUserName = ""
                
                if type == 62 {
                    writingUserName = userInfo["writingUserName"] as! String
                    writingUserName += " is typing"
                }
                else { writingUserName = "typing" }
                DispatchQueue.main.async {
                    for iPath in self.tableNode.indexPathsForVisibleRows() {
                        if user_id == self.conversations[iPath.row].peer_id {
                            self.delegateDict[user_id]?.userIsWriting(writingStr: writingUserName)
                        }
                    }
                }
            }
            else if type == 80 {
                self.tabBarController?.tabBar.items?[2].badgeValue = vkLoadData.requestToFriendCount == 0 ? nil : String(vkLoadData.requestToFriendCount)
            }
        }
        
    }
    
    @objc func reloadConversations() {
        tableNode.view.refreshControl?.endRefreshing()
        vkLoadData.reloadVKConversations { [weak self] (result) in
            if result == true{
                DispatchQueue.main.async {
                    self?.initConversDiff(needCount: 30, isNeedReload: true)
                }
            }
        }
    }
    
    func initConversDiff(needCount: Int, isNeedReload: Bool) {
        conversations = vkDataCache.getVKConversations(count: needCount, isNeedReload: isNeedReload) // need 30
        count = conversations.count
        lastMsgID = conversations.last?.last_message?.id ?? 0
        tabBarController?.tabBar.items?[0].badgeValue = vkLoadData.countUnreadConvers == 0 ? nil : String(vkLoadData.countUnreadConvers)
        
        var convers: [VKConversationView] = []
        
        for conv in conversations {
            convers.append(VKConversationView(lastMessDate: conv.lastMsgDate, isNoSound: conv.push_settings_no_sound, convers: conv))
        }
        
        conversInput = convers
        
    }
    
    @IBAction func onContactBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vkContactController = storyboard.instantiateViewController(withIdentifier: "VKContactsViewControllerID") as? VKContactsViewController {
            vkContactController.userID = vkLoadData.mainUserID
            vkContactController.friendsCount = vkLoadData.mainUser.counters_friends
            self.navigationController?.pushViewController(vkContactController, animated: true)
        }
    }
    
    func deleteConversBy(indexPath: IndexPath) {
//        let convers = conversations[indexPath.row]
//
//        vkLoadData.deleteConversation(peer_id: convers.peer_id, peer_type: convers.peer_type, completion: { [weak self] (result) in
//            if result == true {
//                DispatchQueue.main.async {
////                    self?.tableView.beginUpdates()
////                    try! vkDataCache.bgRealm?.write {
////                        convers.delete(vkDataCache.bgRealm!)
////                        vkDataCache.bgRealm?.delete(convers)
////                    }
////                    self?.conversations.remove(at: indexPath.row)
////                    self?.count = self?.conversations.count ?? 0
////
////                    self?.tableView.deleteRows(at: [indexPath], with: .left)
////                    self?.tableView.endUpdates()
//                }
//            }
//        })
    }
    
    func readConversBy(indexPath: IndexPath) {
        let convers = conversations[indexPath.row]
        vkLoadData.readConversation(peer_id: convers.peer_id) { _ in }
    }
    
    func changePushSettings(disableSound: Bool, howMuch: Int) {
        
    }

}

extension VKConversationTableViewController {
    //MARK: TableNode function
    
    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
        return conversDiff.count
    }
    
    func tableNode(_ tableNode: ASTableNode, nodeBlockForRowAt indexPath: IndexPath) -> ASCellNodeBlock {
        let conv = conversDiff[indexPath.row]
        let peer_id = conv.convers.peer_id
        let convRef = ThreadSafeReference(to: conv.convers)
        return {
            if let val = self.nodeDict[peer_id] {
                return val
            }
            else {
                let node = VKConversationCellNode(convRef: convRef, tableWidth: UIScreen.main.bounds.size.width)
                //self.nodeDict[peer_id] = node
                DispatchQueue.main.async {
                    self.delegateDict[peer_id] = node
                }
                
                return node
            }
            
        }
        //return { return ASCellNode() }
    }

    func tableNode(_ tableNode: ASTableNode, willBeginBatchFetchWith context: ASBatchContext) {
        if self.isLoading == false && self.count > 0 {
            vkLoadData.loadVKConversations(startID: self.lastMsgID, offset: 1, view: self.view, completionBlock: { [weak self] (result) in
                if result == true {
                    DispatchQueue.main.async {
                        self?.initConversDiff(needCount: (self?.conversDiff.count ?? 0) + 30, isNeedReload: false)
                        self?.isLoading = false
                        context.completeBatchFetching(true)
                    }
                } else if result == false {
                    DispatchQueue.main.async {
                        self?.isLoading = false
                        context.completeBatchFetching(true)
                    }
                }
                
            })
        }
    }
    
    func shouldBatchFetch(for tableNode: ASTableNode) -> Bool {
        if self.conversDiff.count < vkLoadData.countConvers { return true }
        return false
    }
    
    
    @objc(tableNode:constrainedSizeForRowAtIndexPath:)
    func tableNode(_ tableNode: ASTableNode, constrainedSizeForRowAt indexPath: IndexPath) -> ASSizeRange {
        let min = CGSize(width: UIScreen.main.bounds.size.width, height: 84)
        let max = CGSize(width: UIScreen.main.bounds.size.width, height: 84)
        return ASSizeRange(min: min, max: max)
    }
    
    func tableNode(_ tableNode: ASTableNode, didSelectRowAt indexPath: IndexPath) {
        tableNode.deselectRow(at: indexPath, animated: true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vkMessageViewController = storyboard.instantiateViewController(withIdentifier: "VKMessageViewController") as? VKMessageViewController {
            //let myIndexPath = self.tableNode.indexPathForSelectedRow!
            vkMessageViewController.convers = self.conversations[indexPath.row]
            self.navigationController?.pushViewController(vkMessageViewController, animated: true)
        }
    }
}

extension VKConversationTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let actionDelete = UIContextualAction(style: .normal, title: "Delete") { [weak self] (action, view, complition) in
            let alert = UIAlertController(title: "Delete conversation?", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { [weak self] (action) in
                self?.deleteConversBy(indexPath: indexPath)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self?.present(alert, animated: true, completion: nil)
            complition(true)
        }
        actionDelete.image = mainIcons.trash_s20
        actionDelete.backgroundColor = mainColors.deleteRow
        
        let convers = self.conversations[indexPath.row]
        let isNoSound = convers.push_settings_no_sound
        //let title = isNoSound == true ? "Unmute" : "Mute"
        
        let actionMute = UIContextualAction(style: .normal, title: "Mute") { [weak self] (action, view, complition) in
            if isNoSound == false {
                let alert = UIAlertController(title: "", message: "Disable notification from this user", preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: "1 hour", style: .default, handler: { [weak self] (action) in
                    self?.changePushSettings(disableSound: true, howMuch: 3600)
                }))
                alert.addAction(UIAlertAction(title: "8 hour", style: .default, handler: { [weak self] (action) in
                    self?.changePushSettings(disableSound: true, howMuch: 28800)
                }))
                alert.addAction(UIAlertAction(title: "1 day", style: .default, handler: { [weak self] (action) in
                    self?.changePushSettings(disableSound: true, howMuch: 86400)
                }))
                alert.addAction(UIAlertAction(title: "Forever", style: .default, handler: { [weak self] (action) in
                    self?.changePushSettings(disableSound: true, howMuch: -1)
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self?.present(alert, animated: true, completion: nil)
                complition(true)
            }
            else {
                self?.changePushSettings(disableSound: false, howMuch: 0)
                complition(true)
            }
        }
        
        actionMute.image = isNoSound == true ? mainIcons.sound_s25 : mainIcons.soundless_s25
        actionMute.backgroundColor = mainColors.lightGray
        
        return UISwipeActionsConfiguration(actions: [actionDelete, actionMute])
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let actionRead = UIContextualAction(style: .normal, title: "Read") { [weak self] (action, view, complition) in
            let alert = UIAlertController(title: "Read all messages?", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Read", style: .default, handler: { [weak self] (action) in
                self?.readConversBy(indexPath: indexPath)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self?.present(alert, animated: true, completion: nil)
            complition(true)
        }
        actionRead.image = mainIcons.readMessage_s25
        actionRead.backgroundColor = mainColors.writeRow
        
        return UISwipeActionsConfiguration(actions: [actionRead])
    }
}
