//
//  VKNotification.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 29.05.2018.
//

import Foundation


extension Notification.Name {
    static let reloadInfoInDialogRow = Notification.Name("reloadInfoInDialogRow")
    static let reloadInfoInMessageTV = Notification.Name("reloadInfoInMessageTV")
    static let deleteOrAddFriend = Notification.Name("deleteOrAddFriend")
    static let deleteOrAddActionInProfile = Notification.Name("deleteOrAddActionInProfile")
    static let updateBabgeOnBarButton = Notification.Name("updateBabgeOnBarButton")
    static let updateAttachmentNode = Notification.Name("updateAttachmentNode")
    static let updateUploadAttachment = Notification.Name("updateUploadAttachment")
    static let addLocation = Notification.Name("addLocation")
}
