//
//  MainPlayer.swift
//  OneApp
//
//  Created by Егор Рынкевич on 10/10/18.
//

import Foundation
import AVFoundation

protocol MainPlayerDelegate: class {
    func startVisualization()
    func pauseVisualization()
    func stopResetVisualization()
}

class MainPlayer {
    
    var audioPlayer: AVPlayer?
    var audioMsgPlayer: AVPlayer?
    
    var currentAudioMsgID: Int64 = 0
    
    var audioMsgDelegate: MainPlayerDelegate?
    var isReadyToPlay: Bool = false
    
    func playAudioMessage(audio_url: String, audio_id: Int64, isAudioPlaying: Bool) {
        
        if currentAudioMsgID != audio_id {
            if audioMsgPlayer != nil {
                audioMsgPlayer?.pause()
                audioMsgPlayer = nil
            }
            
                let playerItem = AVPlayerItem(url: URL(string: audio_url)!)
                audioMsgPlayer = AVPlayer(playerItem: playerItem)

                let audioSession = AVAudioSession.sharedInstance()
                
                do {
                    try audioSession.setCategory(.playback, mode: .default, options: .mixWithOthers)
                } catch let sessionError {
                    print(sessionError)
                }
                audioMsgPlayer?.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main, using: { (cmTime) in
                    if self.audioMsgPlayer!.currentItem?.status == .readyToPlay {
                        if self.isReadyToPlay == false {
                            self.audioMsgDelegate?.startVisualization()
                            self.isReadyToPlay = true
                        }

                        //let time : Int64 = Int64(CMTimeGetSeconds(self.audioMsgPlayer!.currentTime()))
                    } else if self.audioMsgPlayer!.currentItem?.status == AVPlayerItem.Status.failed {

                    }
                })
            
            
            
            currentAudioMsgID = audio_id
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.stopAudioMessage), name: .AVPlayerItemDidPlayToEndTime, object: nil)
        }
        
        
        
        if isAudioPlaying == false {
            audioMsgPlayer?.play()
            if isReadyToPlay == true { audioMsgDelegate?.startVisualization() }
            
        }
        else {
            audioMsgPlayer?.pause()
            audioMsgDelegate?.pauseVisualization()
        }
        
    }
    
    @objc func stopAudioMessage() {
        
        audioMsgPlayer = nil
        isReadyToPlay = false
        currentAudioMsgID = 0
        audioMsgDelegate?.stopResetVisualization()
        audioMsgDelegate = nil
    }
}
