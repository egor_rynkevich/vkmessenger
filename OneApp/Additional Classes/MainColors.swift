//
//  MainColor.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 18.07.2018.
//

import Foundation

struct MessageColor {
    var messageText: UIColor?
    var messageSubText: UIColor?
    var messageIcon: UIColor?
    var messageAditionalIcon: UIColor?
}

class MainColors {
    var usernameText: UIColor?
    var writeRow: UIColor?
    var deleteRow: UIColor?
    var online: UIColor?
    var mainNavigationBarLight: UIColor?
    var mainNavigationBarDark: UIColor?
    var lightGray: UIColor?
    var mainTableView: UIColor?
    var text: UIColor?
    var readMessage: UIColor?
    var darkGray: UIColor?
    var separator: UIColor?
    var sectionHeader: UIColor?
    var textGray: UIColor?
    var mainBackgroundView: UIColor?
    var outgoingMessage: UIColor?
    var outgoingMessageSecond: UIColor?
    var incomingMessage: UIColor?
    var incomingText: UIColor?
    var incomingSubText: UIColor?
    var incomingIcon: UIColor?
    var incomingAdditionalIcon: UIColor?
    
    var conversationMsgText: UIColor?
    
    var outgoingText: UIColor?
    var outgoingSubText: UIColor?
    var outgoingIcon: UIColor?
    var outgoingAdditionalIcon: UIColor?
    var giftNode: UIColor?
    var textBar: UIColor?
    var subTextBar: UIColor?
    var textFooter: UIColor?
    var importantMessage: UIColor?
    var sendMessage: UIColor?
    var unreadLabel: UIColor?
    var tabBarNonTint: UIColor?
    var tabBarTint: UIColor?
    var tabBar: UIColor?
    var tabBarBadge: UIColor?
    var tabBarBadgeText: UIColor?
    var messageInputNode: UIColor?
    var actionViewElements: UIColor?
    let darkMode = false
    
    init() {
        usernameText = darkMode == true ? UIColor(named: "DarkUsernameTextColor") : UIColor(named: "LightUsernameTextColor")
        writeRow = UIColor(named: "WriteRowColor")
        deleteRow = UIColor(named: "DeleteRowColor")
        online = UIColor(named: "OnlineColor")
        mainNavigationBarLight = darkMode == true ? UIColor(named: "DarkMainNavigationBarLightColor") : UIColor(named: "MainNavigationBarLightColor")
        mainNavigationBarDark = darkMode == true ? UIColor(named: "DarkMainNavigationBarDarkColor") : UIColor(named: "MainNavigationBarDarkColor")
        lightGray = UIColor(named: "LightGrayColor")
        mainTableView = darkMode == true ? UIColor(named: "DarkMainTableViewColor") : UIColor(named: "MainTableViewColor")
        text = darkMode == true ? UIColor.white : UIColor.black
        separator = darkMode == true ? UIColor(named: "DarkSeparatorColor") : UIColor(named: "SeparatorColor")
        readMessage = darkMode == true ? UIColor(named: "DarkReadMessageColor") : UIColor(named: "ReadMessageColor")
        darkGray = UIColor(named: "DarkGrayColor")
        sectionHeader = darkMode == true ? UIColor(named: "DarkMainNavigationBarLightColor") : UIColor(named: "SectionHeaderColor")
        textGray = UIColor(named: "TextGrayColor")
        mainBackgroundView = UIColor(named: "MainBackgroundViewColor")
        outgoingMessage = darkMode == true ? UIColor(named: "DarkFirstOutgoingMessageColor") : UIColor(named: "OutgoingMessageColor")
        outgoingMessageSecond = UIColor(named: "DarkSecondOutgoingMessageColor")
        incomingMessage = darkMode == true ? UIColor(named: "DarkIncomingMessageColor") : UIColor(named: "IncomingMessageColor")
        giftNode = UIColor(named: "GiftNodeColor")
        
        conversationMsgText = darkMode == true ? UIColor(named: "DarkConversationMsgTextColor") : UIColor(named: "ConversationMsgTextColor")
        
        incomingText = darkMode == true ? UIColor(named: "DarkIncomingTextColor") : UIColor(named: "IncomingTextColor")
        incomingSubText = darkMode == true ? UIColor(named: "DarkIncomingSubTextColor") : UIColor(named: "IncomingSubTextColor")
        incomingIcon = darkMode == true ? UIColor(named: "IncomingIconColor") : UIColor(named: "IncomingIconColor")
        incomingAdditionalIcon = darkMode == true ? UIColor(named: "IncomingAdditionalIconColor") : UIColor(named: "IncomingAdditionalIconColor")
        outgoingText = darkMode == true ? UIColor(named: "DarkOutgoingTextColor") : UIColor(named: "IncomingTextColor")//"OutgoingTextColor")
        outgoingSubText = darkMode == true ? UIColor(named: "DarkOutgoingSubTextColor") : UIColor(named: "OutgoingSubTextColor")
        outgoingIcon = darkMode == true ? UIColor(named: "OutgoingIconColor") : UIColor(named: "OutgoingIconColor")
        outgoingAdditionalIcon = darkMode == true ? UIColor(named: "OutgoingAdditionalIconColor") : UIColor(named: "OutgoingAdditionalIconColor")
        textBar = darkMode == true ? UIColor(named: "DarkTextBarColor") : UIColor(named: "LightTextBarColor")
        subTextBar = darkMode == true ? UIColor(named: "DarkSubTextBarColor") : UIColor(named: "LightSubTextBarColor")
        textFooter = darkMode == true ? UIColor(named: "DarkTextFooterColor") : UIColor(named: "TextFooterColor")
        importantMessage = UIColor(named: "ImportantMessageColor")
        sendMessage = darkMode == true ? UIColor(named: "DarkSendMessageColor") : UIColor(named: "SendMessageColor")
        unreadLabel = darkMode == true ? UIColor(named: "DarkUnreadLabelColor") : UIColor(named: "UnreadLabelColor")
        tabBarNonTint = darkMode == true ? UIColor(named: "DarkTabBarNonTintColor") : UIColor(named: "LightTabBarNonTintColor")
        tabBarTint = darkMode == true ? UIColor(named: "DarkTabBarTintColor") : UIColor(named: "LightTabBarTintColor")
        tabBar = darkMode == true ? UIColor(named: "DarkTabBarColor") : UIColor(named: "LightTabBarColor")
        tabBarBadge = darkMode == true ? UIColor(named: "DarkTabBarBadgeColor") : UIColor(named: "LightTabBarBadgeColor")
        tabBarBadgeText = darkMode == true ? UIColor(named: "DarkTabBarBadgeTextColor") : UIColor(named: "LightTabBarBadgeTextColor")
        messageInputNode = darkMode == true ? UIColor(named: "DarkMessageInputNodeColor") : UIColor(named: "LightMessageInputNodeColor")
        actionViewElements = darkMode == true ? UIColor(named: "DarkActionViewElementsColor") : UIColor(named: "LightActionViewElementsColor")
    }
    
    func getMessTextColor(_ isOutput: Bool) -> MessageColor {
        var msgColor = MessageColor()
        msgColor.messageText = isOutput == true ? outgoingText : incomingText
        msgColor.messageSubText = isOutput == true ? outgoingSubText : incomingSubText
        msgColor.messageIcon = isOutput == true ? outgoingIcon : incomingIcon
        msgColor.messageAditionalIcon = isOutput == true ? outgoingAdditionalIcon : incomingAdditionalIcon
        
        return msgColor
    }
    
}
