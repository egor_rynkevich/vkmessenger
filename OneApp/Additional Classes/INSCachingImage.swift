//
//  INSCachingImage.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 01.08.2018.
//

import Foundation
import INSPhotoGallery
import RealmSwift

class INSCachingImage: INSPhoto {
    
    var imageURL: URL?
    var image_id: Int64?
    
    convenience init(imageURL: URL?, image_id: Int64?) {
        self.init(imageURL: imageURL, thumbnailImage: nil)
        self.imageURL = imageURL
        self.image_id = image_id
    }
    
    override func loadImageWithCompletionHandler(_ completion: @escaping (UIImage?, Error?) -> ()) {
        if let url = imageURL {
            
            let predicate = NSPredicate(format: "cachedDataID = %@ AND type = %@ AND tag = %@ AND url = %@",argumentArray:[ image_id!, "image", "ava", url.absoluteString])
            let realm = try! Realm()
            let image = realm.objects(VKCachedData.self).filter(predicate).first
            
            if image != nil{
                completion(UIImage(data: (image?.data)! as Data), nil)
                return
            }
            else {
                URLSession.shared.dataTask(with: url as URL) { data, response, error in
                    if error != nil {
                        completion(nil, error as NSError?)
                    } else {
                        let realmAsync = try! Realm()
                        let cacheImg = VKCachedData(value: [self.image_id!, "image", "ava", (self.imageURL?.absoluteString)!, data!, Int(Date().timeIntervalSince1970)])
                        try! realmAsync.write{
                            realmAsync.add(cacheImg, update: true)
                        }
                        completion(UIImage(data: (data)! as Data), nil)
                    }
                    
                    }.resume()
            }
        } else {
            completion(nil, NSError(domain: "PhotoDomain", code: -1, userInfo: [ NSLocalizedDescriptionKey: "Couldn't load image"]))
        }
    }
}
