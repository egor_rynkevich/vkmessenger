//
//  GradientNode.swift
//  OneApp
//
//  Created by Egor Rynkevich on 3/30/19.
//
import AsyncDisplayKit

class GradientNode: ASDisplayNode {
    class func draw(_ bounds: CGRect, withParameters parameters: NSObject?, isCancelled isCancelledBlock: asdisplaynode_iscancelled_block_t, isRasterizing: Bool) {
        let myContext = UIGraphicsGetCurrentContext()
        myContext?.saveGState()
        myContext?.clip(to: bounds)
        
        let componentCount: Int = 2
        
        let zero: CGFloat = 0.0
        let one: CGFloat = 1.0
        let locations = [zero, one]
        let components = [zero, zero, zero, one, zero, zero, zero, zero]
        
        let myColorSpace = CGColorSpaceCreateDeviceRGB()
        let myGradient = CGGradient(colorSpace: myColorSpace, colorComponents: components, locations: locations, count: componentCount)! //as? CGGradient
        
        let myStartPoint = CGPoint(x: bounds.midX, y: bounds.maxY)
        let myEndPoint = CGPoint(x: bounds.midX, y: bounds.midY * 1 / 4)
        
        
        myContext?.drawLinearGradient(myGradient, start: myStartPoint, end: myEndPoint, options: .drawsAfterEndLocation)
        
        myContext?.restoreGState()
    }
}
