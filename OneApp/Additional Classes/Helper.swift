//
//  Helper.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 15.07.2018.
//

import Foundation
import DifferenceKit
import AsyncDisplayKit

class Helper {
    
    class func getLastSeenStr(lastSeen: TimeInterval) -> String {
        var str : String = "last seen "
        var isYstr: Bool = false
        var isMin: Bool = false
        let secondsInHr: TimeInterval = 60 * 60
        let secondsInDay: TimeInterval = 60 * 60 * 24
        var lastSeenTime: TimeInterval = lastSeen
        let dateFormatter = DateFormatter()
        let elapsedTimeInSec = Date().timeIntervalSince(Date(timeIntervalSince1970: lastSeenTime))
        
        let secondsFromDayBegin = Date().timeIntervalSince(Calendar.current.startOfDay(for: Date()))
        
        if elapsedTimeInSec > (secondsFromDayBegin + secondsInDay) {
            dateFormatter.dateFormat = "dd.MMMM.yy"
        } else if elapsedTimeInSec > secondsFromDayBegin {
            dateFormatter.dateFormat = "h:mm a"
            isYstr = true
        } else if elapsedTimeInSec > secondsInHr {
            dateFormatter.dateFormat = "h:mm a"
        } else {
            dateFormatter.dateFormat = "m"
            isMin = true
            lastSeenTime = elapsedTimeInSec
        }
        
        let timeStr = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(lastSeenTime)))
        
        if isYstr == true {
            str.append("yesterday at \(timeStr)")
        } else if isMin == true {
            str.append("\(timeStr) minutes ago")
        } else {
            str.append(timeStr)
        }
        
        return str
    }
    
    class func getDateForMsgSectionTitle(date: Int64) -> String {
        let mesDate = Date(timeIntervalSince1970: TimeInterval(date))
        
        if date < 0 { return "Unread message" }
        
        if Calendar.current.compare(mesDate, to: Date(), toGranularity: .day).rawValue == 0
            && Calendar.current.compare(mesDate, to: Date(), toGranularity: .month).rawValue == 0
            && Calendar.current.compare(mesDate, to: Date(), toGranularity: .year).rawValue == 0 {
            return "Today"
        }
        
        if Calendar.current.compare(mesDate, to: Date().yesterday, toGranularity: .day).rawValue == 0
            && Calendar.current.compare(mesDate, to: Date().yesterday, toGranularity: .month).rawValue == 0
            && Calendar.current.compare(mesDate, to: Date().yesterday, toGranularity: .year).rawValue == 0 {
            return "Yesterday"
        }
        
        let dateFormatter = DateFormatter()
        
        if Calendar.current.compare(mesDate, to: Date(), toGranularity: .year).rawValue == 0 { dateFormatter.dateFormat = "EEEE, MMMM d" }
        else { dateFormatter.dateFormat = "EEEE, MMMM d, yyyy" }

        return dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(date)))
    }
    
    class func getDateForMsgKey(date: Int64) -> Int64 {
        //let mesDate = Date(timeIntervalSince1970: TimeInterval(date))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, MMMM d, yyyy"
        
        return Int64(dateFormatter.date(from: dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(date))))!.timeIntervalSince1970)
    }
    
    class func haveBDateToday(bdate: String) -> Bool {
        if bdate.count < 1 { return false }
        let dateFormatter = DateFormatter()
        let dotCount = bdate.reduce(into: 0) { (count, char) in if char == "." { count += 1 } }
        
        dateFormatter.dateFormat = dotCount == 2 ? "dd.MM.yyyy" : "dd.MM"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")!
        let s = dateFormatter.date(from: bdate)
        
        return Calendar.current.compare(s!, to: Date(), toGranularity: .day).rawValue == 0
            && Calendar.current.compare(s!, to: Date(), toGranularity: .month).rawValue == 0
    }

    class func vkBDtoStr(bdate: String) -> String {
        let dotCount = bdate.reduce(into: 0) { (count, char) in if char == "." { count += 1 } }
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = dotCount == 2 ? "dd.MM.yyyy" : "dd.MM"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")!
        let s = dateFormatter.date(from: bdate)

        dateFormatter.dateFormat = dotCount == 2 ? "d MMM yyyy" : "d MMM"
        let perf = dateFormatter.string(from: s!)
        
        return perf
    }

    class func getBtnWithBadge(img: UIImage, count: Int, isLeft: Bool) -> UIButton {
        var size: CGFloat = 13
        var width: CGFloat = 20
        var x: CGFloat = 25
        if count > 9 && count < 100 { size = 11 }
        else if count >= 100 {
            size = 9
            width = 30
            x = 15
        }
        
        if isLeft == true { x = -13 }
        
        let label = UILabel(frame: CGRect(x: x, y: -10, width: width, height: 20))
        if count > 0 {
            label.layer.borderColor = UIColor.clear.cgColor
            label.layer.borderWidth = 0
            label.layer.cornerRadius = label.bounds.size.height / 2
            label.textAlignment = .center
            label.layer.masksToBounds = true
            
            label.font = UIFont.systemFont(ofSize: size)
            label.textColor = .white
            label.backgroundColor = mainColors.deleteRow
            label.text = String(count)
            label.isUserInteractionEnabled = false
        }
        
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 22))
        btn.setImage(img, for: .normal)
        btn.addSubview(label)
        
        return btn
    }

    class func getDurationString(_ duration: Int64) -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = duration > 59 ? .positional : .abbreviated
        
        let durationStr = formatter.string(from: TimeInterval(duration))!
        
        return durationStr
    }
    
    class func textToImage(drawText text: String, height: Int, width: Int) -> UIImage {
        let textColor = UIColor.white
        let textFont = UIFont(name: "Helvetica Bold", size: 20)!
        
        let scale = UIScreen.main.scale
        
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: width, height: height)
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.colors = [mainColors.deleteRow?.cgColor as Any, mainColors.writeRow?.cgColor as Any]
        
        let image = UIImage().image(fromLayer: gradient)
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        let textFontAttributes = [
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor,
            ] as [NSAttributedString.Key : Any]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        
        let textNode = ASTextNode()
        
        textNode.attributedText = NSAttributedString(string: text, attributes: textFontAttributes)
        let textSize = textNode.calculateSizeThatFits(CGSize(width: width, height: height))
        
        let x = (image.size.width / 2) - (textSize.width / 2)
        let y = (image.size.height / 2) - (textSize.height / 2)
        
        let rect = CGRect(origin: CGPoint(x: x, y: y), size: image.size)
        text.draw(in: rect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

extension Date {
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: Date())!
    }
}

extension UIImage {
    
    func resize(targetSize: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size:targetSize).image { _ in
            self.draw(in: CGRect(origin: .zero, size: targetSize))
        }
    }
    
    func image(fromLayer layer: CALayer) -> UIImage {
        UIGraphicsBeginImageContext(layer.frame.size)
        
        layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return outputImage!
    }
    
}

extension UISearchBar {
    func changeColorMode(_ color: UIColor?) {
        self.barTintColor = color
        for subView in self.subviews {
            subView.backgroundColor = color
            for subViewOne in subView.subviews {
                subViewOne.backgroundColor = color
                if let textField = subViewOne as? UITextField {
                    let textFieldInsideUISearchBarLabel = textField.value(forKey: "placeholderLabel") as? UILabel
                    textFieldInsideUISearchBarLabel?.textColor = mainColors.textGray
                    textField.textColor = mainColors.text
                }
            }
        }
    }
}

extension UIView {
    public func pin(to view: UIView) {
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: view.leadingAnchor),
            trailingAnchor.constraint(equalTo: view.trailingAnchor),
            topAnchor.constraint(equalTo: view.topAnchor),
            bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
}

extension UIColor {
    static var lblue = UIColor(red: 200 / 255, green: 200 / 255 , blue: 255 / 255, alpha: 1.0)
    
    func image(_ size: CGSize = CGSize(width: 1, height: 1)) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { rendererContext in
            self.setFill()
            rendererContext.fill(CGRect(origin: .zero, size: size))
        }
    }
}

extension String {
    func width() -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0)]
        let size = (self as NSString).size(withAttributes: fontAttributes)
        return size.width
    }
}

open class ToastView: UILabel {
    
    var overlayView = UIView()
    var backView = UIView()
    var lbl = UILabel()
    
    class var shared: ToastView {
        struct Static {
            static let instance: ToastView = ToastView()
        }
        return Static.instance
    }
    
    func setup(_ view: UIView,txt_msg:String, isError: Bool = false)
    {
        let white = UIColor ( red: 1/255, green: 0/255, blue:0/255, alpha: 0.0 )
        
        backView.frame = CGRect(x: 0, y: view.frame.height - 200, width: view.frame.width , height: 200)
        backView.center = view.center
        backView.backgroundColor = white
        view.addSubview(backView)
        
        overlayView.frame = CGRect(x: 0, y: 0, width: view.frame.width - 60  , height: isError ? 200 : 50)
        overlayView.center = CGPoint(x: view.bounds.width / 2, y: view.bounds.height - 100)
        overlayView.backgroundColor = UIColor.black
        overlayView.clipsToBounds = true
        overlayView.layer.cornerRadius = 10
        overlayView.alpha = 0
        
        lbl.frame = CGRect(x: 0, y: 0, width: overlayView.frame.width, height: isError ? 200 : 50)
        lbl.numberOfLines = 0
        lbl.textColor = UIColor.white
        lbl.center = overlayView.center
        if isError {
            lbl.font = UIFont.systemFont(ofSize: 13)
        }
        lbl.text = txt_msg
        lbl.textAlignment = .center
        lbl.center = CGPoint(x: overlayView.bounds.width / 2, y: overlayView.bounds.height / 2)
        overlayView.addSubview(lbl)
        
        view.addSubview(overlayView)
    }
    
    open func short(_ view: UIView,txt_msg:String) {
        self.setup(view,txt_msg: txt_msg)
        //Animation
        UIView.animate(withDuration: 1, animations: {
            self.overlayView.alpha = 1
        }) { (true) in
            UIView.animate(withDuration: 1, animations: {
                self.overlayView.alpha = 0
            }) { (true) in
                UIView.animate(withDuration: 1, animations: {
                    DispatchQueue.main.async(execute: {
                        self.overlayView.alpha = 0
                        self.lbl.removeFromSuperview()
                        self.overlayView.removeFromSuperview()
                        self.backView.removeFromSuperview()
                    })
                })
            }
        }
    }
    
    open func long(_ view: UIView,txt_msg:String, isError: Bool = false) {
        self.setup(view,txt_msg: txt_msg, isError: isError)
        //Animation
        UIView.animate(withDuration: 2, animations: {
            self.overlayView.alpha = 1
        }) { (true) in
            UIView.animate(withDuration: 2, animations: {
                self.overlayView.alpha = 0
            }) { (true) in
                UIView.animate(withDuration: 2, animations: {
                    DispatchQueue.main.async(execute: {
                        self.overlayView.alpha = 0
                        self.lbl.removeFromSuperview()
                        self.overlayView.removeFromSuperview()
                        self.backView.removeFromSuperview()
                    })
                })
            }
        }
    }
}

extension PHAsset {
    
    var originalFilename: String? {
        
        var fname:String?
        
        if #available(iOS 9.0, *) {
            let resources = PHAssetResource.assetResources(for: self)
            if let resource = resources.first {
                fname = resource.originalFilename
            }
        }
        
        if fname == nil {
            // this is an undocumented workaround that works as of iOS 9.1
            fname = self.value(forKey: "filename") as? String
        }
        
        return fname
    }
}

extension ASTableNode {
    /// Applies multiple animated updates in stages using `StagedChangeset`.
    ///
    /// - Note: There are combination of changes that crash when applied simultaneously in `performBatchUpdates`.
    ///         Assumes that `StagedChangeset` has a minimum staged changesets to avoid it.
    ///         The data of the dataSource needs to be updated before `performBatchUpdates` in every stages.
    ///
    /// - Parameters:
    ///   - stagedChangeset: A staged set of changes.
    ///   - interrupt: A closure that takes an changeset as its argument and returns `true` if the animated
    ///                updates should be stopped and performed reloadData. Default is nil.
    ///   - setData: A closure that takes the collection as a parameter.
    ///              The collection should be set to dataSource of UICollectionView.
    func reload<C>(
        using stagedChangeset: StagedChangeset<C>,
        interrupt: ((Changeset<C>) -> Bool)? = nil,
        setData: @escaping (C) -> Void
        ) {
        
        func reload() {
            if case .none = view.window, let data = stagedChangeset.last?.data {
                setData(data)
                return reloadData()
            }
            
            for changeset in stagedChangeset {
                if let interrupt = interrupt, interrupt(changeset), let data = stagedChangeset.last?.data {
                    setData(data)
                    return reloadData()
                }
                
                setData(changeset.data)
                
                performBatchUpdates({
                    if !changeset.sectionDeleted.isEmpty {
                        deleteSections(IndexSet(changeset.sectionDeleted), with: .left)
                    }
                    
                    if !changeset.sectionInserted.isEmpty {
                        insertSections(IndexSet(changeset.sectionInserted), with: .none)
                    }
                    
                    if !changeset.sectionUpdated.isEmpty {
                        reloadSections(IndexSet(changeset.sectionUpdated), with: .none)
                    }
                    
                    for (source, target) in changeset.sectionMoved {
                        moveSection(source, toSection: target)
                    }
                    
                    if !changeset.elementDeleted.isEmpty {
                        deleteRows(at: changeset.elementDeleted.map { IndexPath(item: $0.element, section: $0.section) }, with: .left)
                    }
                    
                    if !changeset.elementInserted.isEmpty {
                        insertRows(at: changeset.elementInserted.map { IndexPath(item: $0.element, section: $0.section) }, with: .none)
                    }
                    
                    if !changeset.elementUpdated.isEmpty {
                        reloadRows(at: changeset.elementUpdated.map { IndexPath(item: $0.element, section: $0.section) }, with: .none)
                    }
                    
                    for (source, target) in changeset.elementMoved {
                        moveRow(at: IndexPath(item: source.element, section: source.section), to: IndexPath(item: target.element, section: target.section))
                    }
                })
            }
        }
        
        isNodeLoaded ? reload() : onDidLoad { _ in reload() }
    }
}

extension ASCollectionNode {
    /// Applies multiple animated updates in stages using `StagedChangeset`.
    ///
    /// - Note: There are combination of changes that crash when applied simultaneously in `performBatchUpdates`.
    ///         Assumes that `StagedChangeset` has a minimum staged changesets to avoid it.
    ///         The data of the dataSource needs to be updated before `performBatchUpdates` in every stages.
    ///
    /// - Parameters:
    ///   - stagedChangeset: A staged set of changes.
    ///   - interrupt: A closure that takes an changeset as its argument and returns `true` if the animated
    ///                updates should be stopped and performed reloadData. Default is nil.
    ///   - setData: A closure that takes the collection as a parameter.
    ///              The collection should be set to dataSource of UICollectionView.
    func reload<C>(
        using stagedChangeset: StagedChangeset<C>,
        interrupt: ((Changeset<C>) -> Bool)? = nil,
        setData: @escaping (C) -> Void
        ) {
        
        func reload() {
            if case .none = view.window, let data = stagedChangeset.last?.data {
                setData(data)
                return reloadData()
            }
            
            for changeset in stagedChangeset {
                if let interrupt = interrupt, interrupt(changeset), let data = stagedChangeset.last?.data {
                    setData(data)
                    return reloadData()
                }
                
                setData(changeset.data)
                
                performBatchUpdates({
                    if !changeset.sectionDeleted.isEmpty {
                        deleteSections(IndexSet(changeset.sectionDeleted))
                    }
                    
                    if !changeset.sectionInserted.isEmpty {
                        insertSections(IndexSet(changeset.sectionInserted))
                    }
                    
                    if !changeset.sectionUpdated.isEmpty {
                        reloadSections(IndexSet(changeset.sectionUpdated))
                    }
                    
                    for (source, target) in changeset.sectionMoved {
                        moveSection(source, toSection: target)
                    }
                    
                    if !changeset.elementDeleted.isEmpty {
                        deleteItems(at: changeset.elementDeleted.map { IndexPath(item: $0.element, section: $0.section) })
                    }
                    
                    if !changeset.elementInserted.isEmpty {
                        insertItems(at: changeset.elementInserted.map { IndexPath(item: $0.element, section: $0.section) })
                    }
                    
                    if !changeset.elementUpdated.isEmpty {
                        reloadItems(at: changeset.elementUpdated.map { IndexPath(item: $0.element, section: $0.section) })
                    }
                    
                    for (source, target) in changeset.elementMoved {
                        moveItem(at: IndexPath(item: source.element, section: source.section), to: IndexPath(item: target.element, section: target.section))
                    }
                })
            }
        }
        
        isNodeLoaded ? reload() : onDidLoad { _ in reload() }
    }
}

extension UIView {
    
    var recursiveSubviews: [UIView] {
        var subviews = self.subviews.compactMap({$0})
        subviews.forEach { subviews.append(contentsOf: $0.recursiveSubviews) }
        return subviews
    }
}

extension UIAlertController {
    private struct AssociatedKeys {
        static var blurStyleKey = "UIAlertController.blurStyleKey"
    }
    
    public var blurStyle: UIBlurEffect.Style {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.blurStyleKey) as? UIBlurEffect.Style ?? .extraLight
        } set (style) {
            objc_setAssociatedObject(self, &AssociatedKeys.blurStyleKey, style, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            
            view.setNeedsLayout()
            view.layoutIfNeeded()
        }
    }
    
    private var cancelActionView: UIView? {
        return view.recursiveSubviews.compactMap({$0 as? UILabel}).first(where: {$0.text == "Cancel"})?.superview?.superview
    }
    
    public convenience init(title: String?, message: String?, preferredStyle: UIAlertController.Style, blurStyle: UIBlurEffect.Style) {
        self.init(title: title, message: message, preferredStyle: preferredStyle)
        self.blurStyle = blurStyle
    }
    
    open override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        cancelActionView?.backgroundColor = blurStyle == .dark ? mainColors.mainTableView : nil
        let subview = (view.subviews.first?.subviews.first?.subviews.first!)! as UIView
        subview.backgroundColor = blurStyle == .dark ? mainColors.mainTableView : nil
    }
}
