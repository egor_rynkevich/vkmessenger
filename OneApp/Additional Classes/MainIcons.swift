//
//  MainIcon.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 06.08.2018.
//

import Foundation

class MainIcons {
    var deactivatedNSData: NSData?
    var userTyping_1: UIImage?
    var userTyping_2: UIImage?
    var userTyping_3: UIImage?
    var userTyping_4: UIImage?
    var userTyping_5: UIImage?
    var userTyping_6: UIImage?
    
    var userPhoneOnline: UIImage?
    var userOnline: UIImage?
    var userOnline_s10: UIImage?
    var cake: UIImage?
    var down: UIImage?
    var close_s20: UIImage?
    var readMessage: UIImage?
    var readMessage_s25: UIImage?
    var readMessage_s12: UIImage?
    var unreadMessage_s12: UIImage?
    var unreadMessage: UIImage?
    var soundless: UIImage?
    var soundless_s25: UIImage?
    var sound_s25: UIImage?
    
    var newMessage_s20: UIImage?
    var newMessage_s25: UIImage?
    var newMessage_s12: UIImage?
    var addUser_s20: UIImage?
    var addUser_s25: UIImage?
    var trash_s20: UIImage?
    var trash_s25: UIImage?
    var call_s25: UIImage?
    var call_s30: UIImage?
    var userProfile_s50: UIImage?
    var chat_s50: UIImage?
    var createChat_s21: UIImage?
    var createChat_s30: UIImage?
    var search_s21: UIImage?
    var hide_s25: UIImage?
    var cancel_s21: UIImage?
    var addNewPeople_s22: UIImage?
    var back_s21: UIImage?
    var contact_s21: UIImage?
    
    var addAttachments_s24: UIImage?
    var sticker_s24: UIImage?
    var send_s24: UIImage?
    var microphone_s24: UIImage?
    
    
    var recommend_s30: UIImage?
    var userWithInputArrow_s30: UIImage?
    var userWithOutputArrow_s30: UIImage?
    var earth_s30: UIImage?
    
    var checkInCircle_s25: UIImage?
    var tick_s20: UIImage?
    var filter_s20: UIImage?
    
    var errorSend: UIImage?
    var clock_12: UIImage?
    var file_s40: UIImage?
    var important: UIImage?
    var important_s16: UIImage?
    var playAudio_s40: UIImage?
    var pauseAudio_s40: UIImage?
    var play: UIImage?
    var play_s10: UIImage?
    var pause_s10: UIImage?
    var wall_s40: UIImage?
    var gift: UIImage?
    var note: UIImage?
    var flash: UIImage?
    var arrowPage: UIImage?
    var mapPlaceholder: UIImage?
    var messageIcon_s21: UIImage?
    var select_s25: UIImage?
    var unselect_s25: UIImage?
    var forward_s23: UIImage?
    var reply_s23: UIImage?
    var copy_s23: UIImage?
    var threeDots_s23: UIImage?
    var trash_s23: UIImage?
    var newMessage_s23: UIImage?
    
    init() {
        deactivatedNSData = #imageLiteral(resourceName: "deactivatedImage").pngData()! as NSData
        
        let width = 21.0
        let height = 7.0
        
        userTyping_1 = UIImage(named: "userTypingImage_1")?.resize(targetSize: CGSize(width: width, height: height)).withRenderingMode(.alwaysTemplate)
        userTyping_2 = UIImage(named: "userTypingImage_2")?.resize(targetSize: CGSize(width: width, height: height)).withRenderingMode(.alwaysTemplate)
        userTyping_3 = UIImage(named: "userTypingImage_3")?.resize(targetSize: CGSize(width: width, height: height)).withRenderingMode(.alwaysTemplate)
        userTyping_4 = UIImage(named: "userTypingImage_4")?.resize(targetSize: CGSize(width: width, height: height)).withRenderingMode(.alwaysTemplate)
        userTyping_5 = UIImage(named: "userTypingImage_5")?.resize(targetSize: CGSize(width: width, height: height)).withRenderingMode(.alwaysTemplate)
        userTyping_6 = UIImage(named: "userTypingImage_6")?.resize(targetSize: CGSize(width: width, height: height)).withRenderingMode(.alwaysTemplate)
        
        userPhoneOnline = UIImage(named: "userPhoneOnlineIcon")?.withRenderingMode(.alwaysTemplate)
        userOnline = UIImage(named: "userOnlineIcon")?.withRenderingMode(.alwaysTemplate)
        userOnline_s10 = UIImage(named: "userOnlineIcon")?.resize(targetSize: CGSize(width: 10.0, height: 10.0)).withRenderingMode(.alwaysTemplate)
        cake = UIImage(named: "cakeIcon")?.withRenderingMode(.alwaysTemplate)
        down = UIImage(named: "downIcon")?.withRenderingMode(.alwaysTemplate)
        close_s20 = UIImage(named: "closeIcon")?.resize(targetSize: CGSize(width: 20.0, height: 20.0)).withRenderingMode(.alwaysTemplate)
        readMessage = UIImage(named: "twoDotIcon")?.withRenderingMode(.alwaysTemplate) //UIImage(named: "readMessageIcon")?.withRenderingMode(.alwaysTemplate)
        readMessage_s12 = UIImage(named: "twoDotIcon")?.resize(targetSize: CGSize(width: 12, height: 12)).withRenderingMode(.alwaysTemplate) //UIImage(named: "readMessageIcon")?.resize(targetSize: CGSize(width: 12, height: 12)).withRenderingMode(.alwaysTemplate)
        readMessage_s25 = UIImage(named: "twoDotIcon")?.resize(targetSize: CGSize(width: 25.0, height: 25.0)).withRenderingMode(.alwaysTemplate) //UIImage(named: "readMessageIcon")?.resize(targetSize: CGSize(width: 25.0, height: 25.0)).withRenderingMode(.alwaysTemplate)
        unreadMessage = UIImage(named: "oneDotIcon")?.withRenderingMode(.alwaysTemplate) //UIImage(named: "unreadMessageIcon")?.withRenderingMode(.alwaysTemplate)
        unreadMessage_s12 = UIImage(named: "oneDotIcon")?.resize(targetSize: CGSize(width: 12, height: 12)).withRenderingMode(.alwaysTemplate) //UIImage(named: "unreadMessageIcon")?.resize(targetSize: CGSize(width: 12, height: 12)).withRenderingMode(.alwaysTemplate)
        soundless = UIImage(named: "soundlessIcon")?.withRenderingMode(.alwaysTemplate)
        soundless_s25 = UIImage(named: "soundlessIcon")?.resize(targetSize: CGSize(width: 25.0, height: 25.0)).withRenderingMode(.alwaysTemplate)
        sound_s25 = UIImage(named: "soundIcon")?.resize(targetSize: CGSize(width: 25.0, height: 25.0)).withRenderingMode(.alwaysTemplate)
        
        newMessage_s12 = UIImage(named: "newMessageIcon")?.resize(targetSize: CGSize(width: 12, height: 12)).withRenderingMode(.alwaysTemplate)
        newMessage_s20 = UIImage(named: "newMessageIcon")?.resize(targetSize: CGSize(width: 20.0, height: 20.0)).withRenderingMode(.alwaysTemplate)
        newMessage_s25 = UIImage(named: "newMessageIcon")?.resize(targetSize: CGSize(width: 25.0, height: 25.0)).withRenderingMode(.alwaysTemplate)
        addUser_s20 = UIImage(named: "addUserIcon")?.resize(targetSize: CGSize(width: 20.0, height: 20.0)).withRenderingMode(.alwaysTemplate)
        addUser_s25 = UIImage(named: "addUserIcon")?.resize(targetSize: CGSize(width: 25.0, height: 25.0)).withRenderingMode(.alwaysTemplate)
        trash_s20 = UIImage(named: "trashIcon")?.resize(targetSize: CGSize(width: 20.0, height: 20.0)).withRenderingMode(.alwaysTemplate)
        trash_s25 = UIImage(named: "trashIcon")?.resize(targetSize: CGSize(width: 25.0, height: 25.0)).withRenderingMode(.alwaysTemplate)
        call_s25 = UIImage(named: "callIcon")?.resize(targetSize: CGSize(width: 25.0, height: 25.0)).withRenderingMode(.alwaysTemplate)
        call_s30 = UIImage(named: "callIcon")?.resize(targetSize: CGSize(width: 30.0, height: 30.0)).withRenderingMode(.alwaysTemplate)
        userProfile_s50 = UIImage(named: "userProfileIcon")?.resize(targetSize: CGSize(width: 50.0, height: 50.0)).withRenderingMode(.alwaysTemplate)
        chat_s50 = UIImage(named: "chatIcon")?.resize(targetSize: CGSize(width: 50.0, height: 50.0)).withRenderingMode(.alwaysTemplate)
        createChat_s21 = UIImage(named: "createChatIcon")?.resize(targetSize: CGSize(width: 21, height: 21)).withRenderingMode(.alwaysTemplate)
        createChat_s30 = UIImage(named: "createChatIcon")?.resize(targetSize: CGSize(width: 30, height: 30)).withRenderingMode(.alwaysTemplate)
        search_s21 = UIImage(named: "searchIcon2")?.resize(targetSize: CGSize(width: 21, height: 21)).withRenderingMode(.alwaysTemplate)
        hide_s25 = UIImage(named: "hideIcon")?.resize(targetSize: CGSize(width: 25, height: 25)).withRenderingMode(.alwaysTemplate)
        cancel_s21 = UIImage(named: "cancelIcon")?.resize(targetSize: CGSize(width: 21, height: 21)).withRenderingMode(.alwaysTemplate)
        addNewPeople_s22 = UIImage(named: "addNewPeopleIcon")?.resize(targetSize: CGSize(width: 22, height: 22)).withRenderingMode(.alwaysTemplate)
        back_s21 = UIImage(named: "backIcon")?.resize(targetSize: CGSize(width: 21, height: 21)).withRenderingMode(.alwaysTemplate)
        contact_s21 = UIImage(named: "contactIcon")?.resize(targetSize: CGSize(width: 21, height: 21)).withRenderingMode(.alwaysTemplate)
        
        addAttachments_s24 = UIImage(named: "addAttachmentsIcon")?.resize(targetSize: CGSize(width: 24, height: 24)).withRenderingMode(.alwaysTemplate)
        sticker_s24 = UIImage(named: "stickerIcon")?.resize(targetSize: CGSize(width: 24, height: 24)).withRenderingMode(.alwaysTemplate)
        send_s24 = UIImage(named: "sendIcon")?.resize(targetSize: CGSize(width: 24, height: 24)).withRenderingMode(.alwaysTemplate)
        microphone_s24 = UIImage(named: "microphoneIcon")?.resize(targetSize: CGSize(width: 24, height: 24)).withRenderingMode(.alwaysTemplate)
        
        
        recommend_s30 = UIImage(named: "recommendIcon")?.resize(targetSize: CGSize(width: 30, height: 30)).withRenderingMode(.alwaysTemplate)
        userWithInputArrow_s30 = UIImage(named: "userWithInputArrowIcon")?.resize(targetSize: CGSize(width: 30, height: 30)).withRenderingMode(.alwaysTemplate)
        userWithOutputArrow_s30 = UIImage(named: "userWithOutputArrowIcon")?.resize(targetSize: CGSize(width: 30, height: 30)).withRenderingMode(.alwaysTemplate)
        earth_s30 = UIImage(named: "earthIcon")?.resize(targetSize: CGSize(width: 30, height: 30)).withRenderingMode(.alwaysTemplate)
        
        tick_s20 = UIImage(named: "tickIcon")?.resize(targetSize: CGSize(width: 20, height: 20)).withRenderingMode(.alwaysTemplate)
        checkInCircle_s25 = UIImage(named: "checkInCircleIcon")?.resize(targetSize: CGSize(width: 25, height: 25)).withRenderingMode(.alwaysTemplate)
        filter_s20 = UIImage(named: "filterIcon")?.resize(targetSize: CGSize(width: 20, height: 20)).withRenderingMode(.alwaysTemplate)
        errorSend = UIImage(named: "errorSendIcon")?.withRenderingMode(.alwaysTemplate)
        clock_12 = UIImage(named: "clockIcon")?.resize(targetSize: CGSize(width: 12, height: 12)).withRenderingMode(.alwaysTemplate)
        file_s40 = UIImage(named: "fileIcon")?.resize(targetSize: CGSize(width: 40, height: 40)).withRenderingMode(.alwaysTemplate)
        important = UIImage(named: "importantIcon")?.withRenderingMode(.alwaysTemplate)
        important_s16 = UIImage(named: "importantIcon")?.resize(targetSize: CGSize(width: 16, height: 16)).withRenderingMode(.alwaysTemplate)
        playAudio_s40 = UIImage(named: "playAudioIcon")?.resize(targetSize: CGSize(width: 34, height: 34))//.withRenderingMode(.alwaysTemplate)
        pauseAudio_s40 = UIImage(named: "pauseAudioIcon")?.resize(targetSize: CGSize(width: 40, height: 40)).withRenderingMode(.alwaysTemplate)
        play = UIImage(named: "playIcon")?.withRenderingMode(.alwaysTemplate)
        play_s10 = UIImage(named: "playIcon")?.resize(targetSize: CGSize(width: 10, height: 10)).withRenderingMode(.alwaysTemplate)
        pause_s10 = UIImage(named: "pauseIcon")?.resize(targetSize: CGSize(width: 10, height: 10)).withRenderingMode(.alwaysTemplate)
        wall_s40 = UIImage(named: "wallIcon1")?.resize(targetSize: CGSize(width: 36, height: 36)).withRenderingMode(.alwaysTemplate)
        gift = UIImage(named: "giftIcon")?.withRenderingMode(.alwaysTemplate)
        note = UIImage(named: "noteIcon")?.withRenderingMode(.alwaysTemplate)
        flash = UIImage(named: "flashIcon")?.withRenderingMode(.alwaysTemplate)
        arrowPage = UIImage(named: "arrowPageIcon")?.withRenderingMode(.alwaysTemplate)
        
        messageIcon_s21 = UIImage(named: "messageIcon")?.resize(targetSize: CGSize(width: 21, height: 21)).withRenderingMode(.alwaysTemplate)
        mapPlaceholder = UIImage(named: "mapPlaceholderIcon")?.resize(targetSize: CGSize(width: 40, height: 40)).withRenderingMode(.alwaysTemplate)
        select_s25 = UIImage(named: "selectIcon")?.resize(targetSize: CGSize(width: 25, height: 25)).withRenderingMode(.alwaysTemplate)
        unselect_s25 = UIImage(named: "unselectIcon")?.resize(targetSize: CGSize(width: 25, height: 25)).withRenderingMode(.alwaysTemplate)
        
        let size = 16
        trash_s23 = UIImage(named: "trashIcon")?.resize(targetSize: CGSize(width: size, height: size)).withRenderingMode(.alwaysTemplate)
        forward_s23 = UIImage(named: "forwardIcon")?.resize(targetSize: CGSize(width: size, height: size)).withRenderingMode(.alwaysTemplate)
        copy_s23 = UIImage(named: "copyIcon")?.resize(targetSize: CGSize(width: size, height: size)).withRenderingMode(.alwaysTemplate)
        reply_s23 = UIImage(named: "replyIcon")?.resize(targetSize: CGSize(width: size, height: size)).withRenderingMode(.alwaysTemplate)
        threeDots_s23 = UIImage(named: "threeDotsIcon")?.resize(targetSize: CGSize(width: size, height: size)).withRenderingMode(.alwaysTemplate)
        newMessage_s23 = UIImage(named: "newMessageIcon")?.resize(targetSize: CGSize(width: size, height: size)).withRenderingMode(.alwaysTemplate)
    }
}
