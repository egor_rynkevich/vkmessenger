//
//  PrettyCardNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 12.09.2018.
//

import UIKit
import AsyncDisplayKit

class PrettyCardNode: ASCellNode {
    fileprivate let titleTextNode: ASTextNode
    fileprivate let imageNode: ASNetworkImageNode
    fileprivate let oldPriceTextNode: ASTextNode
    fileprivate let newPriceTextNode: ASTextNode
    fileprivate let gradientNode: GradientNode

    weak var delegate: MessageCellNodeDelegate?
    var url: String = ""
    var msgStrID: String = ""
    
    init(cardSrc: VKCards?, contentWidth: Int, delegate: MessageCellNodeDelegate?) {
        titleTextNode = ASTextNode()
        imageNode = ASNetworkImageNode()
        oldPriceTextNode = ASTextNode()
        newPriceTextNode = ASTextNode()
        gradientNode = GradientNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true
        
        guard let card = cardSrc else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineBreakMode = .byWordWrapping
        
        titleTextNode.attributedText = NSAttributedString(string: card.title.count > 0 ? card.title : "...", attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.boldSystemFont(ofSize: 16), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: UIColor.white])
        titleTextNode.truncationMode = .byTruncatingTail
        titleTextNode.backgroundColor = UIColor.clear
        titleTextNode.placeholderColor = mainColors.lightGray
        titleTextNode.maximumNumberOfLines = 2
        titleTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(200))
        
        var attributes: [NSAttributedString.Key : Any] = [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: mainColors.text!]
        attributes[.strikethroughStyle] = NSNumber(integerLiteral: NSUnderlineStyle.single.rawValue)
        
        oldPriceTextNode.attributedText = NSAttributedString(string: card.price_old, attributes: attributes)
        oldPriceTextNode.backgroundColor = UIColor.clear
        oldPriceTextNode.placeholderColor = mainColors.lightGray
        oldPriceTextNode.maximumNumberOfLines = 1
        
        newPriceTextNode.attributedText = NSAttributedString(string: card.price, attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: mainColors.text!])
        newPriceTextNode.backgroundColor = UIColor.clear
        newPriceTextNode.placeholderColor = mainColors.lightGray
        newPriceTextNode.maximumNumberOfLines = 1
        
        gradientNode.isLayerBacked = true
        gradientNode.isOpaque = false
        gradientNode.cornerRadius = 10
        gradientNode.clipsToBounds = true
        
        
        imageNode.style.preferredSize = CGSize(width: 200, height: 200)
        imageNode.cornerRadius = 10
        
        imageNode.url = NSURL(string: card.image_url_mid == "" ? card.image_url_max : card.image_url_mid)! as URL
        url = card.link_url
        msgStrID = card.prntID
        
        imageNode.addTarget(self, action: #selector(openURL), forControlEvents: .touchUpInside)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let overlayImage = ASOverlayLayoutSpec(child: imageNode, overlay: gradientNode)
        
        let priceStack = ASStackLayoutSpec(direction: .horizontal, spacing: 2, justifyContent: .start, alignItems: .center, children: [oldPriceTextNode, newPriceTextNode])
        
        let titleStack = ASStackLayoutSpec(direction: .vertical, spacing: 2, justifyContent: .start, alignItems: .center, children: [titleTextNode, priceStack])
        
        let titleStackInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: CGFloat.infinity, left: 5, bottom: 5, right: 5), child: titleStack)
        
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: ASOverlayLayoutSpec(child: overlayImage, overlay: titleStackInsets))
    }
    
    @objc func openURL() {
        delegate?.openURL(url, msgStrID: msgStrID)
    }
}
