//
//  LinkNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 01.09.2018.
//

import UIKit
import AsyncDisplayKit

class LinkNode: ASCellNode {
    
    fileprivate let titleTextNode: ASTextNode
    fileprivate let captionTextNode: ASTextNode
    fileprivate let linkImageNode: ASNetworkImageNode
    var photo_id: Int64 = 0
    var owner_id: Int64 = 0
    var photo_url: String = ""
    var url: String = ""
    var msgStrID: String = ""
    weak var delegate: MessageCellNodeDelegate?
    
    init(sourceLink: VKLink?, contentWidth: Int, delegate: MessageCellNodeDelegate?, msgColor: MessageColor) {
        titleTextNode = ASTextNode()
        captionTextNode = ASTextNode()
        linkImageNode = ASNetworkImageNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true
        
        guard let link = sourceLink else { return }
        
        url = link.url
        msgStrID = link.prntID
        photo_id = link.photo?.id ?? 0
        owner_id = link.photo?.owner_id ?? 0
        photo_url = link.photo?.photoDocSizeMid?.url ?? ""
        
        let imgWidth = photo_url == "" ? 0 : 56
        
        titleTextNode.attributedText = NSAttributedString(string: link.title.count > 0 ? link.title : "...", attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: msgColor.messageText!])
        titleTextNode.truncationMode = .byTruncatingTail
        titleTextNode.backgroundColor = UIColor.clear
        titleTextNode.placeholderColor = mainColors.lightGray
        titleTextNode.maximumNumberOfLines = 2
        titleTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 10 - imgWidth))
        
        captionTextNode.attributedText = NSAttributedString(string: link.caption == "" ? link.description_vk : link.caption, attributes: [.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: msgColor.messageSubText!])
        captionTextNode.placeholderEnabled = true
        captionTextNode.backgroundColor = UIColor.clear
        captionTextNode.placeholderColor = mainColors.lightGray
        captionTextNode.maximumNumberOfLines = 1
        captionTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 10 - imgWidth))
        
        
        linkImageNode.style.preferredSize = CGSize(width: imgWidth, height: imgWidth)
        
        if photo_url != "" {
            let data = vkDataCache.getDataFromCachedData(sourceID: photo_id, ownerID: owner_id, type: "image", tag: "photo")
            
            if data == nil { linkImageNode.url = NSURL(string: photo_url)! as URL }
            else { linkImageNode.image = UIImage(data: data! as Data) }
            
            linkImageNode.imageModificationBlock = { [weak self] image in
                if vkDataCache.getDataFromCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "photo") == nil {
                    vkDataCache.saveDataInCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "photo", url: self?.photo_url ?? "", image: image)
                }
                return image
            }
            linkImageNode.cornerRadius = 10
        }
        
        
        titleTextNode.addTarget(self, action: #selector(openURL), forControlEvents: .touchUpInside)
        captionTextNode.addTarget(self, action: #selector(openURL), forControlEvents: .touchUpInside)
        linkImageNode.addTarget(self, action: #selector(openURL), forControlEvents: .touchUpInside)
        
        addSubnode(titleTextNode)
        addSubnode(captionTextNode)
        addSubnode(linkImageNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let linkVStackSpec = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .start, alignItems: .start, children: [titleTextNode, captionTextNode])
        
        let finalStack = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .start, alignItems: .center, children: [linkImageNode, linkVStackSpec])
        let finalInsetSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: finalStack)
        return finalInsetSpec
    }
    
    @objc func openURL() {
        delegate?.openURL(url, msgStrID: msgStrID)
    }
}
