//
//  ImageCollectionNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 22.09.2018.
//

import UIKit
import AsyncDisplayKit
import RealmSwift

class ImageCollectionNode: ASCellNode {
    
    fileprivate let imageNode: ASNetworkImageNode
    
    var photo_id: Int64 = 0
    var owner_id: Int64 = 0
    var photo_url: String = ""
    weak var delegate: MessageCellNodeDelegate?
    
    init(photo: VKPhoto, delegate: MessageCellNodeDelegate?) {
        imageNode = ASNetworkImageNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true

        photo_id = photo.id
        owner_id = photo.owner_id
        photo_url = photo.photoDocSizeMid?.url ?? photo.photoDocSizeMax?.url ?? photo.photoDocSizeMin?.url ?? ""
        
        let data = vkDataCache.getDataFromCachedData(sourceID: photo_id, ownerID: owner_id, type: "image", tag: "photo")
        
        if data == nil { imageNode.url = NSURL(string: photo_url)! as URL }
        else { imageNode.image = UIImage(data: data! as Data) }
        
        imageNode.imageModificationBlock = { [weak self] image in
            if vkDataCache.getDataFromCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "photo") == nil {
                vkDataCache.saveDataInCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "photo", url: self?.photo_url ?? "", image: image)
            }
            return image
        }
        
        imageNode.cornerRadius = 5
        
        addSubnode(imageNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let photoSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: imageNode)
        photoSpec.style.maxSize = constrainedSize.max
        photoSpec.style.minSize = constrainedSize.min
        return photoSpec
    }
    
//    func blurryImg(_ size: CGSize) {
//        if imageNode.image != nil {
//            let overlayColor = UIColor(white: 0.85, alpha: 0.15)
//            let blurredImage = Blurry.blurryImage(withOptions: BlurryOptions.pro, overlayColor: overlayColor, forImage: imageNode.image!, size: size, blurRadius: 5.0)
//            imageNode.image = blurredImage
//        }
//    }
}



//extension UIImage {
//
//    func imageByCroppingTransparentPixels() -> UIImage {
//        let rect = self.cropRectForImage(image: self)
//
//        return UIImage(cgImage: self.cgImage!.cropping(to: rect)!)
//        //return cropImage(toRect: rect)
//    }
//
//    func cropRectForImage(image:UIImage) -> CGRect {
//        let imageAsCGImage = image.cgImage
//        let context:CGContext? = self.createARGBBitmapContext(inImage: imageAsCGImage!)
//        if let context = context {
//            let width = imageAsCGImage!.width
//            let height = imageAsCGImage!.height
//            let rect:CGRect = CGRect(x: 0.0, y: 0.0, width: CGFloat(width), height: CGFloat(height))
//            context.draw(imageAsCGImage!, in: rect)
//
//            var lowX:Int = width
//            var lowY:Int = height
//            var highX:Int = 0
//            var highY:Int = 0
//            let data = context.data
//            if let data = data {
//                let dataType = data.assumingMemoryBound(to: UInt8.self)
//
//                    for y in 0..<height {
//                        for x in 0..<width {
//                            let pixelIndex:Int = (width * y + x) * 4 /* 4 for A, R, G, B */;
//                            if (dataType[pixelIndex] != 0) { //Alpha value is not zero; pixel is not transparent.
//                                if (x < lowX) { lowX = x };
//                                if (x > highX) { highX = x };
//                                if (y < lowY) { lowY = y};
//                                if (y > highY) { highY = y};
//                            }
//                        }
//                    }
//
//                free(data)
//            } else {
//                return CGRect.zero
//            }
//            return CGRect(x: CGFloat(lowX), y: CGFloat(lowY), width: CGFloat(highX-lowX), height: CGFloat(highY-lowY))
//
//        }
//        return CGRect.zero
//    }
//
//    func createARGBBitmapContext(inImage: CGImage) -> CGContext {
//        var bitmapByteCount = 0
//        var bitmapBytesPerRow = 0
//
//        //Get image width, height
//        let pixelsWide = inImage.width
//        let pixelsHigh = inImage.height
//
//        // Declare the number of bytes per row. Each pixel in the bitmap in this
//        // example is represented by 4 bytes; 8 bits each of red, green, blue, and
//        // alpha.
//        bitmapBytesPerRow = Int(pixelsWide) * 4
//        bitmapByteCount = bitmapBytesPerRow * Int(pixelsHigh)
//
//        // Use the generic RGB color space.
//        let colorSpace = CGColorSpaceCreateDeviceRGB()
//
//        // Allocate memory for image data. This is the destination in memory
//        // where any drawing to the bitmap context will be rendered.
//        let bitmapData = malloc(bitmapByteCount)
//        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue)
//
//        // Create the bitmap context. We want pre-multiplied ARGB, 8-bits
//        // per component. Regardless of what the source image format is
//        // (CMYK, Grayscale, and so on) it will be converted over to the format
//        // specified here by CGBitmapContextCreate.
//        let context = CGContext(data: bitmapData, width: pixelsWide, height: pixelsHigh, bitsPerComponent: 8, bytesPerRow: bitmapBytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
//
//        return context!
//    }
//}
