//
//  MarketNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 05.09.2018.
//

import UIKit
import AsyncDisplayKit

class MarketNode: ASCellNode {
    
    fileprivate let titleTextNode: ASTextNode
    fileprivate let captionTextNode: ASTextNode
    fileprivate let marketImageNode: ASNetworkImageNode
    var photo_id: Int64 = 0
    var photo_url: String = ""
    weak var delegate: MessageCellNodeDelegate?
    
    init(marketSource: VKMarket?, contentWidth: Int, delegate: MessageCellNodeDelegate?) {
        titleTextNode = ASTextNode()
        captionTextNode = ASTextNode()
        marketImageNode = ASNetworkImageNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true
        
        guard let market = marketSource else { return }
        
        photo_id = market.id
        photo_url = market.thumb_photo
        
        titleTextNode.attributedText = NSAttributedString(string: market.title.count > 0 ? market.title : "...", attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: mainColors.text!])
        titleTextNode.truncationMode = .byTruncatingTail
        titleTextNode.backgroundColor = UIColor.clear
        titleTextNode.placeholderColor = mainColors.lightGray
        titleTextNode.maximumNumberOfLines = 2
        titleTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 70))
        
        captionTextNode.attributedText = NSAttributedString(string: market.price?.text ?? market.description_vk, attributes: [.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: mainColors.darkGray!])
        captionTextNode.placeholderEnabled = true
        captionTextNode.backgroundColor = UIColor.clear
        captionTextNode.placeholderColor = mainColors.lightGray
        captionTextNode.maximumNumberOfLines = 1
        captionTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 66))
        
        marketImageNode.style.preferredSize = CGSize(width: 56, height: 56)
        
        let data = vkDataCache.getDataFromCachedData(sourceID: photo_id, ownerID: photo_id, type: "image", tag: "photo")
        
        if data == nil { marketImageNode.url = NSURL(string: photo_url)! as URL }
        else { marketImageNode.image = UIImage(data: data! as Data) }
        
        marketImageNode.imageModificationBlock = { [weak self] image in
            if vkDataCache.getDataFromCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.photo_id ?? 0, type: "image", tag: "photo") == nil {
                vkDataCache.saveDataInCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.photo_id ?? 0, type: "image", tag: "photo", url: self?.photo_url ?? "", image: image)
            }
            return image
        }
        marketImageNode.cornerRadius = 10
        
        addSubnode(titleTextNode)
        addSubnode(captionTextNode)
        addSubnode(marketImageNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let linkVStackSpec = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .start, alignItems: .start, children: [titleTextNode, captionTextNode])
        
        let finalStack = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .start, alignItems: .center, children: [marketImageNode, linkVStackSpec])
        let finalInsetSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: finalStack)
        return finalInsetSpec
    }
}
