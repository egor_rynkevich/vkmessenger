//
//  MessageNodeCell.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 22.08.2018.
//

import UIKit
import AsyncDisplayKit
import RealmSwift

protocol MessageCellNodeDelegate: class {
    func openProfile(user_id: Int64)
    func openURL(_ url: String, msgStrID: String)
    func openMap(lat: Double, long: Double, msgStrID: String)
    func openGif(gifUrl: String, gifSize: Int32, videoUrl: String, videoSize: Int32, msgStrID: String)
    func openMessageNode(msgID: Int64)
    func pressVote()
}

class MessageCellNode: ASCellNode {
    var msgRef: ThreadSafeReference<VKMessage>
    var msgID: Int64 = 0
    var peerType: String = VKUserType.isUser.rawValue
    var peerID : Int64 = 0
    var isOutput: Bool = false
    var isActionMsg: Bool = false
    var isEdited: Bool = false
    var isImportant: Bool = false
    var isHaveOnlyPhoto: Bool = false
    var isNeedFooter: Bool = false
    var isNeedRect: Bool = false
    var isNeedMsgStatus: Bool = false
    var _isSelectMode: Bool = false
    var isMovingSelectButton: Bool = false
    var contentWidth: Int = 0
    var left: CGFloat = 0
    var right: CGFloat = 0
    var footerTimeInsets: UIEdgeInsets = UIEdgeInsets()
    var contentAligment: ASStackLayoutJustifyContent = .start
    var itemsAligment: ASStackLayoutAlignItems = .start
    weak var delegate: MessageCellNodeDelegate?
    var arr: [ASLayoutSpec] = []
    fileprivate let textNode: ASTextNode
    fileprivate let footerTimeTextNode: ASTextNode
    fileprivate let footerStatusImageNode: ASImageNode
    fileprivate let footerEditedTextNode: ASTextNode
    fileprivate let backgroundImageNode: ASImageNode
    fileprivate let usernameTextNode: ASTextNode
    fileprivate let selectImageNode: ASImageNode
    var arrCell: [ASCellNode] = []
    var arrCellType: [String] = []
    //var wallNode: WallNode?

    init(msgRef: ThreadSafeReference<VKMessage>, tableWidth: CGFloat, lastOutRead: Int64, isNeedFooter: Bool, isNeedRect: Bool, isSelectMode: Bool, delegate: MessageCellNodeDelegate?) {
        self.msgRef = msgRef
        self.contentWidth = Int(tableWidth * 0.8) - 10
        self.delegate = delegate
        textNode = ASTextNode()
        footerTimeTextNode = ASTextNode()
        footerStatusImageNode = ASImageNode()
        footerEditedTextNode = ASTextNode()
        backgroundImageNode = ASImageNode()
        usernameTextNode = ASTextNode()
        selectImageNode = ASImageNode()
        super.init()

        let realm = try! Realm()

        if let msg = realm.resolve(msgRef) {
            automaticallyManagesSubnodes = true
            clipsToBounds = true
            selectionStyle = .none
            msgID = msg.id
            
            peerType = msg.peer_id < 0 ? VKUserType.isGroup.rawValue : (msg.peer_id > 2_000_000_000 ? VKUserType.isChat.rawValue : VKUserType.isUser.rawValue)
            peerID = msg.from_id
            isOutput = msg.from_id == vkLoadData.mainUserID
            isEdited = msg.update_time > 0
            isImportant = msg.important
            self.isNeedFooter = isNeedFooter
            self.isNeedRect = isNeedRect
            _isSelectMode = isSelectMode
            let backColor = getNeedBackColor(isOutput, msg)
            var text = ""
            
            if peerType == VKUserType.isChat.rawValue && msg.action_type != "" {
                text = checkActionType(from_id: msg.from_id, member_id: msg.action_member_id, actionType: msg.action_type, actionText: msg.action_text)
                isActionMsg = true
                contentWidth = Int(tableWidth)
            }
            else if peerType == VKUserType.isChat.rawValue && isOutput == false {
                usernameTextNode.attributedText = NSAttributedString(string: vkDataCache.getUsernameByIDAndType(id: msg.from_id, type: VKUserType.isUser.rawValue), attributes: [.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: mainColors.writeRow!])
                usernameTextNode.placeholderEnabled = true
                usernameTextNode.backgroundColor = UIColor.clear
                usernameTextNode.placeholderFadeDuration = 0.15
                usernameTextNode.placeholderColor = mainColors.lightGray
                usernameTextNode.isUserInteractionEnabled = true
                usernameTextNode.addTarget(self, action: #selector(openProfile), forControlEvents: .touchUpInside)
                text = msg.text
                addSubnode(usernameTextNode)
            } else { text = msg.text }
            
            let msgColor = mainColors.getMessTextColor(isOutput)
            
            backgroundImageNode.cornerRadius = 7
            backgroundImageNode.backgroundColor = isActionMsg == true ? nil : backColor
            backgroundImageNode.contentMode = .scaleToFill
            backgroundImageNode.addTarget(self, action: #selector(openMessageNode), forControlEvents: .touchUpInside)
            
//            if isOutput == true && mainColors.darkMode == true {
//                let gradient = CAGradientLayer()
//                gradient.frame = CGRect(x: 0, y: 0, width: contentWidth, height: 100)
//                gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
//                gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
//                gradient.colors = [mainColors.outgoingMessage?.cgColor as Any, mainColors.outgoingMessageSecond?.cgColor as Any]
//
//                backgroundImageNode.image = UIImage().image(fromLayer: gradient)
//            }
            
            
            
            
            
            //var textSize: CGFloat = 14
//            if msg.text.containsEmoji == true && msg.text.stringByRemovingEmoji().trimmingCharacters(in: .whitespacesAndNewlines).count == 0 { textSize = 40 }
            
            
            if isActionMsg == true {
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = .center
                paragraphStyle.lineBreakMode = .byWordWrapping
                textNode.attributedText = NSAttributedString(string: text, attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: mainColors.darkGray!])
                textNode.style.minWidth = ASDimension(unit: .points, value: tableWidth - 20)
                textNode.textContainerInset = UIEdgeInsets(top: 5, left: 0, bottom: 10, right: 0)
            } else {
                let font = UIFont.preferredFont(forTextStyle: .body)
                textNode.attributedText = NSAttributedString(string: text, attributes: [.font : UIFont.systemFont(ofSize: font.pointSize - 1), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: msgColor.messageText!])
            }
            //UIFont.init(name: "GraphikLCWeb-Regular", size: font.pointSize - 1)
            //
            textNode.placeholderEnabled = true
            textNode.backgroundColor = UIColor.clear
            textNode.placeholderFadeDuration = 0.15
            textNode.placeholderColor = mainColors.lightGray
            textNode.isUserInteractionEnabled = true
            textNode.style.minWidth = ASDimension(unit: .points, value: 50)
            textNode.addTarget(self, action: #selector(openMessageNode), forControlEvents: .touchUpInside)
            
            addSubnode(backgroundImageNode)
            addSubnode(textNode)


            generateNeedCells(msg, msgColor)
            
            if isNeedFooter || ((msg.id == lastOutRead) && isOutput == true) || msg.id == 0 {
                if msg.id == 0 { isNeedMsgStatus = true } 
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "h:mm a"
                let date = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(msg.date)))
                
                footerTimeTextNode.attributedText = NSAttributedString(string: date, attributes: [.font : UIFont.systemFont(ofSize: 11), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: mainColors.textFooter!])
                footerTimeTextNode.placeholderEnabled = true
                footerTimeTextNode.placeholderFadeDuration = 0.15
                footerTimeTextNode.placeholderColor = mainColors.lightGray
                
                footerStatusImageNode.image = msg.id == 0 ? mainIcons.clock_12 : (msg.id <= lastOutRead ? mainIcons.readMessage_s12 : mainIcons.unreadMessage_s12)
                footerStatusImageNode.contentMode = .scaleAspectFill
                footerStatusImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(msg.id <= lastOutRead ? mainColors.readMessage! : mainColors.textFooter!)
            }
//            else if (msg.id == lastOutRead) && isOutput == true {
//                footerStatusImageNode.image = mainIcons.readMessage_s12
//                footerStatusImageNode.contentMode = .scaleAspectFill
//                footerStatusImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(mainColors.readMessage!)
//                isNeedMsgStatus = true
//            }
            
            if isEdited {
                footerEditedTextNode.attributedText = NSAttributedString(string: "(edited)", attributes: [.font : UIFont.systemFont(ofSize: 11), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: mainColors.textFooter!])
                footerTimeTextNode.placeholderEnabled = true
                footerTimeTextNode.placeholderFadeDuration = 0.15
                footerTimeTextNode.placeholderColor = mainColors.lightGray
            }
            
            footerTimeInsets = getInsetsForTimeStack(msg: msg)
            
            addSubnode(footerTimeTextNode)
            addSubnode(footerStatusImageNode)

            left = isOutput ? 0 : 10
            right = isOutput ? 10 : 0
            contentAligment = isOutput ? .end : .start
            itemsAligment = isOutput ? .end : .start
            
            if isActionMsg == true {
                left = 10
                right = 10
                contentAligment = .center
                itemsAligment = .center
            }
        }
    }
    
    override func didLoad() {
        let topLeft = CACornerMask.layerMinXMinYCorner
        let topRight = CACornerMask.layerMaxXMinYCorner
        let bottomLeft = CACornerMask.layerMinXMaxYCorner
        let bottomRight = CACornerMask.layerMaxXMaxYCorner
        
        var arr: CACornerMask = []
        
        if isNeedRect {
            if isOutput {
                arr = [topLeft, bottomLeft]
            } else {
                arr = [topRight, bottomRight]
            }
        }
        else {
            arr = [topLeft, topRight, isOutput == true ? bottomLeft : bottomRight]
        }
        
        backgroundImageNode.layer.maskedCorners = arr
        
        if !isOutput {
            backgroundImageNode.layer.borderWidth = 1
            backgroundImageNode.layer.borderColor = UIColor.black.withAlphaComponent(0.1).cgColor
        }
        
        if _isSelectMode == true {
            moveSelectImageNode(isShow: true)
        }
    }

    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        arr = generateNeedSpecLayout(constrainedSize)

        if (textNode.attributedText?.length ?? 0) > 0 {
            let textSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: arr.count == 0 ? 6 : 3, left: 3, bottom: 1, right: 3) , child: textNode)
            arr.insert(textSpec, at: (isHaveOnlyPhoto == true && isActionMsg == false) ? arr.count : 0)
        }

        let verticalStackSpec = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .start, alignItems: .start, children: arr)
        
        arr.removeAll()

        verticalStackSpec.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth))
        verticalStackSpec.style.flexGrow = 1.0
        
        let finalStack = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .start, alignItems: itemsAligment, children: [verticalStackSpec])
        
        let finalStackInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 5, bottom: 5, right: 5) , child: finalStack)
        let finalBack = ASBackgroundLayoutSpec(child: finalStackInsets, background: backgroundImageNode)

        let cellStack = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .start, alignItems: itemsAligment, children: [finalBack])
        
        if isActionMsg == true {
            verticalStackSpec.alignItems = .center
            verticalStackSpec.justifyContent = .center
        }
        
        if isNeedFooter == true {
            let footerHorStack = ASStackLayoutSpec(direction: .horizontal, spacing: 3, justifyContent: .end, alignItems: .center, children: [footerTimeTextNode])
            
            if isEdited == true { footerHorStack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 2, right: 0) , child: footerEditedTextNode)) }
            if isOutput == true { footerHorStack.children?.append(footerStatusImageNode) }
            
            let footerInsets = ASInsetLayoutSpec(insets: footerTimeInsets , child: footerHorStack)
            
            if isActionMsg == false { cellStack.children?.append(footerInsets) }
        } else if isEdited == true {
            cellStack.children?.append(ASInsetLayoutSpec(insets: footerTimeInsets , child: footerEditedTextNode))
        }
        
        if isNeedFooter == false && isNeedMsgStatus == true {
            cellStack.children?.append(ASInsetLayoutSpec(insets: footerTimeInsets , child: footerStatusImageNode))
        }
        
        if peerType == VKUserType.isChat.rawValue && isOutput == false {
            let nameInset = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 10, bottom: 2, right: 0) , child: usernameTextNode)
            cellStack.children?.insert(nameInset, at: 0)
        }
        let cell = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: left, bottom: 0, right: right) , child: cellStack)
        let contentSizeLayout = ASAbsoluteLayoutSpec()
        contentSizeLayout.sizing = .sizeToFit
        contentSizeLayout.children = [cell]
        contentSizeLayout.style.maxWidth = ASDimension(unit: .points, value: constrainedSize.max.width * (isActionMsg == true ? 1 : 0.8))
        //if isImportant == true { contentSizeLayout.children?.insert(createImportantNode(), at: 0) }
        let contentLayoutSpec = ASStackLayoutSpec(direction: .horizontal, spacing: 0, justifyContent: contentAligment, alignItems: .center, children: [createSpacer(), contentSizeLayout])
        contentSizeLayout.style.flexShrink = 1
        
        if isImportant == true { contentLayoutSpec.children?.insert(createImportantNode(), at: isOutput == true ? 1 : 2) }
        //if _isSelectMode == true {
            return ASStackLayoutSpec(direction: .horizontal, spacing: 1, justifyContent: isOutput == true ? .spaceBetween : .start, alignItems: .center, children: [createSelectButton(), contentLayoutSpec])
        //} else { return contentLayoutSpec }
    }

    func generateNeedCells(_ msg: VKMessage?, _ msgColor: MessageColor) {
        if msg != nil {
            if msg!.geo != nil {
                arrCell.append(GeoNode(geo: msg!.geo, contentWidth: contentWidth, delegate: delegate))
                arrCellType.append("geo")
            }
            
            var photoArr: [VKPhoto?] = []
            for att in msg!.attachments {
                if att.type == "photo" {
                    photoArr.append(att.photo)
                }
            }
            
            if photoArr.count > 0 {
                arrCell.append(ImageCellNode(imgArrSrc: photoArr, contentWidth: isActionMsg == true ? 150 : contentWidth, delegate: delegate))
                arrCellType.append("photo")
            }
            
            for attachment in msg!.attachments {
                switch attachment.type {
                    case "audio":
                        arrCell.append(AudioNode(audio: attachment.audio, contentWidth: contentWidth, delegate: delegate, msgColor: msgColor))
                        arrCellType.append("audio")
                    break
                    //case "photo": return [attachment.type : attachment.photo]
                    case "video":
                        arrCell.append(VideoNode(videoSrc: attachment.video, contentWidth: contentWidth, delegate: delegate))
                        arrCellType.append("video")
                    break
                    case "doc":
                        generateNeedDocCell(attachment.doc, msgColor)
                        break
                    case "link":
                        arrCell.append(LinkNode(sourceLink: attachment.link, contentWidth: contentWidth, delegate: delegate, msgColor: msgColor))
                        arrCellType.append("link")
                    break
                case "audio_message":
                    let inArr = attachment.audio_msg?.waveform ?? List<Int>()
                    var waveForm: [Int] = []
                    for i in inArr { waveForm.append(i) }
                    
                    arrCell.append(AudioMsgNode(audioMsg: attachment.audio_msg, contentWidth: contentWidth, delegate: delegate, msgColor: msgColor, waveForm: waveForm))
                    arrCellType.append("audio_message")
                    break
                case "graffiti":
                    arrCell.append(GraffitiNode(graffitiSrc: attachment.graffiti, contentWidth: contentWidth, delegate: delegate))
                    arrCellType.append("graffiti")
                    break
                case "market":
                    arrCell.append(MarketNode(marketSource: attachment.market, contentWidth: contentWidth, delegate: delegate))
                    arrCellType.append("market")
                    break
                case "market_album":
                    arrCell.append(MarketAlbumNode(marketSource: attachment.market_album, contentWidth: contentWidth, delegate: delegate))
                    arrCellType.append("market_album")
                    break
                case "wall":
                    arrCell.append(WallNode(wall: attachment.wall, contentWidth: contentWidth, delegate: delegate, msgColor: msgColor))
                    arrCellType.append("wall")
                    break
                case "sticker":
                    arrCell.append(StickerNode(sticker: attachment.sticker, delegate: delegate))
                    arrCellType.append("sticker")
                    break
                case "gift":
                    arrCell.append(GiftNode(sourceGift: attachment.gift, contentWidth: contentWidth, delegate: delegate))
                    arrCellType.append("gift")
                    break
                default: break
                }
            }
        
            if arrCellType.count > 0 && arrCellType[0] == "photo" { isHaveOnlyPhoto = true }
            
            for fwdMsg in msg!.fwd_messages {
                let elem = ForwardedMessageNode(msg: fwdMsg, contentWidth: contentWidth, delegate: delegate)
                arrCell.append(elem)
                arrCellType.append("fwd_msg")
                self.addSubnode(elem)
            }
        }
    }

    func generateNeedDocCell(_ doc: VKDocument?, _ msgColor: MessageColor) {
        guard let document = doc else { return }

        switch document.type {
        case 1, 2, 6, 7, 8: // 1 - text doc, 2 - zip, 6 - video, 7 - elect. book, 8 - unknown
            arrCell.append(DocFileNode(doc: document, contentWidth: contentWidth, delegate: delegate, msgColor: msgColor))
            arrCellType.append("doc")
            break
        case 3 : // gif
            arrCell.append(DocGifNode(doc: document, contentWidth: contentWidth, delegate: delegate))
            arrCellType.append("doc")
            break
        case 4: // image
            arrCell.append(DocImgNode(doc: document, contentWidth: contentWidth, delegate: delegate))
            arrCellType.append("doc")
            break
        default:
            return
        }
    }

    func generateNeedSpecLayout(_ constrainedSize: ASSizeRange) -> [ASLayoutSpec] {
        var arrLayoutSpec : [ASLayoutSpec] = []
        var i = 0
        
        for cellType in arrCellType {
            switch cellType {
            case "audio":
                if let elem = arrCell[i] as? AudioNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "photo":
                if let elem = arrCell[i] as? ImageCellNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "video":
                if let elem = arrCell[i] as? VideoNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "doc":
                if let docLayout = generateNeedDocSpecLayout(arrCell[i], constrainedSize) {
                    arrLayoutSpec.append(docLayout)
                }
                break
            case "link":
                if let elem = arrCell[i] as? LinkNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "audio_message":
                if let elem = arrCell[i] as? AudioMsgNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "graffiti":
                if let elem = arrCell[i] as? GraffitiNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "market":
                if let elem = arrCell[i] as? MarketNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "market_album":
                if let elem = arrCell[i] as? MarketAlbumNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "wall":
                if let elem = arrCell[i] as? WallNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "sticker":
                if let elem = arrCell[i] as? StickerNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "geo":
                if let elem = arrCell[i] as? GeoNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "gift":
                if let elem = arrCell[i] as? GiftNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "fwd_msg":
                if let elem = arrCell[i] as? ForwardedMessageNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            default: break
            }
            i += 1
        }
        
        

        return arrLayoutSpec
    }
    
    func generateNeedDocSpecLayout(_ cell: ASCellNode, _ constrainedSize: ASSizeRange) -> ASLayoutSpec? {

        if let elem = cell as? DocFileNode {
            return elem.layoutSpecThatFits(constrainedSize)
        }
        else if let elem = cell as? DocImgNode {
            return elem.layoutSpecThatFits(constrainedSize)
        }
        else if let elem = cell as? DocGifNode {
            return elem.layoutSpecThatFits(constrainedSize)
        }

        return nil
    }

    func checkActionType(from_id: Int64, member_id: Int64, actionType: String, actionText: String) -> String {
        var msg: String = ""
        let fromUsername: String = vkDataCache.getUsernameByIDAndType(id: from_id, type: VKUserType.isUser.rawValue)
        switch actionType {
        case "chat_photo_update":
            msg = fromUsername + " updated the group chat photo:"
            //needShowPhoto = true
            break
        case "chat_photo_remove":
            msg = fromUsername + " removed the group chat photo"
            break
        case "chat_create":
            msg = fromUsername + " created this group chat"
            break
        case "chat_title_update":
            msg = fromUsername + " changed the group chat name to \"" + actionText + "\""
            break
        case "chat_invite_user":
            msg = fromUsername + " invited " + vkDataCache.getUsernameByIDAndType(id: member_id, type: VKUserType.isUser.rawValue)
            break
        case "chat_kick_user":
            msg = fromUsername +  " kicked " + vkDataCache.getUsernameByIDAndType(id: member_id, type: VKUserType.isUser.rawValue) + " out"
            break
        default:
            break
        }
        
        return msg
    }

    func getInsetsForTimeStack(msg: VKMessage) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 1, right: 10)
    }
    
    func createSpacer() -> ASLayoutSpec {
        let spacer = ASLayoutSpec()
        spacer.style.flexGrow = 0
        spacer.style.flexShrink = 0
        spacer.style.minHeight = ASDimension(unit: .points, value: 0)
        return spacer
    }
    
    func createSelectButton() -> ASLayoutSpec {
        selectImageNode.image = mainIcons.unselect_s25
        selectImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(mainColors.writeRow!)
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: -25, bottom: 0, right: 0), child: selectImageNode)
    }
    
    func moveSelectImageNode(isShow: Bool) {
        isMovingSelectButton = isShow
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3, animations: {
                if !self.isOutput {
                    let frame = self.view.frame
                    self.view.bounds = CGRect(x: isShow ? -35 : 0, y: frame.origin.y, width: frame.width, height: frame.height)
                } else {
                    let frame = self.selectImageNode.frame
                    print(self.selectImageNode.frame)
                    //self.selectImageNode.frame.origin.x += isShow ? 35 : -35
                    self.selectImageNode.frame = CGRect(x: isShow ? 10 : -25, y: frame.origin.y, width: frame.width, height: frame.height)
                }
            }) { (complition) in
                if complition && !isShow {
                    self.selectImageNode.image = mainIcons.unselect_s25
                }
            }
        }
        
    }
    
    func setSelect(isSelect: Bool) {
        selectImageNode.image = isSelect == true ? mainIcons.select_s25 : mainIcons.unselect_s25
    }
    
    func createImportantNode() -> ASLayoutSpec {
        let importantImageNode = ASImageNode()
        importantImageNode.image = mainIcons.important_s16
        importantImageNode.contentMode = .scaleAspectFill
        importantImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(mainColors.importantMessage!)
        let needInsetsTop = isNeedFooter == false && isNeedMsgStatus == true  || isNeedFooter == true
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: needInsetsTop ? -15 : 5, left: 10, bottom: 0, right: 10) , child: importantImageNode)
    }

    func getNeedBackColor(_ isOutput: Bool, _ msg: VKMessage) -> UIColor? {
        var haveSticker = false
        var haveGift = false

        for att in msg.attachments {
            if att.type == "gift" {
                haveGift = true
                break
            }
            if att.type == "sticker" || att.type == "graffiti" {
                haveSticker = true
                break
            }
        }
        if haveSticker == true { return mainColors.mainTableView }
        if haveGift == true { return mainColors.giftNode }
        return isOutput == true ? mainColors.outgoingMessage : mainColors.incomingMessage
    }
    
    @objc func openMessageNode() {
        delegate?.openMessageNode(msgID: msgID)
    }

    @objc func openProfile() {
        delegate?.openProfile(user_id: peerID)
    }
}

extension String {
    
    var containsEmoji: Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x1F600...0x1F64F, // Emoticons
            0x1F300...0x1F5FF, // Misc Symbols and Pictographs
            0x1F680...0x1F6FF, // Transport and Map
            0x2600...0x26FF,   // Misc symbols
            0x2700...0x27BF,   // Dingbats
            0xFE00...0xFE0F,   // Variation Selectors
            0x1F900...0x1F9FF, // Supplemental Symbols and Pictographs
            0x1F1E6...0x1F1FF: // Flags
                return true
            default:
                continue
            }
        }
        return false
    }
    
    func stringByRemovingEmoji() -> String {
        return String(unicodeScalars.filter { !$0.isEmoji() })
    }
    
}

extension UnicodeScalar {
    fileprivate func isEmoji() -> Bool {
        switch self.value {
        case 0x1F600...0x1F64F, // Emoticons
        0x1F300...0x1F5FF, // Misc Symbols and Pictographs
        0x1F680...0x1F6FF, // Transport and Map
        0x2600...0x26FF,   // Misc symbols
        0x2700...0x27BF,   // Dingbats
        0xFE00...0xFE0F,   // Variation Selectors
        0x1F900...0x1F9FF, // Supplemental Symbols and Pictographs
        0x1F1E6...0x1F1FF: // Flags
            return true
        default:
            return false
        }
    }
}
