//
//  GraffitiNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 02.09.2018.
//

import UIKit
import AsyncDisplayKit
import RealmSwift

class GraffitiNode: ASCellNode {
    
    fileprivate let imageNode: ASNetworkImageNode
    
    var graffiti_id: Int64 = 0
    var graffiti_url: String = ""
    var owner_id: Int64 = 0
    var height: Int = 0
    var width: Int = 0
    weak var delegate: MessageCellNodeDelegate?
    
    init(graffitiSrc: VKGraffiti?, contentWidth: Int, delegate: MessageCellNodeDelegate?) {
        imageNode = ASNetworkImageNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true
        
        guard let graffiti = graffitiSrc else { return }
        
        if graffiti.height != 0 && graffiti.width != 0 {
            height = Int(Float(graffiti.height) / Float(graffiti.width) * Float(contentWidth - 10))
            width = contentWidth - 10
        } else {
            height = 160
            width = 160
        }
        imageNode.style.preferredSize = CGSize(width: width, height: height)
        
        graffiti_id = graffiti.id
        graffiti_url = graffiti.url
        owner_id = graffiti.owner_id
        
        let data = vkDataCache.getDataFromCachedData(sourceID: graffiti_id, ownerID: owner_id, type: "image", tag: "graffiti")
        
        if data == nil { imageNode.url = NSURL(string: graffiti_url)! as URL }
        else { imageNode.image = UIImage(data: data! as Data) }
        
        imageNode.imageModificationBlock = { [weak self] image in
            if vkDataCache.getDataFromCachedData(sourceID: self?.graffiti_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "graffiti") == nil {
                vkDataCache.saveDataInCachedData(sourceID: self?.graffiti_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "graffiti", url: self?.graffiti_url ?? "", image: image)
            }
            return image
        }
        
        addSubnode(imageNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let stickerSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: -20, right: 0), child: imageNode)
        return stickerSpec
    }
}
