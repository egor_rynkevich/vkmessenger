//
//  MarketAlbumNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 06.09.2018.
//

import UIKit
import AsyncDisplayKit

class MarketAlbumNode: ASCellNode {
    
    fileprivate let titleTextNode: ASTextNode
    fileprivate let captionTextNode: ASTextNode
    fileprivate let marketImageNode: ASNetworkImageNode
    fileprivate let countTextNode: ASTextNode
    fileprivate let backImageNode: ASImageNode
    var photo_id: Int64 = 0
    var photo_url: String = ""
    var owner_id: Int64 = 0
    weak var delegate: MessageCellNodeDelegate?
    
    init(marketSource: VKMarketAlbum?, contentWidth: Int, delegate: MessageCellNodeDelegate?) {
        titleTextNode = ASTextNode()
        captionTextNode = ASTextNode()
        marketImageNode = ASNetworkImageNode()
        countTextNode = ASTextNode()
        backImageNode = ASImageNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true
        
        guard let market = marketSource else { return }
        
        photo_id = market.photo?.id ?? 0
        photo_url = market.photo?.photoDocSizeMin?.url ?? market.photo?.photoDocSizeMid?.url ?? market.photo?.photoDocSizeMax?.url ?? ""
        owner_id = market.photo?.owner_id ?? 0
        
        titleTextNode.attributedText = NSAttributedString(string: market.title, attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: mainColors.text!])
        titleTextNode.truncationMode = .byTruncatingTail
        titleTextNode.backgroundColor = UIColor.clear
        titleTextNode.placeholderColor = mainColors.lightGray
        titleTextNode.maximumNumberOfLines = 2
        titleTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 70))
        
        captionTextNode.attributedText = NSAttributedString(string: "last update: " + String(market.updated_time), attributes: [.font : UIFont.systemFont(ofSize: 11), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: mainColors.darkGray!])
        captionTextNode.placeholderEnabled = true
        captionTextNode.backgroundColor = UIColor.clear
        captionTextNode.placeholderColor = mainColors.lightGray
        captionTextNode.maximumNumberOfLines = 2
        captionTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 70))
        
        
        marketImageNode.style.preferredSize = CGSize(width: 56, height: 56)
        
        let data = vkDataCache.getDataFromCachedData(sourceID: photo_id, ownerID: owner_id, type: "image", tag: "photo")
        
        if data == nil { marketImageNode.url = NSURL(string: photo_url)! as URL }
        else { marketImageNode.image = UIImage(data: data! as Data) }
        
        marketImageNode.imageModificationBlock = { [weak self] image in
            if vkDataCache.getDataFromCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "photo") == nil {
                vkDataCache.saveDataInCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "photo", url: self?.photo_url ?? "", image: image)
            }
            return image
        }
        marketImageNode.cornerRadius = 10
        
        backImageNode.cornerRadius = 7
        backImageNode.backgroundColor = mainColors.mainBackgroundView
        backImageNode.style.minWidth = ASDimension(unit: .points, value: 14)
        
        countTextNode.attributedText = NSAttributedString(string: String(market.count), attributes: [.font : UIFont.systemFont(ofSize: 10), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.white])
        countTextNode.backgroundColor = UIColor.clear
        countTextNode.placeholderColor = mainColors.lightGray
        countTextNode.maximumNumberOfLines = 1
        countTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(56))
        
        addSubnode(titleTextNode)
        addSubnode(captionTextNode)
        addSubnode(marketImageNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let countBack = ASBackgroundLayoutSpec(child: ASInsetLayoutSpec(insets: UIEdgeInsets(top: 1, left: 3, bottom: 1, right: 3), child: countTextNode), background: backImageNode)
        
        let imageOverlay = ASOverlayLayoutSpec(child: marketImageNode, overlay: ASInsetLayoutSpec(insets: UIEdgeInsets(top: CGFloat.infinity, left: 0, bottom: 0, right: CGFloat.infinity), child: countBack))
        
        let vStack = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .start, alignItems: .start, children: [titleTextNode, captionTextNode])
        
        let finalStack = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .start, alignItems: .center, children: [imageOverlay, vStack])
        let finalInsetSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: finalStack)
        return finalInsetSpec
    }
}
