//
//  ImageCellNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 22.09.2018.
//

import UIKit
import AsyncDisplayKit
import RealmSwift

class ImageCellNode: ASCellNode, ASCollectionDelegate, ASCollectionDataSource {
    fileprivate let imgCollectionNode: ASCollectionNode
    weak var delegate: MessageCellNodeDelegate?
    var contentWidth: Int = 0
    var count: Int = 0
    var imgArr: [VKPhoto] = []
    
    
    var isHaveTwoPhotoInTheEnd = false
    var isHaveOnePhotoInTheEnd = false
    var imgSizeForLastPhoto : Int = 0
    let insets = 5
    var imgSize: Int = 0
    var contentHeight: Int = 0
    
    
    init(imgArrSrc: [VKPhoto?], contentWidth: Int, delegate: MessageCellNodeDelegate?) {
        self.contentWidth = contentWidth
        self.delegate = delegate
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumLineSpacing = CGFloat(insets)
        flowLayout.minimumInteritemSpacing = CGFloat(insets)
        imgCollectionNode = ASCollectionNode(collectionViewLayout: flowLayout)
        
        super.init()
        
        automaticallyManagesSubnodes = true
        
        for photo in imgArrSrc {
            if photo != nil {
                let photoCopy = VKPhoto()
                photoCopy.copyValue(photo!)
                imgArr.append(photoCopy)
            }
        }
        calcSize()
        imgCollectionNode.backgroundColor = UIColor.clear
        imgCollectionNode.style.preferredSize = CGSize(width: self.contentWidth, height: contentHeight)
        
        addSubnode(imgCollectionNode)
        
        imgCollectionNode.delegate = self
        imgCollectionNode.dataSource = self
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: imgCollectionNode)
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        return imgArr.count
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, constrainedSizeForItemAt indexPath: IndexPath) -> ASSizeRange {
        if (isHaveOnePhotoInTheEnd == true && indexPath.row == (imgArr.count - 1))
            || (isHaveTwoPhotoInTheEnd == true && (indexPath.row == (imgArr.count - 1) || (indexPath.row + 1) == (imgArr.count - 1))) {
            return ASSizeRange(min: CGSize(width: imgSizeForLastPhoto, height: imgSize), max: CGSize(width: imgSizeForLastPhoto, height: imgSize))
        }
        else {
            return ASSizeRange(min: CGSize(width: imgSize, height: imgSize), max: CGSize(width: imgSize, height: imgSize))
        }
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        return {
            let node = ImageCollectionNode(photo: self.imgArr[indexPath.row], delegate: self.delegate)
            return node
        }
    }
    
    func calcSize() {
        let imgCount = imgArr.count
        switch imgCount {
        case 1, 2, 3, 6, 9:
            if imgCount % 3 == 0 {
                imgSize = Int((contentWidth - (2 * insets) - 10) / 3)
                contentHeight = (imgSize * (imgCount / 3)) + ((imgCount / 3) - 1) * insets
            } else if imgCount == 2 {
                imgSize = Int((contentWidth - 10 - insets) / imgCount)
                contentHeight = imgSize
            } else {
                imgSize = contentWidth - 10
                contentHeight = imgSize
            }
            break
        case 4:
            imgSize = Int((contentWidth - 10 - insets) / 2)
            contentHeight = imgSize * 2 + insets
            break
        case 5, 8:
            isHaveTwoPhotoInTheEnd = true
            imgSize = Int((contentWidth - (2 * insets) - 10) / 3)
            imgSizeForLastPhoto = Int((contentWidth - 10 - insets) / 2)
            contentHeight = imgSize * (Int(imgCount / 3) + 1) + Int(imgCount / 3) * insets
            break
        case 7, 10:
            isHaveOnePhotoInTheEnd = true
            imgSize = Int((contentWidth - (2 * insets) - 10) / 3) 
            imgSizeForLastPhoto = contentWidth - 10
            contentHeight = (imgSize) * (Int(imgCount / 3) + 1) + Int(imgCount / 3) * insets
            break
        default:
            break
        }
    }
}
