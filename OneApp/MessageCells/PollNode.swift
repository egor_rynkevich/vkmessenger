 //////
//////  PollNode.swift
//////  SocialNetworksBrowser
//////
//////  Created by Егор Рынкевич on 13.09.2018.
//////
//
import UIKit
import AsyncDisplayKit
 
 class MyASButtonNode: ASButtonNode {
    var tag: Int64 = 0
 }

class PollNode: ASCellNode {
    fileprivate let titleTextNode: ASTextNode
    fileprivate let anonymousTextNode: ASTextNode
    fileprivate let multipleTextNode: ASTextNode
    fileprivate let imageNode: ASImageNode
    fileprivate let countTextNode: ASTextNode
    fileprivate let voteButtonNode: ASButtonNode
    weak var delegate: MessageCellNodeDelegate?
    var answerSpecArr: [ASLayoutSpec] = []
    @objc var choosingAnswer: [Int64] = []
    var isChoosing: Bool = false
    var isAnswered: Bool = false
    var isMultiple: Bool = false
    var allAnswersHeight: Int = 0
    var owner_id: Int64 = 0
    var poll_id: Int64 = 0
    
    var answerBtnArr: [MyASButtonNode] = []

    init(pollSrc: VKPoll?, contentWidth: Int, delegate: MessageCellNodeDelegate?) {
        titleTextNode = ASTextNode()
        imageNode = ASImageNode()
        countTextNode = ASTextNode()
        voteButtonNode = ASButtonNode()
        anonymousTextNode = ASTextNode()
        multipleTextNode = ASTextNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true

        guard let poll = pollSrc else { return }
        
        for id in poll.answer_ids {
            print(id)
        }
        
        isAnswered = poll.answer_ids.count > 0
        isMultiple = poll.multiple
        owner_id = poll.owner_id
        poll_id = poll.id
        
        if isAnswered == true {
            choosingAnswer = poll.answer_ids.map { $0 }
            for answer in poll.answers {
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = .left
                paragraphStyle.lineBreakMode = .byWordWrapping
                
                let answerTextNode = ASTextNode()
                let votesTextNode = ASTextNode()
                let rateTextNode = ASTextNode()
                
                answerTextNode.attributedText = NSAttributedString(string: answer.text, attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: UIColor.white])
                answerTextNode.backgroundColor = UIColor.clear
                answerTextNode.placeholderColor = mainColors.lightGray
                answerTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 120))
                
                let answerHeight = answerTextNode.calculateSizeThatFits(CGSize(width: contentWidth - 120, height: 900)).height
                allAnswersHeight += Int(answerHeight + 30)
                votesTextNode.attributedText = NSAttributedString(string: " • \(answer.votes)", attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: mainColors.lightGray!])
                votesTextNode.backgroundColor = UIColor.clear
                votesTextNode.placeholderColor = mainColors.lightGray
                
                
                rateTextNode.attributedText = NSAttributedString(string: "\(String(format: "%.2f", answer.rate)) %", attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: UIColor.white])
                rateTextNode.backgroundColor = UIColor.clear
                rateTextNode.placeholderColor = mainColors.lightGray
                
                let votesStackSpec = ASStackLayoutSpec(direction: .horizontal, spacing: 2, justifyContent: .start, alignItems: .center, children: [rateTextNode, votesTextNode])
                let answerStackSpec = ASStackLayoutSpec(direction: .horizontal, spacing: 2, justifyContent: .spaceBetween, alignItems: .center, children: [answerTextNode, votesStackSpec])
                
                let answerButton = ASButtonNode()
                answerButton.cornerRadius = 5
                answerButton.alpha = 0.3
                answerButton.style.preferredSize = CGSize(width: contentWidth - 50, height: Int(answerHeight + 20))
                
                if poll.answer_ids.contains(answer.id) {
                    answerButton.backgroundColor = mainColors.online
                } else { answerButton.backgroundColor = mainColors.mainBackgroundView }
                
                let answerSpec = ASOverlayLayoutSpec(child: answerButton, overlay: ASInsetLayoutSpec(insets: UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7), child: answerStackSpec))
                
                answerSpecArr.append(answerSpec)
            }
        }
        else {
            for answer in poll.answers {
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = .left
                paragraphStyle.lineBreakMode = .byWordWrapping
                
                let answerTextNode = ASTextNode()
                
                answerTextNode.attributedText = NSAttributedString(string: answer.text, attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: UIColor.white])
                answerTextNode.backgroundColor = UIColor.clear
                answerTextNode.placeholderColor = mainColors.lightGray
                answerTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 64))
                
                let answerHeight = answerTextNode.calculateSizeThatFits(CGSize(width: contentWidth - 120, height: 900)).height
                allAnswersHeight += Int(answerHeight + 30)
                
                let answerStackSpec = ASStackLayoutSpec(direction: .horizontal, spacing: 2, justifyContent: .spaceBetween, alignItems: .center, children: [answerTextNode])
                
                let answerButton = MyASButtonNode()
                answerButton.cornerRadius = 5
                answerButton.style.preferredSize = CGSize(width: contentWidth - 50, height: Int(answerHeight + 20))
                
                answerButton.backgroundColor = mainColors.mainBackgroundView
                answerButton.alpha = 0.3
                answerButton.tag = answer.id
                answerButton.addTarget(self, action: #selector(choosingAnswer(_:)), forControlEvents: .touchUpInside)
                answerBtnArr.append(answerButton)
                
                let answerSpec = ASOverlayLayoutSpec(child: answerButton, overlay: ASInsetLayoutSpec(insets: UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7), child: answerStackSpec))
                
                answerSpecArr.append(answerSpec)
            }
        }


        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineBreakMode = .byWordWrapping

        titleTextNode.attributedText = NSAttributedString(string: poll.question, attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.boldSystemFont(ofSize: 20), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: UIColor.white])
        titleTextNode.truncationMode = .byTruncatingTail
        titleTextNode.backgroundColor = UIColor.clear
        titleTextNode.placeholderColor = mainColors.lightGray
        titleTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 50))
        
        anonymousTextNode.attributedText = NSAttributedString(string: poll.anonymous == true ? "Anonymous poll" : "Public poll", attributes: [.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: UIColor.white])
        anonymousTextNode.backgroundColor = UIColor.clear
        anonymousTextNode.placeholderColor = mainColors.lightGray
        anonymousTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 50))
        
        multipleTextNode.attributedText = NSAttributedString(string: poll.multiple == true ? "Select multiple options" : "Select one option", attributes: [.font : UIFont.boldSystemFont(ofSize: 12), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: UIColor.white])
        multipleTextNode.backgroundColor = UIColor.clear
        multipleTextNode.placeholderColor = mainColors.lightGray
        multipleTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 50))
        
        voteButtonNode.style.preferredSize = CGSize(width: 150, height: 30)
        voteButtonNode.backgroundColor = mainColors.deleteRow
        voteButtonNode.alpha = 0.8
        voteButtonNode.cornerRadius = 5
        voteButtonNode.isEnabled = isAnswered == true
        voteButtonNode.setAttributedTitle(NSAttributedString(string: isAnswered == true ? "Unvote" : "Vote", attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: UIColor.white]), for: .normal)
        voteButtonNode.addTarget(self, action: #selector(pressVote), forControlEvents: .touchUpInside)

        countTextNode.attributedText = NSAttributedString(string: "\(poll.votes) people voted", attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: UIColor.white])
        countTextNode.backgroundColor = UIColor.clear
        countTextNode.placeholderColor = mainColors.lightGray
        countTextNode.maximumNumberOfLines = 1

        let titleHieght = titleTextNode.calculateSizeThatFits(CGSize(width: contentWidth - 50, height: 600)).height
        
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: contentWidth-10, height: 200 + allAnswersHeight + Int(titleHieght))
        gradient.startPoint = CGPoint(x: 0.0, y: 0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.colors = [mainColors.deleteRow?.cgColor as Any, mainColors.writeRow?.cgColor as Any, mainColors.deleteRow?.cgColor as Any]
        
        imageNode.style.preferredSize = CGSize(width: contentWidth-10, height: 200 + allAnswersHeight + Int(titleHieght))
        imageNode.cornerRadius = 10
        imageNode.image = UIImage().image(fromLayer: gradient)

    }

    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let titleInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 20, left: 5, bottom: 5, right: 5), child: titleTextNode)
        let anonymousInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 5, bottom: 10, right: 5), child: anonymousTextNode)
        let footerInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 5, bottom: 10, right: 5), child: countTextNode)
        let voteBtnInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0), child: voteButtonNode)
        let multipleInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0), child: multipleTextNode)

        if isAnswered == false { answerSpecArr.append(multipleInsets) }
        
        answerSpecArr.append(voteBtnInsets)
        answerSpecArr.append(footerInsets)
        
        let answersStack = ASStackLayoutSpec(direction: .vertical, spacing: 10, justifyContent: .center, alignItems: .center, children: answerSpecArr)
        
        let titleStack = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .center, alignItems: .center, children: [titleInsets, anonymousInsets])
        
        let pollStack = ASStackLayoutSpec(direction: .vertical, spacing: 2, justifyContent: .spaceBetween, alignItems: .center, children: [titleStack, answersStack])


        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: ASOverlayLayoutSpec(child: imageNode, overlay: pollStack))
    }
    
    @objc func choosingAnswer(_ sender:MyASButtonNode) {
        if let index = choosingAnswer.index(of: sender.tag) {
            sender.backgroundColor = mainColors.mainBackgroundView
            choosingAnswer.remove(at: index)
            if choosingAnswer.count == 0 { voteButtonNode.isEnabled = false }
        } else {
            sender.backgroundColor = mainColors.online
            if isMultiple == false {
                for btn in answerBtnArr {
                    if let _ = choosingAnswer.index(of: btn.tag) { btn.backgroundColor = mainColors.mainBackgroundView }
                }
                choosingAnswer.removeAll()
                
            }
            choosingAnswer.append(sender.tag)
            voteButtonNode.isEnabled = true
        }
        
    }
    
    @objc func pressVote() {
        voteButtonNode.isEnabled = false
        vkLoadData.voteInPoll(owner_id: owner_id, poll_id: poll_id, answersIDs: choosingAnswer, isDelete: isAnswered) { [weak self] (result) in
            if result == true {
                vkDataCache.updatePoll(poll_id: self?.poll_id ?? 0, answerIDs: self?.choosingAnswer ?? [], isDelete: self?.isAnswered ?? false)
                DispatchQueue.main.async { self?.delegate?.pressVote() }
            }
        }
    }
}

