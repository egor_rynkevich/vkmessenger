//
//  DocGifNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 05.09.2018.
//

import UIKit
import AsyncDisplayKit

class DocGifNode: ASCellNode {
    fileprivate let gifTextNode: ASTextNode
    fileprivate let middleDotTextNode: ASTextNode
    fileprivate let nameTextNode: ASTextNode
    fileprivate let sizeTextNode: ASTextNode
    fileprivate let imageNode: ASNetworkImageNode
    fileprivate let headerBackgroundImageNode: ASImageNode
    var img_id: Int64 = 0
    var img_url: String = ""
    var owner_id: Int64 = 0
    var haveInDB: Bool = false
    var gifUrl: String = ""
    var videoUrl: String = ""
    var gifSize: Int32 = 0
    var videoSize: Int32 = 0
    var msgStrID: String = ""
    weak var delegate: MessageCellNodeDelegate?
    
    init(doc: VKDocument?, contentWidth: Int, delegate: MessageCellNodeDelegate?) {
        gifTextNode = ASTextNode()
        sizeTextNode = ASTextNode()
        nameTextNode = ASTextNode()
        imageNode = ASNetworkImageNode()
        headerBackgroundImageNode = ASImageNode()
        middleDotTextNode = ASTextNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true
        
        guard let document = doc else { return }
        
        gifTextNode.attributedText = NSAttributedString(string: "GIF", attributes: [.font : UIFont.systemFont(ofSize: 11), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.white])
        gifTextNode.backgroundColor = UIColor.clear
        gifTextNode.placeholderColor = mainColors.lightGray
        gifTextNode.maximumNumberOfLines = 1
        
        middleDotTextNode.attributedText = NSAttributedString(string: "•", attributes: [.font : UIFont.systemFont(ofSize: 9), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.white])
        middleDotTextNode.backgroundColor = UIColor.clear
        middleDotTextNode.placeholderColor = mainColors.lightGray
        middleDotTextNode.maximumNumberOfLines = 1
        
        let gifTxtWdth = gifTextNode.calculateSizeThatFits(CGSize(width: contentWidth - 50, height: 20)).width
        
        sizeTextNode.attributedText = NSAttributedString(string: ByteCountFormatter.string(fromByteCount: Int64(document.size), countStyle: .file), attributes: [.font : UIFont.systemFont(ofSize: 11), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.white])
        sizeTextNode.backgroundColor = UIColor.clear
        sizeTextNode.placeholderColor = mainColors.lightGray
        sizeTextNode.maximumNumberOfLines = 1
        sizeTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 10))
        
        let sizeTxtWdth = sizeTextNode.calculateSizeThatFits(CGSize(width: contentWidth - 50, height: 20)).width
        
        nameTextNode.attributedText = NSAttributedString(string: document.title, attributes: [.font : UIFont.systemFont(ofSize: 11), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.white])
        nameTextNode.backgroundColor = UIColor.clear
        nameTextNode.placeholderColor = mainColors.lightGray
        nameTextNode.maximumNumberOfLines = 1
        nameTextNode.truncationMode = .byTruncatingMiddle
        nameTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 30) - sizeTxtWdth - gifTxtWdth)
        nameTextNode.style.minWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 30) - sizeTxtWdth - gifTxtWdth)
        
        headerBackgroundImageNode.cornerRadius = 7
        headerBackgroundImageNode.contentMode = .scaleAspectFill
        headerBackgroundImageNode.backgroundColor = mainColors.mainBackgroundView
        
        img_id = document.id
        img_url = document.photoDocSizeMid?.url ?? document.photoDocSizeMax?.url ?? document.photoDocSizeMin?.url ?? ""
        owner_id = document.owner_id
        gifUrl = document.url
        videoUrl = document.gifVideoPreviewUrl
        gifSize = document.size
        videoSize = document.gifVideoPreviewSize
        msgStrID = document.prntID
        
        imageNode.style.preferredSize = CGSize(width: contentWidth-10, height: 150)
        imageNode.cornerRadius = 7
        
        let data = vkDataCache.getDataFromCachedData(sourceID: img_id, ownerID: owner_id, type: "image", tag: "doc_image")
        
        if data == nil { imageNode.url = NSURL(string: img_url)! as URL }
        else { imageNode.image = UIImage(data: data! as Data) }
        
        imageNode.imageModificationBlock = { [weak self] image in
            if vkDataCache.getDataFromCachedData(sourceID: self?.img_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "doc_image") == nil {
                vkDataCache.saveDataInCachedData(sourceID: self?.img_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "doc_image", url: self?.img_url ?? "", image: image)
            }
            return image
        }
        
        imageNode.addTarget(self, action: #selector(openGif), forControlEvents: .touchUpInside)
        
        addSubnode(sizeTextNode)
        addSubnode(nameTextNode)
        addSubnode(imageNode)
        addSubnode(headerBackgroundImageNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let headerStack = ASStackLayoutSpec(direction: .horizontal, spacing: 2, justifyContent: .start, alignItems: .center, children: [gifTextNode, middleDotTextNode, nameTextNode, sizeTextNode])
        let headerStackInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5), child: headerStack)
        let headerBack = ASBackgroundLayoutSpec(child: headerStackInsets, background: headerBackgroundImageNode)
        let headerBackInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: CGFloat.infinity, right: 0), child: headerBack)
        let finalSpec = ASOverlayLayoutSpec(child: imageNode, overlay: headerBackInsets)
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: finalSpec)
    }
    
    @objc func openGif() {
        delegate?.openGif(gifUrl: gifUrl, gifSize: gifSize, videoUrl: videoUrl, videoSize: videoSize, msgStrID: msgStrID)
    }
}
