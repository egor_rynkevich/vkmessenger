//
//  PrettyCardFullNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 12.09.2018.
//

import UIKit
import AsyncDisplayKit
import RealmSwift

class PrettyCardFullNode: ASCellNode, ASCollectionDelegate, ASCollectionDataSource {
    fileprivate let cardsCollectionNode: ASCollectionNode
    weak var delegate: MessageCellNodeDelegate?
    var cardList: [VKCards] = []
    var contentWidth: Int = 0
    
    init(wallSrc: VKWall?, contentWidth: Int, delegate: MessageCellNodeDelegate?) {
        self.contentWidth = contentWidth
        self.delegate = delegate
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        
        cardsCollectionNode = ASCollectionNode(collectionViewLayout: flowLayout)

        super.init()
        
        automaticallyManagesSubnodes = true
        
        guard let wall = wallSrc else { return }
        
        for att in wall.attachments {
            if att.type == "pretty_cards" {
                for card in att.pretty_cards {
                    let cardCopy = VKCards()
                    cardCopy.copyValue(card)
                    cardList.append(cardCopy)
                }
            }
        }
        
        cardsCollectionNode.backgroundColor = mainColors.mainTableView
        cardsCollectionNode.style.preferredSize = CGSize(width: contentWidth, height: 200)
        
        addSubnode(cardsCollectionNode)
        
        cardsCollectionNode.delegate = self
        cardsCollectionNode.dataSource = self
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: cardsCollectionNode)
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        return cardList.count
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        return {
            let node = PrettyCardNode(cardSrc: self.cardList[indexPath.row], contentWidth: self.contentWidth, delegate: self.delegate)
            return node
        }
    }
}

