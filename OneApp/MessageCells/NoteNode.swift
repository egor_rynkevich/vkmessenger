//
//  NoteNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 11.09.2018.
//

import UIKit
import AsyncDisplayKit

class NoteNode: ASCellNode {
    
    fileprivate let titleTextNode: ASTextNode
    fileprivate let noteTextNode: ASTextNode
    fileprivate let noteImageNode: ASImageNode
    weak var delegate: MessageCellNodeDelegate?
    var url: String = ""
    var msgStrID: String = ""
    
    init(noteSrc: VKNote?, contentWidth: Int, delegate: MessageCellNodeDelegate?, msgColor: MessageColor) {
        titleTextNode = ASTextNode()
        noteTextNode = ASTextNode()
        noteImageNode = ASImageNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true
        
        guard let node = noteSrc else { return }
        
        titleTextNode.attributedText = NSAttributedString(string: node.title.count > 0 ? node.title : "...", attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: mainColors.text!])
        titleTextNode.truncationMode = .byTruncatingTail
        titleTextNode.truncationAttributedText = NSAttributedString(string: "...", attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: msgColor.messageText!])
        titleTextNode.backgroundColor = UIColor.clear
        titleTextNode.placeholderColor = mainColors.lightGray
        titleTextNode.maximumNumberOfLines = 2
        titleTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 50))
        
        noteTextNode.attributedText = NSAttributedString(string: "Note", attributes: [.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: msgColor.messageSubText!])
        noteTextNode.placeholderEnabled = true
        noteTextNode.backgroundColor = UIColor.clear
        noteTextNode.placeholderColor = mainColors.lightGray
        noteTextNode.maximumNumberOfLines = 1
        
        noteImageNode.style.preferredSize = CGSize(width: 40, height: 40)
        noteImageNode.image = mainIcons.note
        noteImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(mainColors.writeRow!)
        
        url = node.view_url
        msgStrID = node.prntID
        
        titleTextNode.addTarget(self, action: #selector(openURL), forControlEvents: .touchUpInside)
        noteTextNode.addTarget(self, action: #selector(openURL), forControlEvents: .touchUpInside)
        noteImageNode.addTarget(self, action: #selector(openURL), forControlEvents: .touchUpInside)
        
        addSubnode(titleTextNode)
        addSubnode(noteTextNode)
        addSubnode(noteImageNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let noteVStackSpec = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .start, alignItems: .start, children: [titleTextNode, noteTextNode])
        
        let finalStack = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .start, alignItems: .center, children: [noteImageNode, noteVStackSpec])
        let finalInsetSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: finalStack)
        return finalInsetSpec
    }
    
    @objc func openURL() {
        delegate?.openURL(url, msgStrID: msgStrID)
    }
}
