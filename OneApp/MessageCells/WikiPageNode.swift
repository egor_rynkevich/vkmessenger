//
//  WikiPageNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 11.09.2018.
//

import UIKit
import AsyncDisplayKit

class WikiPageNode: ASCellNode {
    
    fileprivate let titleTextNode: ASTextNode
    fileprivate let pageTextNode: ASTextNode
    fileprivate let pageImageNode: ASImageNode
    weak var delegate: MessageCellNodeDelegate?
    var url: String = ""
    var msgStrID: String = ""
    
    init(pageSrc: VKPage?, contentWidth: Int, delegate: MessageCellNodeDelegate?, msgColor: MessageColor) {
        titleTextNode = ASTextNode()
        pageTextNode = ASTextNode()
        pageImageNode = ASImageNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true
        
        guard let page = pageSrc else { return }
        
        titleTextNode.attributedText = NSAttributedString(string: page.title.count > 0 ? page.title : "...", attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: msgColor.messageText!])
        titleTextNode.truncationMode = .byTruncatingTail
        titleTextNode.truncationAttributedText = NSAttributedString(string: "...", attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: msgColor.messageText!])
        titleTextNode.backgroundColor = UIColor.clear
        titleTextNode.placeholderColor = mainColors.lightGray
        titleTextNode.maximumNumberOfLines = 2
        titleTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 50))
        
        pageTextNode.attributedText = NSAttributedString(string: "Page", attributes: [.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: msgColor.messageSubText!])
        pageTextNode.placeholderEnabled = true
        pageTextNode.backgroundColor = UIColor.clear
        pageTextNode.placeholderColor = mainColors.lightGray
        pageTextNode.maximumNumberOfLines = 1
        
        pageImageNode.style.preferredSize = CGSize(width: 40, height: 40)
        pageImageNode.image = mainIcons.arrowPage
        pageImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(mainColors.writeRow!)
        
        url = page.view_url
        msgStrID = page.prntID
        
        titleTextNode.addTarget(self, action: #selector(openURL), forControlEvents: .touchUpInside)
        pageTextNode.addTarget(self, action: #selector(openURL), forControlEvents: .touchUpInside)
        pageImageNode.addTarget(self, action: #selector(openURL), forControlEvents: .touchUpInside)
        
        addSubnode(titleTextNode)
        addSubnode(pageTextNode)
        addSubnode(pageImageNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let wallVStackSpec = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .start, alignItems: .start, children: [titleTextNode, pageTextNode])
        
        let finalStack = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .start, alignItems: .center, children: [pageImageNode, wallVStackSpec])
        let finalInsetSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: finalStack)
        return finalInsetSpec
    }
    
    @objc func openURL() {
        delegate?.openURL(url, msgStrID: msgStrID)
    }
}

