//
//  WallNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 24.08.2018.
//
import UIKit
import AsyncDisplayKit

class WallNode: ASCellNode {
    
    fileprivate let titleTextNode: ASTextNode
    fileprivate let wallTextNode: ASTextNode
    fileprivate let wallImageNode: ASImageNode
    weak var delegate: MessageCellNodeDelegate?
    
    
    init(wall: VKWall?, contentWidth: Int, delegate: MessageCellNodeDelegate?, msgColor: MessageColor) {
        titleTextNode = ASTextNode()
        wallTextNode = ASTextNode()
        wallImageNode = ASImageNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true
        
        var text = wall!.text.count > 0 ? wall!.text : "..."
        
        if text != "..." { text = String(text.filter { !"\n\t\r".contains($0) }) }
        
        titleTextNode.attributedText = NSAttributedString(string: text, attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: msgColor.messageText!])
        titleTextNode.truncationMode = .byTruncatingTail
        //titleTextNode.truncationAttributedText = NSAttributedString(string: "...", attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedStringKey.strokeColor: mainColors.text!, .foregroundColor: mainColors.text!])
        titleTextNode.backgroundColor = UIColor.clear
        titleTextNode.placeholderColor = mainColors.lightGray
        titleTextNode.maximumNumberOfLines = 1
        titleTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 88))
        
        wallTextNode.attributedText = NSAttributedString(string: "Post", attributes: [.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: msgColor.messageSubText!])
        wallTextNode.placeholderEnabled = true
        wallTextNode.backgroundColor = UIColor.clear
        wallTextNode.placeholderColor = mainColors.lightGray
        wallTextNode.maximumNumberOfLines = 1
        
        wallImageNode.image = mainIcons.wall_s40
        wallImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(msgColor.messageIcon!)
        wallImageNode.cornerRadius = 5
        wallImageNode.borderWidth = 2
        wallImageNode.borderColor = msgColor.messageAditionalIcon?.cgColor
        
        addSubnode(titleTextNode)
        addSubnode(wallTextNode)
        addSubnode(wallImageNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let wallVStackSpec = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .start, alignItems: .start, children: [titleTextNode, wallTextNode])

        let finalStack = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .start, alignItems: .center, children: [ASInsetLayoutSpec(insets: UIEdgeInsets(top: 7, left: 9, bottom: 7, right: 9), child: wallImageNode), wallVStackSpec])
        let finalInsetSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 9), child: finalStack)
        return finalInsetSpec
    }
}
