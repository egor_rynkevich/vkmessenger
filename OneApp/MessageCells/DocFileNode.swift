//
//  DocFileNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 02.09.2018.
//

import UIKit
import AsyncDisplayKit

class DocFileNode: ASCellNode {
    
    fileprivate let fileNameTextNode: ASTextNode
    fileprivate let extTextNode: ASTextNode
    fileprivate let sizeTextNode: ASTextNode
    fileprivate let fileImageNode: ASImageNode
    
    fileprivate let imageNode: ASNetworkImageNode
    fileprivate let headerBackgroundImageNode: ASImageNode
    var img_id: Int64 = 0
    var img_url: String = ""
    var owner_id: Int64 = 0
    var msgStrID: String = ""
    
    weak var delegate: MessageCellNodeDelegate?
    var url: String = ""
    
    init(doc: VKDocument, contentWidth: Int, delegate: MessageCellNodeDelegate?, msgColor: MessageColor) {
        fileNameTextNode = ASTextNode()
        extTextNode = ASTextNode()
        sizeTextNode = ASTextNode()
        fileImageNode = ASImageNode()
        imageNode = ASNetworkImageNode()
        headerBackgroundImageNode = ASImageNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true
        
        url = doc.url
        
        msgStrID = doc.prntID
        
        img_id = doc.id
        img_url =  doc.photoDocSizeMid?.url ?? doc.photoDocSizeMax?.url ?? doc.photoDocSizeMin?.url ?? ""
        owner_id = doc.owner_id
        
        if img_url == "" {
            fileImageNode.image = mainIcons.file_s40
            fileImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(msgColor.messageSubText!)
            
            fileNameTextNode.attributedText = NSAttributedString(string: doc.title.count > 0 ? doc.title : "...", attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: msgColor.messageText!])
            fileNameTextNode.truncationMode = .byTruncatingTail
            fileNameTextNode.backgroundColor = UIColor.clear
            fileNameTextNode.placeholderColor = mainColors.lightGray
            fileNameTextNode.maximumNumberOfLines = 1
            fileNameTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 60))
            
            extTextNode.attributedText = NSAttributedString(string: doc.ext.uppercased(), attributes: [.font : UIFont.systemFont(ofSize: 8), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: msgColor.messageIcon!])
            extTextNode.placeholderEnabled = true
            extTextNode.backgroundColor = UIColor.clear
            extTextNode.placeholderColor = mainColors.lightGray
            extTextNode.cornerRadius = 3
            extTextNode.borderWidth = 1
            extTextNode.borderColor = msgColor.messageIcon?.cgColor
            extTextNode.textContainerInset = UIEdgeInsets(top: 2, left: 3, bottom: 2, right: 3)
            
            sizeTextNode.attributedText = NSAttributedString(string: ByteCountFormatter.string(fromByteCount: Int64(doc.size), countStyle: .file), attributes: [.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: msgColor.messageSubText!])
            sizeTextNode.placeholderEnabled = true
            sizeTextNode.backgroundColor = UIColor.clear
            sizeTextNode.placeholderColor = mainColors.lightGray
        } else {
            headerBackgroundImageNode.cornerRadius = 7
            headerBackgroundImageNode.contentMode = .scaleAspectFill
            headerBackgroundImageNode.backgroundColor = mainColors.mainBackgroundView
            imageNode.style.preferredSize = CGSize(width: contentWidth-10, height: 150)
            imageNode.cornerRadius = 7
            
            sizeTextNode.attributedText = NSAttributedString(string: ByteCountFormatter.string(fromByteCount: Int64(doc.size), countStyle: .file), attributes: [.font : UIFont.systemFont(ofSize: 11), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.white])
            sizeTextNode.backgroundColor = UIColor.clear
            sizeTextNode.placeholderColor = mainColors.lightGray
            sizeTextNode.maximumNumberOfLines = 1
            sizeTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 10))
            
            let sizeTxtWdth = sizeTextNode.calculateSizeThatFits(CGSize(width: contentWidth - 50, height: 20)).width
            
            fileNameTextNode.attributedText = NSAttributedString(string: doc.title, attributes: [.font : UIFont.systemFont(ofSize: 11), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.white])
            fileNameTextNode.backgroundColor = UIColor.clear
            fileNameTextNode.placeholderColor = mainColors.lightGray
            fileNameTextNode.maximumNumberOfLines = 1
            fileNameTextNode.truncationMode = .byTruncatingMiddle
            fileNameTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 27) - sizeTxtWdth)
            fileNameTextNode.style.minWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 27) - sizeTxtWdth)
            
            let data = vkDataCache.getDataFromCachedData(sourceID: img_id, ownerID: owner_id, type: "image", tag: "doc_image")
            
            if data == nil { imageNode.url = NSURL(string: img_url)! as URL }
            else { imageNode.image = UIImage(data: data! as Data) }
            
            imageNode.imageModificationBlock = { [weak self] image in
                if vkDataCache.getDataFromCachedData(sourceID: self?.img_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "doc_image") == nil {
                    vkDataCache.saveDataInCachedData(sourceID: self?.img_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "doc_image", url: self?.img_url ?? "", image: image)
                }
                return image
            }
            
            imageNode.addTarget(self, action: #selector(openURL), forControlEvents: .touchUpInside)
        }
        
        fileImageNode.addTarget(self, action: #selector(openURL), forControlEvents: .touchUpInside)
        fileNameTextNode.addTarget(self, action: #selector(openURL), forControlEvents: .touchUpInside)
        extTextNode.addTarget(self, action: #selector(openURL), forControlEvents: .touchUpInside)
        
        addSubnode(fileNameTextNode)
        addSubnode(extTextNode)
        addSubnode(fileImageNode)
        addSubnode(sizeTextNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        if img_url == "" {
            let titleVStackSpec = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .spaceBetween, alignItems: .start, children: [fileNameTextNode, sizeTextNode])
            
            extTextNode.style.layoutPosition = CGPoint(x: 0, y: 17)
            fileImageNode.style.layoutPosition = CGPoint(x: 0, y: 0)
            
            let absoluteSpec = ASAbsoluteLayoutSpec(children: [fileImageNode, extTextNode])
            absoluteSpec.sizing = .sizeToFit
            
            let finalStack = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .start, alignItems: .center, children: [absoluteSpec, titleVStackSpec])
            
            return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 10, left: 0, bottom: 5, right: 0), child: finalStack)
        } else {
            let headerStack = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .start, alignItems: .center, children: [fileNameTextNode, sizeTextNode])
            let headerStackInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 2, left: 7, bottom: 2, right: 5), child: headerStack)
            let headerBack = ASBackgroundLayoutSpec(child: headerStackInsets, background: headerBackgroundImageNode)
            let headerBackInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: CGFloat.infinity, right: 0), child: headerBack)
            let finalSpec = ASOverlayLayoutSpec(child: imageNode, overlay: headerBackInsets)
            return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: finalSpec)
        }
    }
    
    @objc func openURL() {
        delegate?.openURL(url, msgStrID: msgStrID)
    }
}
