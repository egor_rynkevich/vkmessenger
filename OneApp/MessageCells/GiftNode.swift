//
//  GiftNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 06.09.2018.
//

import UIKit
import AsyncDisplayKit

class GiftNode: ASCellNode {
    
    fileprivate let giftTextNode: ASTextNode
    fileprivate let giftImageNode: ASImageNode
    fileprivate let imageNode: ASNetworkImageNode
    var photo_id: Int64 = 0
    var owner_id: Int64 = 0
    var photo_url: String = ""
    weak var delegate: MessageCellNodeDelegate?
    
    init(sourceGift: VKGift?, contentWidth: Int, delegate: MessageCellNodeDelegate?) {
        giftTextNode = ASTextNode()
        giftImageNode = ASImageNode()
        imageNode = ASNetworkImageNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true
        
        guard let gift = sourceGift else { return }
        
        photo_id = gift.id
        owner_id = gift.id
        photo_url = gift.thumb_256
        
        giftTextNode.attributedText = NSAttributedString(string: "Gift", attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: mainColors.darkGray!])
        giftTextNode.backgroundColor = UIColor.clear
        giftTextNode.placeholderColor = mainColors.lightGray
        
        giftImageNode.image = mainIcons.gift
        giftImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(mainColors.darkGray!)
        giftImageNode.style.preferredSize = CGSize(width: 12, height: 12)
        
        imageNode.style.preferredSize = CGSize(width: contentWidth - 10, height: contentWidth - 10)
        
        let data = vkDataCache.getDataFromCachedData(sourceID: photo_id, ownerID: owner_id, type: "image", tag: "gift")
        
        if data == nil { imageNode.url = NSURL(string: photo_url)! as URL }
        else { imageNode.image = UIImage(data: data! as Data) }
        
        imageNode.imageModificationBlock = { [weak self] image in
            if vkDataCache.getDataFromCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "gift") == nil {
                vkDataCache.saveDataInCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "gift", url: self?.photo_url ?? "", image: image)
            }
            return image
        }
        imageNode.cornerRadius = 15
        addSubnode(giftTextNode)
        addSubnode(giftImageNode)
        addSubnode(imageNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let horStack = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .start, alignItems: .center, children: [giftImageNode, giftTextNode])
        
        
        let vStack = ASStackLayoutSpec(direction: .vertical, spacing: 5, justifyContent: .center, alignItems: .center, children: [imageNode, horStack])

        let finalInsetSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: vStack)
        return finalInsetSpec
    }
}
