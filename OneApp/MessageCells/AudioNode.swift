//
//  AudioNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 26.08.2018.
//

import UIKit
import AsyncDisplayKit

class AudioNode: ASCellNode {
    fileprivate let artistTextNode: ASTextNode
    fileprivate let titleTextNode: ASTextNode
    fileprivate let durationTextNode: ASTextNode
    fileprivate let playBtnNode: ASButtonNode
    fileprivate let textWidth: CGFloat
    weak var delegate: MessageCellNodeDelegate?
    
    init(audio: VKAudio?, contentWidth: Int, delegate: MessageCellNodeDelegate?, msgColor: MessageColor) {
        titleTextNode = ASTextNode()
        artistTextNode = ASTextNode()
        durationTextNode = ASTextNode()
        playBtnNode = ASButtonNode()
        textWidth = CGFloat(contentWidth - 76)
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true
        
        artistTextNode.attributedText = NSAttributedString(string: audio!.artist.count > 0 ? audio!.artist : "...", attributes: [.font : UIFont.systemFont(ofSize: 15), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: msgColor.messageText!])
        artistTextNode.truncationMode = .byTruncatingTail
        artistTextNode.backgroundColor = UIColor.clear
        artistTextNode.placeholderColor = mainColors.lightGray
        artistTextNode.maximumNumberOfLines = 1
        artistTextNode.style.maxWidth = ASDimension(unit: .points, value: textWidth)
        
        durationTextNode.attributedText = NSAttributedString(string: " / " + Helper.getDurationString(audio!.duration), attributes: [.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.strokeColor: mainColors.lightGray!, .foregroundColor: msgColor.messageSubText!])
        durationTextNode.backgroundColor = UIColor.clear
        durationTextNode.placeholderColor = mainColors.lightGray
        durationTextNode.maximumNumberOfLines = 1
        
        let durTextWidth = durationTextNode.calculateSizeThatFits(CGSize(width: textWidth, height: 20)).width
        
        titleTextNode.attributedText = NSAttributedString(string: audio!.title.count > 0 ? audio!.title : "...", attributes: [.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.strokeColor: mainColors.lightGray!, .foregroundColor: msgColor.messageSubText!])
        titleTextNode.truncationMode = .byTruncatingTail
        titleTextNode.backgroundColor = UIColor.clear
        titleTextNode.placeholderColor = mainColors.lightGray
        titleTextNode.maximumNumberOfLines = 1
        titleTextNode.style.maxWidth = ASDimension(unit: .points, value: textWidth - durTextWidth - 12)
        
        
        
        playBtnNode.setImage(mainIcons.play_s10, for: .normal)
        playBtnNode.cornerRadius = 18
        playBtnNode.style.preferredSize = CGSize(width: 36, height: 36)
        playBtnNode.contentEdgeInsets = UIEdgeInsets(top: 0, left: 2, bottom: 0, right: 0)
        playBtnNode.borderWidth = 2
        playBtnNode.borderColor = msgColor.messageAditionalIcon?.cgColor
        playBtnNode.imageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(msgColor.messageIcon!)
        playBtnNode.isUserInteractionEnabled = true
        playBtnNode.addTarget(self, action: #selector(pressBtn), forControlEvents: .touchUpInside)
        
        
        addSubnode(titleTextNode)
        addSubnode(artistTextNode)
        addSubnode(durationTextNode)
        addSubnode(playBtnNode)
        addSubnode(durationTextNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let titleDurStack = ASStackLayoutSpec(direction: .horizontal, spacing: 0, justifyContent: .spaceBetween, alignItems: .notSet, children: [titleTextNode, durationTextNode])
        let titleVStackSpec = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .start, alignItems: .start, children: [artistTextNode, titleDurStack])
        
        let finalStack = ASStackLayoutSpec(direction: .horizontal, spacing: 14, justifyContent: .start, alignItems: .center, children: [ASInsetLayoutSpec(insets: UIEdgeInsets(top: 7, left: 9, bottom: 7, right: 0), child: playBtnNode), titleVStackSpec])
        let final = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 9), child: finalStack)
        return final
    }
    
    @objc func pressBtn() {
        print("pressed Button")
    }
}
