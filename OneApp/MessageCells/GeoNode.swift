//
//  GeoNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 26.08.2018.
//

import UIKit
import AsyncDisplayKit

class GeoNode: ASCellNode {
    
    fileprivate let mapNode: ASMapNode
    var lat: Double = 0.0
    var long: Double = 0.0
    var msgStrID: String = ""
    weak var delegate: MessageCellNodeDelegate?
    
    init(geo: VKGeo?, contentWidth: Int, delegate: MessageCellNodeDelegate?) {
        mapNode = ASMapNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true
        
        if geo != nil {
            mapNode.style.preferredSize = CGSize(width: contentWidth, height: 160)
            lat = Double(geo!.coordinates_latitude)
            long = Double(geo!.coordinates_longitude)
            msgStrID = geo!.prntID
            let location = CLLocationCoordinate2DMake(lat, long)
            let region = MKCoordinateRegion(center: location, span: MKCoordinateSpan(latitudeDelta: 0.009, longitudeDelta: 0.009))
            mapNode.region = region
            mapNode.cornerRadius = 9
            let annotation = MKPointAnnotation()
            annotation.coordinate = location
            mapNode.annotations.append(annotation)
            mapNode.addTarget(self, action: #selector(openMap), forControlEvents: .touchUpInside)

            addSubnode(mapNode)
        }
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let geoSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: mapNode)
        return geoSpec
    }
    
    @objc func openMap() {
        delegate?.openMap(lat: lat, long: long, msgStrID: msgStrID)
    }
}
