//
//  VideoNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 06.09.2018.
//

import UIKit
import AsyncDisplayKit

class VideoNode: ASCellNode {
    fileprivate let playImageNode: ASImageNode
    fileprivate let durationTextNode: ASTextNode
    let imageNode: ASNetworkImageNode
    fileprivate let headerBackgroundImageNode: ASImageNode
    var img_id: Int64 = 0
    var img_url: String = ""
    var owner_id: Int64 = 0
    var backRightInsets: CGFloat = 0
    weak var delegate: MessageCellNodeDelegate?
    
    init(videoSrc: VKVideo?, contentWidth: Int, delegate: MessageCellNodeDelegate?) {
        durationTextNode = ASTextNode()
        playImageNode = ASImageNode()
        imageNode = ASNetworkImageNode()
        headerBackgroundImageNode = ASImageNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true
        
        guard let video = videoSrc else { return }
        
        durationTextNode.attributedText = NSAttributedString(string: Helper.getDurationString(video.duration), attributes: [.font : UIFont.systemFont(ofSize: 11), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.white])
        durationTextNode.backgroundColor = UIColor.clear
        durationTextNode.placeholderColor = mainColors.lightGray
        durationTextNode.maximumNumberOfLines = 1
        
        let durationTxtWdth = durationTextNode.calculateSizeThatFits(CGSize(width: contentWidth - 50, height: 20)).width
        
        headerBackgroundImageNode.cornerRadius = 9
        headerBackgroundImageNode.contentMode = .scaleAspectFill
        headerBackgroundImageNode.backgroundColor = mainColors.mainBackgroundView
        
        backRightInsets = CGFloat(contentWidth) - durationTxtWdth - 38
        
        playImageNode.image = mainIcons.play
        playImageNode.style.preferredSize = CGSize(width: 12, height: 12)
        playImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColor.white)
        
        img_id = video.id
        img_url = video.photo_320 == "" ? video.photo_130 : video.photo_320
        owner_id = video.owner_id
        
        imageNode.style.preferredSize = CGSize(width: contentWidth-10, height: 150)
        imageNode.cornerRadius = 7
        
        let data = vkDataCache.getDataFromCachedData(sourceID: img_id, ownerID: owner_id, type: "image", tag: "video")
        
        if data == nil { imageNode.url = NSURL(string: img_url)! as URL }
        else { imageNode.image = UIImage(data: data! as Data) }
        
        imageNode.imageModificationBlock = { [weak self] image in
            if vkDataCache.getDataFromCachedData(sourceID: self?.img_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "video") == nil {
                vkDataCache.saveDataInCachedData(sourceID: self?.img_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "video", url: self?.img_url ?? "", image: image)
            }
            return image
        }
        
        
        addSubnode(playImageNode)
        addSubnode(durationTextNode)
        addSubnode(imageNode)
        addSubnode(headerBackgroundImageNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let headerStack = ASStackLayoutSpec(direction: .horizontal, spacing: 2, justifyContent: .start, alignItems: .center, children: [playImageNode, durationTextNode])
        let headerStackInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5), child: headerStack)
        let headerBack = ASBackgroundLayoutSpec(child: headerStackInsets, background: headerBackgroundImageNode)
        let headerBackInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 3, left: 3, bottom: CGFloat.infinity, right: backRightInsets), child: headerBack)
        let finalSpec = ASOverlayLayoutSpec(child: imageNode, overlay: headerBackInsets)
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: finalSpec)
    }
}
