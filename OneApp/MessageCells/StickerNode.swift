//
//  StickerNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 23.08.2018.
//

import UIKit
import AsyncDisplayKit
import RealmSwift

class StickerNode: ASCellNode {

    fileprivate let imageNode: ASNetworkImageNode
    
    var sticker_id: Int64 = 0
    var sticker_url: String = ""
    var owner_id: Int64 = 0
    var height: Int = 0
    var width: Int = 0
    weak var delegate: MessageCellNodeDelegate?
    
    init(sticker: VKSticker?, delegate: MessageCellNodeDelegate?) {
        imageNode = ASNetworkImageNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true
        
        if sticker != nil {
            if sticker!.image_height_mid != 0 && sticker!.image_width_mid != 0 {
                height = sticker!.image_height_mid - 80
                width = Int(sticker!.image_width_mid * height / sticker!.image_height_mid)
            } else {
                height = 160
                width = 160
            }
            imageNode.style.preferredSize = CGSize(width: width, height: height)
            
            sticker_id = sticker!.sticker_id
            sticker_url = sticker!.image_url_mid
            owner_id = sticker!.product_id
            
            let data = vkDataCache.getDataFromCachedData(sourceID: sticker_id, ownerID: owner_id, type: "image", tag: "sticker")
            
            if data == nil { imageNode.url = NSURL(string: sticker_url)! as URL }
            else { imageNode.image = UIImage(data: data! as Data) }
            
            imageNode.imageModificationBlock = { [weak self] image in
                if vkDataCache.getDataFromCachedData(sourceID: self?.sticker_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "sticker") == nil {
                    vkDataCache.saveDataInCachedData(sourceID: self?.sticker_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "sticker", url: self?.sticker_url ?? "", image: image)
                }
                return image
            }
            
            addSubnode(imageNode)
        }
    }
    
    override func didLoad() {
        super.didLoad()
        self.imageNode.shadowColor = mainColors.deleteRow?.cgColor
        self.imageNode.shadowRadius = 50
        self.imageNode.shadowOpacity = 0.5
        self.imageNode.shadowOffset = .init(width: 10, height: 10)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let stickerSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: imageNode)
        return stickerSpec
    }
}
