//
//  AudioMsgNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 02.09.2018.
//

import UIKit
import AsyncDisplayKit
import SoundWave
import RealmSwift

class AudioMsgNode: ASCellNode, MainPlayerDelegate {
    fileprivate let durationTextNode: ASTextNode
    fileprivate let playBtnNode: ASButtonNode
    fileprivate let node: ASDisplayNode
    fileprivate var stackWidth: CGFloat
    weak var delegate: MessageCellNodeDelegate?
    var duration: Int16 = 0
    var audio_url: String = ""
    var audio_id: Int64 = 0
    var isPlaying: Bool = false
    
    init(audioMsg: VKAudioMsg?, contentWidth: Int, delegate: MessageCellNodeDelegate?, msgColor: MessageColor, waveForm: [Int]) {
        durationTextNode = ASTextNode()
        playBtnNode = ASButtonNode()
        stackWidth = CGFloat(contentWidth)
        
        node = ASDisplayNode { () -> UIView in
            let audioVis = AudioVisualizationView()
            //audioVis.frame = node.frame
            var outArr: [Float] = []
            var shortArr: [Int] = []
            let width = (Float(contentWidth - 80))
            let maxCount = Int( width / (4.5) )
            let frequency = waveForm.count / maxCount
            
            if frequency != 0 && frequency != 1 {
                for i in 0..<waveForm.count { if i % frequency == 0 { shortArr.append(waveForm[i]) } }
            }
            else {
                shortArr = waveForm
            }
            
            let maxElem = shortArr.max() ?? 1
            for elem in shortArr {
                let res = Float(elem) / Float(maxElem)
                outArr.append(res == 0 ? 0.2 : res)
            }
            
            audioVis.meteringLevelBarWidth = 1.5 //CGFloat((width / Float(outArr.count)) - 0.1)  //5.0 // width one wave
            audioVis.meteringLevelBarInterItem = 3 // width between wave
            audioVis.meteringLevelBarCornerRadius = 0.5
            
            audioVis.gradientStartColor = msgColor.messageAditionalIcon!
            audioVis.gradientEndColor = msgColor.messageIcon!
            
            audioVis.audioVisualizationMode = .read
            audioVis.backgroundColor = UIColor.clear
            audioVis.meteringLevels = outArr
            return audioVis
        }
        
        
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true
        
        guard let audio = audioMsg else { return } 
        
        duration = audio.duration
        audio_id = audio.id
        audio_url = audio.link_mp3
        
        if contentWidth > 260 { stackWidth = 260 }
        
        durationTextNode.attributedText = NSAttributedString(string: Helper.getDurationString(Int64(audio.duration)), attributes: [.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.strokeColor: mainColors.lightGray!, .foregroundColor: msgColor.messageSubText!])
        durationTextNode.backgroundColor = UIColor.clear
        durationTextNode.placeholderColor = mainColors.lightGray
        durationTextNode.maximumNumberOfLines = 1
        
        let durWidth = durationTextNode.calculateSizeThatFits(CGSize(width: stackWidth, height: 30)).width
        
        playBtnNode.setImage(mainIcons.play_s10, for: .normal)
        playBtnNode.cornerRadius = 18
        playBtnNode.style.preferredSize = CGSize(width: 36, height: 36)
        playBtnNode.contentEdgeInsets = UIEdgeInsets(top: 0, left: 2, bottom: 0, right: 0)
        playBtnNode.borderWidth = 2
        playBtnNode.borderColor = msgColor.messageAditionalIcon?.cgColor
        playBtnNode.imageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(msgColor.messageIcon!)
        playBtnNode.isUserInteractionEnabled = true
        playBtnNode.addTarget(self, action: #selector(pressBtn), forControlEvents: .touchUpInside)
        
        node.style.maxSize = CGSize(width: stackWidth - 80 - durWidth, height: 34)
        
        addSubnode(node)
        addSubnode(durationTextNode)
        addSubnode(playBtnNode)
        addSubnode(durationTextNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        //let waveStack = ASStackLayoutSpec(direction: .vertical, spacing: 2, justifyContent: .center, alignItems: .start, children: [node, durationTextNode])
        let finalStack = ASStackLayoutSpec(direction: .horizontal, spacing: 8, justifyContent: .start, alignItems: .center, children: [ASInsetLayoutSpec(insets: UIEdgeInsets(top: 7, left: 9, bottom: 7, right: 6), child: playBtnNode), node, durationTextNode])
        finalStack.style.maxWidth = ASDimension(unit: .points, value: stackWidth)
        finalStack.style.minWidth = ASDimension(unit: .points, value: stackWidth)
        let final = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: finalStack)
        return final
    }
    
    @objc func pressBtn() {
        if mainPlayer.audioMsgDelegate != nil && mainPlayer.currentAudioMsgID != audio_id {
            mainPlayer.audioMsgDelegate?.stopResetVisualization()
        }
        mainPlayer.audioMsgDelegate = self
        mainPlayer.playAudioMessage(audio_url: audio_url, audio_id: audio_id, isAudioPlaying: isPlaying)
        if isPlaying == false {
            playBtnNode.setImage(mainIcons.pause_s10, for: .normal)
            playBtnNode.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            //mainPlayer.playAudioMessage(audio_url: audio_url, audio_id: audio_id, isAudioPlaying: isPlaying)
            isPlaying = true
        } else {
            playBtnNode.setImage(mainIcons.play_s10, for: .normal)
            playBtnNode.contentEdgeInsets = UIEdgeInsets(top: 0, left: 2, bottom: 0, right: 0)
            isPlaying = false
        }
        
        
    }
    
    func startVisualization() {
        (node.view as! AudioVisualizationView).play(for: TimeInterval(duration))
    }
    
    func pauseVisualization() {
        (node.view as! AudioVisualizationView).pause()
    }
    
    func stopResetVisualization() {
        (node.view as! AudioVisualizationView).stop()
        //(node.view as! AudioVisualizationView).reset()
        playBtnNode.setImage(mainIcons.play_s10, for: .normal)
        playBtnNode.contentEdgeInsets = UIEdgeInsets(top: 0, left: 2, bottom: 0, right: 0)
        isPlaying = false
    }
}
