//
//  AlbumNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 12.09.2018.
//

import UIKit
import AsyncDisplayKit

class AlbumNode: ASCellNode {
    fileprivate let titleTextNode: ASTextNode
    fileprivate let imageNode: ASNetworkImageNode
    fileprivate let countTextNode: ASTextNode
    fileprivate let gradientNode: GradientNode
    var img_id: Int64 = 0
    var img_url: String = ""
    var owner_id: Int64 = 0
    weak var delegate: MessageCellNodeDelegate?
    
    init(albumSrc: VKAlbum?, contentWidth: Int, delegate: MessageCellNodeDelegate?) {
        titleTextNode = ASTextNode()
        imageNode = ASNetworkImageNode()
        countTextNode = ASTextNode()
        gradientNode = GradientNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true
        
        guard let album = albumSrc else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineBreakMode = .byWordWrapping
        
        titleTextNode.attributedText = NSAttributedString(string: album.title.count > 0 ? album.title : "...", attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.boldSystemFont(ofSize: 18), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: UIColor.white])
        titleTextNode.truncationMode = .byTruncatingTail
        titleTextNode.backgroundColor = UIColor.clear
        titleTextNode.placeholderColor = mainColors.lightGray
        titleTextNode.maximumNumberOfLines = 2
        titleTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 50))

        
        countTextNode.attributedText = NSAttributedString(string: "\(album.size) photo(s)", attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: UIColor.white])
        countTextNode.backgroundColor = UIColor.clear
        countTextNode.placeholderColor = mainColors.lightGray
        countTextNode.maximumNumberOfLines = 1
        
        gradientNode.isLayerBacked = true
        gradientNode.isOpaque = false
        gradientNode.cornerRadius = 10
        gradientNode.clipsToBounds = true
        
        img_id = album.thumb?.id ?? 0
        img_url = album.thumb?.photoDocSizeMid?.url ?? album.thumb?.photoDocSizeMax?.url ?? album.thumb?.photoDocSizeMin?.url ?? ""
        owner_id = album.thumb?.owner_id ?? 0
        imageNode.style.preferredSize = CGSize(width: contentWidth-10, height: 200)
        imageNode.cornerRadius = 10
        
        let data = vkDataCache.getDataFromCachedData(sourceID: img_id, ownerID: owner_id, type: "image", tag: "photo")
        
        if data == nil { imageNode.url = NSURL(string: img_url)! as URL }
        else { imageNode.image = UIImage(data: data! as Data) }
        
        imageNode.imageModificationBlock = { [weak self] image in
            if vkDataCache.getDataFromCachedData(sourceID: self?.img_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "photo") == nil {
                vkDataCache.saveDataInCachedData(sourceID: self?.img_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "photo", url: self?.img_url ?? "", image: image)
            }
            return image
        }
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let overlayImage = ASOverlayLayoutSpec(child: imageNode, overlay: gradientNode)
        
        let titleStack = ASStackLayoutSpec(direction: .vertical, spacing: 2, justifyContent: .start, alignItems: .center, children: [titleTextNode, countTextNode])

        let titleStackInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: CGFloat.infinity, left: 5, bottom: 5, right: 5), child: titleStack)

        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: ASOverlayLayoutSpec(child: overlayImage, overlay: titleStackInsets))
    }
}
