//
//  LinkFullNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 11.09.2018.
//

import UIKit
import AsyncDisplayKit

class LinkFullNode: ASCellNode {

    fileprivate let titleTextNode: ASTextNode
    fileprivate let readTextNode: ASTextNode
    fileprivate let flashImageNode: ASImageNode
    fileprivate let readBackImageNode: ASImageNode
    fileprivate let overlayBackImageNode: ASImageNode
    fileprivate let backImageNode: ASNetworkImageNode
    weak var delegate: MessageCellNodeDelegate?
    var photo_id: Int64 = 0
    var photo_url: String = ""
    var owner_id: Int64 = 0
    var url: String = ""
    var minWidth: Int = 0
    var msgStrID: String = ""

    init(linkSrc: VKLink?, contentWidth: Int, delegate: MessageCellNodeDelegate?) {
        titleTextNode = ASTextNode()
        readTextNode = ASTextNode()
        readBackImageNode = ASImageNode()
        flashImageNode = ASImageNode()
        backImageNode = ASNetworkImageNode()
        overlayBackImageNode = ASImageNode()
        self.delegate = delegate
        super.init()
        automaticallyManagesSubnodes = true

        guard let link = linkSrc else { return }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineBreakMode = .byWordWrapping
        
        titleTextNode.attributedText = NSAttributedString(string: link.title.count > 0 ? link.title : "...", attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.boldSystemFont(ofSize: 26), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: mainColors.text!])
        titleTextNode.truncationMode = .byTruncatingTail
        titleTextNode.backgroundColor = UIColor.clear
        titleTextNode.placeholderColor = mainColors.lightGray
        titleTextNode.maximumNumberOfLines = 3
        titleTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 50))

        readTextNode.attributedText = NSAttributedString(string: "Read", attributes: [.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.black])
        readTextNode.placeholderEnabled = true
        readTextNode.backgroundColor = UIColor.clear
        readTextNode.placeholderColor = mainColors.lightGray
        readTextNode.maximumNumberOfLines = 1

        flashImageNode.style.preferredSize = CGSize(width: 20, height: 20)
        flashImageNode.image = mainIcons.flash
        flashImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(mainColors.writeRow!)

        readBackImageNode.cornerRadius = 9
        readBackImageNode.backgroundColor = UIColor.white
        
        overlayBackImageNode.backgroundColor = mainColors.mainBackgroundView
        overlayBackImageNode.cornerRadius = 10

        url = link.url
        msgStrID = link.prntID
        photo_id = link.photo?.id ?? 0
        photo_url = link.photo?.photoDocSizeMid?.url ?? ""
        owner_id = link.photo?.owner_id ?? 0
        
        backImageNode.cornerRadius = 10
        backImageNode.contentMode = .scaleAspectFill

        let data = vkDataCache.getDataFromCachedData(sourceID: photo_id, ownerID: owner_id, type: "image", tag: "photo")
        
        if data == nil { backImageNode.url = NSURL(string: photo_url)! as URL }
        else { backImageNode.image = UIImage(data: data! as Data) }
        
        backImageNode.imageModificationBlock = { [weak self] image in
            if vkDataCache.getDataFromCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "photo") == nil {
                vkDataCache.saveDataInCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "photo", url: self?.photo_url ?? "", image: image)
            }
            return image
        }

        url = link.url
        minWidth = contentWidth - 10

        backImageNode.addTarget(self, action: #selector(openURL), forControlEvents: .touchUpInside)

    }

    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let readStack = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .start, alignItems: .center, children: [flashImageNode, readTextNode])
        let readStackSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5), child: readStack)
        let readBack = ASBackgroundLayoutSpec(child: readStackSpec, background: readBackImageNode)
        
        let titleStackSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 20, left: 0, bottom: 2, right: 0), child: titleTextNode)
        let readBackSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0), child: readBack)
        
        let linkVStackSpec = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .spaceBetween, alignItems: .center, children: [titleStackSpec, readBackSpec])
        linkVStackSpec.style.minWidth = ASDimension(unit: .points, value: CGFloat(minWidth))
        linkVStackSpec.style.minHeight = ASDimension(unit: .points, value: CGFloat(200))
        let backOverlay = ASOverlayLayoutSpec(child: backImageNode, overlay: overlayBackImageNode)
        let linkBackSpec = ASBackgroundLayoutSpec(child: linkVStackSpec, background: backOverlay)

        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: linkBackSpec)
    }

    @objc func openURL() {
        delegate?.openURL(url, msgStrID: msgStrID)
    }
}
