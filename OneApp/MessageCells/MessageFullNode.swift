//
//  MessageFullNode.swift
//  SocialNetworksBrowser
//
//  Created by Егор Рынкевич on 10.09.2018.
//

import UIKit
import AsyncDisplayKit
import RealmSwift

class MessageFullNode: ASCellNode {
    var msgRef: ThreadSafeReference<VKMessage>
    var msgID: Int64 = 0
    var contentWidth: Int = 0
    weak var delegate: MessageCellNodeDelegate?
    var arr: [ASLayoutSpec] = []
    fileprivate let textNode: ASTextNode
    fileprivate let profileImageNode: ASNetworkImageNode
    fileprivate let nameTextNode: ASTextNode
    fileprivate let dateTextNode: ASTextNode
    var arrCell: [ASCellNode] = []
    var arrCellType: [String] = []
    
    var img_url: String = ""
    var img_id: Int64 = 0
    
    init(msgRef: ThreadSafeReference<VKMessage>, tableWidth: CGFloat, delegate: MessageCellNodeDelegate?) {
        self.msgRef = msgRef
        self.contentWidth = Int(tableWidth) - 10
        self.delegate = delegate
        textNode = ASTextNode()
        nameTextNode = ASTextNode()
        profileImageNode = ASNetworkImageNode()
        dateTextNode = ASTextNode()
        super.init()
        
        selectionStyle = .none
        
        let realm = try! Realm()
        realm.refresh()
        if let msg = realm.resolve(msgRef) {
            automaticallyManagesSubnodes = true
            clipsToBounds = true
            msgID = msg.id
            
            //var textSize: CGFloat = 14
            //            if msg.text.containsEmoji == true && msg.text.stringByRemovingEmoji().trimmingCharacters(in: .whitespacesAndNewlines).count == 0 { textSize = 40 }
            
            let msgColor = mainColors.getMessTextColor(mainColors.darkMode)
            
            textNode.attributedText = NSAttributedString(string: msg.text, attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: msgColor.messageText!])
            textNode.placeholderEnabled = true
            textNode.backgroundColor = UIColor.clear
            textNode.placeholderFadeDuration = 0.15
            textNode.placeholderColor = mainColors.lightGray
            
            addSubnode(textNode)
            
            generateNeedCells(msg, msgColor)
            
            var name = ""
            
            if msg.from_id > 0 {
                let user = realm.objects(VKUser.self).filter("id = %@", msg.from_id).first
                img_id = user?.id ?? 0
                img_url = user?.photo_100 ?? ""
                name = vkDataCache.getUsernameByIDAndType(id: img_id, type: VKUserType.isUser.rawValue)
            } else {
                let group = realm.objects(VKGroup.self).filter("id = %@", msg.from_id).first
                img_id = group?.id ?? 0
                img_url = group?.photo_100 ?? ""
                name = vkDataCache.getUsernameByIDAndType(id: img_id, type: VKUserType.isGroup.rawValue)
            }
            
            let data = vkDataCache.getDataFromCachedData(sourceID: img_id, ownerID: img_id, type: "image", tag: "ava")
            
            if data == nil { profileImageNode.url = NSURL(string: img_url)! as URL }
            else { profileImageNode.image = UIImage(data: data! as Data) }
            
            profileImageNode.imageModificationBlock = { [weak self] image in
                if vkDataCache.getDataFromCachedData(sourceID: self?.img_id ?? 0, ownerID: self?.img_id ?? 0, type: "image", tag: "ava") == nil {
                    vkDataCache.saveDataInCachedData(sourceID: self?.img_id ?? 0, ownerID: self?.img_id ?? 0, type: "image", tag: "ava", url: self?.img_url ?? "", image: image)
                }
                return image
            }
            
            profileImageNode.style.preferredSize = CGSize(width: 40, height: 40)
            profileImageNode.cornerRadius = 20.0
            
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "E, d MMM yyyy, h:mm:ss a"
            let timeStr = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(msg.date)))
            dateTextNode.attributedText = NSAttributedString(string: timeStr, attributes: [.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: mainColors.lightGray!])
            dateTextNode.placeholderEnabled = true
            dateTextNode.placeholderFadeDuration = 0.15
            dateTextNode.placeholderColor = mainColors.lightGray
            
            var nameColor: UIColor? = mainColors.text
            
            if msg.from_id > 0 && msg.from_id < 2_000_000_000 {
                nameTextNode.addTarget(self, action: #selector(openProfile), forControlEvents: .touchUpInside)
                nameColor = mainColors.writeRow
            }
            
            nameTextNode.attributedText = NSAttributedString(string: name, attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: nameColor!])
            nameTextNode.placeholderEnabled = true
            nameTextNode.backgroundColor = UIColor.clear
            nameTextNode.placeholderFadeDuration = 0.15
            nameTextNode.placeholderColor = mainColors.lightGray
        }
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        arr = generateNeedSpecLayout(constrainedSize)
        
        if (textNode.attributedText?.length ?? 0) > 0 {
            let textSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 3, left: 3, bottom: 0, right: 3) , child: textNode)
            arr.insert(textSpec, at: 0)
        }
        
        let verticalStackSpec = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .start, alignItems: .start, children: arr)
        
        arr.removeAll()
        
        let profileVStackSpec = ASStackLayoutSpec(direction: .vertical, spacing: 1, justifyContent: .start, alignItems: .start, children: [nameTextNode, dateTextNode])
        
        let profileStack = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .start, alignItems: .center, children: [profileImageNode, profileVStackSpec])
        
        verticalStackSpec.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth))
        verticalStackSpec.style.flexGrow = 1.0
        
        
        let finalStack = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .start, alignItems: .start, children: [profileStack, verticalStackSpec])
        
        let cell = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10) , child: finalStack)
        return cell
    }
    
    func generateNeedCells(_ msg: VKMessage?, _ msgColor: MessageColor) {
        if msg != nil {
            if msg!.geo != nil {
                arrCell.append(GeoNode(geo: msg!.geo, contentWidth: contentWidth, delegate: delegate))
                arrCellType.append("geo")
            }
            for attachment in msg!.attachments {
                switch attachment.type {
                case "audio":
                    arrCell.append(AudioNode(audio: attachment.audio, contentWidth: contentWidth, delegate: delegate, msgColor: msgColor))
                    arrCellType.append("audio")
                    break
                //case "photo": return [attachment.type : attachment.photo]
                case "video":
                    arrCell.append(VideoNode(videoSrc: attachment.video, contentWidth: contentWidth, delegate: delegate))
                    arrCellType.append("video")
                    break
                case "doc":
                    generateNeedDocCell(attachment.doc, msgColor)
                    break
                case "link":
                    arrCell.append(LinkNode(sourceLink: attachment.link, contentWidth: contentWidth, delegate: delegate, msgColor: msgColor))
                    arrCellType.append("link")
                    break
                case "audio_message":
                    let inArr = attachment.audio_msg?.waveform ?? List<Int>()
                    var waveForm: [Int] = []
                    for i in inArr { waveForm.append(i) }
                    
                    arrCell.append(AudioMsgNode(audioMsg: attachment.audio_msg, contentWidth: contentWidth, delegate: delegate, msgColor: msgColor, waveForm: waveForm))
                    arrCellType.append("audio_message")
                    break
                case "graffiti":
                    arrCell.append(GraffitiNode(graffitiSrc: attachment.graffiti, contentWidth: contentWidth, delegate: delegate))
                    arrCellType.append("graffiti")
                    break
                case "market":
                    arrCell.append(MarketNode(marketSource: attachment.market, contentWidth: contentWidth, delegate: delegate))
                    arrCellType.append("market")
                    break
                case "market_album":
                    arrCell.append(MarketAlbumNode(marketSource: attachment.market_album, contentWidth: contentWidth, delegate: delegate))
                    arrCellType.append("market_album")
                    break
                case "wall":
                    guard let wall = attachment.wall else { return }
                    arrCell.append(WallFullNode(wall: wall, contentWidth: contentWidth, delegate: delegate, msgColor: msgColor))
                    arrCellType.append("wall")
                    break
                case "sticker":
                    arrCell.append(StickerNode(sticker: attachment.sticker, delegate: delegate))
                    arrCellType.append("sticker")
                    break
                case "gift":
                    arrCell.append(GiftNode(sourceGift: attachment.gift, contentWidth: contentWidth, delegate: delegate))
                    arrCellType.append("gift")
                    break
                default: break
                }
            }
            
            for fwdMsg in msg!.fwd_messages {
                arrCell.append(ForwardedMessageNode(msg: fwdMsg, contentWidth: contentWidth, delegate: delegate))
                arrCellType.append("fwd_msg")
            }
        }
    }
    
    func generateNeedDocCell(_ doc: VKDocument?, _ msgColor: MessageColor) {
        guard let document = doc else { return }
        
        switch document.type {
        case 1, 2, 6, 7, 8: // 1 - text doc, 2 - zip, 6 - video, 7 - elect. book, 8 - unknown
            arrCell.append(DocFileNode(doc: document, contentWidth: contentWidth, delegate: delegate, msgColor: msgColor))
            arrCellType.append("doc")
            break
        case 3 : // gif
            arrCell.append(DocGifNode(doc: document, contentWidth: contentWidth, delegate: delegate))
            arrCellType.append("doc")
            break
        case 4: // image
            arrCell.append(DocImgNode(doc: document, contentWidth: contentWidth, delegate: delegate))
            arrCellType.append("doc")
            break
        default:
            return
        }
    }
    
    func generateNeedSpecLayout(_ constrainedSize: ASSizeRange) -> [ASLayoutSpec] {
        var arrLayoutSpec : [ASLayoutSpec] = []
        var i = 0
        
        for cellType in arrCellType {
            switch cellType {
            case "audio":
                if let elem = arrCell[i] as? AudioNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            //case "photo": return [attachment.type : attachment.photo]
            case "video":
                if let elem = arrCell[i] as? VideoNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "doc":
                if let docLayout = generateNeedDocSpecLayout(arrCell[i], constrainedSize) {
                    arrLayoutSpec.append(docLayout)
                }
                break
            case "link":
                if let elem = arrCell[i] as? LinkNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "audio_message":
                if let elem = arrCell[i] as? AudioMsgNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "graffiti":
                if let elem = arrCell[i] as? GraffitiNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "market":
                if let elem = arrCell[i] as? MarketNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "market_album":
                if let elem = arrCell[i] as? MarketAlbumNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "wall":
                if let elem = arrCell[i] as? WallFullNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "sticker":
                if let elem = arrCell[i] as? StickerNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "geo":
                if let elem = arrCell[i] as? GeoNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "gift":
                if let elem = arrCell[i] as? GiftNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "fwd_msg":
                if let elem = arrCell[i] as? ForwardedMessageNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            default: break
            }
            i += 1
        }
        
        return arrLayoutSpec
    }
    
    func generateNeedDocSpecLayout(_ cell: ASCellNode, _ constrainedSize: ASSizeRange) -> ASLayoutSpec? {
        
        if let elem = cell as? DocFileNode {
            return elem.layoutSpecThatFits(constrainedSize)
        }
        else if let elem = cell as? DocImgNode {
            return elem.layoutSpecThatFits(constrainedSize)
        }
        else if let elem = cell as? DocGifNode {
            return elem.layoutSpecThatFits(constrainedSize)
        }
        
        return nil
    }
    
    @objc func openProfile() {
        delegate?.openProfile(user_id: img_id)
    }
}
