//
//  ForwardedMessageNode.swift
//  OneApp
//
//  Created by Егор Рынкевич on 10/20/18.
//

import UIKit
import AsyncDisplayKit
import RealmSwift

class ForwardedMessageNode: ASCellNode {
    var msgID: Int64 = 0
    var peerType: String = VKUserType.isUser.rawValue
    var peerID : Int64 = 0
    var isHaveOnlyPhoto: Bool = false
    var contentWidth: Int = 0
    var contentAligment: ASStackLayoutJustifyContent = .start
    var itemsAligment: ASStackLayoutAlignItems = .start
    weak var delegate: MessageCellNodeDelegate?
    var arr: [ASLayoutSpec] = []
    fileprivate let textNode: ASTextNode
    fileprivate let dateTextNode: ASTextNode
    fileprivate let usernameTextNode: ASTextNode
    fileprivate let sepNode: ASDisplayNode
    var arrCell: [ASCellNode] = []
    var arrCellType: [String] = []
    var size_for_calc: CGSize?
    //var wallNode: WallNode?
    
    init(msg: VKMessage, contentWidth: Int, delegate: MessageCellNodeDelegate?) {
        self.contentWidth = contentWidth - 20
        self.delegate = delegate
        textNode = ASTextNode()
        dateTextNode = ASTextNode()
        usernameTextNode = ASTextNode()
        sepNode = ASDisplayNode()
        super.init()
        
        automaticallyManagesSubnodes = true
        clipsToBounds = true
        msgID = msg.id
        
        peerType = msg.peer_id < 0 ? VKUserType.isGroup.rawValue : (msg.peer_id > 2_000_000_000 ? VKUserType.isChat.rawValue : VKUserType.isUser.rawValue)
        peerID = msg.from_id
        
        sepNode.backgroundColor = mainColors.writeRow
        sepNode.cornerRadius = 1
        sepNode.style.width = ASDimension(unit: .points, value: 2)
        sepNode.style.minHeight = ASDimension(unit: .points, value: 30)
        sepNode.style.flexGrow = 1
        sepNode.style.flexShrink = 1
        
        var nameColor: UIColor? = mainColors.text
        var name = "Forwarded from "
        
        if msg.from_id > 0 {
            name += vkDataCache.getUsernameByIDAndType(id: msg.from_id, type: VKUserType.isUser.rawValue)
        } else {
            name += vkDataCache.getUsernameByIDAndType(id: msg.from_id, type: VKUserType.isGroup.rawValue)
        }
        
        if msg.from_id > 0 && msg.from_id < 2_000_000_000 {
            usernameTextNode.addTarget(self, action: #selector(openProfile), forControlEvents: .touchUpInside)
            nameColor = mainColors.writeRow
        }
        
        usernameTextNode.attributedText = NSAttributedString(string: name, attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: nameColor!])
        usernameTextNode.placeholderEnabled = true
        usernameTextNode.backgroundColor = UIColor.clear
        usernameTextNode.placeholderFadeDuration = 0.15
        usernameTextNode.placeholderColor = mainColors.lightGray
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, d MMM yyyy, h:mm:ss a"
        let date = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(msg.date)))
        
        dateTextNode.attributedText = NSAttributedString(string: date, attributes: [.font : UIFont.systemFont(ofSize: 11), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: mainColors.textFooter!])
        dateTextNode.placeholderEnabled = true
        dateTextNode.placeholderFadeDuration = 0.15
        dateTextNode.placeholderColor = mainColors.lightGray
        
        
        let msgColor = mainColors.getMessTextColor(true)
        
        let font = UIFont.preferredFont(forTextStyle: .body)
        textNode.attributedText = NSAttributedString(string: msg.text, attributes: [.font : UIFont.systemFont(ofSize: font.pointSize - 1), NSAttributedString.Key.strokeColor: mainColors.text!, .foregroundColor: msgColor.messageText!])
        
        textNode.placeholderEnabled = true
        textNode.backgroundColor = UIColor.clear
        textNode.placeholderFadeDuration = 0.15
        textNode.placeholderColor = mainColors.lightGray
        textNode.isUserInteractionEnabled = true
        textNode.style.minWidth = ASDimension(unit: .points, value: 50)
        textNode.addTarget(self, action: #selector(openMessageNode), forControlEvents: .touchUpInside)
        
        addSubnode(textNode)
        
        
        generateNeedCells(msg, msgColor)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let profileVStackSpec = ASStackLayoutSpec(direction: .vertical, spacing: 1, justifyContent: .start, alignItems: .start, children: [usernameTextNode, dateTextNode])
        
        arr = generateNeedSpecLayout(constrainedSize)
        
        if (textNode.attributedText?.length ?? 0) > 0 {
            let textSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 3, left: 3, bottom: 1, right: 3) , child: textNode)
            arr.insert(textSpec, at: (isHaveOnlyPhoto == true) ? arr.count : 0)
        }
        
        let verticalStackSpec = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .start, alignItems: .start, children: arr)
        
        arr.removeAll()
        
        verticalStackSpec.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth + 10))
        verticalStackSpec.style.flexGrow = 1.0
        
        let finalContentStack = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .start, alignItems: itemsAligment, children: [profileVStackSpec, verticalStackSpec])

        let finalStack = ASStackLayoutSpec(direction: .horizontal, spacing: 0, justifyContent: .start, alignItems: .stretch, children: [ASInsetLayoutSpec(insets: UIEdgeInsets(top: 3, left: -2, bottom: 0, right: 5) , child: sepNode), finalContentStack])
        
        //let finalBack = ASOverlayLayoutSpec(child: finalContentStack, overlay: separatorInsets)
        
        let finalStackInsets = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5) , child: finalStack)
        
        return finalStackInsets
    }
    
    func generateNeedCells(_ msg: VKMessage?, _ msgColor: MessageColor) {
        if msg != nil {
            if msg!.geo != nil {
                arrCell.append(GeoNode(geo: msg!.geo, contentWidth: contentWidth, delegate: delegate))
                arrCellType.append("geo")
            }
            
            var photoArr: [VKPhoto?] = []
            for att in msg!.attachments {
                if att.type == "photo" {
                    photoArr.append(att.photo)
                }
            }
            
            if photoArr.count > 0 {
                arrCell.append(ImageCellNode(imgArrSrc: photoArr, contentWidth: contentWidth, delegate: delegate))
                arrCellType.append("photo")
            }
            
            for attachment in msg!.attachments {
                switch attachment.type {
                case "audio":
                    arrCell.append(AudioNode(audio: attachment.audio, contentWidth: contentWidth, delegate: delegate, msgColor: msgColor))
                    arrCellType.append("audio")
                    break
                //case "photo": return [attachment.type : attachment.photo]
                case "video":
                    arrCell.append(VideoNode(videoSrc: attachment.video, contentWidth: contentWidth, delegate: delegate))
                    arrCellType.append("video")
                    break
                case "doc":
                    generateNeedDocCell(attachment.doc, msgColor)
                    break
                case "link":
                    arrCell.append(LinkNode(sourceLink: attachment.link, contentWidth: contentWidth, delegate: delegate, msgColor: msgColor))
                    arrCellType.append("link")
                    break
                case "audio_message":
                    let inArr = attachment.audio_msg?.waveform ?? List<Int>()
                    var waveForm: [Int] = []
                    for i in inArr { waveForm.append(i) }
                    
                    arrCell.append(AudioMsgNode(audioMsg: attachment.audio_msg, contentWidth: contentWidth, delegate: delegate, msgColor: msgColor, waveForm: waveForm))
                    arrCellType.append("audio_message")
                    break
                case "graffiti":
                    arrCell.append(GraffitiNode(graffitiSrc: attachment.graffiti, contentWidth: contentWidth, delegate: delegate))
                    arrCellType.append("graffiti")
                    break
                case "market":
                    arrCell.append(MarketNode(marketSource: attachment.market, contentWidth: contentWidth, delegate: delegate))
                    arrCellType.append("market")
                    break
                case "market_album":
                    arrCell.append(MarketAlbumNode(marketSource: attachment.market_album, contentWidth: contentWidth, delegate: delegate))
                    arrCellType.append("market_album")
                    break
                case "wall":
                    arrCell.append(WallNode(wall: attachment.wall, contentWidth: contentWidth, delegate: delegate, msgColor: msgColor))
                    arrCellType.append("wall")
                    break
                case "sticker":
                    arrCell.append(StickerNode(sticker: attachment.sticker, delegate: delegate))
                    arrCellType.append("sticker")
                    break
                case "gift":
                    arrCell.append(GiftNode(sourceGift: attachment.gift, contentWidth: contentWidth, delegate: delegate))
                    arrCellType.append("gift")
                    break
                default: break
                }
            }
            
            if arrCellType.count > 0 && arrCellType[0] == "photo" { isHaveOnlyPhoto = true }
            
            for fwdMsg in msg!.fwd_messages {
                arrCell.append(ForwardedMessageNode(msg: fwdMsg, contentWidth: contentWidth, delegate: delegate))
                arrCellType.append("fwd_msg")
            }
        }
    }
    
    func generateNeedDocCell(_ doc: VKDocument?, _ msgColor: MessageColor) {
        guard let document = doc else { return }
        
        switch document.type {
        case 1, 2, 6, 7, 8: // 1 - text doc, 2 - zip, 6 - video, 7 - elect. book, 8 - unknown
            arrCell.append(DocFileNode(doc: document, contentWidth: contentWidth, delegate: delegate, msgColor: msgColor))
            arrCellType.append("doc")
            break
        case 3 : // gif
            arrCell.append(DocGifNode(doc: document, contentWidth: contentWidth, delegate: delegate))
            arrCellType.append("doc")
            break
        case 4: // image
            arrCell.append(DocImgNode(doc: document, contentWidth: contentWidth, delegate: delegate))
            arrCellType.append("doc")
            break
        default:
            return
        }
    }
    
    func generateNeedSpecLayout(_ constrainedSize: ASSizeRange) -> [ASLayoutSpec] {
        var arrLayoutSpec : [ASLayoutSpec] = []
        var i = 0
        
        for cellType in arrCellType {
            switch cellType {
            case "audio":
                if let elem = arrCell[i] as? AudioNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "photo":
                if let elem = arrCell[i] as? ImageCellNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "video":
                if let elem = arrCell[i] as? VideoNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "doc":
                if let docLayout = generateNeedDocSpecLayout(arrCell[i], constrainedSize) {
                    arrLayoutSpec.append(docLayout)
                }
                break
            case "link":
                if let elem = arrCell[i] as? LinkNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "audio_message":
                if let elem = arrCell[i] as? AudioMsgNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "graffiti":
                if let elem = arrCell[i] as? GraffitiNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "market":
                if let elem = arrCell[i] as? MarketNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "market_album":
                if let elem = arrCell[i] as? MarketAlbumNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "wall":
                if let elem = arrCell[i] as? WallNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "sticker":
                if let elem = arrCell[i] as? StickerNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "geo":
                if let elem = arrCell[i] as? GeoNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "gift":
                if let elem = arrCell[i] as? GiftNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            case "fwd_msg":
                if let elem = arrCell[i] as? ForwardedMessageNode { arrLayoutSpec.append(elem.layoutSpecThatFits(constrainedSize)) }
                break
            default: break
            }
            i += 1
        }
        
        
        
        return arrLayoutSpec
    }
    
    func generateNeedDocSpecLayout(_ cell: ASCellNode, _ constrainedSize: ASSizeRange) -> ASLayoutSpec? {
        
        if let elem = cell as? DocFileNode {
            return elem.layoutSpecThatFits(constrainedSize)
        }
        else if let elem = cell as? DocImgNode {
            return elem.layoutSpecThatFits(constrainedSize)
        }
        else if let elem = cell as? DocGifNode {
            return elem.layoutSpecThatFits(constrainedSize)
        }
        
        return nil
    }
    
    func checkActionType(from_id: Int64, member_id: Int64, actionType: String, actionText: String) -> String {
        var msg: String = ""
        let fromUsername: String = vkDataCache.getUsernameByIDAndType(id: from_id, type: VKUserType.isUser.rawValue)
        switch actionType {
        case "chat_photo_update":
            msg = fromUsername + " updated the group chat photo:"
            //needShowPhoto = true
            break
        case "chat_photo_remove":
            msg = fromUsername + " removed the group chat photo"
            break
        case "chat_create":
            msg = fromUsername + " created this group chat"
            break
        case "chat_title_update":
            msg = fromUsername + " changed the group chat name to \"" + actionText + "\""
            break
        case "chat_invite_user":
            msg = fromUsername + " invited " + vkDataCache.getUsernameByIDAndType(id: member_id, type: VKUserType.isUser.rawValue)
            break
        case "chat_kick_user":
            msg = fromUsername +  " kicked " + vkDataCache.getUsernameByIDAndType(id: member_id, type: VKUserType.isUser.rawValue) + " out"
            break
        default:
            break
        }
        
        return msg
    }
    
    func getInsetsForTimeStack(msg: VKMessage) -> UIEdgeInsets {
        //        if msg.attachments.count != 0 || msg.geo != nil {
        //            return UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)
        //        }
        return UIEdgeInsets(top: 5, left: 5, bottom: 1, right: 10)
    }
    
    func createSpacer() -> ASLayoutSpec {
        let spacer = ASLayoutSpec()
        return spacer
    }
    
    
    @objc func openMessageNode() {
        delegate?.openMessageNode(msgID: msgID)
    }
    
    @objc func openProfile() {
        delegate?.openProfile(user_id: peerID)
    }
}

