//
//  ViewController.swift
//  SocialNetworksBrowser
//
//  Created by NCHD on 3/7/18.
//  Copyright © 2018 EgoSecure. All rights reserved.
//

import UIKit
import SwiftyVK
//import KeychainPasswordItem

class ViewController: UIViewController {

    @IBOutlet weak var userIDLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var tokenLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pswItem = KeychainPasswordItem(service: "vk", account: "vk", accessGroup: nil)
        var token: String?
        do{
            try token = pswItem.readPassword()
            NSLog("token read \(token!)")
        }
        catch{
            NSLog("storeLoginAndPsw: keychainError")
        }
    
        if token != nil {
            self.tokenLbl.text! += " " + token!
            self.tokenLbl.numberOfLines = 0
            self.tokenLbl.lineBreakMode = .byCharWrapping
            self.tokenLbl.sizeToFit()
            self.tokenLbl.adjustsFontSizeToFitWidth = true
        }

        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    @IBAction func signInClick(_ sender: UIButton) {
        VK.sessions.default.logIn(onSuccess: { info in
            NSLog("SwiftyVK: success authorize with", info)
            
            let token = info["access_token"]
            //let user_id = info["user_id"]
            //let email = info["email"]
            
//            self.userIDLbl.text! += " " + user_id!
//            self.userIDLbl.numberOfLines = 0
//            self.userIDLbl.lineBreakMode = .byCharWrapping
//            self.userIDLbl.sizeToFit()
//            self.userIDLbl.adjustsFontSizeToFitWidth = true
//
//            self.emailLbl.text! += " " + email!
//            self.emailLbl.numberOfLines = 0
//            self.emailLbl.lineBreakMode = .byCharWrapping
//            self.emailLbl.sizeToFit()
//            self.emailLbl.adjustsFontSizeToFitWidth = true
//
//            self.tokenLbl.text! += " " + token!
//            self.tokenLbl.numberOfLines = 0
//            self.tokenLbl.lineBreakMode = .byCharWrapping
//            self.tokenLbl.sizeToFit()
//            self.tokenLbl.adjustsFontSizeToFitWidth = true
            
            do {
                let pswItem = KeychainPasswordItem(service: "vk", account: "vk", accessGroup: nil)
                try pswItem.savePassword(token!)
                NSLog("token stored \(token!)")
                
                DispatchQueue.main.sync {
                    let appDlgt = UIApplication.shared.delegate as! AppDelegate
                    appDlgt.loadTabBar()
                }
                
                }
            catch {
                NSLog("storeLoginAndPsw: keychainError")
            }
            
            
        }) { error in
            NSLog("ERROR \(error)")
            DispatchQueue.main.sync {
                let appDlgt = UIApplication.shared.delegate as! AppDelegate
                appDlgt.loadTabBar()
            }
        }
        
    }
    
    @IBAction func logOutClick(_ sender: UIButton) {
        VK.sessions.default.logOut()
        self.userIDLbl.text! = "User_id:"
        self.emailLbl.text! = "Email:"
        self.tokenLbl.text! = "Token:"
        let pswItem = KeychainPasswordItem(service: "vk", account: "vk", accessGroup: nil)
        try! pswItem.deleteItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

