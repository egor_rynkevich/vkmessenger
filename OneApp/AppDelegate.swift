//
//  AppDelegate.swift
//  SocialNetworksBrowser
//
//  Created by NCHD on 3/7/18.
//  Copyright © 2018 EgoSecure. All rights reserved.
//

import UIKit
import CoreData
import SwiftyVK
import RealmSwift

var vkDelegate : VKDelegate?
var vkDataCache = VKDataCache()
var vkLoadData = VKLoadData()
var mainIcons = MainIcons()
var mainColors = MainColors()
var mainPlayer = MainPlayer()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
                
        Realm.Configuration.defaultConfiguration = VKDataCache.getRealmConfig()
        
        vkDelegate = VKDelegate()
        
        let pswItem = KeychainPasswordItem(service: "vk", account: "vk", accessGroup: nil)
        var token: String?
        do{
            try token = pswItem.readPassword()
            NSLog("token read \(token!)")
            
//            let appDlgt = UIApplication.shared.delegate as! AppDelegate
//            let context = appDlgt.persistentContainer.viewContext
//            let entity = NSEntityDescription.entity(forEntityName: "MainOneAppSettings", in: context)
//            let settings = NSManagedObject(entity: entity!, insertInto: context)
//            
//            settings.setValue(true, forKeyPath: "haveAnyAccounts")
//            
//            do {
//                try context.save()
//            } catch let error as NSError {
//                print("Could not save. \(error), \(error.userInfo)")
//            }
        }
        catch { NSLog("storeLoginAndPsw: keychainError") }
        
//        let qFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "MainOneAppSettings")
//
//        let query = try! persistentContainer.viewContext.fetch(qFetch)
//
//        if(query.first != nil){
//
//            let mainSet : MainOneAppSettings = query.first as! MainOneAppSettings
//
//            if mainSet.haveAnyAccounts == true {
//                print("have acc")
//            }
//        }
//        else {
//            print ("don't have acc")
//        }
        
        if token != nil {
            do {
                try VK.sessions.default.logIn(rawToken: token!, expires: 0)
                loadTabBar()
                //loadNeedViewController("VKNavCtrl")
            }
            catch{
                let nsError = error as NSError
                NSLog("error = \(nsError)")
                print("\(VK.sessions.default.config.apiVersion)")
                switch(nsError.code){
                case 5:
                    //loadNeedViewController("VKNavCtrl")
                    loadTabBar()
                    break
                default:
                    break
                }
            }
        }
        
        return true
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let app = options[.sourceApplication] as? String
        VK.handle(url: url, sourceApplication: app)
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        VK.handle(url: url, sourceApplication: sourceApplication)
        return true
    }
    
    func loadNeedViewController(_ identVC: String){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: identVC)
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
    }
    
    func loadTabBar(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "tabBarCtrl")
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "SocialNetworksBrowser")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

