//
//  AddLocationVC.swift
//  OneApp
//
//  Created by Егор Рынкевич on 11/20/18.
//

import UIKit
import MapKit

protocol HandleMapSearch {
    func setNewPlace(placemark: MKPlacemark)
}

class AddLocationVC: UIViewController, HandleMapSearch {
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var streetLbl: UILabel!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var currentLocBtn: UIButton!
    
    var resultSearchController:UISearchController? = nil
    var locationSearchTable: LocationSearchTVTableViewController?
    
    let locationManager = CLLocationManager()
    let regionInMeters: Double = 100
    var previousLocation: CLLocation?
    
    let geoCoder = CLGeocoder()
    var directionsArray: [MKDirections] = []
    
    var centerLoc: CLLocation?
    
    var selectedPin: MKPlacemark? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        locationSearchTable = LocationSearchTVTableViewController(nibName: "LocationSearchTVTableViewController", bundle: nil)
        locationSearchTable?.mapView = mapView
        locationSearchTable?.handleMapSearchDelegate = self
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        resultSearchController?.searchResultsUpdater = locationSearchTable

        let searchBar = resultSearchController!.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = "Search for places"
        searchBar.searchBarStyle = UISearchBar.Style.prominent
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
        searchBar.changeColorMode(mainColors.mainNavigationBarLight)
       
        resultSearchController?.hidesNavigationBarDuringPresentation = false
        resultSearchController?.obscuresBackgroundDuringPresentation = false
        definesPresentationContext = true
        
        navigationItem.searchController = resultSearchController
        
        view.backgroundColor = mainColors.mainTableView
        
        addBtn.setImage(mainIcons.send_s24, for: .normal)
        addBtn.addTarget(self, action: #selector(getLocation), for: .touchUpInside)
        addBtn.backgroundColor = mainColors.sendMessage
        addBtn.layer.masksToBounds = true
        addBtn.layer.cornerRadius = 20
        
        currentLocBtn.addTarget(self, action: #selector(setToCenter), for: .touchUpInside)
        
        cityLabel.textColor = mainColors.writeRow
        streetLbl.textColor = mainColors.sendMessage
        
        checkLocationServices()
        
        mapView(mapView, regionDidChangeAnimated: true)
        mapView.isZoomEnabled = true
        configureTileOverlay()
    }
    
    private func configureTileOverlay() {
        // We first need to have the path of the overlay configuration JSON
        
        if mainColors.darkMode == true { return }
        
        guard let overlayFileURLString = Bundle.main.path(forResource: "geoDarkStyle", ofType: "json") else {
            return
        }
        let overlayFileURL = URL(fileURLWithPath: overlayFileURLString)
        
        // After that, you can create the tile overlay using MapKitGoogleStyler
        guard let tileOverlay = try? MapKitGoogleStyler.buildOverlay(with: overlayFileURL) else {
            return
        }
        
        // And finally add it to your MKMapView
        mapView.addOverlay(tileOverlay)
    }

    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    func centerViewOnUserLocation() {
        if let location = locationManager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            mapView.setRegion(region, animated: true)
        }
    }
    
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLocationAuthorization()
        } else {
            // Show alert letting the user know they have to turn this on.
        }
    }
    
    func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            startTackingUserLocation()
        case .denied:
            // Show alert instructing them how to turn on permissions
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            // Show an alert letting them know what's up
            break
        case .authorizedAlways:
            break
        }
    }
    
    func startTackingUserLocation() {
        mapView.showsUserLocation = true
        mapView.delegate = self
        centerViewOnUserLocation()
        locationManager.startUpdatingLocation()
        previousLocation = getCenterLocation(for: mapView)
    }
    
    func getCenterLocation(for mapView: MKMapView) -> CLLocation {
        let latitude = mapView.centerCoordinate.latitude
        let longitude = mapView.centerCoordinate.longitude
        
        return CLLocation(latitude: latitude, longitude: longitude)
    }
    
    @objc func getLocation() {
        self.resultSearchController?.searchBar.removeFromSuperview()
        self.navigationController?.popViewController(animated: true)
        
        var locationInfo: [String: Double] = [:]
        locationInfo["lat"] = previousLocation?.coordinate.latitude ?? 0
        locationInfo["long"] = previousLocation?.coordinate.longitude ?? 0
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addLocation"), object: nil, userInfo: locationInfo)
    }
    
    @objc func setToCenter() {
        if centerLoc != nil { mapView.setCenter(centerLoc!.coordinate, animated: true) }
    }
    
    func createDirectionsRequest(from coordinate: CLLocationCoordinate2D) -> MKDirections.Request {
        let destinationCoordinate       = getCenterLocation(for: mapView).coordinate
        let startingLocation            = MKPlacemark(coordinate: coordinate)
        let destination                 = MKPlacemark(coordinate: destinationCoordinate)
        
        let request                     = MKDirections.Request()
        request.source                  = MKMapItem(placemark: startingLocation)
        request.destination             = MKMapItem(placemark: destination)
        request.transportType           = .automobile
        request.requestsAlternateRoutes = true
        
        return request
    }
    
    func resetMapView(withNew directions: MKDirections) {
        mapView.removeOverlays(mapView.overlays)
        directionsArray.append(directions)
        let _ = directionsArray.map { $0.cancel() }
    }

    func setNewPlace(placemark: MKPlacemark) {
        selectedPin = placemark
        let region = MKCoordinateRegion(center: placemark.coordinate, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
        mapView.setRegion(region, animated: true)
    }
}

extension AddLocationVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
}


extension AddLocationVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let center = getCenterLocation(for: mapView)
        
        let previousLocation = self.previousLocation
        
        if centerLoc == nil { centerLoc = center }
        
        if previousLocation != nil {
            guard center.distance(from: previousLocation!) > 20 else { return }
        }
        
        self.previousLocation = center
        
        geoCoder.cancelGeocode()
        
        geoCoder.reverseGeocodeLocation(center) { [weak self] (placemarks, error) in
            guard let self = self else { return }
            
            if let _ = error {
                //TODO: Show alert informing the user
                return
            }
            
            guard let placemark = placemarks?.first else {
                //TODO: Show alert informing the user
                return
            }
            
            let streetName = placemark.thoroughfare ?? ""
            let city = placemark.locality ?? ""
            print(placemark)
            DispatchQueue.main.async {
                self.streetLbl.text = "\(streetName)"
                self.cityLabel.text = city
            }
        }
    }
    
//    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
//        let renderer = MKPolylineRenderer(overlay: overlay as! MKPolyline)
//        renderer.strokeColor = .blue
//
//        return renderer
//    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        // This is the final step. This code can be copied and pasted into your project
        // without thinking on it so much. It simply instantiates a MKTileOverlayRenderer
        // for displaying the tile overlay.
        if let tileOverlay = overlay as? MKTileOverlay {
            return MKTileOverlayRenderer(tileOverlay: tileOverlay)
        } else {
            return MKOverlayRenderer(overlay: overlay)
        }
    }
}
