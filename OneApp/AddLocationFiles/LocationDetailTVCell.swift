//
//  LocationDetailTVCell.swift
//  OneApp
//
//  Created by Егор Рынкевич on 11/26/18.
//

import UIKit

class LocationDetailTVCell: UITableViewCell {

    @IBOutlet weak var textLbl: UILabel!
    @IBOutlet weak var detailTextLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
