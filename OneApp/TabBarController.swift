//
//  TabBarController.swift
//  OneApp
//
//  Created by Егор Рынкевич on 10/6/18.
//

import UIKit

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.unselectedItemTintColor = mainColors.tabBarNonTint
        self.tabBar.tintColor = mainColors.tabBarTint
        
        self.tabBarItem.imageInsets = UIEdgeInsets(top: 16, left: 0, bottom: -6, right: 0)
        
        self.tabBar.items?[0].image = mainIcons.messageIcon_s21
        self.tabBar.items?[0].imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
        self.tabBar.items?[0].badgeColor = mainColors.tabBarBadge
        self.tabBar.items?[0].setBadgeTextAttributes([.foregroundColor: mainColors.tabBarBadgeText!], for: .normal)
        self.tabBar.items?[1].image = mainIcons.search_s21
        self.tabBar.items?[1].imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
        self.tabBar.items?[2].image = mainIcons.createChat_s21
        self.tabBar.items?[2].imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
        self.tabBar.items?[2].badgeColor = mainColors.tabBarBadge
        self.tabBar.items?[2].setBadgeTextAttributes([.foregroundColor: mainColors.tabBarBadgeText!], for: .normal)
        self.tabBar.items?[3].image = mainIcons.contact_s21
        self.tabBar.items?[3].imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
        

            //self.tabBar.barStyle = UIBarStyle.black
            let gradient = CAGradientLayer()
            //let sizeLength = UIScreen.main.bounds.size.height * 2
            //let defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: 64)
            gradient.frame = (self.tabBar.bounds) //defaultNavigationBarFrame
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.colors = [mainColors.tabBar?.cgColor as Any, mainColors.tabBar?.cgColor as Any]
            
            self.tabBar.backgroundImage = UIImage().image(fromLayer: gradient)
            self.tabBar.isTranslucent = false
            
        
        
        //tabBar.items?.first?.title = ""
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        if self.tabBar.frame.height > 60 {
            var frame = self.tabBar.frame
            frame.origin.y = self.view.frame.size.height - 60
            self.tabBar.frame = frame
        }
//        } else {
//            var frame = self.tabBar.frame
//            print(frame)
//            frame.origin.y = self.view.frame.size.height - 48
//            self.tabBar.frame = frame
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
